from .logger import logger
from . import h2oprop, naclprops, tvc, design_equalAhx, design_equalDT, design_equalAph_DTpreh, design_parallcross,\
    tools, design_equalAph_DTpreh_parallcross, design_equalAph_DTpreh_Ucalc
from . import medmodel  # TODO: medmodel.py is simply a copy of __init__.py, since the medmodel function from the init file wasn't getting imported
import numpy as np

# program_config = tools.read_program_config('./program_config.yml')

def solver_based_on_equal_dt(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']
    if N < 9:
        Mms_firstguess = 0.5
    elif 9 <= N < 13:
        Mms_firstguess = 0.3
    else:
        Mms_firstguess = 0.2

    Tsw = parameter_config['Tsw_design']
    Xsw = parameter_config['Xsw_design']
    Mfeed = parameter_config['Mfeed_design']
    Xb_out = parameter_config['Xout_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']
    DT_ph = parameter_config['DeltaT_preh']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mb = np.zeros(N)
    Xb = np.zeros(N)
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N - 1)
    DeltaTph_firstguess = np.zeros(N-1)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N - 1)
    DTLM = np.zeros(N - 1)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Tn = parameter_config['Tn_design']
    DeltaT_tot_eff = (Ts_design - Tn)
    Tcw_out = Tcw_in + DT_cond
    T[N - 1] = Tn
    T_preh[N - 1] = Tcw_in + DT_cond
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    n_tubes = [8000 for i in range(0, N)]
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    Mvap = np.array([medmodel.mvap_firstguess(Mdist, N) for i in range(0, N)])
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    DeltaXb = (Xb_out - Xf) / N
    for iEffect in range(0, N):
        Xb[iEffect] = Xf + (iEffect + 1) * DeltaXb
        Mb[iEffect] = Mfeed * Xf / Xb[iEffect]
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0
    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat=[], T=[])
    DeltaT, T = design_equalDT.temp_profile(Ts, Tn, N)
    DeltaT = np.array(DeltaT)
    T = np.array(T)
    print(DeltaT)
    DeltaT_tot = sum(DeltaT)
    correction = parameter_config['preh_correction']
    T_preh = T - correction

    for iEffect in tools.itereffects(0, (N - 1)):
        M_cond_ph[iEffect] = Mfeed * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / h2oprop.latent_heat(Tvsat_prime[iEffect])
        alfa_cond[iEffect] = M_cond_ph[iEffect] / Mvap[iEffect]

    solver = design_equalDT.MedSolver(N, DeltaT, DeltaT_tot, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Mb, Md, M_fbrine, M_fb, Mvap,
                 Xb, Mbrine,design_based_on_equal_ahx_aph, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, h_preh, h_vprime, h_cprime, h_b, Ms, TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, Tcw_out, correction, n_tubes)
    return solver, Mms_firstguess, DeltaTph_firstguess, DeltaT


def solver_based_on_equal_ahx(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']
    if N < 9:
        Mms_firstguess = 0.5
    elif 9 <= N < 13:
        Mms_firstguess = 0.2
    else:
        Mms_firstguess = 0.2

    Tsw = parameter_config['Tsw_design']
    Xsw = parameter_config['Xsw_design']
    Mfeed = parameter_config['Mfeed_design']
    Xb_out = parameter_config['Xout_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mb = np.zeros(N)
    Xb = np.zeros(N)
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N - 1)
    DeltaTph_firstguess = np.zeros(N-1)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N - 1)
    DTLM = np.zeros(N - 1)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Tn = parameter_config['Tn_design']
    T_preh[N-1] = Tn - DTT_cond
    T[N - 1] = Tn
    Tcw_out = T_preh[N - 1]
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    n_tubes = 8000
    DeltaT_tot_eff = Ts_design - Tn
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    Mvap = np.array([medmodel.mvap_firstguess(Mdist, N) for i in range(0, N)])
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    DeltaXb = (Xb_out - Xf) / N
    for iEffect in range(0, N):
        Xb[iEffect] = Xf + (iEffect + 1) * DeltaXb
        Mb[iEffect] = Mfeed * Xf / Xb[iEffect]
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0
    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat = [], T = [])
    DeltaT_tot = np.sum(DeltaT_first)
    DeltaT = DeltaT_first
    correction = parameter_config['preh_correction']
    T_preh = T - correction
    for iEffect in tools.itereffects(0, (N - 1)):
        M_cond_ph[iEffect] = Mfeed * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / h2oprop.latent_heat(Tvsat_prime[iEffect])
        alfa_cond[iEffect] = M_cond_ph[iEffect] / Mvap[iEffect]
    alfa_cond[N - 1] = 0
    solver = design_equalAhx.MedSolver(N, DeltaT, DeltaT_tot, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Mb, Md,
                                       M_fbrine, M_fb,
                                       Mvap, Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, h_preh, h_vprime,
                                       h_cprime, h_b, Ms, TVC_on, Mms, Pm, Mss, ER, Mc, A_hx,
                                       A_preh, Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist,
                                       parameter_config, DTLM, Tcw_out, correction, n_tubes)
    return solver, Mms_firstguess, DeltaTph_firstguess, DeltaT_first


def solver_based_on_equal_dt_aph(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']
    if N < 9:
        Mms_firstguess = 0.5
    elif 9 <= N < 13:
        Mms_firstguess = 0.3
    else:
        Mms_firstguess = 0.2

    Tsw = parameter_config['Tsw_design']
    Xsw = parameter_config['Xsw_design']
    Mfeed = parameter_config['Mfeed_design']
    Xb_out = parameter_config['Xout_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    Tn_design = parameter_config['Tn_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mb = np.zeros(N)
    DeltaX = (Xb_out - Xsw) / N
    Xb = np.zeros(N)
    for i in range(0, N):
        Xb[i] = Xsw + (i + 1) * DeltaX
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N - 1)
    alfac_first = np.zeros(N - 1)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N - 1)
    DTLM = np.zeros(N - 1)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect

    # first guess values
    Tn = Tn_design
    Tcw_out = Tcw_in + DT_cond
    T[N - 1] = Tn
    T_preh[N - 1] = Tcw_in + DT_cond
    DeltaT_tot_eff = Ts_design - Tn
    n_tubes = [8000 for i in range(0, N)]
    # n_tubes = 300
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    Mvap = np.array([medmodel.mvap_firstguess(Mdist, N) for i in range(0, N)])
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    DeltaXb = (Xb_out - Xf) / N
    for iEffect in range(0, N):
        Xb[iEffect] = Xf + (iEffect + 1) * DeltaXb
        Mb[iEffect] = Mfeed * Xf / Xb[iEffect]
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0
    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat=[], T=[])
    DeltaT, T = design_equalDT.temp_profile(Ts, Tn, N)
    DeltaT = np.array(DeltaT)
    DeltaT_first = DeltaT * 1
    T = np.array(T)
    T_dist_fb = Tc * 1
    print(DeltaT)
    DeltaT_tot = sum(DeltaT)

    DeltaT_phfirst, T_prehfirst = design_equalAph_DTpreh.tpreh_profile(T[0], DTT_preh, T_preh[N - 1], N)
    DeltaT_ph = np.array(DeltaT_phfirst)
    T_preh = np.array(T_prehfirst)
    for iEffect in tools.itereffects(0, (N - 1)):
        alfa_cond[iEffect] = Mfeed * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / (h2oprop.latent_heat(Tvsat_prime[iEffect]) * Mvap[iEffect])
        M_cond_ph[iEffect] = alfac_first[iEffect] * Mvap[iEffect]
    alfa_cond[N - 1] = 0
    solver = design_equalAph_DTpreh.MedSolver(N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime,
                                              Tc, T_preh, Tcw_in, DT_cond, Tcw_out, Mb, Md, M_fbrine,
                                              M_fb, Mvap, Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond,
                                              DTT_preh, DTT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                                              TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh, Msteam, X_max, A_av, DeltaA,
                                              SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, n_tubes, T_dist_fb)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first


def solver_based_on_equal_ahx_aph(parameter_config, program_config, N, Mfeed, Xsw, Xb_out, Ts_design):
    # input and parameter definition inside the program
    # N = parameter_config['Nstages']  # commenta SOLO per analisi di sensitivita, mettendo N tra gli argomenti
    # Mfeed = parameter_config['Mfeed_design']
    # Xsw = parameter_config['Xsw_design']
    # Xb_out = parameter_config['Xout_design']
    # Ts_design = parameter_config['Ts_design']
    CF = Xb_out/Xsw
    if Ts_design < 80:
        if Mfeed <= 200:
            if N <= 4:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, 4, Mfeed)
            elif 5<=N<=15:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, N, Mfeed)
            else:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, 15, Mfeed) * 15/N
        else:
            if N < 8:
                Mms_firstguess = 10
            elif 8 <= N < 10:
                Mms_firstguess = 5
            elif 10 <= N <= 13:
                Mms_firstguess = 3
            else:
                Mms_firstguess = 2.5
    else:
        if Mfeed <= 200:
            if N < 4:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, 4, Mfeed)* 1.5
            elif 5 <= N <= 15:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, N, Mfeed) * 1.5
            elif 16 <=N<27:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, 15, Mfeed) * 1.5 * 15/N
            else:
                Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, 15, Mfeed) * 1  * 15 / N  #era 1.6 usato per plane MED
        else:
            if N < 8:
                Mms_firstguess = 15
            elif 8 <= N < 10:
                Mms_firstguess = 8
            elif 10 <= N <= 13:
                Mms_firstguess = 5
            else:
                Mms_firstguess = 4
    # if N < 8:
    #     if Mfeed<=10:
    #         Mms_firstguess = 0.4
    #     elif Mfeed > 10:
    #         Mms_firstguess = 10
    # elif 8 <= N < 14:
    #     if Mfeed <= 5:
    #         if Xsw < 45000:
    #             Mms_firstguess = 0.3
    #         elif 45000 <= Xsw < 70000:
    #             Mms_firstguess = 0.2
    #         else:
    #             Mms_firstguess = 0.15
    #     elif 5 < Mfeed < 10:
    #         Mms_firstguess = 0.5
    #     elif 10 <= Mfeed <= 40:
    #         Mms_firstguess = 1
    #     elif Mfeed > 40:
    #         Mms_firstguess = 2
    # else:
    #     if Mfeed <=5:
    #         Mms_firstguess = 0.2
    #     elif 10 <= Mfeed <= 20:
    #         Mms_firstguess = 1
    #     elif Mfeed > 20:
    #         Mms_firstguess = 5

    Tsw = parameter_config['Tsw_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Tn_design = parameter_config['Tn_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mb = np.zeros(N)
    DeltaX = (Xb_out - Xsw) / N
    Xb = np.zeros(N)
    for i in range(0, N):
        Xb[i] = Xsw + (i + 1) * DeltaX
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N - 1)
    alfac_first = np.zeros(N - 1)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N)
    DTLM = np.zeros(N - 1)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    # n_tubes = parameter_config['A_cross'] / (np.pi * parameter_config['d_evap']**2 / 4)
    n_tubes = 8000
    # n_tubes = 300

    # first guess values
    # Tn = Tcw_in + DT_cond + DTT_cond
    Tn = Tn_design
    # Tcw_out = Tcw_in + DT_cond
    T[N - 1] = Tn
    # T_preh[N - 1] = Tcw_in + DT_cond
    Tcw_out = Tn - DTT_cond
    T_preh[N-1] = Tcw_out
    DeltaT_tot_eff = Ts_design - Tn
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    Mvap = np.array([medmodel.mvap_firstguess(Mdist, N) for i in range(0, N)])
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    DeltaXb = (Xb_out - Xf) / N
    for iEffect in range(0, N):
        Xb[iEffect] = Xf + (iEffect + 1) * DeltaXb
        Mb[iEffect] = Mfeed * Xf / Xb[iEffect]
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0

    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat = [], T = [])
    T_dist_fb = Tc * 1
    DeltaT_tot = np.sum(DeltaT_first)
    DeltaT = DeltaT_first * 1
    DeltaT_phfirst, T_prehfirst = design_equalAph_DTpreh.tpreh_profile(T[0], DTT_preh, T_preh[N - 1], N, deltat_ph = [], T_preh = [])
    DeltaT_ph = np.array(DeltaT_phfirst)
    T_preh = np.array(T_prehfirst)
    for iEffect in tools.itereffects(0, (N - 1)):
        alfa_cond[iEffect] = Mfeed * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / (h2oprop.latent_heat(Tvsat_prime[iEffect]) * Mvap[iEffect])
        M_cond_ph[iEffect] = alfac_first[iEffect] * Mvap[iEffect]
    alfa_cond[N - 1] = 0
    solver = design_equalAph_DTpreh.MedSolver(N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime,
                                              Tc, T_preh, Tcw_in, DT_cond, Tcw_out, Mb, Md, M_fbrine,
                                              M_fb, Mvap, Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond,
                                              DTT_preh, DTT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                                              TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh, Msteam, X_max, A_av, DeltaA,
                                              SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, n_tubes, T_dist_fb)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first


def solver_based_on_equal_ahx_aph_Ucalc(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']  # commenta SOLO per analisi di sensitivita, mettendo N tra gli argomenti
    Mfeed = parameter_config['Mfeed_design']
    Xsw = parameter_config['Xsw_design']
    Xb_out = parameter_config['Xout_design']
    CF = Xb_out/Xsw
    Mms_firstguess = medmodel.Mms_forwardfeed_firstguess(program_config, CF, N, Mfeed)
    # if N < 8:
    #     if Mfeed<=10:
    #         Mms_firstguess = 0.4
    #     elif Mfeed > 10:
    #         Mms_firstguess = 10
    # elif 8 <= N < 14:
    #     if Mfeed <= 5:
    #         if Xsw < 45000:
    #             Mms_firstguess = 0.3
    #         elif 45000 <= Xsw < 70000:
    #             Mms_firstguess = 0.2
    #         else:
    #             Mms_firstguess = 0.15
    #     elif 5 < Mfeed < 10:
    #         Mms_firstguess = 0.5
    #     elif 10 <= Mfeed <= 40:
    #         Mms_firstguess = 1
    #     elif Mfeed > 40:
    #         Mms_firstguess = 2
    # else:
    #     if Mfeed <=5:
    #         Mms_firstguess = 0.2
    #     elif 10 <= Mfeed <= 20:
    #         Mms_firstguess = 1
    #     elif Mfeed > 20:
    #         Mms_firstguess = 5

    Tsw = parameter_config['Tsw_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    Tn_design = parameter_config['Tn_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mb = np.zeros(N)
    DeltaX = (Xb_out - Xsw) / N
    Xb = np.zeros(N)
    for i in range(0, N):
        Xb[i] = Xsw + (i + 1) * DeltaX
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N - 1)
    alfac_first = np.zeros(N - 1)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N)
    DTLM = np.zeros(N - 1)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    # n_tubes = parameter_config['A_cross'] / (np.pi * parameter_config['d_evap']**2 / 4)
    n_tubes = 8000
    # n_tubes = 300

    # first guess values
    # Tn = Tcw_in + DT_cond + DTT_cond
    Tn = Tn_design
    # Tcw_out = Tcw_in + DT_cond
    T[N - 1] = Tn
    # T_preh[N - 1] = Tcw_in + DT_cond
    Tcw_out = Tn - DTT_cond
    T_preh[N-1] = Tcw_out
    DeltaT_tot_eff = Ts_design - Tn
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    Mvap = np.array([medmodel.mvap_firstguess(Mdist, N) for i in range(0, N)])
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    DeltaXb = (Xb_out - Xf) / N
    for iEffect in range(0, N):
        Xb[iEffect] = Xf + (iEffect + 1) * DeltaXb
        Mb[iEffect] = Mfeed * Xf / Xb[iEffect]
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0

    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat = [], T = [])
    U_evap = medmodel.heattransfer.u_evaporator(T)
    T_dist_fb = Tc * 1
    DeltaT_tot = np.sum(DeltaT_first)
    DeltaT = DeltaT_first * 1
    DeltaT_phfirst, T_prehfirst = design_equalAph_DTpreh.tpreh_profile(T[0], DTT_preh, T_preh[N - 1], N, deltat_ph = [], T_preh = [])
    DeltaT_ph = np.array(DeltaT_phfirst)
    T_preh = np.array(T_prehfirst)
    for iEffect in tools.itereffects(0, (N - 1)):
        alfa_cond[iEffect] = Mfeed * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / (h2oprop.latent_heat(Tvsat_prime[iEffect]) * Mvap[iEffect])
        M_cond_ph[iEffect] = alfac_first[iEffect] * Mvap[iEffect]
    alfa_cond[N - 1] = 0
    solver = design_equalAph_DTpreh_Ucalc.MedSolver(N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime,
                                              Tc, T_preh, Tcw_in, DT_cond, Tcw_out, Mb, Md, M_fbrine,
                                              M_fb, Mvap, Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond,
                                              DTT_preh, DTT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                                              TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh, Msteam, X_max, A_av, DeltaA,
                                              SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, n_tubes, T_dist_fb, U_evap)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first


def solver_based_on_equal_dt_parallcross(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']
    if N < 9:
        Mms_firstguess = 0.5
    elif N > 9:
        Mms_firstguess = 0.3

    Tsw = parameter_config['Tsw_design']
    Xsw = parameter_config['Xsw_design']
    Mfeed = parameter_config['Mfeed_design']
    Xb_out = parameter_config['Xout_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']
    DT_ph = parameter_config['DeltaT_preh']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    Mvap = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mfeed_ph = np.zeros(N)
    Mb = np.zeros(N)
    Xb = np.array([Xb_out for i in range(0, N)])
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N)
    DT_ph = np.array([DT_ph for i in range(0, N - 1)])
    DTLM = np.zeros(N)
    Mb_g = np.zeros(N)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Tn = parameter_config['Tn_design']
    Tcw_out = float(Tcw_in + DT_cond)
    T[N - 1] = Tn
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    DeltaT_tot_eff = Ts_design - Tn
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    FeedFraction = Mfeed / N
    Mb[0] = FeedFraction * Xf / Xb_out
    Mvap[0] = FeedFraction - Mb[0]
    for iEffect in range(1, N):
        Xb[iEffect] = Xb_out
        Mb[iEffect] = (Mb[iEffect - 1] * Xb_out + FeedFraction * Xf) / Xb_out
        Mvap[iEffect] = Mb[iEffect - 1] + FeedFraction - Mb[iEffect]
        Mb_g[iEffect] = FeedFraction - Md[iEffect]
    Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    Mfeed_ph[0] = FeedFraction
    for i_effect in tools.itereffects(1, N):
        Mfeed_ph[i_effect] = Mfeed_ph[i_effect - 1] + FeedFraction
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0
    n_tubes = [8000 for i in range(0, N)]
    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N)
    DeltaT, T = design_equalDT.temp_profile(Ts, Tn, N)
    DeltaT = np.array(DeltaT)
    DeltaT_first = DeltaT * 1.
    DeltaT_phfirst = DeltaT * 1.
    T = np.array(T)
    print(DeltaT)
    DeltaT_tot = sum(DeltaT)
    correction = parameter_config['preh_correction']
    T_preh = T - correction

    for iEffect in tools.itereffects(0, (N - 1)):
        M_cond_ph[iEffect] = Mfeed_ph[iEffect] * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / (h2oprop.latent_heat(Tvsat_prime[iEffect]))
        alfa_cond[iEffect] = M_cond_ph[iEffect] / Mvap[iEffect]

    solver = design_parallcross.MedSolver_parallcross(N, DeltaT, DeltaT_tot, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Mb, Mb_g, Md, M_fbrine, M_fb, Mf, Mvap, Mfeed_ph,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, h_preh, h_vprime, h_cprime, h_b, Ms, TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, Tcw_out, correction, n_tubes, DTT_cond)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first

def solver_based_on_equal_ahx_parallcross(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']  # commenta SOLO se fai un'analisi di sensitivita! altrimenti inserisci e togli N dagli argomenti
    if N < 6:
        Mms_firstguess = 0.4
    elif 6<= N <8:
        Mms_firstguess = 0.3
    elif 8<=N<12:
        Mms_firstguess = 0.2
    else:
        Mms_firstguess = 0.1

    Tsw = parameter_config['Tsw_design']
    Xsw = parameter_config['Xsw_design']  # commenta SOLO se fai un'analisi di sensitivita! altrimenti inserisci e togli Xsw dagli argomenti
    Mfeed = parameter_config['Mfeed_design']
    Xb_out = parameter_config['Xout_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']
    Ts_design = parameter_config['Ts_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']
    DT_ph = parameter_config['DeltaT_preh']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    Mvap = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mfeed_ph = np.zeros(N)
    Mb = np.zeros(N)
    Mb_g = np.zeros(N)
    DeltaX = (Xb_out - Xsw) / N
    Xb = np.zeros(N)
    for i in range(0, N):
        Xb[i] = Xsw + (i + 1) * DeltaX
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    # Xb = np.array([Xb_out for i in range(0, N)])
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N)
    DT_ph = np.array([DT_ph for i in range(0, N - 1)])
    DTLM = np.zeros(N)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Tn = parameter_config['Tn_design']
    Tcw_out = Tn - DTT_cond
    T[N - 1] = Tn
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    n_tubes = [8000 for i in range(0, N)]

    DeltaT_tot_eff = Ts_design - Tn
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    FeedFraction = Mfeed / N
    Xb[0] = Xb_out
    Mb[0] = FeedFraction * Xf / Xb_out
    Mvap[0] = FeedFraction - Mb[0]
    for iEffect in range(1, N):
        Xb[iEffect] = Xb_out
        Mb[iEffect] = (Mb[iEffect - 1] * Xb_out + FeedFraction * Xf) / Xb_out
        Mvap[iEffect] = Mb[iEffect - 1] + FeedFraction - Mb[iEffect]
        Mb_g[iEffect] = FeedFraction * Xf/Xb_out
        Md[iEffect] = FeedFraction - Mb_g[iEffect]
    #Md = 0.9 * Mvap
    M_fbrine[0] = 0
    M_fb[0] = 0.1 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.05
        M_fb[i_effect] = Mvap[i_effect] * 0.05
    Mfeed_ph[0] = FeedFraction
    for i_effect in tools.itereffects(1, N):
        Mfeed_ph[i_effect] = Mfeed_ph[i_effect - 1] + FeedFraction
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0

    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat=[], T=[])
    DeltaT_first = np.array(DeltaT_first)
    DeltaT_tot = np.sum(DeltaT_first)
    DeltaT = DeltaT_first * 1.
    correction = parameter_config['preh_correction']
    T_preh = T - correction
    DeltaT_phfirst = DeltaT * 1.

    for iEffect in tools.itereffects(0, (N - 1)):
        M_cond_ph[iEffect] = Mfeed_ph[iEffect] * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect+1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect+1]) / h2oprop.latent_heat(Tvsat_prime[iEffect])
        alfa_cond[iEffect] = M_cond_ph[iEffect] / Mvap[iEffect]
    Mc[0] = M_cond_ph[0]
    for iEffect in tools.itereffects(0, (N-1)):
        Mc[iEffect] = Mc[iEffect-1] + M_cond_ph[iEffect] - M_fb[iEffect] + (1-alfa_cond[iEffect-1]) * Mvap[iEffect-1]
    Mc[N-1] = Mdist

    solver = design_parallcross.MedSolver_parallcross(N, DeltaT, DeltaT_tot, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Mb, Mb_g, Md, M_fbrine, M_fb, Mf, Mvap, Mfeed_ph,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, h_preh, h_vprime, h_cprime, h_b, Ms, TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, Tcw_out, correction, n_tubes, DTT_cond)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first

def solver_based_on_equal_ahx_aph_parallcross(parameter_config, program_config):
    # input and parameter definition inside the program
    N = parameter_config['Nstages']
    Mfeed = parameter_config['Mfeed_design']
    Xsw = parameter_config['Xsw_design']
    Xb_out = parameter_config['Xout_design']
    Ts_design = parameter_config['Ts_design']
    CF = Xb_out / Xsw
    if Ts_design < 80:
        if Mfeed <= 200:
            Mms_firstguess = medmodel.Mms_parallcross_firstguess(program_config, CF, N, Mfeed)
        else:
            if 4 <= N < 6:
                Mms_firstguess = 11
            elif 6<= N <=8:
                Mms_firstguess = 5
            # elif 8 < N <= 13:
            #     Mms_firstguess = 3.5 # 4
            else:
                Mms_firstguess = 3.5 # 3.5
    else:
        if Mfeed <= 200:
            Mms_firstguess = medmodel.Mms_parallcross_firstguess(program_config, CF, N, Mfeed) * 1.5
        else:
            if 4 <= N < 6:
                Mms_firstguess = 18
            elif 6<= N <=8:
                Mms_firstguess = 8
            # elif 8 < N <= 13:
            #     Mms_firstguess = 3.5 # 4
            else:
                Mms_firstguess = 6 # 3.5
    # Mms_firstguess = 4
    # if N < 6:
    #     Mms_firstguess = 0.5
    # elif 6<= N <=8:
    #     Mms_firstguess = 0.3
    # elif 8<N<13:
    #     if Mfeed <=3:
    #         Mms_firstguess = 0.1
    #     elif Mfeed <=5:
    #         if 25000 <= Xsw < 45000:
    #             Mms_firstguess = 0.25  #0.3
    #         elif 45000 <= Xsw < 70000:
    #             Mms_firstguess = 0.2  #0.25
    #         else:
    #             Mms_firstguess = 0.15  #0.1
    #     elif 5 < Mfeed < 10:
    #         Mms_firstguess = 0.5
    #     elif 10 <= Mfeed <= 40:
    #         Mms_firstguess = 1
    #     elif Mfeed > 40:
    #         Mms_firstguess = 2
    # else:
    #     Mms_firstguess = 0.15


    Tsw = parameter_config['Tsw_design']
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
    else:
        TVC_on = bool(False)
    X_max = parameter_config['X_max']
    DT_cond = parameter_config['DeltaT_condenser']
    Pm_design = parameter_config['Pm_design']

    Tn_design = parameter_config['Tn_design']
    DT_s = parameter_config['DeltaT_s']
    DT_min = parameter_config['DeltaT_min']
    DTT_cond = parameter_config['DTT_Condenser']
    DTT_preh = parameter_config['DTT_Preheater']
    DT_ph = parameter_config['DeltaT_preh']

    # initialization of array variables
    T = np.zeros(N)
    Tvsat = np.zeros(N)
    Tvsat_prime = np.zeros(N)
    Tc_prime = np.zeros(N)
    Tc = np.zeros(N)
    h_vprime = np.zeros(N)
    h_cprime = np.zeros(N)
    h_c = np.zeros(N)
    T_preh = np.zeros(N)
    h_preh = np.zeros(N)
    Mf = np.zeros(N)
    Md = np.zeros(N)
    Mvap = np.zeros(N)
    M_fb = np.zeros(N)
    Mc = np.zeros(N)
    M_fbrine = np.zeros(N)
    Mfeed_ph = np.zeros(N)
    Mb = np.zeros(N)
    Xb = np.zeros(N)
    for i in range(0, N):
        Xb[i] = Xb_out
    BPE_eff = np.zeros(N)
    for i in range(0, N):
        BPE_eff[i] = naclprops.bpe_brine_act(Ts_design, Xb[i])
    BPE_tot = sum(BPE_eff)
    h_b = np.zeros(N)
    A_hx = np.zeros(N)
    A_preh = np.zeros(N-1)
    alfac_first = np.zeros(N)
    alfa_cond = np.zeros(N)
    M_cond_ph = np.zeros(N)
    DTLM = np.zeros(N)
    Xff = np.zeros(N)
    Xb_int = np.zeros(N)

    # assumptions
    Tcw_in = Tsw
    Xf = Xsw
    Xff[0] = Xf
    Xb[N - 1] = Xb_out
    alfa_cond[N - 1] = 0  # no preheater in the last effect
    # first guess values
    Tn = Tn_design
    Tcw_out = Tn - DTT_cond
    T[N - 1] = Tn
    T_preh[N - 1] = Tcw_out
    # n_tubes = 2000
    n_tubes = [8000 for i in range(0, N)]
    DeltaT_tot_eff = Ts_design - Tn
    if BPE_tot > DeltaT_tot_eff:
        logger.warning('Too high concentrations for the temperature range, decrease N or enlarge the T range!')
    if TVC_on:
        Pm = Pm_design
        Tm = h2oprop.sat_temp(Pm)
        Ts = Ts_design
        if (Ts + DT_s) < Tm:
            logger.warning('choose another Xb: (Xb + DTs) has to be higher than Tm!')
        Pev = h2oprop.sat_pressure(Tn)
        Ps = h2oprop.sat_pressure(Ts)
        if (Ps / Pev) < 1.81:
            Ts = Tn + N * DT_min
            Ps = h2oprop.sat_pressure(Ts)
        # TVC properties
        ER = tvc.tvc_entrainment_ratio(Pm, Ts, Tn, Xb_out, parameter_config)
        Mms = Mms_firstguess
        Ms, Mss = tvc.tvc_steam(ER, Mms)
    else:
        Ts = Ts_design
        Ms = Mms_firstguess
        Mms = Ms
        Pm = 0
        Mss = 0
        ER = 0

    # global balance
    Mbrine, Mdist = design_equalAhx.global_balance_mfeed(Mfeed, Xf, Xb_out)
    print(Mbrine, Mdist)

    # first guess values
    FeedFraction = Mfeed / N
    Mf[0] = FeedFraction
    # Mvap = np.array([Mdist/N for i in range(0, N)])
    if Xb_out/Xsw > 3:
        Md[0] = 0.7 * FeedFraction
    else:
        Md[0] = 0.6 * FeedFraction
    Mvap[0] = Md[0] / 0.999
    Mb_g = FeedFraction - Md
    Xb_int[0] = Xb_out
    Xb[0] = FeedFraction * Xf / Mb_g[0]
    Mb[0] = Mb_g[0]
    Mb[N-1] = Mbrine
    for iEffect in range(1, N):
        Mb_g[iEffect] = (Mb[N - 1] - Mb[0]) / (N - 1)
        Mb[iEffect] = Mb[iEffect-1] + Mb_g[iEffect]
        Xb[iEffect] = (Mb[iEffect-1] * Xb[iEffect-1] + FeedFraction * Xf)/Mb[iEffect]
        Md[iEffect] = FeedFraction - Mb_g[iEffect]
        # Mvap[iEffect] = Mb[iEffect-1] + FeedFraction - Mb[iEffect]
        Mf[iEffect] = Mb[iEffect-1] + FeedFraction
        Xff[iEffect] = (Mb[iEffect-1] * Xb_out + FeedFraction * Xf) / Mf[iEffect]
        Xb_int[iEffect] = FeedFraction * Xf / (FeedFraction - Md[iEffect])
    Mvap = Md / 0.95
    M_fbrine[0] = 0
    M_fb[0] = 0.001 * Mvap[0]
    for i_effect in range(1, N):
        M_fbrine[i_effect] = Mvap[i_effect] * 0.025
        M_fb[i_effect] = Mvap[i_effect] * 0.025
    Mfeed_ph[0] = FeedFraction
    for i_effect in tools.itereffects(1, N):
        Mfeed_ph[i_effect] = Mfeed_ph[i_effect - 1] + FeedFraction
    Msteam = Ms
    A_av = 0
    DeltaA = 0
    SumSquareErrorArea = 0
    Mdist_real = Mdist
    Error_Mdist = 0

    U, invSumU = medmodel.u_firstguess(Ts, N)
    DeltaT_first, T, Tvsat_prime, Tc_prime, Tc = medmodel.t_firstguess(Ts, Tn, U, invSumU, Xf, N, deltat = [], T = [])
    DeltaT_tot = np.sum(DeltaT_first)
    DeltaT = DeltaT_first * 1
    DeltaT_phfirst, T_prehfirst = design_equalAph_DTpreh_parallcross.tpreh_profile(T[0], DTT_preh, T_preh[N - 1], N, deltat_ph = [], T_preh = [])
    DeltaT_ph = np.array(DeltaT_phfirst)
    T_preh = np.array(T_prehfirst)
    for iEffect in tools.itereffects(0, (N - 1)):
        M_cond_ph[iEffect] = Mfeed_ph[iEffect] * naclprops.cp_seawater((T_preh[iEffect] + T_preh[iEffect + 1]) / 2, Xf) * (
        T_preh[iEffect] - T_preh[iEffect + 1]) / (h2oprop.latent_heat(Tvsat_prime[iEffect]))
        alfa_cond[iEffect] = M_cond_ph[iEffect] / Mvap[iEffect]
    alfa_cond[N - 1] = 0
    solver = design_equalAph_DTpreh_parallcross.MedSolver_eqAph_parallcross(N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Tcw_out, Tcw_in, Mb, Md, M_fbrine,
                 M_fb, Mf, Mvap, Mfeed_ph,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, DTT_preh, DTT_cond, DT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                 TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, parameter_config, DTLM, n_tubes, Xb_int)
    return solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first

# define all solver functions above this point!
import inspect
import sys
solverReg = {name.split('solver_based_on_')[-1]: obj for name, obj in inspect.getmembers(sys.modules[__name__])
                     if (inspect.isfunction(obj) and
                         name.startswith('solver_based_on'))}