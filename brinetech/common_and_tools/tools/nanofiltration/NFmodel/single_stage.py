from . import linear_system_coeff as ls
from . import mass_transfer_coeff as mt
import operator
from . import membrane_profiles, mass_transfer_coeff
from scipy.optimize import minimize
import numpy as np
from .logger import logger

from ....common.properties import density_mixtures

# Refactored from NFmodel/solution_prop -- there these ions were ignored
# TODO: Figure out why these ions should be ignored, and note it here!
IGNORE_IONS_IN_DENSITY = ["COD", "dye"]

'''function for the design of the RO single stage'''

def global_balance_mpermeate(mp, r1):
    mf = mp/r1
    mc = mf - mp
    return mf, mc

def global_balance_mfeed(mf, r1):
    mp = mf * r1
    mc = mf - mp
    return mp, mc

def global_balance_xc(xf, r1, sr1):
    xc = xf * (1 + (r1 * sr1) / (1 - r1))
    return xc

def average_feed_salinity(xf, r, cpf, tcf):
    afs = xf/r * np.log(1/(1-r)) * cpf * tcf
    return afs

def osmotic_pressure_out(config, xc, xp):
    Posm_out = config['osmotic_coeff'] * (xc - xp) * 1e-4
    return Posm_out

def feed_pressure(config, posm_out, p_losses):
    Pin = posm_out * config['security_factor'] + p_losses
    return Pin

def feed_Re(config, u):
    De = 4 * config['h_feedchannel'] * config['w_membrane'] / (2 * (config['h_feedchannel'] + config['w_membrane']))
    Re = config['rho_bulk'] * u * De / (config['eta_bulk']* 1e-3)
    # print('Value of Re (delete this after rectifying the error:', Re)
    return Re

def friction_factor_feed(Re):
    # f = 6.23 / np.power(Re, 0.3)   # Original of Marina. Changed due to error: RuntimeWarning: invalid value encountered in power
    try:
        f = 6.23 / np.power(Re, 0.3)
    except RuntimeWarning:
        print('Some error with value in power. Value of Re:',Re)


    #f = 6.23 / np.sign(Re) * np.power(abs(Re), 0.3)
    return f

def feed_pressure_losses(f, x, config, u): # f: friction coefficient, x lenght along the feed channel in the flow direction,
    # u bulk velocity of flow
    De = 4 * config['h_feedchannel'] * config['w_membrane'] / (2 * (config['h_feedchannel'] + config['w_membrane']))
    DP = f * x * config['rho_bulk'] * u**2 / (2 * De)
    return DP

def net_driving_pressure(p_in, posm_out, p_losses, config):
    ndp = p_in - posm_out - p_losses /(config['n_elements1'] * 2)
    return ndp

def minimum_Pfeed(Xfeed):
    Pfeed_min = 0.0008*Xfeed + 6.9
    return Pfeed_min

def estimation_n_vessel(config, deltaP, eta, n_elem):
    n_vessel = config['Recovery'] * config['Mfeed'] / (membrane_profiles.solvent_flux_HP(deltaP, config)
                                                       * config['A_membrane'] * n_elem)
    return n_vessel

def solver_membrane_definition(C_b, phi_steric, phi_DE, Jv, parameter_config, N_nodes, dx, deltax,
                               k_d_pore, k_c_pore, k_c_prime, k_c_bulk, D_p, D_inf, l, z, ions,  R_ion, Ncomp, pos):
    C_p = {}
    C_bm = {}
    C_m = {}
    Delta_Cp = {}
    Delta_Cm = {}
    Cp_first = {}
    Cm_first = {}
    psi_p = 0
    psi_p_first = 0
    csi = 0
    psi_m = np.zeros(len(range(N_nodes)))
    psi_m_first = np.zeros(len(range(N_nodes)))
    for key in ions:
        C_m[key] = np.array([C_b[key] for i in range(N_nodes)])
        Delta_Cm[key] = np.zeros(len(range(N_nodes)))
        C_p[key] = C_b[key]
        C_bm[key] = C_b[key]

    variables = np.array([])
    ordered_ions = [key for (key, value) in sorted(pos.items(), key=operator.itemgetter(1))]
    for key in ordered_ions:
        variables = np.concatenate((variables, C_m[key]))
    variables = np.concatenate((variables, psi_m))
    var = []
    for key in ordered_ions:
        var.append(C_p[key])
    for key in ordered_ions:
        var.append(C_bm[key])
    var.append(csi)
    var.append(psi_p)
    var = np.array(var)
    variables = np.concatenate((variables, var))
    sol = np.zeros(len(variables))
    res = 0
    res_bool = True

    solver = membrane_profiles.DSPMDEsolver(C_b, C_p, C_m, C_bm, psi_m, psi_p, csi, phi_steric, phi_DE, Jv,
                                            parameter_config, variables, N_nodes, dx,
                                            deltax, k_d_pore, k_c_pore, k_c_prime, k_c_bulk, D_p, D_inf, l, z, ions,
                                            Delta_Cp, Delta_Cm, Cm_first, Cp_first,
                                            psi_p_first, psi_m_first, R_ion, sol, res, res_bool, Ncomp, pos)
    return solver


def solver_element_definition_Rejectiongiven(config, ions, pos, Pfeed, Mfeed, C_feed, N_nodes, n_discr_L, n_elem,
                                             n_vessel, R_ion):
    Ncomp = len(ions)
    l = {}
    k_d_pore = {}
    k_c_pore = {}
    k_c_bulk = {}
    k_c_prime = {}
    z = {}
    phi_steric = {}
    phi_DE = {}
    D_p = {}
    D_inf = {}
    C_b = {}
    C_p = {}
    C_p_cum = {}
    C_conc = {}
    C_conc_out_elem = {}
    C_p_out_elem = {}
    Fs = {}
    P = np.zeros(n_discr_L)
    M_b = np.zeros(n_discr_L)
    M_conc = np.zeros(n_discr_L)
    C_p_cum_x = {}
    Fs_cum_x = {}
    Fw_cum_x = np.zeros(n_discr_L)
    M_p_cum_x = np.zeros(n_discr_L)
    for key in pos.keys():
        C_p_out_elem[key] = C_feed[key] * (1-R_ion[key])

    M_p = np.zeros(n_discr_L)
    Fw = np.zeros(n_discr_L)
    Posm = np.zeros(n_discr_L)
    M_conc_out_elem = np.zeros(n_elem)
    M_p_out_elem = np.zeros(n_elem)
    for key in ions:
        C_p_cum[key] = np.zeros(n_discr_L)
        Fs_cum_x[key] = np.zeros(n_discr_L)
        # Fs[key] = np.zeros((n_discr_L, n_discr_w))
        Fs[key] = np.zeros(n_discr_L)
        C_b[key] = np.zeros(n_discr_L)
        C_conc[key] = np.zeros(n_discr_L)
        # C_p[key] = np.zeros((n_discr_L, n_discr_w))
        C_p[key] = np.zeros(n_discr_L)
        for x in range(n_discr_L):
            C_b[key][x] = C_feed[key]
            C_conc[key][x] = C_feed[key]
            C_p[key][x] = C_p_out_elem[key]
        C_conc_out_elem[key] = 0
        C_p_out_elem[key] = 0

    eta_solvent = config['eta_solvent'] * 1e-3  # Pa s
    rho_bulk = 1000
    Rec_elem = 0.2
    Posm_out = 0
    Mpermeate_calc = 0
    Amembr = config['A_membrane']
    Amembr_tot = Amembr * n_elem

    for key in ions:
        z[key] = config['z_' + key]
        D_inf[key] = config['D_inf_' + key] * 1e-9  # [m2/s]
#       l[key] = config['r_' + key] / config['rp']
#       k_d_pore[key] = mt.hindrance_factor_diffusion(l[key])
#       k_c_pore[key] = mt.hindrance_factor_convection(l[key])
#       D_p[key] = k_d_pore[key] * config['D_inf_' + key] * 1e-9  # [m2/s]
#       phi_steric[key] = (1 - l[key]) ** 2
#       phi_DE[key] = np.exp(-membrane_profiles.DW_dielectric_exclusion(z[key], config['r_' + key], config['epsilon_r_water'],
#                                                                        config['epsilon_r_pore'], config) / (config['k_b'] * config['T']))

    solver = NFsolverElement1Ddiscretization(Rec_elem, P, Pfeed, C_b, C_feed, C_conc, M_b, M_p, M_p_out_elem, C_p_out_elem, M_conc_out_elem,
                 C_conc_out_elem, C_p, C_p_cum, Mfeed, Fw, Fs, Amembr, config, n_elem, Posm_out, Mpermeate_calc,
                 n_discr_L, M_conc, Amembr_tot, Posm, phi_steric, phi_DE, N_nodes,
                 k_d_pore, k_c_pore, k_c_bulk, k_c_prime, eta_solvent, D_p, D_inf, l, z, ions, R_ion, Ncomp, pos, n_vessel)

    return solver

def NF_pressurevessel_solver_definition(parameter_config, pos, Cfeed_plant, Pfeed, recovery_req, Mfeed_plant, R_ion_given):
    ions = {}
    for key in pos:
        ions[key] = 0
    Ncomp = len(ions)
    N_nodes = parameter_config['N_nodes']
    n_discr_L = parameter_config['N_discr_L']
    n_elem = parameter_config['n_elements']
    Mfeed_elem = np.zeros(n_elem)
    Mconc_elem = np.zeros(n_elem)
    Mp_elem = np.zeros(n_elem)
    Pin_elem = np.zeros(n_elem)
    Cp_elem = {}
    Cconc_elem = {}
    Cfeed_elem = {}
    R_ion = {}
    for key in pos:
        Cp_elem[key] = np.zeros(n_elem)
        Cconc_elem[key] = np.zeros(n_elem)
        R_ion[key] = np.zeros(n_elem)
    Mp_out_plant = Mfeed_plant * recovery_req
    Mconc_out_plant = Mfeed_plant - Mp_out_plant
    Cp_out_plant = {}
    Cconc_out_plant = {}
    R_ion_plant = {}
    for key in pos:
        Cconc_out_plant[key] = Cfeed_plant[key]
        Cp_out_plant[key] = 0
        R_ion_plant[key] = 0
    eta_solvent = parameter_config['eta_solvent'] * 1e-3
    n_vessel = estimation_n_vessel(parameter_config, Pfeed * 0.5, eta_solvent, n_elem)
    logger.info('membrane parameters: rp={}, deltax={}, epsilonp={}, Xd={}'.format(parameter_config['rp'],
                                                                                   parameter_config[
                                                                                       'thickness_membrane'],
                                                                                   parameter_config['epsilon_r_pore'],
                                                                                   parameter_config['charge_density']))
    solver = NFSolverPressureVessel_Rejectiongiven(n_elem, parameter_config, ions, pos, Mfeed_elem,
                                                                Mconc_elem,
                                                                Mp_elem, Cp_elem, Cconc_elem, Cfeed_elem,
                                                                R_ion, Pin_elem, n_discr_L, Mconc_out_plant,
                                                                Mp_out_plant, Cp_out_plant,
                                                                Cconc_out_plant, R_ion_plant, N_nodes, n_vessel,
                                                                recovery_req, Mfeed_plant,
                                                                Cfeed_plant, R_ion_given)
    return solver

class NFsolverElement1Ddiscretization:
    def __init__(self, Rec_elem, P, Pfeed, C_b, C_feed, C_conc, M_b, M_p, M_p_out_elem, C_p_out_elem, M_conc_out_elem,
                 C_conc_out_elem, C_p, C_p_cum, Mfeed, Fw, Fs, Amembr, config, nelem, Posm_out, Mpermeate_calc,
                 ndiscr_L, M_conc, Amembr_tot, Posm, phi_steric, phi_DE, N_nodes,
                 k_d_pore, k_c_pore, k_c_bulk, k_c_prime, eta_solvent, D_p, D_inf, l, z, ions, R_ion, Ncomp, pos, n_vessel):
        self.Rec_elem = Rec_elem
        self.P = P
        self.Pfeed = Pfeed
        self.C_b = C_b
        self.C_feed = C_feed
        self.C_conc = C_conc
        self.M_b = M_b
        self.M_p = M_p
        self.M_p_out_elem = M_p_out_elem
        self.C_p_out_elem = C_p_out_elem
        self.M_conc_out_elem = M_conc_out_elem
        self.C_conc_out_elem = C_conc_out_elem
        self.C_p = C_p
        self.Mfeed = Mfeed
        self.Fw = Fw
        self.Fs = Fs
        self.Amembr = Amembr
        self.config = config
        self.nelem = nelem
        self.Posm_out = Posm_out
        self.Posm = Posm
        self.Mpermeate_calc = Mpermeate_calc
        self.C_p_cum = C_p_cum
        self.ndiscr_L = ndiscr_L
        self.M_conc = M_conc
        self.Amembr_tot = Amembr_tot
        self._log = []
        self.Amembr_tot_calc = 0
        self.phi_steric = phi_steric
        self.phi_DE = phi_DE
        self.k_d_pore = k_d_pore
        self.k_c_pore = k_c_pore
        self.k_c_bulk = k_c_bulk
        self.k_c_prime = k_c_prime
        self.eta_solvent = eta_solvent
        self.D_p = D_p
        self.D_inf = D_inf
        self.l = l
        self.z = z
        self.ions = ions
        self.N_nodes = N_nodes
        self.pos = pos
        self.R_ion = R_ion
        self.Ncomp = Ncomp
        self.n_vessel = n_vessel
        self.toll_calc = 1e-4
        self.w = self.config['w_membrane']
        self.L = self.config['L_membrane']
        self.thickness = self.config['thickness_membrane'] * 1e-6  # m
        self.deltax = self.L / self.ndiscr_L
        self.deltaz = self.thickness / self.N_nodes
        self.C_f_cell = {}
        self.C_bm = {}
        self.C_p_av = {}
        for key in self.ions:
            self.C_bm[key] = self.C_b[key] * 1
        self.DP_membrane = np.zeros(self.ndiscr_L)
        self.conc_dict_elem = []
        for i in range(self.ndiscr_L):
            self.conc_dict_elem.append({})
            for key in self.ions:
                self.conc_dict_elem[i][key] = self.C_conc[key][i]


    def resolution_NFelem_activitycoeff_Rejectiongiven(self, Pfeed_el, Cfeed_el, Mfeed_el):
        error_Cp = 1000.0
        i = 0
        while error_Cp > 0.15:
            Cp_first = 0
            for key in self.ions:
                Cp_first += self.C_p_out_elem[key]
            '''resolution of the first deltax'''
            self.P[0] = Pfeed_el  # bar
            for key in self.ions:
                self.C_b[key][0] = Cfeed_el[key]  # mol/m3
                self.C_bm[key][0] = self.C_b[key][0] * mass_transfer_coeff.concentration_polarization_factor(self.Rec_elem)
            self.M_b[0] = Mfeed_el  # m3/s
            self.Posm[0] = 0
            for key in self.ions:
                self.Posm[0] += (self.C_bm[key][0] - self.C_p[key][0])
            self.Posm[0] = self.Posm[0] * self.config['R'] * self.config['T'] * 1e-5  # bar
            self.DP_membrane[0] = (self.P[0] - 1) - self.Posm[0] # bar
            if 0 < self.DP_membrane[0] < 3:
                print('P too low')
            elif self.DP_membrane[0] < 0:
                self.DP_membrane[0] = self.P[0] * 0.375
            if Mfeed_el < 7e-5 and i > 0:
                self.DP_membrane[0] = self.DP_membrane[3]
            self.Fw[0] = membrane_profiles.solvent_flux_HP(self.DP_membrane[0], self.config)  # m/s

            self.M_p[0] = self.Fw[0] * self.Amembr_tot / (self.nelem * self.ndiscr_L)  # m3/s
            ions_minus_Na = {}
            for key in self.ions:
                ions_minus_Na[key] = 0
            del ions_minus_Na['Na']
            for key in self.ions:
                self.C_p[key][0] = self.C_b[key][0] * (1 - self.R_ion[key])
            self.C_p['Na'][0] = 0
            for key_minus_Na in ions_minus_Na:
                self.C_p['Na'][0] += -1 * self.C_p[key_minus_Na][0] * self.z[key_minus_Na]
                # self.C_p[key][0] = self.M_p[0] * self.C_p_cum[key][0] * (self.nelem * self.ndiscr_L) / self.Amembr_tot / self.Fw[0]
            for key in self.ions:
                self.Fs[key][0] = self.Fw[0] * self.C_p[key][0]  # mol/(m2 s)
                self.C_p_cum[key][0] = (self.Fs[key][0] * self.Amembr_tot / (self.nelem * self.ndiscr_L)) / self.M_p[0]
            self.M_conc[0] = (Mfeed_el * density_mixtures(self.config, Cfeed_el, ignore_ions=IGNORE_IONS_IN_DENSITY) - self.Fw[0]
                              * self.Amembr_tot / (self.nelem * self.ndiscr_L) * 1000) / \
                             density_mixtures(self.config, self.conc_dict_elem[0], ignore_ions=IGNORE_IONS_IN_DENSITY)  # m3/s
            for key in self.ions:
                self.C_conc[key][0] = (Mfeed_el * Cfeed_el[key] - self.Fs[key][0] * self.Amembr_tot
                                    / (self.nelem * self.ndiscr_L)) / self.M_conc[0]  # mol/m3
                self.conc_dict_elem[0][key] = self.C_conc[key][0]
            '''resolution of the generic cells'''
            for x in range(1, self.ndiscr_L):
                self.M_b[x] = self.M_conc[x-1]
                for key in self.ions:
                    self.C_b[key][x] = self.C_conc[key][x-1]
                    self.C_bm[key][x] = self.C_b[key][x] * mass_transfer_coeff.concentration_polarization_factor(
                        self.Rec_elem)
                self.P[x] = self.P[x - 1] - feed_pressure_losses(friction_factor_feed(feed_Re(self.config, self.M_b[x] /
                                            (self.config['area_feedchannel']))), self.deltax, self.config,
                                            self.M_b[x] / (self.config['area_feedchannel'])) * 1e-5 #  * self.n_vessel
                self.Posm[x] = 0
                for key in self.ions:
                    self.Posm[x] += (self.C_bm[key][x] - self.C_p[key][x-1])  #  self.C_p_av[key]
                self.Posm[x] = self.Posm[x] * self.config['R'] * self.config['T'] * 1e-5  # bar
                self.DP_membrane[x] = (self.P[x] - 1) - self.Posm[x]
                # if 0 < self.DP_membrane[x] < 3:
                #     print('P too low')
                if self.DP_membrane[x] < 0 or self.DP_membrane[x] > self.P[x]:
                    self.DP_membrane[x] = self.DP_membrane[x-1] # self.P[x] * 0.375
                if x == 1 or x == 2: #
                    if Mfeed_el < 6e-5 and i > 0:
                        self.DP_membrane[x] = self.DP_membrane[3]
                self.Fw[x] = membrane_profiles.solvent_flux_HP(self.DP_membrane[x], self.config)  # m/s
                for key in self.ions:
                    if Mfeed_el < 7e-5:  # and i > 0
                        self.k_c_bulk[key] = mt.mass_transfer_coeff_spiralwound(self.config, self.D_inf[key],
                                                                                mt.Schmidt_number(self.eta_solvent,
                                                                                                  self.config['rho_bulk'],
                                                                                                  self.D_inf[key]),
                                                                                mt.Peclet_number(self.config,
                                                                                                 self.D_inf[key],
                                                                                                 self.M_b[x] * 5 / (
                                                                                                 self.config[
                                                                                                     'area_feedchannel'])))  # * self.n_vessel
                    else:
                        self.k_c_bulk[key] = mt.mass_transfer_coeff_spiralwound(self.config, self.D_inf[key],
                                                                                mt.Schmidt_number(self.eta_solvent,
                                                                                                  self.config['rho_bulk'],
                                                                                                  self.D_inf[key]),
                                                                                mt.Peclet_number(self.config,
                                                                                                 self.D_inf[key],
                                                                                                 self.M_b[x] / (self.config[
                                                                                                                    'area_feedchannel'])))  # * self.n_vessel
                    #print('value of Jv/kc (delete after correcting the error):', self.Fw[x] / self.k_c_bulk[key])
                    self.k_c_prime[key] = self.k_c_bulk[key] * mt.correction_factor_kc_interface(self.Fw[x], self.k_c_bulk[
                        key])  # [m/s]
                    self.C_bm[key][x] = self.C_b[key][x] * np.exp(self.Fw[x] / (self.k_c_prime[key]))
                self.M_p[x] = self.M_p[x-1] + self.Fw[x] * self.Amembr_tot / (self.nelem * self.ndiscr_L)  # m3/s
                for key in self.ions:
                    self.C_p[key][x] = self.C_b[key][x] * (1 - self.R_ion[key])
                self.C_p['Na'][0] = 0
                for key_minus_Na in ions_minus_Na:
                    self.C_p['Na'][0] += -1 * self.C_p[key_minus_Na][0] * self.z[key_minus_Na]
                    # self.C_p[key][x] = (self.M_p[x] * self.C_p_cum[key][x] - self.C_p_cum[key][x-1] * self.M_p[x-1]) * \
                    #                    (self.nelem * self.ndiscr_L) / self.Amembr_tot / self.Fw[x]
                for key in self.ions:
                    self.Fs[key][x] = self.Fw[x] * self.C_p[key][x]  # mol/(m2 s)
                    self.C_p_cum[key][x] = (self.C_p[key][x-1] * self.M_p[x-1] + self.Fs[key][x]
                                               * self.Amembr_tot / (self.nelem * self.ndiscr_L)) / self.M_p[x]
                self.M_conc[x] = (self.M_b[x] * density_mixtures(self.config, self.conc_dict_elem[x-1], ignore_ions=IGNORE_IONS_IN_DENSITY)
                                  - self.Fw[x] * self.Amembr_tot / (self.nelem * self.ndiscr_L) * 1000) / \
                                 density_mixtures(self.config, self.conc_dict_elem[x], ignore_ions=IGNORE_IONS_IN_DENSITY)  # m3/s
                if self.M_conc[x] <= 0:
                    self.Fw[x] = self.Fw[x] * 0.9
                    self.M_conc[x] = (self.M_b[x] * density_mixtures(self.config, self.conc_dict_elem[x - 1], ignore_ions=IGNORE_IONS_IN_DENSITY)
                                      - self.Fw[x] * self.Amembr_tot / (self.nelem * self.ndiscr_L) * 1000) / \
                                     density_mixtures(self.config, self.conc_dict_elem[x], ignore_ions=IGNORE_IONS_IN_DENSITY)  # m3/s
                    if self.M_conc[x] < 0:
                        self.M_conc[x] = 1.0e-7
                for key in self.ions:
                    self.C_conc[key][x] = (self.M_b[x] * self.C_b[key][x] - self.Fs[key][x] * self.Amembr_tot
                                            / (self.nelem * self.ndiscr_L)) / self.M_conc[x]  # mol/m3
                    self.conc_dict_elem[x][key] = self.C_conc[key][x]
            a = 1
            '''output of the NF unit'''
            self.M_conc_out_elem = self.M_conc[self.ndiscr_L - 1]
            self.M_p_out_elem = self.M_p[self.ndiscr_L - 1]
            self.Rec_elem = self.M_p_out_elem / self.Mfeed
            electroneutrality_check_p = 0
            electroneutrality_check_conc = 0
            for key in self.ions:
                self.C_conc_out_elem[key] = self.C_conc[key][self.ndiscr_L - 1]
                self.C_p_out_elem[key] = self.C_p_cum[key][self.ndiscr_L-1]
                electroneutrality_check_p += self.C_p_out_elem[key] * self.config['z_'+key]
                electroneutrality_check_conc += self.C_conc_out_elem[key] * self.config['z_' + key]
                # self.R_ion[key] = 1 - self.C_p_out_elem[key] / Cfeed_el[key]
#            logger.info('electroneutrality_check_p is: "' + str(electroneutrality_check_p) + '"')
#            logger.info('electroneutrality_check_conc is: "' + str(electroneutrality_check_conc) + '"')
            if abs(electroneutrality_check_conc) > 1e-3:
                self.C_conc_out_elem['Na'] = 0
                for key in self.ions:
                    if key != 'Na':
                        self.C_conc_out_elem['Na'] += -1 * self.config['z_'+key] * self.C_conc_out_elem[key]
                self.C_p_out_elem['Na'] = (self.Mfeed * self.C_feed['Na'] - self.M_conc_out_elem *
                                              self.C_conc_out_elem['Na']) / self.M_p_out_elem
                self.C_p_out_elem['Cl'] = 0
                for key in self.ions:
                    if key != 'Cl':
                        self.C_p_out_elem['Cl'] += 1 * self.config['z_' + key] * self.C_p_out_elem[key]
                if self.C_p_out_elem['Na'] < 0:
                    logger.warning('concentration has to be positive (C_p_out_elem[Na] is neg ({})), the system is not working'.format(self.C_p_out_elem['Na']))
            electroneutrality_check_p = 0
            for key in self.ions:
                electroneutrality_check_p += self.C_p_out_elem[key] * self.config['z_'+key]
            if abs(electroneutrality_check_p) > 1e-3:
                self.C_p_out_elem['Cl'] = 0
                for key in self.ions:
                    if key != 'Cl':
                        self.C_p_out_elem['Cl'] += 1 * self.config['z_'+key] * self.C_p_out_elem[key]
                self.C_conc_out_elem['Cl'] = (self.Mfeed * self.C_feed['Cl'] - self.M_p_out_elem *
                                              self.C_p_out_elem['Cl']) / self.M_conc_out_elem
                self.C_conc_out_elem['Na'] = 0
                for key in self.ions:
                    if key != 'Na':
                        self.C_conc_out_elem['Na'] += -1 * self.config['z_' + key] * self.C_conc_out_elem[key]
                if self.C_conc_out_elem['Cl'] < 0:
                    logger.warning('concentration has to be positive (C_conc_out_elem[Cl] is neg), the system is not working')
            electroneutrality_check_conc = 0
            for key in self.ions:
                electroneutrality_check_conc += self.C_conc_out_elem[key] * self.config['z_' + key]
            Cp_out_calc = 0
            for key in self.ions:
                Cp_out_calc += self.C_p_out_elem[key]
            error_Cp = abs(Cp_out_calc - Cp_first)
            i += 1
            # print('iteration unit for the NF element', i, 'error Cp unit', error_Cp)
            if i > 100:
                print('too many iteration in the element resolution')
                break

class NFSolverPressureVessel_Rejectiongiven:
    def __init__(self, n_elem, config, ions, pos, Mfeed_elem, Mconc_elem, Mp_elem, Cp_elem, Cconc_elem, Cfeed_elem,
                 R_ion, Pin_elem, ndiscr_L, Mconc_out_plant, Mp_out_plant, Cp_out_plant, Cconc_out_plant,
                 R_ion_plant, N_nodes, n_vessel, recovery_req, Mfeed_tot, Cfeed_plant, R_ion_given):
        self.n_elem = n_elem
        self.config = config
        self.ions = ions
        self.pos = pos
        self.Mfeed_elem = Mfeed_elem
        self.Mconc_elem = Mconc_elem
        self.Mp_elem = Mp_elem
        self.Cp_elem = Cp_elem
        self.Cconc_elem = Cconc_elem
        self.Cfeed_elem = Cfeed_elem
        self.R_ion = R_ion
        self.Pin_elem = Pin_elem
        self.ndiscr_L = ndiscr_L
        self.Mconc_out_plant = Mconc_out_plant
        self.Mp_out_plant = Mp_out_plant
        self.Cp_out_plant = Cp_out_plant
        self.Cconc_out_plant = Cconc_out_plant
        self.R_ion_plant = R_ion_plant
        self.N_nodes = N_nodes
        self.Fw_average_elem = np.zeros(n_elem)
        self.recovery = np.zeros(n_elem)
        self.n_vessel = n_vessel
        self.Fw_average_vessel = 0
        self.recovery_calc = 0
        self.recovery_req = recovery_req
        self.element = {}
        self.MpCp = {}
        self.Mfeed_tot = Mfeed_tot
        self.Cfeed_plant = Cfeed_plant
        self.R_ion_given = R_ion_given


    def resolution_pressurevessel_Rejectiongiven(self, Pfeed):
        i = 0
        self.recovery_calc = 0
        while abs(self.recovery_calc - self.recovery_req) > 5e-2:
            self.Pin_elem[0] = Pfeed
            self.Mfeed_elem[0] = self.Mfeed_tot / self.n_vessel
            if self.Mfeed_elem[0] < 11e-5:
                self.n_vessel = self.n_vessel / 1.1
                self.Mfeed_elem[0] = self.Mfeed_tot / self.n_vessel
            for key in self.ions:
                self.Cfeed_elem[key] = self.Cfeed_plant[key]
            self.element[0] = solver_element_definition_Rejectiongiven(self.config, self.ions, self.pos, Pfeed, self.Mfeed_elem[0],
                                                        self.Cfeed_elem, self.N_nodes, self.ndiscr_L, self.n_elem, self.n_vessel, self.R_ion_given)
            self.element[0].resolution_NFelem_activitycoeff_Rejectiongiven(Pfeed, self.Cfeed_elem, self.Mfeed_elem[0])
            self.Mconc_elem[0] = self.element[0].M_conc_out_elem
            self.Mp_elem[0] = self.element[0].M_p_out_elem
            self.Fw_average_elem[0] = np.sum(self.element[0].Fw) / self.ndiscr_L
            for key in self.ions:
                self.Cconc_elem[key][0] = self.element[0].C_conc_out_elem[key]
                self.Cp_elem[key][0] = self.element[0].C_p_out_elem[key]
                self.R_ion[key][0] = self.element[0].R_ion[key]
            for i_elem in range(1, self.n_elem):
                self.Pin_elem[i_elem] = self.element[i_elem-1].P[self.ndiscr_L-1]
                self.Mfeed_elem[i_elem] = self.Mconc_elem[i_elem-1]
                for key in self.ions:
                    self.Cfeed_elem[key] = self.Cconc_elem[key][i_elem-1]
                self.element[i_elem] = solver_element_definition_Rejectiongiven(self.config, self.ions, self.pos, self.Pin_elem[i_elem],
                                                                 self.Mfeed_elem[i_elem], self.Cfeed_elem, self.N_nodes,
                                                                 self.ndiscr_L, self.n_elem, self.n_vessel, self.R_ion_given)
                self.element[i_elem].resolution_NFelem_activitycoeff_Rejectiongiven(self.Pin_elem[i_elem], self.Cfeed_elem,
                                                                     self.Mfeed_elem[i_elem])
                self.Mconc_elem[i_elem] = self.element[i_elem].M_conc_out_elem
                self.Mp_elem[i_elem] = self.element[i_elem].M_p_out_elem
                self.Fw_average_elem[i_elem] = np.sum(self.element[i_elem].Fw) / self.ndiscr_L
                for key in self.ions:
                    self.Cconc_elem[key][i_elem] = self.element[i_elem].C_conc_out_elem[key]
                    self.Cp_elem[key][i_elem] = self.element[i_elem].C_p_out_elem[key]
                    self.R_ion[key][i_elem] = self.element[i_elem].R_ion[key]
            self.Mconc_out_plant = self.Mconc_elem[self.n_elem - 1] * self.n_vessel
            for key in self.ions:
                self.Cconc_out_plant[key] = self.Cconc_elem[key][self.n_elem - 1]
            self.Mp_out_plant = np.sum(self.Mp_elem) * self.n_vessel
            self.Fw_average_vessel = np.sum(self.Fw_average_elem[:-1]) / (self.n_elem-1)
            for key in self.ions:
                self.MpCp[key] = 0
                for i_elem in range(0, self.n_elem):
                    self.MpCp[key] += self.Mp_elem[i_elem] * self.Cp_elem[key][i_elem]
                self.Cp_out_plant[key] = self.MpCp[key] / np.sum(self.Mp_elem)   # Cp_out_plant = Cp_out del singolo vessel
                #self.R_ion_plant[key] = 1 - (self.Cp_out_plant[key] / self.Cfeed_plant[key])
                ## Nikhil: added try except to make R_ion zero if it is invalid value (or else the excel sheet opening encounters an error)
                import warnings
                warnings.simplefilter("error", RuntimeWarning)
                try:
                    self.R_ion_plant[key] = 1 - (self.Cp_out_plant[key] / self.Cfeed_plant[key])
                except RuntimeWarning:
                    print('ion with error in R_ion:', key)
                    self.R_ion_plant[key] = 0
            self.recovery_calc = self.Mp_out_plant / self.Mfeed_tot
            ##logger.info('recovery_calc, Mp_out_plant and key are: "' + str(self.recovery_calc) + ' ' + str(self.Mp_out_plant) + ' ' + str(key) + '"')
            # self.n_vessel = int(self.recovery_req * self.Mfeed_tot / self.Fw_average_vessel / (self.config['A_membrane'] * self.n_elem)) + 1
            if self.recovery_req < self.recovery_calc:
                self.n_vessel = int(self.n_vessel * (self.recovery_req / self.recovery_calc) ** (1 / 4))
            else:
                self.n_vessel = int(self.n_vessel * (self.recovery_req / self.recovery_calc) ** (1 / 4)) + 1
            if self.recovery_calc > 0.9:
                self.n_vessel = int(self.recovery_req * self.Mfeed_tot / self.Fw_average_vessel / (
                        self.config['A_membrane'] * self.n_elem)) + 1
                print(i, self.recovery_calc, self.n_vessel)
            i += 1
            if i > 40:
                print('N vessel iterations > 40')
                print(self.recovery_calc, self.n_vessel)
                break


    def resolution_pressurevessel_approxfirstelem_recoveryreq_Rejectiongiven(self, Pfeed):
        i = 0
        # while abs(self.recovery_calc - self.recovery_req) > 0.05:
        while abs(self.recovery_calc - self.recovery_req) > 1e-1:
            print('{} iteration vessels, {} recovery_calc, {} recovery_req, {} n_vessel'.format(i, self.recovery_calc, self.recovery_req, self.n_vessel))
            self.Pin_elem[0] = Pfeed
            self.Mfeed_elem[0] = self.Mfeed_tot / self.n_vessel
            for key in self.ions:
                self.Cfeed_elem[key] = self.Cfeed_plant[key]
            self.element[0] = solver_element_definition_Rejectiongiven(self.config, self.ions, self.pos, Pfeed, self.Mfeed_elem[0],
                                                        self.Cfeed_elem, self.N_nodes, self.ndiscr_L, self.n_elem, self.n_vessel, self.R_ion_given)
            self.element[0].resolution_NFelem_activitycoeff_Rejectiongiven(Pfeed, self.Cfeed_elem, self.Mfeed_elem[0])
            self.Mconc_elem[0] = self.element[0].M_conc_out_elem
            self.Mp_elem[0] = self.element[0].M_p_out_elem
            self.Fw_average_elem[0] = np.sum(self.element[0].Fw) / self.ndiscr_L
            self.recovery[0] = self.Mp_elem[0] / self.Mfeed_elem[0]
            for key in self.ions:
                self.Cconc_elem[key][0] = self.element[0].C_conc_out_elem[key]
                self.Cp_elem[key][0] = self.element[0].C_p_out_elem[key]
                self.R_ion[key][0] = self.element[0].R_ion[key]
            for i_elem in range(1, self.n_elem):
                self.recovery[i_elem] = self.recovery[0]
                self.Mfeed_elem[i_elem] = self.Mconc_elem[i_elem-1]
                self.Mp_elem[i_elem] = self.recovery[i_elem] * self.Mfeed_elem[i_elem]
                self.Mconc_elem[i_elem] = self.Mfeed_elem[i_elem] - self.Mp_elem[i_elem]

            self.Mconc_out_plant = self.Mconc_elem[self.n_elem - 1] * self.n_vessel
            for key in self.ions:
                self.Cconc_out_plant[key] = self.Cconc_elem[key][0] # self.Cconc_elem[key][self.n_elem - 1]
            self.Mp_out_plant = np.sum(self.Mp_elem) * self.n_vessel
            for key in self.ions:
                self.Cp_out_plant[key] = self.Cp_elem[key][0] # self.MpCp[key] / np.sum(self.Mp_elem)   # Cp_out_plant = Cp_out del singolo vessel
                self.R_ion_plant[key] = 1 - (self.Cp_out_plant[key] / self.Cfeed_plant[key])
            self.recovery_calc = self.Mp_out_plant / self.Mfeed_tot
            print('end of vessel loop: recovery calc', self.recovery_calc)
            self.n_vessel = int(self.n_vessel * self.recovery_req / self.recovery_calc) + 1
            print('end of vessel loop: n vessel', self.n_vessel)
            i += 1
            if i > 50:
                print('too many iteration')
                break


class NFplant_solver_Rejectiongiven:
    def __init__(self, R_ion_given, parameter_config, pos, Cfeed_plant, Pfeed, recovery_req, Mfeed_plant):
        self.R_ion_given = R_ion_given
        self.config = parameter_config
        self.pos = pos
        self.Cfeed_plant = Cfeed_plant
        self.Pfeed = Pfeed
        self.recovery_req = recovery_req
        self.Mfeed_plant = Mfeed_plant
        self.solver_PV = 0

    def resolution_function(self, corr_factor_list):
        print(corr_factor_list)
        corr_factor_dict = {}
        for key, value in self.pos.items():
            corr_factor_dict[key] = corr_factor_list[value]
        ions = {}
        for key in self.pos:
            ions[key] = 0
        Ncomp = len(ions)
        N_nodes = self.config['N_nodes']
        n_discr_L = self.config['N_discr_L']
        n_elem = self.config['n_elements']
        corr_factor_rej_add = {}
        R_ion_given_corr = {}
        for key in ions:
            corr_factor_rej_add[key] = corr_factor_dict[key] * self.R_ion_given[key]
        R_ion_given_sum = 0
        for key in ions:
            R_ion_given_sum += self.R_ion_given[key]
        for key in self.R_ion_given.keys():
            R_ion_given_corr[key] = corr_factor_rej_add[key] + self.R_ion_given[key]
            if R_ion_given_corr[key] >= 1:
                R_ion_given_corr[key] = 0.98
        Mfeed_elem = np.zeros(n_elem)
        Mconc_elem = np.zeros(n_elem)
        Mp_elem = np.zeros(n_elem)
        Pin_elem = np.zeros(n_elem)
        Cp_elem = {}
        Cconc_elem = {}
        Cfeed_elem = {}
        R_ion = {}
        for key in self.pos:
            Cp_elem[key] = np.zeros(n_elem)
            Cconc_elem[key] = np.zeros(n_elem)
            R_ion[key] = np.zeros(n_elem)
        Mp_out_plant = self.Mfeed_plant * self.recovery_req
        Mconc_out_plant = self.Mfeed_plant - Mp_out_plant
        Cp_out_plant = {}
        Cconc_out_plant = {}
        R_ion_plant = {}
        for key in self.pos:
            Cconc_out_plant[key] = self.Cfeed_plant[key]
            Cp_out_plant[key] = 0
            R_ion_plant[key] = 0
        eta_solvent = self.config['eta_solvent'] * 1e-3
        n_vessel = estimation_n_vessel(self.config, self.Pfeed * 0.5, eta_solvent, n_elem)
        self.solver_PV = NFSolverPressureVessel_Rejectiongiven(n_elem, self.config, ions, self.pos, Mfeed_elem,
                                                                    Mconc_elem,
                                                                    Mp_elem, Cp_elem, Cconc_elem, Cfeed_elem,
                                                                    R_ion, Pin_elem, n_discr_L, Mconc_out_plant,
                                                                    Mp_out_plant, Cp_out_plant,
                                                                    Cconc_out_plant, R_ion_plant, N_nodes, n_vessel,
                                                                    self.recovery_req, self.Mfeed_plant,
                                                                    self.Cfeed_plant, R_ion_given_corr)

        self.solver_PV.resolution_pressurevessel_approxfirstelem_recoveryreq_Rejectiongiven(self.Pfeed)
        self.solver_PV.resolution_pressurevessel_Rejectiongiven(self.Pfeed)
        R_ion_plant_sum = 0
        for key in ions:
            R_ion_plant_sum += self.solver_PV.R_ion_plant[key]
        print(corr_factor_rej_add, self.solver_PV.R_ion_plant)
        # for ions in self.R_ion_given.keys():
        #     self.R_ion_given[ions] = corr_factor_list * self.R_ion_given[ions]
        #     if self.R_ion_given[ions] >= 1:
        #         self.R_ion_given[ions] = 0.98
        # solver = NF_pressurevessel_solver_definition(self.config, self.pos, self.Cfeed_plant, self.Pfeed, self.recovery_req,
        #                                              self.Mfeed_plant, self.R_ion_given)
        # solver.resolution_pressurevessel_approxfirstelem_recoveryreq_Rejectiongiven(self.Pfeed)
        # print(solver.R_ion_plant)
        error_rej = {}
        error_tot = 0
        for key in ions:
            error_rej[key] = (self.R_ion_given[key] - self.solver_PV.R_ion_plant[key])**2
            error_tot += error_rej[key]
        error_tot = error_tot ** (1/2)
        # error_tot = abs(R_ion_given_sum - R_ion_plant_sum)
        return error_tot


