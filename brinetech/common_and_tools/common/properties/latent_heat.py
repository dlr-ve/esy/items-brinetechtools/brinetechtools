def latent_heat(t):  # T is the saturation temperature in [°C] and lambda_vap in [kJ/kg]
    """Calculates latent heat of vaporization of NaCl water (confirm this)"""
    #TODO: Confirm if the formula is for NaCl water or water in general
    #TODO: Reference for the formula?
    lambda_vap = 2501.897149 - 2.4070640037 * t + 1.192212 * 1e-3 * t**2 - 1.5863 * 1e-5 * t**3
    return lambda_vap