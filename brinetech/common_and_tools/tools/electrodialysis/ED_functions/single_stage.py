import numpy as np

from . import eqns_transport_perm_constants as eqns
import numpy as np

'''function for the design of the ED single stage'''


def calculate_net_flux(Sconc_gm_per_kg, Sdial_gm_per_kg, Dconc_kgm3, Ddial_kgm3, current_density, tech_config_and_params):
    """
    Calculates the net flux of salt and water through a membrane based on concentration, current density, 
    and system parameters.
    
    :param Sconc_gm_per_kg: Concentration of concentrate [g/kg]
    :param Sdial_gm_per_kg: Concentration of diluate [g/kg]
    :param Dconc_kgm3: Density of concentrate [kg/m³]
    :param Ddial_kgm3: Density of diluate [kg/m³]
    :param current_density: Current density [A/m²]
    :param tech_config_and_params: Dictionary containing technical configuration and parameters
    :return: Jsalt_mol_per_m2_s: Salt flux [mol/m².s]
    :return: Jwater_mol_per_m2_s: Water flux [mol/m².s]
    """

    # Transport numbers and permeability of salt and water
    salt_transport_number, water_transport_number, Psalt_mol_per_m2_s_bar, Pwater_mol_per_m2_s_bar = eqns.calculate_transport_permeability(Sconc_gm_per_kg, Sdial_gm_per_kg)

    # Bulk concentration in concentrate and diluate
    Cconc_bulk_molm3 = Sconc_gm_per_kg * Dconc_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m³]
    Cdial_bulk_molm3 = Sdial_gm_per_kg * Ddial_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m³]

    # Membrane concentration in concentrate and diluate
    t_dash_cu = (salt_transport_number + 1) / 2
    delta_conc = (t_dash_cu - tech_config_and_params['t_cu']) * current_density * 2 * tech_config_and_params['h'] / (
            tech_config_and_params['D'] * tech_config_and_params['F'] * tech_config_and_params['sh'])  # [mol/m³]

    Cconc_mol_per_m3 = Cconc_bulk_molm3 + delta_conc  # [mol/m³]
    Cdial_mol_per_m3 = Cdial_bulk_molm3 - delta_conc  # [mol/m³]

    # Molarity [mol/L] and molality [mol/kg]
    Cconc_mol_per_L = Cconc_mol_per_m3 / 1000
    Cdial_mol_per_L = Cdial_mol_per_m3 / 1000

    Cconc_mol_per_kg = Cconc_mol_per_m3 / Dconc_kgm3
    Cdial_mol_per_kg = Cdial_mol_per_m3 / Ddial_kgm3

    # Osmotic coefficient of NaCl
    osmotic_coeff_conc = eqns.calculate_osmotic_coefficient_nacl(Cconc_mol_per_L)
    osmotic_coeff_dial = eqns.calculate_osmotic_coefficient_nacl(Cdial_mol_per_L)

    # Partial molar volume of water in NaCl solution
    Qpart_water_conc_m3_per_mol = eqns.calculate_partial_molar_volume_water(Cconc_mol_per_kg)  # [m³/mol]
    Qpart_water_dial_m3_per_mol = eqns.calculate_partial_molar_volume_water(Cdial_mol_per_kg)  # [m³/mol]

    # Molar fraction of water in NaCl solution
    num_mol_water_conc_molm3 = (10 ** 6 / tech_config_and_params['v_molar_water']) - Cconc_mol_per_m3  # [mol/m³]
    num_mol_water_dial_molm3 = (10 ** 6 / tech_config_and_params['v_molar_water']) - Cdial_mol_per_m3  # [mol/m³]

    mol_frac_water_conc = num_mol_water_conc_molm3 / (num_mol_water_conc_molm3 + Cconc_mol_per_m3)  # [-]
    mol_frac_water_dial = num_mol_water_dial_molm3 / (num_mol_water_dial_molm3 + Cdial_mol_per_m3)  # [-]

    # Osmotic pressure
    pi_conc_bar = (-tech_config_and_params['R'] * tech_config_and_params['T'] / Qpart_water_conc_m3_per_mol *
                   osmotic_coeff_conc * np.log(mol_frac_water_conc)) / 10 ** 5  # [bar]
    pi_dial_bar = (-tech_config_and_params['R'] * tech_config_and_params['T'] / Qpart_water_dial_m3_per_mol *
                   osmotic_coeff_dial * np.log(mol_frac_water_dial)) / 10 ** 5  # [bar]

    # Salt and water flux
    Jsalt_mol_per_m2_s = salt_transport_number * current_density / tech_config_and_params['F'] - Psalt_mol_per_m2_s_bar * (Cconc_mol_per_m3 - Cdial_mol_per_m3)  # [mol/m².s]
    Jwater_mol_per_m2_s = water_transport_number * current_density / tech_config_and_params['F'] - Pwater_mol_per_m2_s_bar * (pi_conc_bar - pi_dial_bar)  # [mol/m².s]

    return Jsalt_mol_per_m2_s, Jwater_mol_per_m2_s



# calculate_net_flux(120, 35, 1088.33, 1023.03, 300, 'parameters.yml')

def calculate_cell_pair_voltage(Sconc_gm_per_kg, Sdial_gm_per_kg, Dconc_kgm3, Ddial_kgm3, current_density, num_of_cell_pairs, tech_config_and_params):
    """
    Calculates the cell-pair voltage (v_cp) of a single cell pair.
    
    :param Sconc_gm_per_kg: Concentration of concentrate [g/kg]
    :param Sdial_gm_per_kg: Concentration of diluate [g/kg]
    :param Dconc_kg_per_m3: Density of concentrate [kg/m³]
    :param Ddial_kg_per_m3: Density of diluate [kg/m³]
    :param current_density: Current density [A/m²]
    :param num_of_cell_pairs: Number of cell-pairs [-]
    :param tech_config_and_params: Dictionary containing technical configuration and parameters
    :return: v_cp: Cell-pair voltage [V]
    """

    # 1. calc. related parameters for E_am + E_cm calc.
    # 1.1 transport numbers and permeability of salt and water
    salt_transport_number, water_transport_number, Psalt_molm2sbar, Pwater_molm2sbar = eqns.calculate_transport_permeability(Sconc_gm_per_kg, Sdial_gm_per_kg)
    print('salt_transport_number, water_transport_number, Psalt_molm2sbar, Pwater_molm2sbar', salt_transport_number, water_transport_number, Psalt_molm2sbar, Pwater_molm2sbar)

    # 1.2 chemical potential of NaCl and water
    # bulk concentration in concentrate and diluate
    Cconc_bulk_molm3 = Sconc_gm_per_kg * Dconc_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m3]
    Cdial_bulk_molm3 = Sdial_gm_per_kg * Ddial_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m3]
    print('Cdial_bulk_molm3, Cconc_bulk_molm3', Cdial_bulk_molm3, Cconc_bulk_molm3)

    # membrane concentration in concentrate and diluate
    t_dash_cu_j = (salt_transport_number + 1) / 2
    delta_conc_j = ((t_dash_cu_j - tech_config_and_params['t_cu']) * current_density * 2 * tech_config_and_params['h'] / (
            tech_config_and_params['D'] * tech_config_and_params['F'] * tech_config_and_params['sh']))  # mol/m3
    
    Cconc_molm3 = Cconc_bulk_molm3 + delta_conc_j  # [mol/m3]
    Cdial_molm3 = Cdial_bulk_molm3 - delta_conc_j  # [mol/m3]

    # molarity [mol/L] and molality [mol/kg]
    Cconc_bulk_molL = Cconc_bulk_molm3 / 1000
    Cdial_bulk_molL = Cdial_bulk_molm3 / 1000

    Cconc_molkg = Cconc_molm3 / Dconc_kgm3   # assuming density of bulk soln (slightly inaccurate)
    Cdial_molkg = Cdial_molm3 / Ddial_kgm3   # assuming density of bulk soln (slightly inaccurate)

    Cconc_bulk_molkg = Cconc_bulk_molm3 / Dconc_kgm3
    Cdial_bulk_molkg = Cdial_bulk_molm3 / Ddial_kgm3
    

    mu_nacl_conc_m_Jmol, mu_water_c_m_j = eqns.calculate_chemical_potential_nacl_water(Cconc_molkg)  # [J/mol]
    mu_nacl_dial_m_Jmol, mu_water_d_m_j = eqns.calculate_chemical_potential_nacl_water(Cdial_molkg)  # [J/mol]
    # Electrochemical potential across membranes
    e_am_plus_e_cm = salt_transport_number / tech_config_and_params['F'] * (mu_nacl_conc_m_Jmol - mu_nacl_dial_m_Jmol) + water_transport_number / tech_config_and_params['F'] * (
            mu_water_c_m_j - mu_water_d_m_j)
    print('e_am_plus_e_cm', e_am_plus_e_cm)

    # equivalent conductances or molar conductivity of NaCL in bulk solution
    equi_cond_nacl_conc_bulk_Sm2mol = eqns.calculate_equivalent_conductivity_nacl(Cconc_bulk_molL)  # [S.m2/mol]
    equi_cond_nacl_dial_bulk_Sm2mol = eqns.calculate_equivalent_conductivity_nacl(Cdial_bulk_molL)  # [S.m2/mol]
    print('equi_cond_nacl_conc_bulk_Sm2mol, equi_cond_nacl_dial_bulk_Sm2mol', equi_cond_nacl_conc_bulk_Sm2mol, equi_cond_nacl_dial_bulk_Sm2mol)
    
    
    # 2. Calculate v_cp (cell potential voltage)
    v_cp_first_part = (tech_config_and_params['r_dash_am'] + tech_config_and_params['r_dash_cm'] +
                       (tech_config_and_params['h'] / 
                        (tech_config_and_params['sigma'] * equi_cond_nacl_dial_bulk_Sm2mol * Cdial_bulk_molm3)) +
                       (tech_config_and_params['h'] / 
                        (tech_config_and_params['sigma'] * equi_cond_nacl_conc_bulk_Sm2mol * Cconc_bulk_molm3))) * current_density
    
    v_cp_second_part = tech_config_and_params['r_dash_cm'] * current_density / num_of_cell_pairs

    # Total cell potential
    v_cp = v_cp_first_part + v_cp_second_part + e_am_plus_e_cm

    return v_cp
