import numpy as np

from .medmodel import h2oprop, economics_function, tools, design_equalAhx
from .medmodel.logger import logger
from .medmodel.design_functions import designReg
from .medmodel.solver_function import solverReg

from ...common.properties import density_seawater_liquid
from ...common.economics import calculate_annual_depreciation_cost
from ...common.iotools import ResultList

def multi_effect_distillation_model(program_config, parameter_config, N, Mfeed, Xsw, Xb_out, Ts_design, electricity_cost, heat_cost):
    logger.info('using desing method: "' + program_config['DesignMethodToUse'] + '"')
    if parameter_config['TVC mode'] == 'On':
        TVC_on = bool(True)
        logger.info('TVC on')
    else:
        TVC_on = bool(False)
        logger.info('TVC off')

    solver, Mms_firstguess, DeltaT_phfirst, DeltaT_first = solverReg[program_config['DesignMethodToUse']](
        parameter_config, program_config, N, Mfeed, Xsw, Xb_out, Ts_design)
    if TVC_on:
        Ms_firstguess = Mms_firstguess * (1 + 1 / solver.ER)
    else:
        Ms_firstguess = Mms_firstguess
    parameter_config['runtime'] = {}
    parameter_config['runtime']['y0'] = Ms_firstguess
    parameter_config['runtime']['x0'] = DeltaT_first
    parameter_config['runtime']['z0'] = DeltaT_phfirst

    res = designReg[program_config['DesignMethodToUse']](parameter_config, solver)
    Mcond = solver.Mvap[N - 1] - solver.Mss
    Mcw = design_equalAhx.coolingwater_flowrate(Mcond, solver.Tc_prime[N - 1], solver.Xf,
                                                solver.Tcw_out, parameter_config['Tsw_design'])
    A_cond = design_equalAhx.area_condenser(Mcw, parameter_config['Tsw_design'], solver.Tcw_out,
                                            solver.Tc_prime[N - 1], Xsw)
    Atot = np.sum(solver.A_hx) + np.sum(solver.A_preh) + A_cond
    '''performance parameters'''
    Spec_area = Atot / solver.Mdist  # [m2 s/kg]
    CF = Xb_out / Xsw  # [-]
    RR = solver.Mdist / solver.Mfeed  # [-]
    Tm = h2oprop.sat_temp(parameter_config['Pm_design'] * 100)
    if TVC_on:
        Spec_heat_2 = solver.Mms * h2oprop.latent_heat(Tm) / solver.Mdist
        GOR = solver.Mdist / solver.Mms  # [-]
    else:
        Spec_heat_2 = solver.Ms * h2oprop.latent_heat(solver.Ts) / solver.Mdist
        GOR = solver.Mdist / solver.Ms  # [-]

    A_evaporator = solver.A_hx
    A_condenser = A_cond
    A_preheater = solver.A_preh
    Mfeed = solver.Mfeed
    Mdist = solver.Mdist
    Mbrine = Mfeed - Mdist
    A_tot = np.sum(A_evaporator) + np.sum(A_preheater) + A_condenser
    Xbrine = Xb_out
    Ms = solver.Ms
    Mms = solver.Mms
    Mcw = Mcw
    brine_density = density_seawater_liquid(25, Xbrine)
    Pm = parameter_config['Pm_design']
    Ts = Ts_design
    # heat_cost = 3.1e-2 #  [US$/MWh]10

    evap_price_gebel, plant_price_gebel = economics_function.capital_costs_gebel('CuNi90_10', A_tot, parameter_config)
    spec_price_gebel = plant_price_gebel/(Mbrine * 3600 * 24 / brine_density)

    Cp_0_single_evap = np.zeros(N)
    Cp_0_single_preh = np.zeros(N - 1)
    Cp_0_single_flashbox = np.zeros(N - 1)
    Cbm_single_evap = np.zeros(N)
    Cbm_single_preh = np.zeros(N - 1)
    Cbm_single_flashbox = np.zeros(N - 1)

    for i_effect, Aevap in enumerate(A_evaporator):
        Cp_0_single_evap[i_effect], Cbm_single_evap[i_effect] = economics_function.bare_module_cost_general_evap_cond(1, parameter_config,
                                                                                    Aevap, 'heat_exchanger', parameter_config['material_evap'])
    Cbm_tot_evap = np.sum(Cbm_single_evap)
    for i_effect, Aph in enumerate(A_preheater):
        Cp_0_single_preh[i_effect], Cbm_single_preh[i_effect] = economics_function.bare_module_cost_general_evap_cond(1, parameter_config,
                                                                                    Aph, 'heat_exchanger', parameter_config['material_preh'])
    Cbm_tot_preh = np.sum(Cbm_single_preh)
    Cp_0_endcondenser, Cbm_endcondenser = economics_function.bare_module_cost_general_evap_cond(1, parameter_config,
                                                                                    A_condenser, 'heat_exchanger', parameter_config['material_endcond'])
    if TVC_on:
        Cbm_compressor = 60 * Mdist * 3600*24/1000
    else:
        Cbm_compressor = 0
    for i_effect in range(1, N):
        Cp_0_single_flashbox[i_effect-1], Cbm_single_flashbox[i_effect-1] = economics_function.bare_module_cost_equipment(1, parameter_config, parameter_config['V_vessel'],
                                                                            'vessel', parameter_config['material_vessel'])
    Cbm_tot_flashbox = np.sum(Cbm_single_flashbox)
    Cbm_tot = np.sum(Cbm_single_flashbox) + np.sum(Cbm_single_evap) + np.sum(Cbm_single_preh) + Cbm_endcondenser
    Ctm_turton = Cbm_tot * (1 + 0.15 + 0.05)
    Cgr_turton = Ctm_turton + Cbm_tot * 0.5
    specific_price_turton = Ctm_turton/(Mbrine * 3600 * 24 / brine_density)

    spec_annual_capcost_gebel = calculate_annual_depreciation_cost(parameter_config['interest_rate'], parameter_config['depr_period'], plant_price_gebel)
    spec_annual_capcost_turton = calculate_annual_depreciation_cost(parameter_config['interest_rate'], parameter_config['depr_period'], Ctm_turton)
    spec_volume_capcost_gebel = spec_annual_capcost_gebel / (Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    spec_volume_capcost_turton = spec_annual_capcost_turton / (Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    spec_dist_volume_capcost_t = spec_annual_capcost_turton / (Mdist * 3600 / 1000 * 24 * 365 * parameter_config['plant_availability'])

    '''operating costs'''
    # depreciation = economics_function.depreciation_calculation(parameter_config['interest_rate'], parameter_config['depr_period'], Ctm_turton)
    #personnel_costs_annual = parameter_config['staff'] * parameter_config['av_personnel_cost_year'] / parameter_config['Mfeed_ref_pers'] * parameter_config['Mfeed_design']  # Marina's. Seems to be a mistake to take Mfeed_design as it is given externally
    personnel_costs_annual = parameter_config['staff'] * parameter_config['av_personnel_cost_year'] / parameter_config[
        'Mfeed_ref_pers'] * Mfeed

    personnel_costs_volume = parameter_config['staff'] * parameter_config['av_personnel_cost_year'] / (Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    personnel_cost_dist_volume = parameter_config['staff'] * parameter_config['av_personnel_cost_year'] / (parameter_config['annual_hours']
                                                * parameter_config['plant_availability'] * Mdist * 3600/1000)
    manteinance_costs_annual_turton = economics_function.manteinance_costs(parameter_config, Ctm_turton, personnel_costs_annual)
    manteinance_costs_volume_turton = manteinance_costs_annual_turton/(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    manteinance_cost_dist_volume_t = manteinance_costs_annual_turton/(Mdist * 3600 / 1000 * 24 * 365 * parameter_config['plant_availability'])
    manteinance_costs_annual_gebel = economics_function.manteinance_costs(parameter_config, plant_price_gebel, personnel_costs_annual)
    manteinance_costs_volume_gebel = manteinance_costs_annual_gebel/(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    Pel = parameter_config['Pel_specific'] / parameter_config['efficiency_pump']
    annual_electric_cost, spec_volume_dist_electric_cost = economics_function.specific_electric_cost(parameter_config, Pel, Mdist, electricity_cost)
    spec_volume_electric_cost = annual_electric_cost /(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    # annual_heat_cost, spec_volume_dist_heat_cost = economics_function.specific_heat_cost(parameter_config, Ms, Pm, Tm, Mdist)
    if TVC_on:
        annual_heat_cost, spec_volume_dist_heat_cost = economics_function.specific_heat_cost_input_heatcost(parameter_config, Mms, Pm, Tm, Mdist, heat_cost)
    else:
        annual_heat_cost, spec_volume_dist_heat_cost = economics_function.specific_heat_cost_input_heatcost(parameter_config, Ms, Pm, Ts, Mdist, heat_cost)
    spec_volume_heat_cost = annual_heat_cost /(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    annual_chem_cost, spec_volume_dist_chemical_cost = economics_function.specific_chem_cost(parameter_config, Mcw, Mfeed, Mdist)
    spec_volume_chemical_cost = annual_chem_cost /(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    spec_annual_operating_gebel = personnel_costs_annual + manteinance_costs_annual_gebel + annual_electric_cost + annual_heat_cost + annual_chem_cost
    spec_annual_operating_turton = personnel_costs_annual + manteinance_costs_annual_turton + annual_electric_cost + annual_heat_cost + annual_chem_cost
    annual_revenue_purewater = parameter_config['specific_cost_water'] / 1000 * Mdist * 3600 * parameter_config['annual_hours']* parameter_config['plant_availability']
    spec_volume_revenue_purewater = annual_revenue_purewater /(Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability'])
    annual_Mbrine = Mbrine * 3600 / brine_density * 24 * 365 * parameter_config['plant_availability']
    annual_Mdist = Mdist * 3600 / 1000 * 24 * 365 * parameter_config['plant_availability']
    LBC_fixed_t = spec_volume_capcost_turton + personnel_costs_volume + manteinance_costs_volume_turton
    LBC_variable = spec_volume_electric_cost + spec_volume_heat_cost + spec_volume_chemical_cost - spec_volume_revenue_purewater
    LBC_tot_t = LBC_fixed_t + LBC_variable
    LWC_fixed_t = spec_dist_volume_capcost_t + personnel_cost_dist_volume + manteinance_cost_dist_volume_t
    LWC_variable = spec_volume_dist_electric_cost + spec_volume_dist_heat_cost + spec_volume_dist_chemical_cost
    LWC_tot_t = LWC_fixed_t + LWC_variable

    med_inputs = ResultList(title="MED simulation", xls_sheet="multi_effect_distillation", results=[
        ('input and parameters', ('', '')),
        ('Mfeed', (Mfeed, 'kg/s')),
        ('Xfeed', (Xsw, 'ppm')),
        ('Xbrine', (Xb_out, 'ppm')),
        ('Ts', (Ts_design, '°C')),
        ('Tn', (parameter_config['Tn_design'], '°C')),
        ('N', (N, '-')),
        ('Pm', (parameter_config['Pm_design'], 'bar')),
        ('Tn', (parameter_config['Tn_design'], '°C')),
        ('electricity cost', (electricity_cost, '$/kWh_el')),
        ('heat cost', (heat_cost, '$/kWh_th'))
        ])

    med_results = ResultList(title="MED technical results", xls_sheet="multi_effect_distillation", xls_y0=4, results=[
        ('Mbrine', (solver.Mbrine, 'kg/s')),
        ('Mdist', (solver.Mdist, 'kg/s')),
        ('Specific area', (Spec_area, 'm2 s/kg')),
        ('CF', (CF, '-')),
        ('RR', (RR, '-')),
        ('specific heat demand', (Spec_heat_2[0], 'kJ/kg')),
        ('GOR', (GOR[0], '-')),
        ('Area heat exchanger', (solver.A_hx[0], 'm2')),
        ('Area preheater', (solver.A_preh[0], 'm2')),
        ('Area condenser', (A_cond, 'm2')),
        ('Mcw', (Mcw, 'kg/s')),
        ('Ms', (solver.Ms[0], 'kg/s')),
        ('Mms', (solver.Mms[0], 'kg/s')),
        ('electric demand', (Pel * solver.Mdist * 3600 / 1000, 'kWh_el')),
        ('thermal demand', (Spec_heat_2[0] * solver.Mdist, 'kWh_th'))
        ])

    med_economic_results = ResultList(title="MED economic results", xls_sheet="multi_effect_distillation", xls_y0=8, results=[
        ('tot cost evap', (Cbm_tot_evap, '$')),
        ('tot cost preh', (Cbm_tot_preh, '$')),
        ('cost condenser', (Cbm_endcondenser, '$')),
        ('cost TVC', (Cbm_compressor, '$')),
        ('tot cost flashbox', (Cbm_tot_flashbox, '$')),
        ('tot annual fixed cost', (spec_annual_capcost_turton, '$/y')),
        ('annual personnel', (personnel_costs_annual, '$/y')),
        ('annual maintenance', (manteinance_costs_annual_turton, '$/y')),
        ('annual electric', (annual_electric_cost, '$/y')),
        ('annual heat', (annual_heat_cost[0], '$/y')),
        ('annual chemicals', (annual_chem_cost, '$/y')),
        ('tot annual operating cost', (spec_annual_operating_turton[0], '$/y')),
        ('tot annual revenue water', (annual_revenue_purewater, '$/y')),
        ('LBC fixed', (LBC_fixed_t, '$/m3')),
        ('LBC operating', (LBC_variable[0], '$/m3')),
        ('LBC tot', (LBC_tot_t[0], '$/m3')),
        ('LWC fixed', (LWC_fixed_t, '$/m3')),
        ('LWC operating', (LWC_variable[0], '$/m3')),
        ('LWC tot', (LWC_tot_t[0], '$/m3')),
        ('Mbrine annual', (annual_Mbrine, 'm3/y')),
        ('Mdist annual', (annual_Mdist, 'm3/y'))
        ])

    return [med_inputs, med_results, med_economic_results]

#driver('./program_config.yml', './input_data.yml', './parameters.yml', 6, 4.52, 26209, 90000, 100, 0.1035, 0.01, "main_design_economics_LBC")