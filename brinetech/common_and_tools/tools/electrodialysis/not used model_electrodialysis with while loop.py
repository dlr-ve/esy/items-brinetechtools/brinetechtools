
from .ED_functions import single_stage, economic_function
from .ED_functions import eqns_transport_perm_constants as eqns
from ...common import calculate_annual_depreciation_cost
from ...common import density_seawater_liquid

from ...common.iotools import ResultList


def electrodialysis_model(tech_config_and_params, proj_config_and_params, Sfeed_ppm, Sconc_outlet_ppm, current_density, Mfeed_plant_kgs, dil_to_conc_ratio,
           elec_cost):
    """
    Simulates the electrodialysis (ED) process for brine and diluate production.

    Parameters:
    -----------
    tech_config_and_params : dict
        Technical configuration and parameters (temperature, molecular weights, cell counts, etc.).
    proj_config_and_params : dict
        Project configuration (interest rate, CEPCI values, depreciation periods, etc.).
    Sfeed_ppm : float
        Feed salinity in parts per million (ppm).
    Sconc_outlet_ppm : float
        Desired concentrate outlet salinity in ppm.
    current_density : float
        Current density applied in A/m².
    Mfeed_plant_kgs : float
        Feed mass flow rate in kg/s.
    dil_to_conc_ratio : float
        Diluate to concentrate flow rate ratio.
    elec_cost : float
        Cost of electricity in USD/kWh.

    Returns:
    --------
    tuple
        Returns a tuple containing:
        - Detailed result lists for reporting purposes: [ed_inputs, ed_flow_outputs, ed_technical_outputs, ed_economic_outputs]
    """

    # initially equate diluate outlet salinity to feed salinity (assuming SDR=0, that is, no amount of seawater is sent into diluate channel)
    #Sdial_outlet_gm_per_kg = Sfeed_ppm
    #dil_to_conc_ratio = dil_to_conc_ratio
    # increase DCR to increase the salinity of diluate to 45 g/kg
    #dil_to_conc_ratio = dil_to_conc_ratio * 1.05

    # Initialize loop variables
    Sdial_outlet_gm_per_kg = 0
    tolerance = 0.01  # Tolerance for comparison with target salinity
    target_sdial_outlet_gm_per_kg = 45
    max_iterations = 200  # Limit to prevent infinite loops
    
    iteration = 0

    iteration = 0
    while Sdial_outlet_gm_per_kg < target_sdial_outlet_gm_per_kg and iteration < max_iterations:
        iteration += 1
        
        # splitting the ED feed stream into concentrate inlet and dialate inlet
        Mconc_inlet_kgs = Mfeed_plant_kgs / (dil_to_conc_ratio + 1)  # [kg/s]
        Mdial_inlet_kgs = Mfeed_plant_kgs - Mconc_inlet_kgs  # [kg/s]

        #print('Mconc_inlet_kgs', Mconc_inlet_kgs)
        #print('Mdial_inlet_kgs', Mdial_inlet_kgs)

        ## Salinity and density calculations for concentrate and diluate streams
        # Assuming ppm assumed as mg/L, we need to first calculate density to convert it to g_salt/kg_sol
        # We assume the solution as equivalent to sea water for density calc.
        Dconc_inlet_kgm3 = density_seawater_liquid(tech_config_and_params['T'] - 273, Sfeed_ppm)  # [kg_sol/m3]
        Dconc_outlet_kgm3 = density_seawater_liquid(tech_config_and_params['T'] - 273, Sconc_outlet_ppm)  # [kg_sol/m3]
        Ddial_inlet_kgm3 = Dconc_inlet_kgm3  # [kg_sol/m3] assuming same inlet for concentrate and diluate

        # Volumetric flow rates [m³/s]
        Qfeed_m3s =  Mfeed_plant_kgs / Dconc_inlet_kgm3  # [m3/s]
        Qconc_inlet_m3s =  Mconc_inlet_kgs / Dconc_inlet_kgm3  # [m3/s]
        Qdial_inlet_m3s = Mdial_inlet_kgs / Ddial_inlet_kgm3  # [m3/s]
        Qconc_inlet_m3yr = Qconc_inlet_m3s * 3600 * 24 * 365 * tech_config_and_params['plant_availability']
        Qdial_inlet_m3yr = Qdial_inlet_m3s * 3600 * 24 * 365 * tech_config_and_params['plant_availability']

        # Salinity in g/kg
        Sconc_inlet_gm_per_kg = Sfeed_ppm / Dconc_inlet_kgm3  # [g_salt/kg_sol]
        Sconc_outlet_gm_per_kg = Sconc_outlet_ppm / Dconc_outlet_kgm3  # [g_salt/kg_sol]
        Sdial_inlet_gm_per_kg = Sconc_inlet_gm_per_kg  # [g_salt/kg_sol]

        

        #print('salt flow rate [g_salt/h] in ED inlet', Sconc_inlet_gm_per_kg * Dconc_inlet_kgm3 * (Mfeed_plant_kgs / Dconc_inlet_kgm3 * 3600))

        # Molar flow rates for salt and water [mol/s] at j=1 or inlet for concentrate and diluate
        Nconc_inlet_salt_mols = Mconc_inlet_kgs * Sconc_inlet_gm_per_kg / tech_config_and_params['MW_nacl']   # [mol_salt/s]
        Nconc_inlet_water_mols = Mconc_inlet_kgs * (1000 - Sconc_inlet_gm_per_kg) / tech_config_and_params['MW_water']   # [mol_water/s]
        Ndial_inlet_salt_mols = Mdial_inlet_kgs * Sdial_inlet_gm_per_kg / tech_config_and_params['MW_nacl']   # [mol_salt/s]
        Ndial_inlet_water_mols = Mdial_inlet_kgs * (1000 - Sdial_inlet_gm_per_kg) / tech_config_and_params['MW_water']   # [mol_water/s]

        #### Concentration
        Cconc_inlet_molm3 = Sconc_inlet_gm_per_kg * Dconc_inlet_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m3]
        #Cconc_outlet_molm3 = Sconc_outlet_gm_per_kg * Dconc_outlet_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m3]
        #Cdial_inlet_molm3 = Cconc_inlet_molm3  # [mol/m3]

        # Initialize lists to store results
        results = {
            'Mconc_kgs': [Mconc_inlet_kgs],
            'Mdial_kgs': [Mdial_inlet_kgs],
            'Sconc_gm_per_kg': [Sconc_inlet_gm_per_kg],
            'Dconc_kgm3': [Dconc_inlet_kgm3],
            'Ddial_kgm3': [Ddial_inlet_kgm3],
            'Cconc_molm3': [Cconc_inlet_molm3],
            'Sdial_gm_per_kg': [Sdial_inlet_gm_per_kg],
            'Nconc_salt_mols': [Nconc_inlet_salt_mols],
            'Nconc_water_mols': [Nconc_inlet_water_mols],
            'Ndial_salt_mols': [Ndial_inlet_salt_mols],
            'Ndial_water_mols': [Ndial_inlet_water_mols],
            'area_cell_pair_m2': [],
        }

        ## calculating salinities for all the comp. cells on cencentrate side
        Sconc_gm_per_kg_step = (Sconc_outlet_gm_per_kg - Sconc_inlet_gm_per_kg) / (tech_config_and_params['N_comp_cells'] - 1)  # [g_salt/kg_sol]
        Sconc_gm_per_kg_current = Sconc_inlet_gm_per_kg
        Mconc_kgs_current = Mconc_inlet_kgs

        for k in range(0, tech_config_and_params['N_comp_cells'] - 1):
            Sconc_gm_per_kg_next = Sconc_gm_per_kg_current + Sconc_gm_per_kg_step
            Mconc_kgs_next = Mconc_kgs_current + (Mconc_kgs_current * Sconc_gm_per_kg_step / 1000)  # [kg_sol/s] this value is
            # inaccurate as it does not account for water flux getting transferred to concentrate channel from diluate
            # we will approx. account for it in next loop by considering the molar water flow rate in the j+1 element (which
            # is also not very accurate since this does not consider accurate m_dot

            # Assuming ppm as mg/kg, calculate density
            Dconc_kgm3_next = density_seawater_liquid(tech_config_and_params['T']-273, Sconc_gm_per_kg_next * 1000)  # [kg_sol/m3]
            Cconc_molm3_next = Sconc_gm_per_kg_next * Dconc_kgm3_next / tech_config_and_params['MW_nacl']  # [mol/m3]
            Nconc_salt_mols_next = Mconc_kgs_next * Sconc_gm_per_kg_next / tech_config_and_params['MW_nacl']

            # Update results
            results['Mconc_kgs'].append(Mconc_kgs_next)
            results['Sconc_gm_per_kg'].append(Sconc_gm_per_kg_next)
            results['Dconc_kgm3'].append(Dconc_kgm3_next)
            results['Cconc_molm3'].append(Cconc_molm3_next)
            results['Nconc_salt_mols'].append(Nconc_salt_mols_next)

            # set 'j+1' values to 'j'
            Sconc_gm_per_kg_current = Sconc_gm_per_kg_next
            Mconc_kgs_current = Mconc_kgs_next
        #print("results['Sconc_gm_per_kg']", len(results['Sconc_gm_per_kg']), results['Sconc_gm_per_kg'])

        ## Conc. channel outlet (brine) properties
        # annual brine production (this is inaccurate as it does neglects water flux from diluate to concentrate)
        Mconc_outlet_kgs = results['Mconc_kgs'][-1]  # [kg_sol/s]
        Dconc_outlet_kgm3 = results['Dconc_kgm3'][-1]  # [kg_sol/m3_sol]
        Qconc_outlet_m3s = Mconc_outlet_kgs / Dconc_outlet_kgm3  # [m3_sol/s]
        #print('Qconc_outlet_m3s m3/s', Qconc_outlet_m3s)
        Qconc_outlet_m3yr = Qconc_outlet_m3s * 3600 * 24 * 365 * tech_config_and_params['plant_availability']
        #print('annual brine prod [m3/yr]:', Qconc_outlet_m3yr)


        results['Mconc_kgs'].append(Mconc_kgs_next)
        results['Sconc_gm_per_kg'].append(Sconc_gm_per_kg_next)
        results['Dconc_kgm3'].append(Dconc_kgm3_next)

        ## technical design of ED: determination of cell flux, cell pair area and numbers, voltage and energy consumption
        Mdial_kgs_current = Mdial_inlet_kgs
        Sdial_gm_per_kg_current = Sdial_inlet_gm_per_kg
        Ddial_kgm3_current = Ddial_inlet_kgm3
        Nconc_water_mols_current = Nconc_inlet_water_mols
        Ndial_salt_mols_current = Ndial_inlet_salt_mols
        Ndial_water_mols_current = Ndial_inlet_water_mols
        #list_j_s_j = []
        #list_j_w_j = []

        for j in range(0, tech_config_and_params['N_comp_cells'] - 1):
            Jsalt_molm2s_current, Jwater_molm2s_current = single_stage.calculate_net_flux(results['Sconc_gm_per_kg'][j], Sdial_gm_per_kg_current, results['Dconc_kgm3'][j], Ddial_kgm3_current, current_density,
                                                    tech_config_and_params)
            #list_j_s_j.append(Jsalt_molm2s_current)
            #list_j_w_j.append(Jwater_molm2s_current)

            Nconc_salt_mols_current = results['Nconc_salt_mols'][j]
            Nconc_salt_mols_next = results['Nconc_salt_mols'][j + 1]
            area_cell_pair_m2_current = (Nconc_salt_mols_next - Nconc_salt_mols_current) / Jsalt_molm2s_current
            Nconc_water_mols_next = Nconc_water_mols_current + (area_cell_pair_m2_current * Jwater_molm2s_current)

            results['area_cell_pair_m2'].append(area_cell_pair_m2_current)
            results['Nconc_water_mols'].append(Nconc_water_mols_next)

            # calculate diluate water and salt flow rates for 'j+1'
            Ndial_salt_mols_next = Ndial_salt_mols_current - (area_cell_pair_m2_current * Jsalt_molm2s_current)
            Ndial_water_mols_next = Ndial_water_mols_current - (area_cell_pair_m2_current * Jwater_molm2s_current)
            results['Ndial_salt_mols'].append(Ndial_salt_mols_next)
            results['Ndial_water_mols'].append(Ndial_water_mols_next)

            # calculate salinity and density of diluate for 'j+1'
            # adjust mass flow rate to account for salt flux (effect of water flux is neglected, just as for concentrate)
            Mdial_kgs_next = Mdial_kgs_current - (area_cell_pair_m2_current * Jsalt_molm2s_current * tech_config_and_params['MW_nacl'] / 1000)
            Sdial_gm_per_kg_next = Ndial_salt_mols_next * tech_config_and_params['MW_nacl'] / Mdial_kgs_next
            Ddial_kgm3_next = density_seawater_liquid(tech_config_and_params['T']-273, Sdial_gm_per_kg_next * 1000)  # [kg_sol/m3]

            results['Mdial_kgs'].append(Mdial_kgs_next)
            results['Sdial_gm_per_kg'].append(Sdial_gm_per_kg_next)
            results['Ddial_kgm3'].append(Ddial_kgm3_next)

            # change diluate values for next loop (j+1 becomes j)
            Mdial_kgs_current = Mdial_kgs_next
            Ddial_kgm3_current = Ddial_kgm3_next
            Sdial_gm_per_kg_current = Sdial_gm_per_kg_next
            Ndial_salt_mols_current = Ndial_salt_mols_next
            Ndial_water_mols_current = Ndial_water_mols_next
        #print('list_j_w_j:', list_j_w_j)
        ## Diluate channel outlet properties
        
        # annual diluate production (this is inaccurate as it does neglects water flux from diluate to concentrate)
        Mdial_outlet_kgs = results['Mdial_kgs'][-1]  # [kg_sol/s]
        Ddial_outlet_kgm3 = results['Ddial_kgm3'][-1]  # [kg_sol/m3_sol]
        
        Sdial_outlet_gm_per_kg = results['Sdial_gm_per_kg'][-1]  # [g_salt/kg_sol]
        # Break loop if Sdial reaches target
        if Sdial_outlet_gm_per_kg >= target_sdial_outlet_gm_per_kg:
            break

        # Increase dil_to_conc_ratio by 1%
        dil_to_conc_ratio *= 1.01


        
    Qdial_outlet_m3s = Mdial_outlet_kgs / Ddial_outlet_kgm3  # [m3_sol/s]
    Qdial_outlet_m3yr = Qdial_outlet_m3s * 3600 * 24 * 365 * tech_config_and_params['plant_availability']
    print('annual diluate prod [m3/yr]:', Qdial_outlet_m3yr)

    ## salt mass flow rates [kh/h]
    # in concentrate
    Msalt_in_conc_inlet_kgh = Sconc_inlet_gm_per_kg * Dconc_inlet_kgm3 * Qconc_inlet_m3s / 1000 * 3600
    Msalt_in_conc_outlet_kgh = Sconc_outlet_gm_per_kg * Dconc_outlet_kgm3 * Qconc_outlet_m3s / 1000 * 3600
    # in diluate
    Msalt_in_dial_inlet_kgh = Sdial_inlet_gm_per_kg * Ddial_inlet_kgm3 * Qdial_inlet_m3s / 1000 * 3600
    Msalt_in_dial_outlet_kgh = Sdial_outlet_gm_per_kg * Ddial_outlet_kgm3 * Qdial_outlet_m3s / 1000 * 3600

    # number of cell-pairs
    num_of_cell_pairs = sum(results['area_cell_pair_m2']) / tech_config_and_params['a_m']  # [-]
    area_cell_pair_tot_m2 = sum(results['area_cell_pair_m2'])  # [m2]

    


    area_cell_pair_tot_m2_corrected = area_cell_pair_tot_m2 * tech_config_and_params['area_corr_factor']
    num_of_cell_pairs_corrected = area_cell_pair_tot_m2_corrected / tech_config_and_params['a_m']  # [-]
    print("Number of cell pairs (before and after correction): ", num_of_cell_pairs, num_of_cell_pairs_corrected)
    print("Area of cell pairs (before and after correction): ", area_cell_pair_tot_m2, area_cell_pair_tot_m2_corrected)

    # voltage
    voltage_cell_pair = single_stage.calculate_cell_pair_voltage(Sconc_inlet_gm_per_kg, Sdial_inlet_gm_per_kg, Dconc_inlet_kgm3, Ddial_inlet_kgm3, current_density, num_of_cell_pairs_corrected, tech_config_and_params)
    voltage_stack = voltage_cell_pair * num_of_cell_pairs_corrected + tech_config_and_params['v_el']

    # stack power
    #list_a_cp_j = [x / num_of_cell_pairs for x in results['area_cell_pair_m2']]  # area of jth element in one cell-pair only
    # power_stack = sum(list_a_cp_j) * current_density * voltage_stack  # [W]
    power_stack = (area_cell_pair_tot_m2_corrected / num_of_cell_pairs_corrected) * current_density * voltage_stack  # [W]

    # pumping power (Nayar's method)
    vel_conc_ms = Qconc_inlet_m3s / (tech_config_and_params['h'] * num_of_cell_pairs_corrected * tech_config_and_params['W_stack'])  # [m/s]
    vel_dial_ms = Qdial_inlet_m3s / (tech_config_and_params['h'] * num_of_cell_pairs_corrected * tech_config_and_params['W_stack'])  # [m/s]
    pr_drop_conc_pa, pr_drop_dial_pa = eqns.calculate_pumping_power(Sconc_inlet_gm_per_kg, Sdial_inlet_gm_per_kg, Dconc_inlet_kgm3, Ddial_inlet_kgm3, vel_conc_ms, vel_dial_ms,
                                                            tech_config_and_params)
    power_pump = ((pr_drop_conc_pa) * Qconc_inlet_m3s / tech_config_and_params['eta_pump']) + (
            (pr_drop_dial_pa) * Qdial_inlet_m3s / tech_config_and_params['eta_pump'])  # [W]

    # total power
    power_total = power_stack + power_pump  # [W]

    # Electricity consumption
    elec_consumption_annual = power_total / 1000 * 8760 * tech_config_and_params['plant_availability']  # [kWh_el/year]
    elec_consumption_per_vol_conc_outlet = power_total / 1000 / (Qconc_outlet_m3s * 3600)  # [kWh_el/m3_conc_outlet]
    elec_consumption_per_vol_dial_outlet = power_total / 1000 / (Qdial_outlet_m3s * 3600)  # [kWh_el/m3_dial_outlet]
    elec_consumption_per_salt_mass_in_conc_outlet = power_total / 1000 / Msalt_in_conc_outlet_kgh * 1000  # [kWh_el/ton_salt_in_conc_outlet]

    # total membrane area [m2]
    area_membrane_tot_m2 = 2 * area_cell_pair_tot_m2_corrected / tech_config_and_params['m_eff']
    print('area_membrane_tot_m2 [m2]', area_membrane_tot_m2)

    ## economic calculations
    # Capital cost and capex
    cost_year = tech_config_and_params['cost_year']
    capital_cost_plant = area_membrane_tot_m2 * tech_config_and_params['sp_cost_high_salinity_ed_plant'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year]  # [USD_current]
    capex_total = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['depr_period']['civil'], capital_cost_plant)  # [USD_current/year]

    # OPEX [USD_current/yr]
    opex_energy, opex_mem, opex_maint, opex_chem, opex_labor, opex_ed_plant = economic_function.get_opex(power_total,
                                                                                                          elec_cost,
                                                                                                          area_membrane_tot_m2,
                                                                                                          tech_config_and_params,
                                                                                                          proj_config_and_params,
                                                                                                          )
    # total annual cost (TAC) [USD_current/yr]
    tac_plant = capex_total + opex_ed_plant

    # specific cost [USD_current/m3] of brine and diluate
    levelized_cost_per_brine = tac_plant / Qconc_outlet_m3yr
    levelized_cost_per_dial = tac_plant / Qdial_outlet_m3yr

    print('levelized_cost_per_brine [USD_current/m3]', levelized_cost_per_brine)
    print('levelized_cost_per_dial [USD_current/m3]', levelized_cost_per_dial)


    print('salt flow rate [g_salt/h] in last element of diluate',
          results['Sdial_gm_per_kg'][-1] * results['Ddial_kgm3'][-1] * (Mdial_inlet_kgs / Ddial_inlet_kgm3 * 3600))
    print('salt flow rate [g_salt/h] in last element of concentrate',
          results['Sconc_gm_per_kg'][-1] * results['Dconc_kgm3'][-1] * (Mconc_inlet_kgs / Dconc_inlet_kgm3 * 3600))

    ed_inputs = ResultList(title="ED simulation", xls_sheet="electrodialysis", results=[
        ('input and parameters', ('')),
        ('Feed mass flow rate [kg_sol/s]', (Mfeed_plant_kgs)),
        ('Feed salinity [ppm of NaCl]', (Sfeed_ppm)),
        ('Expected salinity of concentrate outlet [ppm of NaCl]', (Sconc_outlet_ppm)),
        ('Dilute-to-concentrate mass flow ratio [-]', (dil_to_conc_ratio)),
        ('current density [A/m2]', (current_density)),
        ('voltage across electrodes [V]', (tech_config_and_params['v_el'])),
        ('electricity cost [USD_current/kWh_e]', (elec_cost)),
        ('Current cost year', (proj_config_and_params['CEPCI_current_time'])),
        ('Plant availability', (tech_config_and_params['plant_availability'])),
        ])
    
    ed_flow_outputs = ResultList(title="Flow characteristics", xls_x0=13, xls_sheet="electrodialysis", results=[
        ('', ('concentrate inlet', 'diluate inlet', 'concentrate outlet', 'diluate outlet')),
        ('Mass flow rate of solution [kg_sol/h]', (Mconc_inlet_kgs * 3600, Mdial_inlet_kgs * 3600, Mconc_outlet_kgs * 3600, Mdial_outlet_kgs * 3600)),
        ('Density of solution [kg_sol/m3_sol]', (Dconc_inlet_kgm3, Ddial_inlet_kgm3, Dconc_outlet_kgm3, Ddial_outlet_kgm3)),
        ('Volume flow rate of solution [m3_sol/h]', (Qconc_inlet_m3s * 3600, Qdial_inlet_m3s * 3600, Qconc_outlet_m3s * 3600, Qdial_outlet_m3s * 3600)),
        ('Annual volume flow rate of solution [m3_sol/yr]', (Qconc_inlet_m3yr, Qdial_inlet_m3yr, Qconc_outlet_m3yr, Qdial_outlet_m3yr)),
        ('Salinity [ppm of NaCl]', (Sfeed_ppm, Sfeed_ppm, Sconc_outlet_ppm, Sdial_outlet_gm_per_kg * Ddial_outlet_kgm3)),
        ('Salinity [g_nacl / kg_sol]', (Sconc_inlet_gm_per_kg, Sdial_inlet_gm_per_kg, Sconc_outlet_gm_per_kg, Sdial_outlet_gm_per_kg)),
        ('Mass flow rate of salt in solution [kg_salt/h]', (Msalt_in_conc_inlet_kgh, Msalt_in_dial_inlet_kgh, Msalt_in_conc_outlet_kgh, Msalt_in_dial_outlet_kgh)),
        ])

    
    ed_technical_outputs = ResultList(title="technical results", xls_y0=6, xls_sheet="electrodialysis", results=[
        ('preliminary cell-pair area [m2]', (area_cell_pair_tot_m2)),
        ('corrected cell-pair area [m2]', (area_cell_pair_tot_m2_corrected)),
        ('corrected number of cell-pairs [-]', (num_of_cell_pairs_corrected)),
        ('actual membrane area [m2]', (area_membrane_tot_m2)),
        ('stack voltage [V]', (voltage_stack)),
        ('stack power [W]', (power_stack)),
        ('pump power [W]', (power_pump)),
        ('total ED power [W]', (power_total)),
        ('Elec. consumption (annual) [kWh_el/yr]', (elec_consumption_annual)),
        ('Specific elec. consumption [kWh_el/m3_conc_outlet]', (elec_consumption_per_vol_conc_outlet)),
        ('Specific elec. consumption [kWh_el/m3_dial_outlet]', (elec_consumption_per_vol_dial_outlet)),
        ('Specific elec. consumption [kWh_el/ton_salt_in_conc_outlet]', (elec_consumption_per_salt_mass_in_conc_outlet)),
        ])

    ed_economic_outputs = ResultList(title="economic results", xls_y0=9, xls_sheet="electrodialysis", results=[
        ('capital investment in plant', ('USD_current', 'USD_current/yr', 'USD_current/m3_perm_capacity/day')),
        ('capital investment in plant [USD_current]', (capital_cost_plant)),
        ('capex [USD_current/yr]', (capex_total)),
        ('opex costs', ('')),
        ('opex_energy [USD_current/yr]', (opex_energy)),
        ('opex_membrane [USD_current/yr]', (opex_mem)),
        ('opex_maintenance [USD_current/yr]', (opex_maint)),
        ('opex_chemicals [USD_current/yr]', (opex_chem)),
        ('opex_labor [USD_current/yr]', (opex_labor)),
        ('opex_ed_plant [USD_current/yr]', (opex_ed_plant)),
        ('total annual cost [USD_current/yr]', (tac_plant)),
        ('Levelized cost', ('')),
        ('brine based [USD_current/m3_brine]', (levelized_cost_per_brine)),
        ('diluate based [USD_current/m3_brine]', (levelized_cost_per_dial)),
        ])

    return [ed_inputs, ed_flow_outputs, ed_technical_outputs, ed_economic_outputs]