from argparse import ArgumentParser
from sys import argv
import os
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from common_and_tools import save_resultlists
from common_and_tools import model_multi_effect_distillation

def driver(model, programconfigpath, parameterconfigfilepath, N_effects, Mfeed, Xfeed, Xret_out, Ts_design, electricity_cost, heat_cost,
           output_directory, output_namefile, output_filename):
    with open(programconfigpath, "r") as file_program:
        program_config = yaml.load(file_program, Loader=Loader)    
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)

    result_lists = model(program_config, parameter_config, N_effects, Mfeed, Xfeed, Xret_out, Ts_design, electricity_cost, heat_cost)

    for result_list in result_lists:
        print(result_list)

    output_filename = f"results_N_{N_effects}_Xin_{Xfeed}_Xout_{Xret_out}_mfeed={round(Mfeed, 2)}.xls"

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
    'multi_effect_distillation': model_multi_effect_distillation
    }
    parser = ArgumentParser(prog='brinetech_multi_effect_distillation', description='Multi-effect distillation simulation tool')
    parser.add_argument('-m', '--model')
    parser.add_argument('-ne', '--num-of-effects', type=int, required=True, help="Number of effects [-]")
    parser.add_argument('-fm', '--feed-m', type=float, required=True, help="Feed flow rate M in kg/s")
    parser.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser.add_argument('-xro', '--x-ret-out', type=float, required=True, help="Desired salinity of the retentate stream [ppm]")
    parser.add_argument('-ts', '--ts-design', type=float,required=True, help="Feed steam temperature [°C]")
    parser.add_argument('-e', '--electricity-cost', type=float, default=0.1035, help="Cost of electricity in $/kWh_el")
    parser.add_argument('-he', '--heat-cost', type=float, default=0.01, help="Cost of heat in $/kWh_th")
    args = parser.parse_args(argv)

    ## getting path for the parameters.yml:
    main_tool_name = ""
    main_tool_name = args.model
    # parameters file path, output directory & output file name
    param_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'parameters.yml')
    program_config_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'program_config.yml')
    output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name)
    #output_filename = f"results_mfeed={round(args.feed_m, 2)}.xls"
    # name of the dat. file to store output file name:
    output_namefile = "fila_name.dat"

    return driver(
        model=models[args.model],
        programconfigpath=program_config_path,
        parameterconfigfilepath=param_path,
        Mfeed=args.feed_m,
        Xfeed=args.x_feed,
        Xret_out=args.x_ret_out,
        N_effects=args.num_of_effects,
        Ts_design=args.ts_design,
        electricity_cost=args.electricity_cost,
        heat_cost=args.heat_cost,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=None,
    )

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()

"""
## model_multi_effect_distillation
python main_multi_effect_distillation.py -m "multi_effect_distillation" -fm 4.52 -xf 26209 -xro 90000 -ne 6 -ts 100 -e 0.103 -he 0.01
"""