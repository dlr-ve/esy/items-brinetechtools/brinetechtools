from argparse import ArgumentParser
from sys import argv
import os
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from common_and_tools import save_resultlists
from common_and_tools import model_electrodialysis

def driver(model, parameterconfigfilepath, X_feed_NaCl_ppm, X_c_o_NaCl_ppm, i_j, Mfeed, Mdial_con_ratio, electricity_cost,
           output_directory, output_namefile, output_filename):
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)

    result_lists = model(parameter_config, X_feed_NaCl_ppm, X_c_o_NaCl_ppm, i_j, Mfeed, Mdial_con_ratio, electricity_cost)

    for result_list in result_lists:
        print(result_list)

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
    'electrodialysis': model_electrodialysis
    }
    parser = ArgumentParser(prog='brinetech_electrodialysis', description='Electrodialysis simulation tool')
    parser.add_argument('-m', '--model')  # should be 'electrodialysis'
    parser.add_argument('-xf', '--x-feed', type=float, required=True, help="NaCl salinity of the feed stream [ppm of NaCl]")
    parser.add_argument('-xco', '--x-conc-out', type=float, required=True, help="Desired NaCl salinity of the concentrate outlet stream [ppm of NaCl]")
    parser.add_argument('-i', '--i-j', type=float, required=True, help="Current density [A/m**2]")
    parser.add_argument('-fm', '--feed-m', type=float, required=True, help="Feed flow rate M in kg/s")
    parser.add_argument('-mr', '--m-ratio', type=float, required=True, help="Dialuate-to-conc splitting ratio (at ED inlet) for mass flow rate [-]")
    parser.add_argument('-e', '--electricity-cost', type=float, default=0.1035, help="Cost of electricity in $/kWh")    
    args = parser.parse_args(argv)

    ## getting path for the parameters.yml:
    main_tool_name = ""
    main_tool_name = args.model
    # parameters file path, output directory & output file name
    param_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'parameters.yml')
    output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name)
    output_filename = f"results_mfeed={round(args.feed_m, 2)}_xf_{round(args.x_feed, 2)}_xco_{round(args.x_conc_out, 2)}_.xls"
    # name of the dat. file to store output file name:
    output_namefile = "fila_name.dat"

    return driver(
        model=models[args.model],
        parameterconfigfilepath=param_path,
        X_feed_NaCl_ppm=args.x_feed,
        X_c_o_NaCl_ppm=args.x_conc_out,
        i_j=args.i_j,
        Mfeed=args.feed_m,
        Mdial_con_ratio=args.m_ratio,
        electricity_cost=args.electricity_cost,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=output_filename,
    )

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()

"""
## model_electrodialysis
python main_electrodialysis.py -m "electrodialysis" -xf 40050 -xco 204675 -i 250 -fm 0.285218 -mr 15.94915 -e 0.1035
python main_electrodialysis.py -m "electrodialysis" -xf 61000 -xco 184000 -i 300 -fm 0.0819127 -mr 5.2893 -e 0.1035
"""