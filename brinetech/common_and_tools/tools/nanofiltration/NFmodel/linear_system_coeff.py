import numpy as np

def coeff_FBL_A(kc_prime, Jv):
    A_FBL = kc_prime - Jv
    return A_FBL

def coeff_FBL_B(Jv):
    B_FBL = Jv
    return B_FBL

def coeff_FBL_C (z, C_bm, D_inf, config):
    C_FBL = z * C_bm * D_inf * config['F'] / (config['R'] * config['T'])
    return C_FBL

def coeff_FBL_D(kc_prime, C_b):
    D_FBL = kc_prime * C_b
    return D_FBL

def coeff_FMI_A(z, C_bm, phi_steric, phi_DE, psi_m, config):
    F_RT = config['F'] / (config['R'] * config['T'])
    A_FMI = C_bm * z * F_RT * phi_steric * phi_DE * np.exp(-z * F_RT * psi_m[0])
    return A_FMI

def coeff_FMI_B(z, C_bm, phi_steric, phi_DE, psi_m, config):
    F_RT = config['F'] / (config['R'] * config['T'])
    B_FMI = C_bm * phi_steric * phi_DE * np.exp(-z * F_RT * psi_m[0]) * (1 + z * F_RT * psi_m[0])
    return B_FMI

def coeff_FMI_A_activitycoeff(z, C_bm, phi_steric, phi_DE, psi_m, config, gamma_bm):
    F_RT = config['F'] / (config['R'] * config['T'])
    A_FMI = gamma_bm * C_bm * z * F_RT * phi_steric * phi_DE * np.exp(-z * F_RT * psi_m[0])
    return A_FMI

def coeff_FMI_B_activitycoeff(z, C_bm, phi_steric, phi_DE, psi_m, config, gamma_bm):
    F_RT = config['F'] / (config['R'] * config['T'])
    B_FMI = gamma_bm * C_bm * phi_steric * phi_DE * np.exp(-z * F_RT * psi_m[0]) * (1 + z * F_RT * psi_m[0])
    return B_FMI

def coeff_MPI_A(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, config):
    F_RT = config['F'] / (config['R'] * config['T'])
    A_MPI = phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1]))
    return A_MPI

def coeff_MPI_B(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, Cp, config):
    F_RT = config['F'] / (config['R'] * config['T'])
    B_MPI = Cp * z * F_RT * phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1]))
    return B_MPI

def coeff_MPI_C(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, Cp, config):
    F_RT = config['F'] / (config['R'] * config['T'])
    C_MPI = Cp * phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1])) * z * F_RT * (- psi_p + psi_m[N_nodes - 1])
    return C_MPI

def coeff_MPI_A_activitycoeff(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, config, gamma_permeate):
    F_RT = config['F'] / (config['R'] * config['T'])
    A_MPI = gamma_permeate * phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1]))
    return A_MPI

def coeff_MPI_B_activitycoeff(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, Cp, config, gamma_permeate):
    F_RT = config['F'] / (config['R'] * config['T'])
    B_MPI = gamma_permeate * Cp * z * F_RT * phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1]))
    return B_MPI

def coeff_MPI_C_activitycoeff(z, phi_steric, phi_DE, psi_m, psi_p, N_nodes, Cp, config, gamma_permeate):
    F_RT = config['F'] / (config['R'] * config['T'])
    C_MPI = gamma_permeate * Cp * phi_steric * phi_DE * np.exp(z * F_RT * (psi_p - psi_m[N_nodes - 1])) * z * F_RT *\
            (- psi_p + psi_m[N_nodes - 1])
    return C_MPI

def coeff_ENP_A(j, Dp, dx, Kc, Jv, z, psi, config):
    A = Dp / dx + 0.5 * Kc * Jv - 0.5 * z * Dp * config['F'] / (config['R'] * config['T']) * (psi[j+1] - psi[j]) / dx
    return A

def coeff_ENP_B(j, Dp, dx, Kc, Jv, z, psi, config):
    B = -Dp / dx + 0.5 * Kc * Jv - 0.5 * z * Dp * config['F'] / (config['R'] * config['T']) * (psi[j+1] - psi[j]) / dx
    return B

def coeff_ENP_C(j, Dp, dx, z, c, config):
    C = 0.5 * z * Dp * config['F'] / (config['R'] * config['T']) * (c[j+1] + c[j]) / dx
    return C

def coeff_ENP_D(j, Dp, dx, z, c, config):
    D = -0.5 * z * Dp * config['F'] / (config['R'] * config['T']) * (c[j+1] + c[j]) / dx
    return D

def coeff_ENP_E(Jv):
    E = -Jv
    return E

def coeff_ENP_F(j, Dp, dx, z, c, psi, config):
    F = -0.5 * z * Dp * config['F'] / (config['R'] * config['T']) * (c[j] + c[j+1]) * (psi[j+1] - psi[j]) / dx
    return F