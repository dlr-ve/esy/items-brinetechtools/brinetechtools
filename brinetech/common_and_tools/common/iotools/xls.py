import pint
import xlwt
from os.path import join as pjoin
from os import makedirs


def save_resultlists(partner_name, filename, namefile, result_lists):
    wb = xlwt.Workbook()
    sheets = set()
    for result_list in result_lists:
        sheet = result_list.xls_sheet
        ws = wb.get_sheet(sheet) if sheet in sheets else wb.add_sheet(sheet)
        sheets.add(sheet)
        _write_resultlist(ws, result_list)
    return _save_workbook(partner_name, filename, namefile, wb)


def _write_resultlist(ws, result_list):
    """Write the given ResultList as a block to an XLS file"""
    x0 = result_list.xls_x0
    y0 = result_list.xls_y0
    if result_list.title:
        ws.write(x0, y0, result_list.title)
    for i, (key, value) in enumerate(result_list):
        x = x0 + i + (1 if result_list.title else 0)
        ws.write(x, y0, key)
        if isinstance(value, pint.Quantity):
            ws.write(x, y0 + 1, value.magnitude)
            ws.write(x, y0 + 2, str(value.units))
        if isinstance(value, tuple):
            for i, v in enumerate(value):
                ws.write(x, y0 + 1 + i, v)
        else:
            ws.write(x, y0 + 1, value)


def _save_workbook(partner_name, filename, namefile, workbook):
    # Create a local folder (if it does not exist) to store the excel output
    results_directory = pjoin('results', partner_name)
    makedirs(results_directory, exist_ok=True)

    # Create a local text file (if it does not exist) to store name of excel file
    with open(pjoin(results_directory, namefile), 'w') as f:
        f.write(filename)

    # Save the workbook
    full_output_name = pjoin(results_directory, filename)
    workbook.save(full_output_name)
    return full_output_name
