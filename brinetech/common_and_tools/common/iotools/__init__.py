'''This module contains I/O functions and helper classes'''

from .resultlist import ResultList
from .xls import save_resultlists

__all__ = ["ResultList", "save_resultlists"]
