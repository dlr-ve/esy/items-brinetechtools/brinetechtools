import os
import yaml

import numpy as np
from colorama import Fore, Style


from ...common import ResultList


def levelized_costs_plant_config_1_model(proj_config_and_params, Qfeed_intake_tot_m3h, recovery_techs, Cannual_costs_techs_with_elec_wo_brine, Qupw_requirement_tot_m3h, Qfeed_water_mgcryst_tot_m3h, Qeffluent_mgcryst_tot_m3h, Mmgoh2_mgcryst_tot_tonh, Mnacl_naclcryst_tot_tonh):

    #print("Qfeed_intake_tot_m3h, recovery_techs, Cannual_costs_techs_with_elec_wo_brine, Qupw_requirement_tot_m3h, Qfeed_water_mgcryst_tot_m3h, Qeffluent_mgcryst_tot_m3h, Mmgoh2_mgcryst_tot_tonh, Mnacl_naclcryst_tot_tonh", Qfeed_intake_tot_m3h, recovery_techs, Cannual_costs_techs_with_elec_wo_brine, Qupw_requirement_tot_m3h, Qfeed_water_mgcryst_tot_m3h, Qeffluent_mgcryst_tot_m3h, Mmgoh2_mgcryst_tot_tonh, Mnacl_naclcryst_tot_tonh)
    ## read params from proj_config_and_params
    # specific costs and values of certain commodities 
    spec_cost_brine_disposal_usd_m3 = proj_config_and_params['spec_cost_commodity']['brine_disposal']
    market_value_nacl_usd_ton = proj_config_and_params['spec_price_commodity']['NaCl']
    market_value_water_usd_m3 = proj_config_and_params['spec_price_commodity']['water']
    market_value_upw_usd_m3 = proj_config_and_params['spec_price_commodity']['upw']

    plant_availability = proj_config_and_params['plant_availability']

    ### calculate flow rates in feed, perm and retentate streams of all techs
    
    ## Intake and pretreatment
    print("recovery intake", recovery_techs['intake'], type(recovery_techs['intake']))
    Qperm_intake_tot_m3h = Qfeed_intake_tot_m3h * recovery_techs['intake']
    Qloss_intake_tot_m3h = Qfeed_intake_tot_m3h - Qperm_intake_tot_m3h #! brine disposal cost to be taken into account

    Qfeed_pretreat_tot_m3h = Qperm_intake_tot_m3h
    Qperm_pretreat_tot_m3h = Qfeed_pretreat_tot_m3h * recovery_techs['pretreat']
    Qbackwash_pretreat_tot_m3h = Qfeed_pretreat_tot_m3h - Qperm_pretreat_tot_m3h #! brine disposal cost to be taken into account


    ## NF1
    Qfeed_nf1_tot_m3h = Qperm_pretreat_tot_m3h
    Qperm_nf1_tot_m3h = Qfeed_nf1_tot_m3h * recovery_techs['nf1']
    Qretent_nf1_tot_m3h = Qfeed_nf1_tot_m3h * (1 - recovery_techs['nf1'])

    ## NF2
    Qfeed_nf2_tot_m3h = Qretent_nf1_tot_m3h
    Qperm_nf2_tot_m3h = Qfeed_nf2_tot_m3h * recovery_techs['nf2']
    Qretent_nf2_tot_m3h = Qfeed_nf2_tot_m3h * (1 - recovery_techs['nf2'])

    ## Mg Cryst
    Qfeed_mgcryst_tot_m3h = Qretent_nf2_tot_m3h
    Qperm_mgcryst_tot_m3h = 0

    ## SWRO
    Qfeed_swro_tot_m3h = Qperm_nf1_tot_m3h + Qperm_nf2_tot_m3h
    Qperm_swro_tot_m3h = Qfeed_swro_tot_m3h * recovery_techs['swro']
    Qretent_swro_tot_m3h = Qfeed_swro_tot_m3h * (1 - recovery_techs['swro'])

    ## HPRO
    Qfeed_hpro_tot_m3h = Qretent_swro_tot_m3h
    Qperm_hpro_tot_m3h = Qfeed_hpro_tot_m3h * recovery_techs['hpro']
    Qretent_hpro_tot_m3h = Qfeed_hpro_tot_m3h * (1 - recovery_techs['hpro'])

    ## OARO
    Qfeed_oaro_tot_m3h = Qretent_hpro_tot_m3h
    Qperm_oaro_tot_m3h = 0
    Qretent_oaro_tot_m3h = Qfeed_oaro_tot_m3h * recovery_techs['oaro']

    ## NaCl Cryst
    Qfeed_naclcryst_tot_m3h = Qretent_oaro_tot_m3h
    Qperm_naclcryst_tot_m3h = Qfeed_naclcryst_tot_m3h * recovery_techs['naclcryst']
    Qpurge_naclcryst_tot_m3h = Qfeed_naclcryst_tot_m3h - Qperm_naclcryst_tot_m3h #! brine disposal cost to be taken into account

    ## secondpass RO + IX
    Qperm_secondpass_ro_ix_tot_m3h = Qupw_requirement_tot_m3h
    Qfeed_secondpass_ro_ix_tot_m3h = Qperm_secondpass_ro_ix_tot_m3h / recovery_techs['secondpass_ro_ix']
    Qretent_secondpass_ro_ix_tot_m3h = Qfeed_secondpass_ro_ix_tot_m3h * (1 - recovery_techs['secondpass_ro_ix'])

    ## posttreat
    Qfeed_posttreat_tot_m3h = Qperm_swro_tot_m3h + Qperm_hpro_tot_m3h + Qperm_naclcryst_tot_m3h - Qfeed_secondpass_ro_ix_tot_m3h - Qfeed_water_mgcryst_tot_m3h + Qretent_secondpass_ro_ix_tot_m3h
    Qperm_posttreat_tot_m3h = Qfeed_posttreat_tot_m3h * recovery_techs['posttreat']
    Qwaste_posttreat_tot_m3h = Qfeed_posttreat_tot_m3h * (1 - recovery_techs['posttreat']) #! brine disposal cost to be taken into account

    # Qfeed_m3h_dict, Qperm_m3h_dict, Qretent_m3h_dict, Qdisposal_m3h_dict
    Qfeed_m3h_dict = {
        "intake": Qfeed_intake_tot_m3h,
        "pretreat": Qfeed_pretreat_tot_m3h,
        "nf1": Qfeed_nf1_tot_m3h,
        "nf2": Qfeed_nf2_tot_m3h,
        "mgcryst": Qfeed_mgcryst_tot_m3h,
        "swro": Qfeed_swro_tot_m3h,
        "hpro": Qfeed_hpro_tot_m3h,
        "oaro": Qfeed_oaro_tot_m3h,
        "naclcryst": Qfeed_naclcryst_tot_m3h,
        "secondpass_ro_ix": Qfeed_secondpass_ro_ix_tot_m3h,
        "posttreat": Qfeed_posttreat_tot_m3h
    }


    Qperm_m3h_dict = {
        "intake": Qperm_intake_tot_m3h,
        "pretreat": Qperm_pretreat_tot_m3h,
        "nf1": Qperm_nf1_tot_m3h,
        "nf2": Qperm_nf2_tot_m3h,
        "mgcryst": Qperm_mgcryst_tot_m3h,
        "swro": Qperm_swro_tot_m3h,
        "hpro": Qperm_hpro_tot_m3h,
        "oaro": Qperm_oaro_tot_m3h,
        "naclcryst": Qperm_naclcryst_tot_m3h,
        "secondpass_ro_ix": Qperm_secondpass_ro_ix_tot_m3h,
        "posttreat": Qperm_posttreat_tot_m3h
    }

    # Qretent_m3h_dict
    # retentates: only the one used within the treatment chain. Waste/purge/effluent streams have separate dict
    Qretent_m3h_dict = {
        "intake": 0,
        "pretreat": 0,
        "nf1": Qretent_nf1_tot_m3h,
        "nf2": Qretent_nf2_tot_m3h,
        "mgcryst": 0,
        "swro": Qretent_swro_tot_m3h,
        "hpro": Qretent_hpro_tot_m3h,
        "oaro": Qretent_oaro_tot_m3h,
        "naclcryst": 0,
        "secondpass_ro_ix": Qretent_secondpass_ro_ix_tot_m3h,
        "posttreat": 0
    }

    Qdisposal_m3h_dict = {
        "intake": Qloss_intake_tot_m3h,
        "pretreat": Qbackwash_pretreat_tot_m3h,
        "nf1": 0,
        "nf2": 0,
        "mgcryst": Qeffluent_mgcryst_tot_m3h,
        "swro": 0,
        "hpro": 0,
        "oaro": 0,
        "naclcryst": Qpurge_naclcryst_tot_m3h,
        "secondpass_ro_ix": 0,
        "posttreat": Qwaste_posttreat_tot_m3h
    }

    Cannual_costs_techs_with_elec_with_brine = Cannual_costs_techs_with_elec_wo_brine.copy()
    Cannual_costs_techs_with_elec_with_brine['brine_disposal'] = sum(Qdisposal_m3h_dict.values()) * spec_cost_brine_disposal_usd_m3

    ## Cost share: Mg(OH)2
    cost_share_maghydro_dict = {
    'intake': ((1-recovery_techs['nf1'])*(1-recovery_techs['nf2']))+(((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'pretreat': ((1-recovery_techs['nf1'])*(1-recovery_techs['nf2']))+(((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf1': ((1-recovery_techs['nf1'])*(1-recovery_techs['nf2']))+(((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf2': (1-recovery_techs['nf2'])+((recovery_techs['nf2']*recovery_techs['swro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*recovery_techs['hpro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['secondpass_ro_ix']),
    'mgcryst': 1,
    'swro': (recovery_techs['swro']+(1-recovery_techs['swro'])*recovery_techs['hpro'])*Qfeed_water_mgcryst_tot_m3h/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'hpro': recovery_techs['hpro']*Qfeed_water_mgcryst_tot_m3h/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'oaro': 0,
    'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']    ),
    'secondpass_ro_ix': 0,
    'posttreat': 0,
    }
    cost_share_maghydro_dict["brine_disposal"] = cost_share_maghydro_dict["intake"]

    ## Cost share: NaCl
    cost_share_nacl_dict = {
    'intake': (recovery_techs['nf1']+((1-recovery_techs['nf1'])*recovery_techs['nf2']))*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*(1-recovery_techs['naclcryst']),
    'pretreat': (recovery_techs['nf1']+((1-recovery_techs['nf1'])*recovery_techs['nf2']))*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*(1-recovery_techs['naclcryst']),
    'nf1': (recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*(1-recovery_techs['naclcryst']),
    'nf2': recovery_techs['nf2']*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*(1-recovery_techs['naclcryst']),
    'mgcryst': 0,
    'swro': (1-recovery_techs['swro'])*(1-recovery_techs['hpro']),
    'hpro': (1-recovery_techs['hpro']),
    'oaro': 1,
    'naclcryst': (market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])),
    'secondpass_ro_ix': 0,
    'posttreat': 0,
    }
    cost_share_nacl_dict["brine_disposal"] = cost_share_nacl_dict["intake"]
    
    ## Cost share: Potable water
    cost_share_potable_water_dict = {
        'intake': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'pretreat': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'nf1': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'nf2': ((recovery_techs['nf2']*recovery_techs['swro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*recovery_techs['hpro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'mgcryst': 0,
        'swro': (recovery_techs['swro']+(1-recovery_techs['swro'])*recovery_techs['hpro'])*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'hpro': recovery_techs['hpro']*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'oaro': 0,
        'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'secondpass_ro_ix': (Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)/((Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)+(Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)),
        'posttreat': 1,
        }
    cost_share_potable_water_dict["brine_disposal"] = cost_share_potable_water_dict["intake"]

    ## Cost share: UPW
    cost_share_upw_dict = {
        'intake': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'pretreat': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'nf1': (((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*recovery_techs['swro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*recovery_techs['hpro'])+((recovery_techs['nf1']+(1-recovery_techs['nf1'])*recovery_techs['nf2'])*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'nf2': ((recovery_techs['nf2']*recovery_techs['swro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*recovery_techs['hpro'])+(recovery_techs['nf2']*(1-recovery_techs['swro'])*(1-recovery_techs['hpro'])*recovery_techs['naclcryst']))*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'mgcryst': 0,
        'swro': (recovery_techs['swro']+(1-recovery_techs['swro'])*recovery_techs['hpro'])*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'hpro': recovery_techs['hpro']*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'oaro': 0,
        'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
        'secondpass_ro_ix': (Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)/((Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)+(Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)),
        'posttreat': 0,
        }
    cost_share_upw_dict["brine_disposal"] = cost_share_upw_dict["intake"]

    # levelized cost (MgHydro)
    levelized_cost_maghydro_dict = {key: cost_share_maghydro_dict[key] * Cannual_costs_techs_with_elec_with_brine[key] / (Mmgoh2_mgcryst_tot_tonh * 8760 * plant_availability) for key in cost_share_maghydro_dict}

    # levelized cost (NaCl)
    levelized_cost_nacl_dict = {key: cost_share_nacl_dict[key] * Cannual_costs_techs_with_elec_with_brine[key] / (Mnacl_naclcryst_tot_tonh * 8760 * plant_availability) for key in cost_share_nacl_dict}

    # levelized cost (potable water)
    levelized_cost_potable_water_dict = {key: cost_share_potable_water_dict[key] * Cannual_costs_techs_with_elec_with_brine[key] / (Qperm_m3h_dict['posttreat'] * 8760 * plant_availability) for key in cost_share_potable_water_dict}

    # levelized cost (upw)
    levelized_cost_upw_dict = {key: cost_share_upw_dict[key] * Cannual_costs_techs_with_elec_with_brine[key] / (Qperm_m3h_dict['secondpass_ro_ix'] * 8760 * plant_availability) for key in cost_share_upw_dict}

    products = ['maghydro', 'nacl', 'potable_water', 'upw']

    # Initialize the dictionaries
    cost_share_dict = {}
    levelized_cost_dict = {}

    # Populate cost_share_dict and levelized_cost_dict using f-strings
    for product in products:
        # Use f-strings to dynamically reference the existing dictionaries
        cost_share_dict[product] = eval(f"cost_share_{product}_dict")
        levelized_cost_dict[product] = eval(f"levelized_cost_{product}_dict")



    result_inputs = ResultList(title="Main inputs", xls_sheet="levelized_cost", results=[
        ('Plant total feed vol. flow rate', (Qfeed_intake_tot_m3h, 'm3/h')),
        ('UPW required feed vol. flow rate', (Qupw_requirement_tot_m3h, 'm3/h')),
        ('MgCryst parameters', ('', '')),
        ('Potable water required by MgCryst', (Qfeed_water_mgcryst_tot_m3h, 'm3/h')),
        ('Effluent generated by MgCryst', (Qeffluent_mgcryst_tot_m3h, 'm3/h')),
        ('Hourly amount of Mg(OH)2 recovered by MgCryst', (Mmgoh2_mgcryst_tot_tonh, 'ton/h')),
        ('NaClCryst parameters', ('', '')),
        ('Hourly amount of NaCl recovered by NaClCryst', (Mnacl_naclcryst_tot_tonh, 'ton/h')),
        ('Costs and market values', ('', '')),
        ('Brine disposal cost', (spec_cost_brine_disposal_usd_m3, 'USD_current/m3')),
        ('Market values (used only to determine value-based cost distribution)', ('', '')),
        ('NaCl', (market_value_nacl_usd_ton, 'USD_current/ton')),
        ('Potable water', (market_value_water_usd_m3, 'USD_current/m3')),
        ('UPW', (market_value_upw_usd_m3, 'USD_current/m3')),
        ('Plant availability', (plant_availability, '-')),
        ])

    result_flowrates = ResultList(title="Flow-rate results", xls_sheet="levelized_cost", xls_y0=4, results=[
        ('Technology', ('Recovery (input) [-]', 'Qfeed [m3/h]', 'Qperm [m3/h]', 'Qretent [m3/h]', 'Qbrine_disposal [m3/h]')),
        ] +
        [(i_tech, (recovery_techs[i_tech], Qfeed_m3h_dict[i_tech], Qperm_m3h_dict[i_tech], Qretent_m3h_dict[i_tech], Qdisposal_m3h_dict[i_tech])) for i_tech in Qfeed_m3h_dict.keys()]
        )
    
    
    result_technical = ResultList(title="Cost-share results", xls_sheet="levelized_cost", xls_y0=11, results=[
        ('Technology', ('Annual costs (input) [USD_current/a]', 'MgHydro [-]', 'NaCl [-]', 'Potable water [-]', 'UPW [-]')),
        ] +
        [(i_tech, (Cannual_costs_techs_with_elec_with_brine[i_tech], cost_share_maghydro_dict[i_tech], cost_share_nacl_dict[i_tech], cost_share_potable_water_dict[i_tech], cost_share_upw_dict[i_tech])) for i_tech in cost_share_maghydro_dict.keys()]
        )
    
    result_eco = ResultList(title="Levelized-cost results", xls_sheet="levelized_cost", xls_y0=18, results=[
        ('Technology', ('MgHydro [USD_current/t_mghydro]', 'NaCl [USD_current/t_nacl]', 'Potable water [USD_current/m3]', 'UPW [USD_current/m3]')),
        ] +
        [(i_tech, (levelized_cost_maghydro_dict[i_tech], levelized_cost_nacl_dict[i_tech], levelized_cost_potable_water_dict[i_tech], levelized_cost_upw_dict[i_tech])) for i_tech in levelized_cost_maghydro_dict.keys()]
        )


    return [result_inputs, result_flowrates, result_technical, result_eco]