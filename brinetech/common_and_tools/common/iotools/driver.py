
def setup_driver(main_tool_name, sub_tool_name=None):
    common_params = {
        "model": models[args.model],
        "programconfigpath": None,
        "tech_config_params_filepath": param_path,
        "proj_config_params_filepath": proj_config_and_params_path,
        "output_directory": output_directory,
        "output_namefile": output_namefile,
        "output_filename": output_filename,
        "electricity_cost": electricity_cost,
        "N_trains": no_of_trains,
    }

    specific_params = {}

    if main_tool_name == 'nanofiltration':
        specific_params.update({
            "Cfeed_filepath": c_feed_path,
            "Pfeed": args.feed_p,
            "Pfeed_pump": args.pump_feed_pressure,
            "recovery_req": args.req_rec,
            "Qfeed": args.feed_q/no_of_trains,
            "Rionfilepath": rej_feed_path,
            "nNF": args.nNF,
            "with_erd": args.with_erd,
            "erd_pressure_in_hp": args.erd_pressure_in_hp,
            "erd_feed_q_in_hp_train": args.erd_feed_q_in_hp_train,
            "with_pretreatment_cost": args.with_pretreatment_cost,
        })

    elif main_tool_name == 'crystallizer':
        specific_params.update({
            "Cfeed_filepath": c_feed_path,
            "Qfeed": args.feed_q/no_of_trains if sub_tool_name != 'crystallizer_single_self_sodchl' else None,
            "Mfeed": args.feed_m/no_of_trains if sub_tool_name == 'crystallizer_single_self_sodchl' else None,
            "heat_cost": args.heat_cost if sub_tool_name == 'crystallizer_single_self_sodchl' else None,
            "T_in": args.t_n_design if sub_tool_name == 'crystallizer_single_self_sodchl' else None,
        })

    elif main_tool_name == 'reverse_osmosis':
        specific_params.update({
            "Mfeed": args.feed_m/no_of_trains,
            "with_swip_and_posttreatment": args.with_swip_and_posttreat,
            "with_preceding_nf": args.with_preceding_nf,
            "nelem_1": args.n1_elem,
            "nelem_2": args.n2_elem,
            "Xfeed": args.x_feed,
            "Xret_out": args.x_ret_out,
        })

    elif main_tool_name in ['pretreatment', 'posttreatment', 'intake_system']:
        specific_params.update({
            "Qfeed": args.feed_q/no_of_trains,
        })

    elif main_tool_name == 'multi_effect_distillation':
        specific_params.update({
            "Mfeed": args.feed_m/no_of_trains,
            "heat_cost": args.heat_cost,
            "N_effects": args.num_of_effects,
            "Ts_design": args.ts_design,
            "Xfeed": args.x_feed,
            "Xret_out": args.x_ret_out,
        })

    elif main_tool_name == 'electrodialysis':
        specific_params.update({
            "Mfeed": args.feed_m/no_of_trains,
            "Xfeed": args.x_feed,
            "Xret_out": args.x_ret_out,
            "i_j": args.i_j,
            "Mdial_con_ratio": args.m_ratio,
        })

    return driver(**{**common_params, **specific_params})
