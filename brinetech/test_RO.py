
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

df_output = pd.read_excel('./results/reverse_osmosis/arabian_gulf_kuwait/average_tds/results_double_stage_mfeedkgs_train=126.77_with_erd.xls')

df_output

#%%
Qfeed_m3h_RO_per_train = df_output.iloc[1,1]
no_of_trains = df_output.iloc[2,1]
Qfeed_m3h_RO_tot = Qfeed_m3h_RO_per_train * no_of_trains
Qfeed_m3h_RO_tot

#%%


## Mass and volume flow rates
if 'double' in df_output.columns[0]:
    
    recovery_swro = df_output.iloc[2,8]
    ## Mass flow rates
    Mperm_kgs_RO_per_train = df_output.iloc[3,8]
    Mretent_kgs_RO_per_train = df_output.iloc[5,8]

    # Feed pressure of last stage
    Pfeed_last_stage = df_output.iloc[1,7]

    ## Volume flow rates
    Qperm_m3h_RO_per_train = df_output.iloc[8,8]
    Qretent_m3h_RO_per_train = df_output.iloc[9,8]
    
    ## Flow concentrations
    Xperm_NaCl_ppm_RO = df_output.iloc[6,8]
    Xretent_NaCl_ppm_RO = df_output.iloc[7,8]

elif 'single' in df_output.columns[0]:
    
    recovery_swro = df_output.iloc[1,6]
    ## Mass flow rates
    Mperm_kgs_RO_per_train = df_output.iloc[2,6]
    Mretent_kgs_RO_per_train = df_output.iloc[3,6]

    # Feed pressure of last stage
    Pfeed_last_stage = df_output.iloc[0,6]

    ## Volume flow rates
    Qperm_m3h_RO_per_train = df_output.iloc[7,8]
    Qretent_m3h_RO_per_train = df_output.iloc[8,8]

    ## Flow concentrations
    Xperm_NaCl_ppm_RO = df_output.iloc[4,6]
    Xretent_NaCl_ppm_RO = df_output.iloc[5,6]


print(f"recovery_swro: {recovery_swro}, Mperm_kgs_RO_per_train: {Mperm_kgs_RO_per_train}, Pfeed_last_stage: {Pfeed_last_stage}, Qperm_m3h_RO_per_train: {Qperm_m3h_RO_per_train}, Qretent_m3h_RO_per_train: {Qretent_m3h_RO_per_train}, Mretent_kgs_RO_per_train: {Mretent_kgs_RO_per_train}, Xperm_NaCl_ppm_RO: {Xperm_NaCl_ppm_RO}, Xretent_NaCl_ppm_RO: {Xretent_NaCl_ppm_RO}")

#%%
## Economic and energy requirements (same cells for single and double stage)
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
capex_RO_per_train = float(df_output.iloc[0,17])  # [USD/y]
opex_RO_per_train = float(df_output.iloc[3,17])  # [USD/y]
electric_power_req_RO_per_train = float(df_output.iloc[18,6])   # kW_el
water_revenue_RO_per_train = float(df_output.iloc[9,17])  # [USD/y]
Mdist_annual_RO_per_train = float(df_output.iloc[10,17])  # [m3/y]

print(f"capex_RO_per_train: {capex_RO_per_train}, opex_RO: {opex_RO_per_train}, electric_power_req_RO_per_train: {electric_power_req_RO_per_train}, water_revenue_RO_per_train: {water_revenue_RO_per_train}, Mdist_annual_RO_per_train: {Mdist_annual_RO_per_train}")

#%%
df_output.iloc[0,17]

# %%
