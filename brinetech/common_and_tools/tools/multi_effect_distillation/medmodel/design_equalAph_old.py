import logging
import math
import numpy as np
from medmodel import h2oprop, naclprops, pressure, heattransfer, tools, tvc
from scipy.optimize import minimize
from medmodel.modelparams import fixed_DeltaTcond

logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)

''' functions for the design problem'''


def global_balance_mdist(m_dist, xf, xn):  # Mdist in [kg/s]
    m_brine = m_dist * xf / (xn - xf)
    m_feed = m_brine + m_dist
    return m_brine, m_feed


def global_balance_mfeed(m_feed, xf, xn):
    m_brine = m_feed * xf / xn
    m_dist = m_feed - m_brine
    return m_brine, m_dist


def calc_tvsat(tbrine, xb, config):
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime = h2oprop.sat_pressure(tvsat) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                      config['delta_w']) * 1E-3
    tvsat_prime = h2oprop.sat_temp(pv_prime)
    if tvsat_prime > tvsat:
        return pv_prime, tvsat
    return pv_prime, tvsat_prime


def deltat_losses(m_vap, tbrine, xb, config):  # Mvap in [kg/s], Tbrine in [°C]
    tbrine = float(tbrine)
    xb = float(xb)
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime, tvsat_prime = calc_tvsat(tbrine, xb, config)
    pc_prime = pv_prime - pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tvsat_prime),
                                                    config['d_tube']) * 1E-3 * config['L_tube']
    tc_prime = h2oprop.sat_temp(pc_prime)
    if tc_prime > tvsat_prime:
        tc_prime = tvsat_prime
    pc = pc_prime - pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime),
                                                  h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'],
                                                  config['L_evap']) * 1E-3 - \
         pressure.acceleration_deltap(m_vap, config['A_cross'], h2oprop.density_satwat_vapor(tc_prime),
                                      h2oprop.density_satwat_liquid(tc_prime),
                                      h2oprop.density_satwat_vapor(tc_prime),
                                      h2oprop.density_satwat_liquid(tc_prime)) * 1E-3
    tc = h2oprop.sat_temp(pc)
    if tc > tc_prime:
        tc = tc_prime
    return tvsat, tvsat_prime, tc_prime, tc


def tbrineout_fromtcout(tc_n, tc_prime, tv_prime, xb_out, m_vap, config, tbrine):
    tc_n = float(tc_n)
    xb_out = float(xb_out)
    pc = h2oprop.sat_pressure(tc_n)
    pc_prime = pc + pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime),
                                                  h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'],
                                                  config['L_evap']) * 1E-3 + \
               pressure.acceleration_deltap(m_vap, config['A_cross'], h2oprop.density_satwat_vapor(tc_prime),
                                            h2oprop.density_satwat_liquid(tc_prime),
                                            h2oprop.density_satwat_vapor(tc_prime),
                                            h2oprop.density_satwat_liquid(tc_prime)) * 1E-3
    tc_prime = h2oprop.sat_temp(pc_prime)
    deltat_evap = tc_prime - tc_n
    pv_prime = pc_prime + pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tv_prime),
                                                    config['d_tube']) * 1E-3 * config['L_tube']
    deltat_lines = h2oprop.sat_temp(pv_prime) - tc_prime
    if deltat_lines < 0:
        deltat_lines = 0
    pv = pv_prime + pressure.demister_deltap(config['rho_d'], config['v_vap'], config['delta_w']) * 1E-3
    deltat_dem = h2oprop.sat_temp(pv) - h2oprop.sat_temp(pv_prime)
    if deltat_dem < 0:
        deltat_dem = 0
    tbrine = tc_n + naclprops.bpe_brine_act(tbrine, xb_out) + deltat_dem + deltat_lines + deltat_evap
    return tbrine


def tpreh_profile(t_preh_next, alfa_c, m_vap, tvsat_prime, mfeed, t_mean, Xf):
    t_preh = t_preh_next + (alfa_c * m_vap * h2oprop.latent_heat(tvsat_prime)) / (
    mfeed * naclprops.cp_seawater(t_mean, Xf))
    return t_preh


def tpreh_profile(t_0, tpreh_n, n, deltat_ph = [], T_preh = []):
    deltat_ph.append(5.0)
    T_preh.append(t_0 - deltat_ph[0])
    for i in range(1, n):
        deltat_ph.append((T_preh[0]-tpreh_n)/(n-1))
    for i in range(1, n):
        T_preh.append(T_preh[i-1] - deltat_ph[i])
    return deltat_ph, T_preh


def alfa_preheater(mfeed, i_effect, t_preh, Xf, m_vap, tvsat_prime):
    alfa_c = mfeed * naclprops.cp_seawater((t_preh[i_effect] + t_preh[i_effect + 1]) / 2, Xf) * (
    t_preh[i_effect] - t_preh[i_effect + 1]) \
             / (m_vap[i_effect] * h2oprop.latent_heat(tvsat_prime[i_effect]))
    return alfa_c


def first_flashbox(alfa_c, m_vap, tc, t_fb, tv):  # not used
    m_fb = alfa_c * m_vap * (h2oprop.enthalpy_satwat_liquid(tc) - h2oprop.enthalpy_satwat_liquid(t_fb)) / (
    h2oprop.enthalpy_satwat_vapor(tv) - h2oprop.enthalpy_satwat_liquid(t_fb))
    mc = alfa_c * m_vap - m_fb
    return m_fb, mc


def generic_flashbox(i_effect, mfb_previous, tv_prime, tc, m_vap, alfa_c):
    mc = (alfa_c[i_effect] * m_vap[i_effect] * (
    h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) -
                                                              h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (
             h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = alfa_c[i_effect] * m_vap[i_effect] + (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def last_flashbox(i_effect, alfa_cond, mfb_previous, tv_prime, tc, m_vap):
    mc = (m_vap[i_effect] * (
    h2oprop.enthalpy_satwat_liquid(tc[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] * (
          h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (
             h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = m_vap[i_effect] + (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def brine_flash(i_effect, t, mb, xb):
    tbrine_fe = t[i_effect] + naclprops.non_equilibrium_allowance(t[i_effect - 1], t[i_effect], xb[i_effect - 1])
    m_fbrine = mb[i_effect - 1] * naclprops.cp_seawater((t[i_effect - 1] - tbrine_fe) / 2, xb[i_effect - 1]) * \
               (t[i_effect - 1] - tbrine_fe) / h2oprop.latent_heat(tbrine_fe)
    return m_fbrine


def area_hx(i_effect, mfeed, xf, t, t_preh, m_d, ts, alfa_cond, m_vap, tc):
    if i_effect == 0:
        return (mfeed * naclprops.cp_seawater((t[i_effect] + t_preh[i_effect]) / 2, xf) * (
        t[i_effect] - t_preh[i_effect]) +
                m_d[0] * h2oprop.latent_heat(t[i_effect] - naclprops.bpe_brine_act(t[i_effect], xf))) / (
               heattransfer.u_evaporator(t[i_effect]) * (ts - t[i_effect]))

    return ((1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1]) * h2oprop.latent_heat(tc[i_effect - 1]) / (
    heattransfer.u_evaporator(t[i_effect]) * (tc[i_effect - 1] - t[i_effect]))


def deltat_logmean(i_effect, tv_prime, t_preh):
    return ((tv_prime[i_effect] - t_preh[i_effect + 1]) - (tv_prime[i_effect] - t_preh[i_effect])) / \
           np.log((tv_prime[i_effect] - t_preh[i_effect + 1]) / (tv_prime[i_effect] - t_preh[i_effect]))


def area_preh(i_effect, alfa_c, m_vap, tv_prime, DTLM):
    return alfa_c[i_effect] * m_vap[i_effect] * h2oprop.latent_heat(tv_prime[i_effect]) / (
    heattransfer.u_condenser(tv_prime[i_effect]) * DTLM[i_effect])


def coolingwater_flowrate(mvap_out, tc_prime, xf, tcw_out, tcw_in):
    return mvap_out * h2oprop.latent_heat(tc_prime) / (
    naclprops.cp_seawater((tcw_out + tcw_in) / 2, xf) * (tcw_out - tcw_in))


def last_alfapreh(mb, t, xb, m_vap, tv_prime, m_fb, tc, N):
    m_steam = (mb[N - 1] * naclprops.enthalpy_seawater(t[N - 1], xb[N - 1]) + m_vap[
        N - 1] * h2oprop.enthalpy_satwat_vapor(tv_prime[N - 1]) - m_fb[N - 1] * h2oprop.enthalpy_satwat_vapor(
        tv_prime[N - 1]) -
               mb[N - 2] * naclprops.enthalpy_seawater(t[N - 2], xb[N - 2])) / h2oprop.latent_heat(tc[N - 1])
    return 1 - (m_steam / m_vap[N - 1])


def area_condenser(m_cw, tcw_in, tcw_out, tvap, xf):
    deltat_ln = (tcw_out - tcw_in) / np.log((tvap - tcw_in) / (tvap - tcw_out))
    area_c = m_cw * naclprops.cp_seawater((tcw_in + tcw_out) / 2, xf) * (tcw_out - tcw_in) / (
    deltat_ln * heattransfer.u_condenser(tvap))
    return area_c


class MedSolver:
    def __init__(self, N, DeltaT, DeltaT_tot, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Tcw_out, Mb, Md, M_fbrine,
                 M_fb, Mvap,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, DTT_preh, DTT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                 TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, config, DTLM):
        self.N = N
        self.DeltaT = DeltaT
        self.DeltaT_tot = DeltaT_tot
        self.T = T
        self.Ts = Ts
        self.Tn = Tn
        self.Tvsat = Tvsat
        self.Tvsat_prime = Tvsat_prime
        self.Tc_prime = Tc_prime
        self.Tc = Tc
        self.T_preh = T_preh
        self.Tcw_out = Tcw_out
        self.Mb = Mb
        self.Md = Md
        self.M_fbrine = M_fbrine
        self.M_fb = M_fb
        self.Mvap = Mvap
        self.Xb = Xb
        self.Mbrine = Mbrine
        self.Mdist = Mdist
        self.Mfeed = Mfeed
        self.Xf = Xf
        self.Xb_out = Xb_out
        self.M_cond_ph = M_cond_ph
        self.alfa_cond = alfa_cond
        self.DTT_preh = DTT_preh
        self.DTT_cond = DTT_cond
        self.h_preh = h_preh
        self.h_vprime = h_vprime
        self.h_cprime = h_cprime
        self.h_b = h_b
        self.Ms = Ms
        self.TVC_on = TVC_on
        self.Mms = Mms
        self.Pm = Pm
        self.Mss = Mss
        self.ER = ER
        self.Mc = Mc
        self.A_hx = A_hx
        self.A_preh = A_preh
        self.Msteam = Msteam
        self.X_max = X_max
        self.A_av = A_av
        self.DeltaA = DeltaA
        self.SumSquareArea = SumSquareErrorArea
        self.Mdist_real = Mdist_real
        self.Error_Mdist = Error_Mdist
        self.config = config
        self.DTLM = DTLM

    def areahxcalculation(self, x):
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn)
            self.Ms, self.Mss = tvc.tvc_steam(self.ER, self.Mms)
        else:
            self.Ms = self.Mms
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT[i_effect] = x[i_effect]
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(1, self.N - 1):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect], self.Tc[
                i_effect] = deltat_losses(self.Mvap[i_effect], self.T[i_effect], self.Xb[i_effect], self.config)
        self.Tvsat[0], self.Tvsat_prime[0], self.Tc_prime[0], self.Tc[0] = deltat_losses(self.Mvap[0], self.T[0],
                                                                                         self.Xf, self.config)
        self.Tvsat[self.N - 1], self.Tvsat_prime[self.N - 1], self.Tc_prime[self.N - 1], self.Tc[
            self.N - 1] = deltat_losses(self.Mvap[self.N - 1], self.T[self.N - 1], self.Xb[self.N - 1], self.config)

        if fixed_DeltaTcond:
            for iEffect in range(self.N-2, -1, -1):
                self.T_preh[iEffect] = self.T_preh[iEffect+1] + (self.M_cond_ph[iEffect] * h2oprop.latent_heat(self.Tvsat_prime[iEffect]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect] + self.T_preh[iEffect+1])/2, self.Xf))
        else:
            self.T_preh[0] = self.Tvsat_prime[0] - self.DTT_preh
            for iEffect in range(1, self.N):
                self.T_preh[iEffect] = self.T_preh[iEffect-1] - (self.M_cond_ph[iEffect-1] * h2oprop.latent_heat(self.Tvsat_prime[iEffect-1]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect-1] + self.T_preh[iEffect])/2, self.Xf))
            self.Tn = self.T_preh[self.N-1] + self.DTT_cond
        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5

        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
            self.M_cond_ph[i_effect] = self.alfa_cond[i_effect] * self.Mvap[i_effect]

        # first effect
        self.Mb[0] = self.Mfeed - self.Mvap[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.M_fb[0] = 0
        self.M_fbrine[0] = 0
        self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Ts) - self.Mfeed * naclprops.cp_seawater(
            (self.T_preh[0] + self.T[0]) / 2, self.Xf) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        self.Mvap[0] = self.Md[0]
        self.Mc[0] = self.M_cond_ph[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond, self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) / h2oprop.latent_heat(
                self.T[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / h2oprop.latent_heat(
            self.T[self.N - 1])
        self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)

        self.A_av = np.sum(self.A_hx) / self.N
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        self.SumSquareErrorArea = (np.sum(self.DeltaA ** 2)) ** 0.5
        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        Atot_av = self.A_av + Aph_av
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        Tot_error = self.SumSquareErrorArea * (self.A_av/Atot_av) + SumSquareErrorAreaph * (Aph_av/Atot_av)
        return self.SumSquareErrorArea


    def areaphcalculation(self, z):
        for i_effect in tools.itereffects(0, self.N-1):
            self.alfa_cond[i_effect] = z[i_effect]
            self.M_cond_ph[i_effect] = self.alfa_cond[i_effect] * self.Mvap[i_effect]
            # if self.alfa_cond[i_effect] < self.alfa_cond[i_effect - 1]:
            #     self.alfa_cond[i_effect] = self.alfa_cond[i_effect - 1] / 0.99
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn)
            self.Ms, self.Mss = tvc.tvc_steam(self.ER, self.Mms)
        else:
            self.Ms = self.Mms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(1, self.N - 1):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect], self.Tc[
                i_effect] = deltat_losses(self.Mvap[i_effect], self.T[i_effect], self.Xb[i_effect], self.config)
        self.Tvsat[0], self.Tvsat_prime[0], self.Tc_prime[0], self.Tc[0] = deltat_losses(self.Mvap[0], self.T[0],
                                                                                         self.Xf, self.config)
        self.Tvsat[self.N - 1], self.Tvsat_prime[self.N - 1], self.Tc_prime[self.N - 1], self.Tc[
            self.N - 1] = deltat_losses(self.Mvap[self.N - 1], self.T[self.N - 1], self.Xb[self.N - 1], self.config)

        if fixed_DeltaTcond:
            for iEffect in range(self.N-2, -1, -1):
                self.T_preh[iEffect] = self.T_preh[iEffect+1] + (self.M_cond_ph[iEffect] * h2oprop.latent_heat(self.Tvsat_prime[iEffect]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect] + self.T_preh[iEffect+1])/2, self.Xf))
        else:
            self.T_preh[0] = self.Tvsat_prime[0] - self.DTT_preh
            for iEffect in range(1, self.N):
                self.T_preh[iEffect] = self.T_preh[iEffect-1] - (self.M_cond_ph[iEffect-1] * h2oprop.latent_heat(self.Tvsat_prime[iEffect-1]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect-1] + self.T_preh[iEffect])/2, self.Xf))
            self.Tn = self.T_preh[self.N-1] + self.DTT_cond
        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5

        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)

        # first effect
        self.Mb[0] = self.Mfeed - self.Mvap[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.M_fb[0] = 0
        self.M_fbrine[0] = 0
        self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Ts) - self.Mfeed * naclprops.cp_seawater(
            (self.T_preh[0] + self.T[0]) / 2, self.Xf) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        self.Mvap[0] = self.Md[0]
        self.Mc[0] = self.M_cond_ph[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond, self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) / h2oprop.latent_heat(
                self.T[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / h2oprop.latent_heat(
            self.T[self.N - 1])
        self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)

        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        return SumSquareErrorAreaph

    def steamcalculation(self, y):
        self.Mms = y
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn)
            self.Ms, self.Mss = tvc.tvc_steam(self.ER, self.Mms)
        else:
            self.Ms = self.Mms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(1, self.N - 1):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect], self.Tc[
                i_effect] = deltat_losses(self.Mvap[i_effect], self.T[i_effect], self.Xb[i_effect], self.config)
        self.Tvsat[0], self.Tvsat_prime[0], self.Tc_prime[0], self.Tc[0] = deltat_losses(self.Mvap[0], self.T[0],
                                                                                         self.Xf, self.config)
        self.Tvsat[self.N - 1], self.Tvsat_prime[self.N - 1], self.Tc_prime[self.N - 1], self.Tc[
            self.N - 1] = deltat_losses(self.Mvap[self.N - 1], self.T[self.N - 1], self.Xb[self.N - 1], self.config)

        if fixed_DeltaTcond:
            for iEffect in range(self.N-2, -1, -1):
                self.T_preh[iEffect] = self.T_preh[iEffect+1] + (self.M_cond_ph[iEffect] * h2oprop.latent_heat(self.Tvsat_prime[iEffect]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect] + self.T_preh[iEffect+1])/2, self.Xf))
        else:
            self.T_preh[0] = self.Tvsat_prime[0] - self.DTT_preh
            for iEffect in range(1, self.N):
                self.T_preh[iEffect] = self.T_preh[iEffect-1] - (self.M_cond_ph[iEffect-1] * h2oprop.latent_heat(self.Tvsat_prime[iEffect-1]))/\
                                                                      (self.Mfeed * naclprops.cp_seawater((self.T_preh[iEffect-1] + self.T_preh[iEffect])/2, self.Xf))
            self.Tn = self.T_preh[self.N-1] + self.DTT_cond
        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5

        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
            self.M_cond_ph[i_effect] = self.alfa_cond[i_effect] * self.Mvap[i_effect]

        # first effect
        self.Mb[0] = self.Mfeed - self.Mvap[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.M_fb[0] = 0
        self.M_fbrine[0] = 0
        self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Ts) - self.Mfeed * naclprops.cp_seawater(
            (self.T_preh[0] + self.T[0]) / 2, self.Xf) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        self.Mvap[0] = self.Md[0]
        self.Mc[0] = self.M_cond_ph[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond, self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) / h2oprop.latent_heat(
                self.T[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / h2oprop.latent_heat(
            self.T[self.N - 1])
        self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)

        self.A_av = np.sum(self.A_hx) / self.N
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        self.Mdist_real = sum(self.Md) + sum(self.M_fbrine)
        self.Error_Mdist = abs(self.Mdist_real - self.Mdist)
        return self.Error_Mdist


def minim_eqAhx_eqAph(MedSolver, x0, y0, z0, DeltaT_res, alfac_res, Mms_res, loop_counter):
    logging.info('iteration number: {}'.format(loop_counter))
    DeltaT_min = minimize(MedSolver.areahxcalculation, x0, method='Nelder-Mead', options={'maxfev': 10000})
    DeltaT = np.array(DeltaT_min.x[0:MedSolver.N])
    MedSolver.DeltaT = DeltaT
    DeltaT_res.append(DeltaT)
    Eff_area_err = MedSolver.areahxcalculation(DeltaT)
    alfac_min = minimize(MedSolver.areaphcalculation, z0, method='Nelder-Mead', options={'maxfev': 10000})
    alfa_cond = alfac_min.x
    MedSolver.alfa_cond = alfa_cond
    alfac_res.append(alfa_cond)
    Eff_areaph_err = MedSolver.areaphcalculation(alfa_cond)
    alfac_err = float(np.abs(np.sum(alfac_res[loop_counter] - alfac_res[loop_counter - 1])))
    Ms_min = minimize(MedSolver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    Mms = Ms_min.x
    MedSolver.Mms = Mms
    Mms_res.append(Mms)
    Eff_error_Mdist = MedSolver.steamcalculation(Mms)
    DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
    Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
    loop_counter += 1
    return loop_counter, DeltaT_err, Ms_err, alfac_err, Eff_area_err, Eff_error_Mdist, Eff_areaph_err

def minim_eqAhxTprehfix(MedSolver, x0, y0, DeltaT_res, Mms_res, loop_counter):
    logging.info('iteration number: {}'.format(loop_counter))
    DeltaT_min = minimize(MedSolver.areahxcalculation, x0, method='Nelder-Mead', options={'maxfev': 10000})
    DeltaT = np.array(DeltaT_min.x[0:MedSolver.N])
    MedSolver.DeltaT = DeltaT
    DeltaT_res.append(DeltaT)
    Eff_area_err = MedSolver.areahxcalculation(DeltaT)
    Ms_min = minimize(MedSolver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    Mms = Ms_min.x
    MedSolver.Mms = Mms
    Mms_res.append(Mms)
    Eff_error_Mdist = MedSolver.steamcalculation(Mms)
    DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
    Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
    loop_counter += 1
    return loop_counter, DeltaT_err, Ms_err, Eff_area_err, Eff_error_Mdist, DeltaT, Mms

def minim_eqDT_eqAph(self, y0, z0, alfac_res, Mms_res, loop_counter):
    logging.info('iteration number: {}'.format(loop_counter))
    alfac_min = minimize(self.areaphcalculation, z0, method='Nelder-Mead', options={'maxfev': 10000})
    alfa_cond = alfac_min.x
    self.alfa_cond = alfa_cond
    alfac_res.append(alfa_cond)
    Eff_areaph_err = self.areaphcalculation(alfa_cond)
    alfac_err = float(np.abs(np.sum(alfac_res[loop_counter] - alfac_res[loop_counter - 1])))
    Ms_min = minimize(self.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    Mms = Ms_min.x
    self.Mms = Mms
    Mms_res.append(Mms)
    Eff_error_Mdist = self.steamcalculation(Mms)
    Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
    loop_counter += 1
    return loop_counter, Ms_err, alfac_err, Eff_error_Mdist, Eff_areaph_err

def minim_eqDT_Tprexfix(self, y0, Mms_res, loop_counter):
    logging.info('iteration number: {}'.format(loop_counter))
    Ms_min = minimize(self.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    Mms = Ms_min.x
    self.Mms = Mms
    Mms_res.append(Mms)
    Eff_error_Mdist = self.steamcalculation(Mms)
    Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
    loop_counter += 1
    return loop_counter, Ms_err, Eff_error_Mdist
