import yaml


def density_mixtures(param_config, conc_dict):  # conc in mol/m3, v_molar and alpa in [cm3/mol], Mw in g/mol, rho_mix in kg/m3
    conc_molarity = {}
    mole_fraction = {}
    conc_ions = {}
    for key in conc_dict:
        conc_ions[key] = 0
    if 'dye' in conc_dict:
        del conc_ions['dye']
    if 'COD' in conc_dict:
        del conc_ions['COD']
    for ion in conc_ions.keys():
        conc_molarity[ion] = conc_dict[ion] / 1000
    l_ions_lsol = 0
    for ion in conc_ions.keys():
        l_ions_lsol += conc_molarity[ion] * param_config['v_molar_'+ion] * 1e-3
    l_H2O_lsol = 1 - l_ions_lsol
    molarity_H2o = l_H2O_lsol * 1000 / param_config['Mw_water']
    mol_tot_lsol = molarity_H2o
    for ion in conc_ions.keys():
        mol_tot_lsol += conc_molarity[ion]
    for ion in conc_ions.keys():
        mole_fraction[ion] = conc_molarity[ion] / mol_tot_lsol
    sum_xi_Mi = 0
    sum_xi = 0
    sum_vi_xi = 0
    sum_ai_xi = 0
    for ion in conc_ions.keys():
        sum_xi_Mi += mole_fraction[ion] * param_config['MW_'+ion]
        sum_xi += mole_fraction[ion]
        sum_vi_xi += mole_fraction[ion] * param_config['v_molar_'+ion]
        sum_ai_xi += mole_fraction[ion] * param_config['alpha_'+ion]
    rho_mix = (sum_xi_Mi + param_config['Mw_water'] * (1 - sum_xi)) / (sum_vi_xi + param_config['v_molar_water'] *
                                                                       (1-sum_xi) + sum_ai_xi * (1-sum_xi)) * 1000
    return rho_mix


def density_seawater_liquid(t, C):   # rho_SW in [kg/m3], X in [ppm] and T in [°C]
    b = (2 * C / 1000 - 150) / 150
    g1 = 0.5
    g2 = b
    g3 = 2 * b**2 - 1
    a1 = 4.032219 * g1 + 0.115313 * g2 + 3.26 * 1e-4 * g3
    a2 = -0.108199 * g1 + 1.571 * 1e-3 * g2 - 4.23 * 1e-4 * g3
    a3 = -0.012247 * g1 + 1.74 * 1e-3 * g2 - 9 * 1e-6 * g3
    a4 = 6.92 * 1e-4 * g1 - 8.7 * 1e-5 * g2 - 5.3 * 1e-5 * g3
    a = (2 * t - 200)/160
    f1 = 0.5
    f2 = a
    f3 = 2 * a**2 - 1
    f4 = 4 * a**3 - 3 * a
    rho_seawater = 1e3 * (a1 * f1 + a2 * f2 + a3 * f3 + a4 * f4)
    return rho_seawater