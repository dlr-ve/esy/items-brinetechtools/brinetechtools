from .model_multi_effect_distillation import multi_effect_distillation_model as model_multi_effect_distillation

__all__ = [
    "model_multi_effect_distillation"
]
