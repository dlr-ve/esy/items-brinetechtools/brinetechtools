
from .pretreatment_functions import estimation_cost_pretreatment, estimation_electric_cost_for_vol

from ...common import calculate_annual_depreciation_cost
from ...common import ResultList


#%%

def pretreatment_model(tech_config_and_params, proj_config_and_params, N_trains, feed_vol_flow_per_train_m3h, electricity_cost):
    
    # flow rate of Vfeed_out
    feed_vol_out_flow_per_train_m3h = feed_vol_flow_per_train_m3h * tech_config_and_params['recovery_rate']
    Vfeed_out_annual = feed_vol_out_flow_per_train_m3h * 8760 * tech_config_and_params['plant_availability']  # [m3 of feed_out/year]
    feed_vol_out_capacity_per_train_m3d = feed_vol_out_flow_per_train_m3h * 24  # [capacity of m3_perm/day]

    # Capital cost and annual chemical cost
    capital_cost_physical_and_chemical_pretreatment, annual_pretreatment_chem_cost = estimation_cost_pretreatment(feed_vol_flow_per_train_m3h, tech_config_and_params, proj_config_and_params)
    # Annualization of costs (if not already annual)
    annualized_pretreament_cost = calculate_annual_depreciation_cost(proj_config_and_params["interest_rate"], tech_config_and_params['depr_period'], capital_cost_physical_and_chemical_pretreatment)
    
    # Energy consumption
    spec_elec_consumption = tech_config_and_params["spec_elec_consumption"]  # [kWh/m3_feed_out]
    annual_electric_consumption = spec_elec_consumption * Vfeed_out_annual  # [kWh/a]
    
    ## OPEX
    # fixed (% of capital cost)
    fixed_opex_cost = capital_cost_physical_and_chemical_pretreatment * tech_config_and_params['perc_maint_CAPEX']  # [$/a]
    # variable (apart from annual chemical cost)
    annual_electric_cost = estimation_electric_cost_for_vol(spec_elec_consumption, feed_vol_flow_per_train_m3h, electricity_cost, tech_config_and_params)  # [$/a]
    # total
    annual_operating_cost = fixed_opex_cost + annual_electric_cost + annual_pretreatment_chem_cost

    ## total (fixed + variable operating)
    annual_total_cost = annualized_pretreament_cost + annual_operating_cost  # [$/a]

    pretreat_inputs = ResultList(title="Pretreatment (chemical+physical)", xls_sheet="pretreatment", results=[
        #('input and parameters', ('', '')),
        ('Number of trains', (N_trains, '-')),
        ('Qfeed per train', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('Recovery rate', (tech_config_and_params['recovery_rate'], '-')),
        ('specific electricity consumption', (spec_elec_consumption, 'kWh/m3_feed')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ])

    pretreat_results = ResultList(title="Technical results per train", xls_sheet="pretreatment", xls_y0=4, results=[
        ('Qfeed_out per train', (feed_vol_out_flow_per_train_m3h, 'm3/h')),
        ('annual electricity consumption', (annual_electric_consumption, 'kWh/a')),
        ])
    
    pretreat_results_economic = ResultList(title="Economic results per train", xls_sheet="pretreatment", xls_y0=8, results=[
        ('capex', ('$/y', '$', '$/m3_feed_out_capacity/day')),
        ('Pretreatment system', (annualized_pretreament_cost, capital_cost_physical_and_chemical_pretreatment, capital_cost_physical_and_chemical_pretreatment/feed_vol_out_capacity_per_train_m3d)),
        ('fixed opex', ('$/y', '' , '$/m3_feed_out_capacity/day')),
        ('fixed', (fixed_opex_cost, '', fixed_opex_cost/feed_vol_out_capacity_per_train_m3d)),
        ('variable opex', ('$/y', '', '$/m3_feed_out')),
        ('electric energy', (annual_electric_cost, '', annual_electric_cost/Vfeed_out_annual)),
        ('chemical cost', (annual_pretreatment_chem_cost, '', annual_pretreatment_chem_cost/Vfeed_out_annual)),
        ('annual opex (fixed+variable) cost', (annual_operating_cost, '', annual_operating_cost/Vfeed_out_annual)),
        ('levelized or total annual cost (capex+opex)', (annual_total_cost, '',  annual_total_cost/Vfeed_out_annual)),
        ])

    return [pretreat_inputs, pretreat_results, pretreat_results_economic]
