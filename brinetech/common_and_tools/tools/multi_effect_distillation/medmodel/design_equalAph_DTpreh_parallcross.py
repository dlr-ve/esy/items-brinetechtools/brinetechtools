import logging
import math
import numpy as np
from . import h2oprop, naclprops, pressure, heattransfer, tools, tvc
from scipy.optimize import minimize
from .modelparams import fixed_DeltaTcond
from .logger import logger
from .modelparams import Preheating_fractionated_feed


''' functions for the design problem'''


def global_balance_mdist(m_dist, xf, xn):  # Mdist in [kg/s]
    m_brine = m_dist * xf / (xn - xf)
    m_feed = m_brine + m_dist
    return m_brine, m_feed


def global_balance_mfeed(m_feed, xf, xn):
    m_brine = m_feed * xf / xn
    m_dist = m_feed - m_brine
    return m_brine, m_dist


def calc_tvsat(tbrine, xb, config):
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime = h2oprop.sat_pressure(tvsat) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                      config['delta_w']) * config['thickness'] * 1E-3 * 1E-3
    tvsat_prime = h2oprop.sat_temp(pv_prime)
    if tvsat_prime > tvsat:
        return pv_prime, tvsat
    return pv_prime, tvsat_prime


def deltat_losses(m_vap, tbrine, xb, config):  # Mvap in [kg/s], Tbrine in [°C]
    tbrine = float(tbrine)
    xb = float(xb)
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime, tvsat_prime = calc_tvsat(tbrine, xb, config)
    pc_prime = pv_prime - pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tvsat_prime),
                                                    config['d_tube']) * 1E-3 * config['L_tube']
    tc_prime = h2oprop.sat_temp(pc_prime)
    if tc_prime > tvsat_prime:
        tc_prime = tvsat_prime
    return tvsat, tvsat_prime, tc_prime

def deltat_cond(m_vap, tc_prime, config, n_tubes):
    dp_cond = 0.99 * pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tc_prime, config['epsilon']) * 1E-3 -\
              (pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime),
                                                  h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'],
                                                  config['L_evap']) * 1E-3)
    pc = h2oprop.sat_pressure(tc_prime) - dp_cond
    # pc = pc_prime - pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tbrine,
    #                                 config['epsilon']) * 1E-3
    tc = h2oprop.sat_temp(pc)
    if tc > tc_prime:
        tc = tc_prime
    return tc

def mvap_effectbalance(i_effect, m_steam, t_steam, Mfeed, t_preh, alfa_c, tv_prime, t, m_fb, mb, xb, N, Xf, FeedFraction):
    if i_effect == 0:
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) + Mfeed * naclprops.enthalpy_seawater(t_preh[i_effect + 1], Xf)
                 + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                 - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) \
             / ((1 - alfa_c[i_effect]) * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                + alfa_c[i_effect] * h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]))
    elif i_effect != 0 and i_effect != N-1:  # in this case m_steam = (1-alfa(iEffect-1))*Mvap(iEffect-1) and t_steam is Tc(iEffect-1)
        # m_vap = (m_steam * h2oprop.latent_heat(t_steam) +
        #      + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
        #      + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1])
        #      - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) / \
        #      (h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) + Mfeed * naclprops.cp_seawater(
            (t_preh[i_effect + 1] + t_preh[i_effect]) / 2, Xf) * (t_preh[i_effect + 1] - t_preh[i_effect])
                 + FeedFraction * naclprops.enthalpy_seawater(t_preh[i_effect], Xf)
                 + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                 + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1])
                 - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) / \
                ((1 - alfa_c[i_effect]) * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]) + alfa_c[
                    i_effect] * h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]))
    elif i_effect == N-1:  # i_effect == N-1
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                 + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1]) -
                 mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])
                 + FeedFraction * naclprops.enthalpy_seawater(t_preh[i_effect], Xf)) / h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
    return m_vap


def tbrineout_fromtcout(tc_n, tv_prime, xb_out, m_vap, config, tbrine):
    tc_n = float(tc_n)
    xb_out = float(xb_out)
    pc_prime = h2oprop.sat_pressure(tc_n)
    pv_prime = pc_prime + pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tv_prime), config['d_tube']) * 1E-3 * config['L_tube']
    deltat_lines = h2oprop.sat_temp(pv_prime) - tc_n
    if deltat_lines < 0:
        deltat_lines = 0
    pv = pv_prime + pressure.demister_deltap(config['rho_d'], config['v_vap'], config['delta_w']) * 1E-3
    deltat_dem = h2oprop.sat_temp(pv) - h2oprop.sat_temp(pv_prime)
    if deltat_dem < 0:
        deltat_dem = 0
    tbrine = tc_n + naclprops.bpe_brine_act(tbrine, xb_out) + deltat_dem + deltat_lines
    return tbrine

def tpreh_profile(t_0, DTT_preh, tpreh_n, n, deltat_ph = [], T_preh = []):
    T_preh.append(t_0 - DTT_preh)
    for i in range(0, n-1):
        deltat_ph.append((T_preh[0]-tpreh_n)/(n-1))
    for i in range(1, n):
        T_preh.append(T_preh[i-1] - deltat_ph[i-1])
    return deltat_ph, T_preh


def generic_flashbox(i_effect, mfb_previous, tv_prime, tc, m_vap, alfa_c):
    mc = (alfa_c[i_effect] * m_vap[i_effect] * (
    h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) -
                                                              h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (
             h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = alfa_c[i_effect] * m_vap[i_effect] + (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def last_flashbox(i_effect, alfa_cond, mfb_previous, tv_prime, tc, m_vap, m_vap_out, tc_prime):
    mc = (m_vap_out * (h2oprop.enthalpy_satwat_liquid(tc_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = m_vap_out + (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def brine_flash(i_effect, t, mb, xb):
    tbrine_fe = t[i_effect] + naclprops.non_equilibrium_allowance(t[i_effect - 1], t[i_effect], xb[i_effect - 1])
    m_fbrine = mb[i_effect - 1] * naclprops.cp_seawater((t[i_effect - 1] + tbrine_fe) / 2, xb[i_effect - 1]) * \
               (t[i_effect - 1] - tbrine_fe) / naclprops.latent_heat_seawater(tbrine_fe, xb[i_effect-1])
    #(h2oprop.latent_heat(tbrine_fe)*(1-xb[i_effect - 1]*1e-6))
    return m_fbrine


def area_hx(i_effect, mfeed, xf, t, t_preh, m_d, ts, alfa_cond, m_vap, tc):
    if i_effect == 0:
        return (mfeed * naclprops.cp_seawater((t[i_effect] + t_preh[i_effect]) / 2, xf) * (
        t[i_effect] - t_preh[i_effect]) +
                m_d[0] * naclprops.latent_heat_seawater(t[i_effect] - naclprops.bpe_brine_act(t[i_effect], xf), xf)) / (
               heattransfer.u_evaporator(t[i_effect]) * (ts - t[i_effect]))

    return ((1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1]) * h2oprop.latent_heat(tc[i_effect - 1]) / (
    heattransfer.u_evaporator(t[i_effect]) * (tc[i_effect - 1] - t[i_effect]))


def deltat_logmean_fract(i_effect, tv_prime, t_preh, tcw_out):
    return ((tv_prime[i_effect] - tcw_out) - (tv_prime[i_effect] - t_preh[i_effect])) / \
           np.log((tv_prime[i_effect] - tcw_out) / (tv_prime[i_effect] - t_preh[i_effect]))


def deltat_logmean(i_effect, tv_prime, t_preh):
    return ((tv_prime[i_effect] - t_preh[i_effect + 1]) - (tv_prime[i_effect] - t_preh[i_effect])) / \
           np.log((tv_prime[i_effect] - t_preh[i_effect + 1]) / (tv_prime[i_effect] - t_preh[i_effect]))


def area_preh(i_effect, alfa_c, m_vap, tv_prime, DTLM):
    return alfa_c[i_effect] * m_vap[i_effect] * h2oprop.latent_heat(tv_prime[i_effect]) / (
        heattransfer.u_condenser(tv_prime[i_effect]) * DTLM[i_effect])


def area_preh_2(i_effect, m_feed, t_preh, tcw_out, tv_prime, DTLM, Xf):
    return m_feed[i_effect] * naclprops.cp_seawater((t_preh[i_effect] + tcw_out) / 2, Xf) * (
                                                 t_preh[i_effect] - tcw_out) / (
        heattransfer.u_condenser(tv_prime[i_effect]) * DTLM[i_effect])


def area_preh_3(i_effect, m_feed, deltat_ph, t_preh, tv_prime, Xf):
    return m_feed[i_effect] * naclprops.cp_seawater((t_preh[i_effect] + t_preh[i_effect+1])/2, Xf) / heattransfer.u_condenser(tv_prime[i_effect]) \
           * np.log(1 + deltat_ph[i_effect]/(tv_prime[i_effect] - t_preh[i_effect]))


def coolingwater_flowrate(mvap_out, tc_prime, xf, tcw_out, tcw_in):
    return mvap_out * h2oprop.latent_heat(tc_prime) / (naclprops.cp_seawater((tcw_out + tcw_in) / 2, xf) * (tcw_out - tcw_in))


def area_condenser(m_cw, tcw_in, tcw_out, tvap, xf):
    deltat_ln = (tcw_out - tcw_in) / np.log((tvap - tcw_in) / (tvap - tcw_out))
    area_c = m_cw * naclprops.cp_seawater((tcw_in + tcw_out) / 2, xf) * (tcw_out - tcw_in) / (
        deltat_ln * heattransfer.u_condenser(tvap))
    return area_c


class MedSolver_eqAph_parallcross:
    def __init__(self, N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Tcw_out, Tcw_in, Mb, Md, M_fbrine,
                 M_fb, Mf, Mvap, Mfeed_ph,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, DTT_preh, DTT_cond, DT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                 TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, config, DTLM, n_tubes, Xff):
        self.N = N
        self.DeltaT = DeltaT
        self.DeltaT_tot = DeltaT_tot
        self.DeltaT_ph = DeltaT_ph
        self.T = T
        self.Ts = Ts
        self.Tn = Tn
        self.Tvsat = Tvsat
        self.Tvsat_prime = Tvsat_prime
        self.Tc_prime = Tc_prime
        self.Tc = Tc
        self.T_preh = T_preh
        self.Tcw_out = Tcw_out
        self.Tcw_in = Tcw_in
        self.Mb = Mb
        self.Md = Md
        self.M_fbrine = M_fbrine
        self.M_fb = M_fb
        self.Mf = Mf
        self.Mvap = Mvap
        self.Mfeed_ph = Mfeed_ph
        self.Xb = Xb
        self.Mbrine = Mbrine
        self.Mdist = Mdist
        self.Mfeed = Mfeed
        self.Xf = Xf
        self.Xb_out = Xb_out
        self.M_cond_ph = M_cond_ph
        self.alfa_cond = alfa_cond
        self.DTT_preh = DTT_preh
        self.DTT_cond = DTT_cond
        self.DT_cond = DT_cond
        self.h_preh = h_preh
        self.h_vprime = h_vprime
        self.h_cprime = h_cprime
        self.h_b = h_b
        self.Ms = Ms
        self.TVC_on = TVC_on
        self.Mms = Mms
        self.Pm = Pm
        self.Mss = Mss
        self.ER = ER
        self.Mc = Mc
        self.A_hx = A_hx
        self.A_preh = A_preh
        self.Msteam = Msteam
        self.X_max = X_max
        self.A_av = A_av
        self.DeltaA = DeltaA
        self.SumSquareArea = SumSquareErrorArea
        self.Mdist_real = Mdist_real
        self.Error_Mdist = Error_Mdist
        self.config = config
        self.DTLM = DTLM
        self.n_tubes = n_tubes
        self.Xff = Xff

    def firstguess_parallcross(self, x):
        # self.Tn = tbrineout_fromtcout(self.Tc_n, self.Tc_prime[self.N-1], self.Tvsat_prime[self.N-1], self.Xb_out, self.Mvap[self.N-1], self.config, self.Tn)
        # self.Tn = self.Tc_n
        self.DeltaT = x[0: self.N]
        self.DeltaT_ph = x[self.N: (2*self.N-1)]
        self.Ms = x[2*self.N-1]
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            if i_effect == 0:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  self.Xf, self.config)
            else:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf+self.Xb[i_effect-1])/2, self.config)

        for i_effect in tools.itereffects(0, self.N-1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes[i_effect])
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5

            # T preheater profile calculation
            DeltaT_totph = 0
            for i_effect in tools.itereffects(0, self.N - 1):
                DeltaT_totph += self.DeltaT_ph[i_effect]
                # self.T_preh[self.N-1] = self.Tcw_in + self.DT_cond
                # for i_effect in range(self.N-2, -1, -1):
                # self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
            # if abs(self.T_preh[0] - (self.T[0] - self.DTT_preh)) > 0.05:
            #     for i_effect in tools.itereffects(0, self.N-1):
            #         self.DeltaT_ph[i_effect] = self.DeltaT_ph[i_effect] * ((self.T[0] - self.DTT_preh) - self.T_preh[self.N-1])/DeltaT_totph
            # for i_effect in range(self.N - 2, -1, -1):
            #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
            self.T_preh[0] = self.T[0] - self.DTT_preh
            for i_effect in tools.itereffects(1, self.N):
                self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect - 1]
            self.Tn = self.config['Tn_design']
            self.Tcw_out = self.T_preh[self.N - 1]

            for iEffect in range(0, self.N - 1):
                if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                    self.T_preh[iEffect] = self.T[iEffect] - 5
                if abs(self.T_preh[iEffect] - self.T_preh[iEffect + 1]) < 0.05:
                    self.DeltaT_ph[iEffect] = 0.5

        FeedFraction = self.Mfeed / self.N

        if Preheating_fractionated_feed:
            for i_effect in tools.itereffects(0, self.N):
                self.Mfeed_ph[i_effect] = FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean_fract(i_effect, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[i_effect] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.Tcw_out) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
            if self.T_preh[self.N - 1] > self.Tcw_out:
                self.DTLM[self.N - 1] = deltat_logmean_fract(self.N - 1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[self.N - 1] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[self.N - 1] + self.Tcw_out) / 2, self.Xf) * (
                                                 self.T_preh[self.N - 1] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[self.N - 1])
                self.alfa_cond[self.N - 1] = self.M_cond_ph[self.N - 1] / self.Mvap[self.N - 1]
            else:
                self.M_cond_ph[self.N - 1] = 0
                self.alfa_cond[self.N - 1] = 0

        else:  # alternative way: all the feed is preheated in the last effect and sent to the previous ones,
            #  loosing in each effect a part = FeedFraction
            self.Mfeed_ph[0] = FeedFraction
            for i_effect in tools.itereffects(1, self.N):
                self.Mfeed_ph[i_effect] = self.Mfeed_ph[i_effect - 1] + FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
                self.M_cond_ph[i_effect] = self.Mfeed_ph[i_effect] * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.T_preh[i_effect + 1]) / h2oprop.latent_heat(
                    self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
                # if self.T_preh[self.N-1] > self.Tcw_out:
                #     self.DTLM[self.N-1] = deltat_logmean(self.N-1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                #     self.M_cond_ph[self.N-1] = self.Mfeed_ph[self.N-1] * naclprops.cp_seawater(
                #         (self.T_preh[self.N-1] + self.Tcw_out) / 2, self.Xb) * (
                #                                    self.T_preh[self.N-1] - self.Tcw_out) / h2oprop.latent_heat(
                #         self.Tvsat_prime[self.N-1])
                #     self.alfa_cond[self.N-1] = self.M_cond_ph[self.N-1] / self.Mvap[self.N-1]
                # else:
                #     self.M_cond_ph[self.N-1] = 0
                #     self.alfa_cond[self.N-1] = 0

        # first effect calculations
        self.Mf[0] = FeedFraction
        self.Mb[0] = self.Mf[0] - self.Md[0]
        self.Xb[0] = self.Mf[0] * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[
            0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(
            self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mf[0] * naclprops.cp_seawater((self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0] + self.M_fb[0]
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mf[0], self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mf[0], self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond, self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects calculations
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.Mf[i_effect] = self.Mb[i_effect - 1] + FeedFraction
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) -
                                 FeedFraction * naclprops.cp_seawater((self.T[i_effect] + self.T_preh[i_effect]) / 2, self.Xf) * (
                                 self.T[i_effect] - self.T_preh[i_effect])) \
                                / h2oprop.latent_heat(self.T[i_effect])
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])
            #                      + FeedFraction * (naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)
            #                                        - naclprops.enthalpy_seawater(self.T[i_effect],
            #                                                                      self.Xb[i_effect]))) / \
            #                     (h2oprop.latent_heat(self.Tvsat_prime[i_effect])
            #                      + h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
            #                         self.T[i_effect], self.Xb[i_effect]))
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) +
            #                      FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)
            #                      - self.Mb_g[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect]))\
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            # if self.Md[i_effect] > FeedFraction:
            #     self.Md[i_effect] = FeedFraction * 0.95
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) - self.M_fbrine[i_effect] * h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            #                      - self.Mb[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])
            #                      + self.Mb[i_effect - 1] * naclprops.enthalpy_seawater(self.T[i_effect - 1], self.Xb[i_effect - 1])
            #                      + FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)) \
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            # self.Mvap[i_effect] = mvap_effectbalance(i_effect, self.Msteam, self.Tc[i_effect-1], self.Mfeed_ph[i_effect], self.T_preh, self.alfa_cond,
            #                                   self.Tvsat_prime,
            #                                   self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xb, FeedFraction)
            # self.Md[i_effect] = self.Mvap[i_effect] - self.M_fb[i_effect] - self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mf[i_effect] - self.Md[i_effect] - self.M_fbrine[i_effect]
            # self.Mb_g[i_effect] = self.Mb[i_effect] - self.Mb[i_effect-1] + self.M_fbrine[i_effect]
            self.Xb[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mb[
                i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mf[i_effect], self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # last effect calculations
        self.Mf[self.N - 1] = self.Mb[self.N - 2] + FeedFraction
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) -
        #                          FeedFraction * naclprops.cp_seawater((self.T[self.N-1] + self.T_preh[self.N-1])/2, self.Xb) * (self.T[self.N-1] - self.T_preh[self.N-1])) /\
        #                       h2oprop.latent_heat(self.T[self.N - 1])
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mvap[self.N - 1] = mvap_effectbalance(self.N - 1, self.Msteam, self.Tc[self.N - 2], self.Mfeed,
                                                   self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                                   self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[self.N - 1] = self.Mvap[self.N - 1] - self.M_fbrine[self.N - 1] - self.M_fb[self.N - 1]
        self.Mb[self.N - 1] = self.Mf[self.N - 1] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mf[self.N - 1], self.Xf, self.T, self.T_preh, self.Md,
                                        self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)
        self.A_av = np.sum(self.A_hx) / self.N
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        self.SumSquareErrorArea = (np.sum(self.DeltaA ** 2)) ** 0.5
        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        self.Mdist_real = sum(self.Md) + sum(self.M_fbrine)
        self.Error_Mdist = abs(self.Mdist_real - self.Mdist)
        tot_error = self.SumSquareErrorArea + SumSquareErrorAreaph + self.Error_Mdist*2
        return tot_error


    def areahxcalculation_parallcross(self, x):
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT[i_effect] = x[i_effect]
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            if i_effect == 0:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xb
            else:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xff

        for i_effect in tools.itereffects(0, self.N-1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes[i_effect])
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N-1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
        # self.T_preh[self.N-1] = self.Tcw_in + self.DT_cond
        # for i_effect in range(self.N-2, -1, -1):
          # self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # if abs(self.T_preh[0] - (self.T[0] - self.DTT_preh)) > 0.05:
        #     for i_effect in tools.itereffects(0, self.N-1):
        #         self.DeltaT_ph[i_effect] = self.DeltaT_ph[i_effect] * ((self.T[0] - self.DTT_preh) - self.T_preh[self.N-1])/DeltaT_totph
        # for i_effect in range(self.N - 2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
             self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect-1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N - 1]

        for iEffect in range(0, self.N-1):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5
            if abs(self.T_preh[iEffect] - self.T_preh[iEffect+1]) < 0.05:
                self.DeltaT_ph[iEffect] = 0.5

        FeedFraction = self.Mfeed / self.N

        if Preheating_fractionated_feed:
            for i_effect in tools.itereffects(0, self.N):
                self.Mfeed_ph[i_effect] = FeedFraction
            for i_effect in tools.itereffects(0, self.N-1):
                self.DTLM[i_effect] = deltat_logmean_fract(i_effect, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[i_effect] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.Tcw_out) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
            if self.T_preh[self.N-1] > self.Tcw_out:
                self.DTLM[self.N-1] = deltat_logmean_fract(self.N-1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[self.N-1] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[self.N-1] + self.Tcw_out) / 2, self.Xf) * (
                                                 self.T_preh[self.N-1] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[self.N-1])
                self.alfa_cond[self.N-1] = self.M_cond_ph[self.N-1] / self.Mvap[self.N-1]
            else:
                self.M_cond_ph[self.N-1] = 0
                self.alfa_cond[self.N-1] = 0

        else:  # alternative way: all the feed is preheated in the last effect and sent to the previous ones,
            #  loosing in each effect a part = FeedFraction
            self.Mfeed_ph[0] = FeedFraction
            for i_effect in tools.itereffects(1, self.N):
                self.Mfeed_ph[i_effect] = self.Mfeed_ph[i_effect-1] + FeedFraction
            for i_effect in tools.itereffects(0, self.N-1):
                self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
                self.M_cond_ph[i_effect] = self.Mfeed_ph[i_effect] * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.T_preh[i_effect+1]) / 2, self.Xf) * (
                    self.T_preh[i_effect] - self.T_preh[i_effect+1]) / h2oprop.latent_heat(self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
            # if self.T_preh[self.N-1] > self.Tcw_out:
            #     self.DTLM[self.N-1] = deltat_logmean(self.N-1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
            #     self.M_cond_ph[self.N-1] = self.Mfeed_ph[self.N-1] * naclprops.cp_seawater(
            #         (self.T_preh[self.N-1] + self.Tcw_out) / 2, self.Xb) * (
            #                                    self.T_preh[self.N-1] - self.Tcw_out) / h2oprop.latent_heat(
            #         self.Tvsat_prime[self.N-1])
            #     self.alfa_cond[self.N-1] = self.M_cond_ph[self.N-1] / self.Mvap[self.N-1]
            # else:
            #     self.M_cond_ph[self.N-1] = 0
            #     self.alfa_cond[self.N-1] = 0

        # first effect calculations
        self.Mf[0] = FeedFraction
        self.Mb[0] = self.Mf[0] - self.Md[0]
        self.Xb[0] = self.Mf[0] * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mf[0] * naclprops.cp_seawater(
        #    (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0] + self.M_fb[0]
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mf[0], self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mf[0], self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond,
                               self.Mvap, self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
        # self.A_preh[0] = area_preh_2(0, self.Mfeed_ph, self.T_preh, self.T_preh[1], self.Tvsat_prime, self.DTLM, self.Xb)
        # self.A_preh[0] = area_preh_3(0, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # intermediate effects calculations
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.Mf[i_effect] = self.Mb[i_effect - 1] + FeedFraction
            self.Xff[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1])/self.Mf[i_effect]
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) -
            #                      FeedFraction * naclprops.cp_seawater((self.T[i_effect] + self.T_preh[i_effect]) / 2, self.Xb) * (self.T[i_effect] - self.T_preh[i_effect])) \
            #                     / h2oprop.latent_heat(self.T[i_effect])
            DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
                self.T[i_effect], self.Xb[i_effect])
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])
                                 + FeedFraction * (naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xf)
                                                   - naclprops.enthalpy_seawater(self.T[i_effect],self.Xb[i_effect]))) / \
                                (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) +
            #                      FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)
            #                      - self.Mb_g[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect]))\
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            # if self.Md[i_effect] > FeedFraction:
            #     self.Md[i_effect] = FeedFraction * 0.95
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) - self.M_fbrine[i_effect] * h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            #                      - self.Mb[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])
            #                      + self.Mb[i_effect - 1] * naclprops.enthalpy_seawater(self.T[i_effect - 1], self.Xb[i_effect - 1])
            #                      + FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)) \
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mf[i_effect] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mf[i_effect], self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
            # self.A_preh[i_effect] = area_preh_2(i_effect, self.Mfeed_ph, self.T_preh, self.T_preh[i_effect+1], self.Tvsat_prime, self.DTLM, self.Xb)
            # self.A_preh[i_effect] = area_preh_3(i_effect, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # last effect calculations
        self.alfa_cond[self.N - 1] = 0
        self.Mf[self.N - 1] = self.Mb[self.N - 2] + FeedFraction
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) -
        #                        FeedFraction * naclprops.cp_seawater((self.T[self.N - 1] + self.T_preh[self.N - 1]) / 2, self.Xb) * (self.T[self.N - 1] - self.T_preh[self.N - 1])) / \
        #                       h2oprop.latent_heat(self.T[self.N - 1])
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mvap[self.N-1] = mvap_effectbalance(self.N-1, self.Msteam, self.Tc[self.N-2], self.Mfeed, self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                       self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[self.N-1] = self.Mvap[self.N-1] - self.M_fbrine[self.N-1] - self.M_fb[self.N-1]
        self.Mb[self.N - 1] = self.Mf[self.N - 1] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mf[self.N - 1], self.Xf, self.T, self.T_preh,self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)
        self.A_av = np.sum(self.A_hx) / self.N
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        self.SumSquareErrorArea = (np.sum(self.DeltaA ** 2)) ** 0.5
        return self.SumSquareErrorArea

    def areaphcalculation_parallcross(self, z):
        for i_effect in tools.itereffects(0, self.N-1):
            self.DeltaT_ph[i_effect] = z[i_effect]

        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            if i_effect == 0:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xb
            else:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xff

        for i_effect in tools.itereffects(0, self.N-1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes[i_effect])
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N - 1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
            # self.T_preh[self.N-1] = self.Tcw_in + self.DT_cond
            # for i_effect in range(self.N-2, -1, -1):
            # self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # if abs(self.T_preh[0] - (self.T[0] - self.DTT_preh)) > 0.05:
        #     for i_effect in tools.itereffects(0, self.N-1):
        #         self.DeltaT_ph[i_effect] = self.DeltaT_ph[i_effect] * ((self.T[0] - self.DTT_preh) - self.T_preh[self.N-1])/DeltaT_totph
        # for i_effect in range(self.N - 2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
            self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect - 1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N - 1]

        for iEffect in range(0, self.N - 1):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5
            if abs(self.T_preh[iEffect] - self.T_preh[iEffect + 1]) < 0.05:
                self.DeltaT_ph[iEffect] = 0.5

        FeedFraction = self.Mfeed / self.N

        if Preheating_fractionated_feed:
            for i_effect in tools.itereffects(0, self.N):
                self.Mfeed_ph[i_effect] = FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean_fract(i_effect, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[i_effect] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.Tcw_out) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
            if self.T_preh[self.N - 1] > self.Tcw_out:
                self.DTLM[self.N - 1] = deltat_logmean_fract(self.N - 1, self.Tvsat_prime, self.T_preh,
                                                             self.Tcw_out)
                self.M_cond_ph[self.N - 1] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[self.N - 1] + self.Tcw_out) / 2, self.Xf) * (
                                                 self.T_preh[self.N - 1] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[self.N - 1])
                self.alfa_cond[self.N - 1] = self.M_cond_ph[self.N - 1] / self.Mvap[self.N - 1]
            else:
                self.M_cond_ph[self.N - 1] = 0
                self.alfa_cond[self.N - 1] = 0

        else:  # alternative way: all the feed is preheated in the last effect and sent to the previous ones,
            #  loosing in each effect a part = FeedFraction
            self.Mfeed_ph[0] = FeedFraction
            for i_effect in tools.itereffects(1, self.N):
                self.Mfeed_ph[i_effect] = self.Mfeed_ph[i_effect - 1] + FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
                self.M_cond_ph[i_effect] = self.Mfeed_ph[i_effect] * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.T_preh[
                                                   i_effect + 1]) / h2oprop.latent_heat(self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
                # if self.T_preh[self.N-1] > self.Tcw_out:
                #     self.DTLM[self.N-1] = deltat_logmean(self.N-1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                #     self.M_cond_ph[self.N-1] = self.Mfeed_ph[self.N-1] * naclprops.cp_seawater(
                #         (self.T_preh[self.N-1] + self.Tcw_out) / 2, self.Xb) * (
                #                                    self.T_preh[self.N-1] - self.Tcw_out) / h2oprop.latent_heat(
                #         self.Tvsat_prime[self.N-1])
                #     self.alfa_cond[self.N-1] = self.M_cond_ph[self.N-1] / self.Mvap[self.N-1]
                # else:
                #     self.M_cond_ph[self.N-1] = 0
                #     self.alfa_cond[self.N-1] = 0

        # first effect calculations
        self.Mf[0] = FeedFraction
        self.Mb[0] = self.Mf[0] - self.Md[0]
        self.Xb[0] = self.Mf[0] * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[
            0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(
            self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mf[0] * naclprops.cp_seawater(
        #    (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0] + self.M_fb[0]
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mf[0], self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mf[0], self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond,
                               self.Mvap, self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
        # self.A_preh[0] = area_preh_2(0, self.Mfeed_ph, self.T_preh, self.T_preh[1], self.Tvsat_prime, self.DTLM, self.Xb)
        # self.A_preh[0] = area_preh_3(0, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # intermediate effects calculations
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.Mf[i_effect] = self.Mb[i_effect - 1] + FeedFraction
            self.Xff[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mf[
                i_effect]
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1],
                                                                      self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) -
            #                      FeedFraction * naclprops.cp_seawater((self.T[i_effect] + self.T_preh[i_effect]) / 2, self.Xb) * (self.T[i_effect] - self.T_preh[i_effect])) \
            #                     / h2oprop.latent_heat(self.T[i_effect])
            DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
                self.T[i_effect], self.Xb[i_effect])
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])
                                 + FeedFraction * (naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xf)
                                                   - naclprops.enthalpy_seawater(self.T[i_effect],
                                                                                 self.Xb[i_effect]))) / \
                                (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) +
            #                      FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)
            #                      - self.Mb_g[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect]))\
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            # if self.Md[i_effect] > FeedFraction:
            #     self.Md[i_effect] = FeedFraction * 0.95
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) - self.M_fbrine[i_effect] * h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            #                      - self.Mb[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])
            #                      + self.Mb[i_effect - 1] * naclprops.enthalpy_seawater(self.T[i_effect - 1], self.Xb[i_effect - 1])
            #                      + FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)) \
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mf[i_effect] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mb[
                i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mf[i_effect], self.Xf, self.T, self.T_preh, self.Md,
                                          self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
            # self.A_preh[i_effect] = area_preh_2(i_effect, self.Mfeed_ph, self.T_preh, self.T_preh[i_effect+1], self.Tvsat_prime, self.DTLM, self.Xb)
            # self.A_preh[i_effect] = area_preh_3(i_effect, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # last effect calculations
        self.alfa_cond[self.N - 1] = 0
        self.Mf[self.N - 1] = self.Mb[self.N - 2] + FeedFraction
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) -
        #                        FeedFraction * naclprops.cp_seawater((self.T[self.N - 1] + self.T_preh[self.N - 1]) / 2, self.Xb) * (self.T[self.N - 1] - self.T_preh[self.N - 1])) / \
        #                       h2oprop.latent_heat(self.T[self.N - 1])
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mvap[self.N - 1] = mvap_effectbalance(self.N - 1, self.Msteam, self.Tc[self.N - 2], self.Mfeed,
                                                   self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                                   self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf,
                                                   FeedFraction)
        self.Md[self.N - 1] = self.Mvap[self.N - 1] - self.M_fbrine[self.N - 1] - self.M_fb[self.N - 1]
        self.Mb[self.N - 1] = self.Mf[self.N - 1] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mf[self.N - 1], self.Xf, self.T, self.T_preh, self.Md,
                                        self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)

        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        return SumSquareErrorAreaph

    def steamcalculation_parallcross(self, y):
        self.Ms = y
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            if i_effect == 0:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xb
            else:
                self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                    deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                                  (self.Xf + self.Xb[i_effect]) / 2, self.config)  # era Xff

        for i_effect in tools.itereffects(0, self.N-1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes[i_effect])
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N - 1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
            # self.T_preh[self.N-1] = self.Tcw_in + self.DT_cond
            # for i_effect in range(self.N-2, -1, -1):
            # self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # if abs(self.T_preh[0] - (self.T[0] - self.DTT_preh)) > 0.05:
        #     for i_effect in tools.itereffects(0, self.N-1):
        #         self.DeltaT_ph[i_effect] = self.DeltaT_ph[i_effect] * ((self.T[0] - self.DTT_preh) - self.T_preh[self.N-1])/DeltaT_totph
        # for i_effect in range(self.N - 2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
            self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect - 1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N - 1]

        for iEffect in range(0, self.N - 1):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - 5
            if abs(self.T_preh[iEffect] - self.T_preh[iEffect + 1]) < 0.05:
                self.DeltaT_ph[iEffect] = 0.5

        FeedFraction = self.Mfeed / self.N

        if Preheating_fractionated_feed:
            for i_effect in tools.itereffects(0, self.N):
                self.Mfeed_ph[i_effect] = FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean_fract(i_effect, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                self.M_cond_ph[i_effect] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.Tcw_out) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
            if self.T_preh[self.N - 1] > self.Tcw_out:
                self.DTLM[self.N - 1] = deltat_logmean_fract(self.N - 1, self.Tvsat_prime, self.T_preh,
                                                             self.Tcw_out)
                self.M_cond_ph[self.N - 1] = FeedFraction * naclprops.cp_seawater(
                    (self.T_preh[self.N - 1] + self.Tcw_out) / 2, self.Xf) * (
                                                 self.T_preh[self.N - 1] - self.Tcw_out) / h2oprop.latent_heat(
                    self.Tvsat_prime[self.N - 1])
                self.alfa_cond[self.N - 1] = self.M_cond_ph[self.N - 1] / self.Mvap[self.N - 1]
            else:
                self.M_cond_ph[self.N - 1] = 0
                self.alfa_cond[self.N - 1] = 0

        else:  # alternative way: all the feed is preheated in the last effect and sent to the previous ones,
            #  loosing in each effect a part = FeedFraction
            self.Mfeed_ph[0] = FeedFraction
            for i_effect in tools.itereffects(1, self.N):
                self.Mfeed_ph[i_effect] = self.Mfeed_ph[i_effect - 1] + FeedFraction
            for i_effect in tools.itereffects(0, self.N - 1):
                self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
                self.M_cond_ph[i_effect] = self.Mfeed_ph[i_effect] * naclprops.cp_seawater(
                    (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                                               self.T_preh[i_effect] - self.T_preh[
                                                   i_effect + 1]) / h2oprop.latent_heat(self.Tvsat_prime[i_effect])
                self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
                # if self.T_preh[self.N-1] > self.Tcw_out:
                #     self.DTLM[self.N-1] = deltat_logmean(self.N-1, self.Tvsat_prime, self.T_preh, self.Tcw_out)
                #     self.M_cond_ph[self.N-1] = self.Mfeed_ph[self.N-1] * naclprops.cp_seawater(
                #         (self.T_preh[self.N-1] + self.Tcw_out) / 2, self.Xb) * (
                #                                    self.T_preh[self.N-1] - self.Tcw_out) / h2oprop.latent_heat(
                #         self.Tvsat_prime[self.N-1])
                #     self.alfa_cond[self.N-1] = self.M_cond_ph[self.N-1] / self.Mvap[self.N-1]
                # else:
                #     self.M_cond_ph[self.N-1] = 0
                #     self.alfa_cond[self.N-1] = 0

        # first effect calculations
        self.Mf[0] = FeedFraction
        self.Mb[0] = self.Mf[0] - self.Md[0]
        self.Xb[0] = self.Mf[0] * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[
            0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(
            self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mf[0] * naclprops.cp_seawater(
        #    (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0] + self.M_fb[0]
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mf[0], self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf, FeedFraction)
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mf[0], self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond,
                               self.Mvap, self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
        # self.A_preh[0] = area_preh_2(0, self.Mfeed_ph, self.T_preh, self.T_preh[1], self.Tvsat_prime, self.DTLM, self.Xb)
        # self.A_preh[0] = area_preh_3(0, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # intermediate effects calculations
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.Xff[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mf[
                i_effect]
            self.Mf[i_effect] = self.Mb[i_effect - 1] + FeedFraction
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1],
                                                                      self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) -
            #                      FeedFraction * naclprops.cp_seawater((self.T[i_effect] + self.T_preh[i_effect]) / 2, self.Xb) * (self.T[i_effect] - self.T_preh[i_effect])) \
            #                     / h2oprop.latent_heat(self.T[i_effect])
            DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
                self.T[i_effect], self.Xb[i_effect])
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])
                                 + FeedFraction * (naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xf)
                                                   - naclprops.enthalpy_seawater(self.T[i_effect],
                                                                                 self.Xb[i_effect]))) / \
                                (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) +
            #                      FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)
            #                      - self.Mb_g[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect]))\
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            # if self.Md[i_effect] > FeedFraction:
            #     self.Md[i_effect] = FeedFraction * 0.95
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1]) - self.M_fbrine[i_effect] * h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            #                      - self.Mb[i_effect] * naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])
            #                      + self.Mb[i_effect - 1] * naclprops.enthalpy_seawater(self.T[i_effect - 1], self.Xb[i_effect - 1])
            #                      + FeedFraction * naclprops.enthalpy_seawater(self.T_preh[i_effect], self.Xb)) \
            #                     / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect])
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mf[i_effect] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = (FeedFraction * self.Xf + self.Mb[i_effect - 1] * self.Xb[i_effect - 1]) / self.Mb[
                i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mf[i_effect], self.Xf, self.T, self.T_preh, self.Md,
                                          self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
            # self.A_preh[i_effect] = area_preh_2(i_effect, self.Mfeed_ph, self.T_preh, self.T_preh[i_effect+1], self.Tvsat_prime, self.DTLM, self.Xb)
            # self.A_preh[i_effect] = area_preh_3(i_effect, self.Mfeed_ph, self.DeltaT_ph, self.T_preh, self.Tvsat_prime, self.Xb)
        # last effect calculations
        self.alfa_cond[self.N - 1] = 0
        self.Mf[self.N - 1] = self.Mb[self.N - 2] + FeedFraction
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) -
        #                        FeedFraction * naclprops.cp_seawater((self.T[self.N - 1] + self.T_preh[self.N - 1]) / 2, self.Xb) * (self.T[self.N - 1] - self.T_preh[self.N - 1])) / \
        #                       h2oprop.latent_heat(self.T[self.N - 1])
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Mvap[self.N - 1] = mvap_effectbalance(self.N - 1, self.Msteam, self.Tc[self.N - 2], self.Mfeed,
                                                   self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                                   self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf,
                                                   FeedFraction)
        self.Md[self.N - 1] = self.Mvap[self.N - 1] - self.M_fbrine[self.N - 1] - self.M_fb[self.N - 1]
        self.Mb[self.N - 1] = self.Mf[self.N - 1] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mf[self.N - 1], self.Xf, self.T, self.T_preh, self.Md,
                                        self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)

        self.Mdist_real = sum(self.Md) + sum(self.M_fbrine)
        self.Error_Mbrine = abs(self.Mb[self.N - 1] - self.Mbrine)
        self.Error_Mdist = abs(self.Mdist_real - self.Mdist)
        return self.Error_Mdist


