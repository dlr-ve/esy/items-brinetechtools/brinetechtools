from argparse import ArgumentParser
from sys import argv
import os
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from common_and_tools import save_resultlists
from common_and_tools import model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd
from common_and_tools import model_crystallizer_double_dol_maghyd_self_calsul
from common_and_tools import model_crystallizer_double_dol_maghyd_sodcar_calcar
from common_and_tools import model_crystallizer_single_sodhyd_maghyd
from common_and_tools import model_crystallizer_single_self_sodchl
from common_and_tools import model_crystallizer_single_sodcar_calcar


def driver(model, parameterconfigfilepath, Cfeed_filepath, Qfeed, Mfeed, electricity_cost,
           heat_cost, T_in, output_directory, output_namefile, output_filename, output_yamlname):
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)
    with open(Cfeed_filepath, "r") as file_conc:
        Cfeed = yaml.load(file_conc, Loader=Loader)

    if "crystallizer_nacl_model" in str(model):
        result_lists = model(parameter_config, Cfeed, Mfeed, electricity_cost, heat_cost, T_in)
    else:
        result_lists = model(parameter_config, Cfeed, Qfeed, electricity_cost)

    with open(output_directory + '/' + output_yamlname, 'w') as file:
        documents = yaml.dump(result_lists, file)
    # TODO: code for saving output (excel and yaml) should be part of save_resultlists module

    for result_list in result_lists:
        print(result_list)

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
        'crystallizer_double_sodhyd_maghyd_sodhyd_calhyd': model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd,
        'crystallizer_double_dol_maghyd_self_calsul': model_crystallizer_double_dol_maghyd_self_calsul,
        'crystallizer_double_dol_maghyd_sodcar_calcar': model_crystallizer_double_dol_maghyd_sodcar_calcar,
        'crystallizer_single_sodhyd_maghyd': model_crystallizer_single_sodhyd_maghyd,
        'crystallizer_single_self_sodchl': model_crystallizer_single_self_sodchl,
        'crystallizer_single_sodcar_calcar': model_crystallizer_single_sodcar_calcar
    }
    
    # Create top-level parcer
    parser = ArgumentParser(prog='brinetech_crystallizer', description='Precipitation simulation tool with different\
         models depending on precipitants and reactants')
    parser.add_argument('-m', '--model', choices=models.keys())
    parser.add_argument('-e', '--electricity-cost', type=float, default=0.1035, help="Cost of electricity in $/kWh_el")
    subparsers = parser.add_subparsers(help='sub-command help')
    
    # create a parser for all crystallizers, except NaCl cryst
    parser_crystallizer = subparsers.add_parser('crystallizer', help='Crystallizer help for non-nacl crystallizers')
    parser_crystallizer.add_argument('-q', '--feed-q', type=float, required=True, help="Feed flux Q in m³/h")

    # create a parser for NaCl crystallizer
    parser_nacl = subparsers.add_parser('nacl_crystallizer', help='Crystallizer help for nacl crystallizer')
    parser_nacl.add_argument('-fm', '--feed-m', type=float, required=True, help="Feed mass flow rate FM in kg/h")
    parser_nacl.add_argument('-he', '--heat-cost', type=float, default=0.01, required=True, help="Cost of heat in $/kWh_th")
    parser_nacl.add_argument('-t', '--t-n-design', type=float, default=25, required=True, help="Tn_design temperature in °C")
    
    args = parser.parse_args(argv)

    ## getting paths for parameters.yml and concentration_ion.yml for each tool:
    main_tool_name = ""
    sub_tool_name = ""
    model_name = args.model
    main_tool_name = model_name.split("_", 1)[0]
    # parameters file path
    param_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'parameters.yml')
    
    # concentration file path, output directory
    sub_tool_name = model_name
    c_feed_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
            sub_tool_name, 'concentration_ion.yml')
    output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name,
            sub_tool_name)
    
    # output file name
    if sub_tool_name =='crystallizer_single_self_sodchl':
        common_string = f"results_mfeed_{int(round(args.feed_m, 0))}"
        output_filename = common_string + ".xls"
        output_yamlname = common_string + ".yaml"
    else:
        common_string = f"results_qfeed_{int(round(args.feed_q, 0))}"
        output_filename = common_string + ".xls"
        output_yamlname = common_string + ".yaml"
    
    # name of the dat. file to store output file name:
    output_namefile = "fila_name.dat"

    if sub_tool_name == 'crystallizer_single_self_sodchl':
        return driver(
        model=models[args.model],
        parameterconfigfilepath=param_path,
        Cfeed_filepath=c_feed_path,
        Qfeed=None,
        Mfeed=args.feed_m,
        electricity_cost=args.electricity_cost,
        heat_cost=args.heat_cost,
        T_in=args.t_n_design,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=output_filename,
        output_yamlname = output_yamlname
            )
    else:
        return driver(
        model=models[args.model],
        parameterconfigfilepath=param_path,
        Cfeed_filepath=c_feed_path,
        Qfeed=args.feed_q,
        Mfeed=None,
        electricity_cost=args.electricity_cost,
        heat_cost=None,
        T_in=None,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=output_filename,
        output_yamlname = output_yamlname
        )

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()

"""
## model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd
python main_crystallizer.py -m "crystallizer_double_sodhyd_maghyd_sodhyd_calhyd" -e 0.3 "crystallizer" -q 0.02777778
python main_crystallizer.py -m "crystallizer_double_sodhyd_maghyd_sodhyd_calhyd" -e 0.06 "crystallizer" -q 10  ## feed in m3/h (EW nexus, with unit of q changed from m3/s to m3/h)

## model_crystallizer_double_dol_maghyd_self_calsul
python main_crystallizer.py -m "crystallizer_double_dol_maghyd_self_calsul" -e 0.1035 "crystallizer" -q 0.000048333

## model_crystallizer_double_dol_maghyd_sodcar_calcar
python main_crystallizer.py -m "crystallizer_double_dol_maghyd_sodcar_calcar" -e 0.06 "crystallizer" -q 0.011666666

## model_crystallizer_single_sodhyd_maghyd
python main_crystallizer.py -m "crystallizer_single_sodhyd_maghyd" -e 0.06 "crystallizer" -q 0.000277777
python main_crystallizer.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 10000 -he 0.01 -t 39 (EW nexus)
python main_crystallizer.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 1298294 -he 0.01 -t 39 (EW nexus)
python main_crystallizer.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 1681667.411 -he 0.01 -t 39 (EW nexus: similar as Nicolay Las Palmas Saudi plant. Around 35 MLD using a density of 1264.66 kg/m3)

## model_crystallizer_single_self_sodchl
python main_crystallizer.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 7668.8 -he 0.01 -t 25

## model_crystallizer_single_sodcar_calcar
python main_crystallizer.py -m "crystallizer_single_sodcar_calcar" -e 0.06 "crystallizer" -q 2.777777e-4
"""