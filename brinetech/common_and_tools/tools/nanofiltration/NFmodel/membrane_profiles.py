import numpy as np
import yaml
import operator
from . import linear_system_coeff as ls
from . import mass_transfer_coeff as mt

# with open('./parameters.yml', "r") as file_params:
#     parameter_config = yaml.load(file_params)

def solvent_flux_HP_params(deltaP, eta, config):  # deltaP in bar, eta in Pa s, Jv in m/s
    Jv = (config['rp'] * 1e-9)**2 * ((deltaP) * 1e5) / (8 * eta * config['thickness_membrane'] * 1e-6)
    return Jv

def solvent_flux_HP(deltaP, config):  # deltaP in bar, eta in Pa s, Jv in m/s
    Jv = config['perm_membr_TS80'] / 3600 * 1e-3 * deltaP
    return Jv


def DW_dielectric_exclusion(z, radius, epsilon_b, epsilon_p, config):
    DW = z**2 * config['e0']**2 / (8 * np.pi * config['epsilon_0'] * radius * 1e-9) * (1 / epsilon_p - 1 / epsilon_b)
    return DW

class DSPMDEsolver:
    def __init__(self, C_b, C_p, C_m, C_bm, psi_m, psi_p, csi, phi_steric, phi_DE, Jv, config, variables, N_nodes, dx,
                 deltax, k_d, k_c_pore, k_c_prime, k_c_bulk, D_p, D_inf, l, z, ions, Delta_Cp, Delta_Cm, Cm_first, Cp_first,
                 psi_p_first, psi_m_first, R_ion, sol, res, res_bool, Ncomp, pos):
        self.C_b = C_b
        self.C_m = C_m
        self.C_p = C_p
        self.C_bm = C_bm
        self.psi_m = psi_m
        self.psi_p = psi_p
        self.csi = csi
        self.phi_steric = phi_steric
        self.phi_DE = phi_DE
        self.Jv = Jv
        self.config = config
        self.variables = variables
        self.N_nodes = N_nodes
        self.dx = dx
        self.deltax = deltax
        self.k_d = k_d
        self.k_c_pore = k_c_pore
        self.k_c_prime = k_c_prime
        self.k_c_bulk = k_c_bulk
        self.D_p = D_p
        self.D_inf = D_inf
        self.l = l
        self.z = z
        self.ions = ions
        self.Delta_Cp = Delta_Cp
        self.Delta_Cm = Delta_Cm
        self.Cm_first = Cm_first
        self.Cp_first = Cp_first
        self.psi_p_first = psi_p_first
        self.psi_m_first = psi_m_first
        self.R_ion = R_ion
        self.sol = sol
        self.res = res
        self.res_bool = res_bool
        self.Ncomp = Ncomp
        self.pos = pos
        self.ordered_ions = [key for (key, value) in sorted(self.pos.items(), key=operator.itemgetter(1))]
        self.error_ENP_tot = 1000
        self.error_elecroneutrality = 1000
        self.error_interface = 1000
        self.error_Cp = 1000
        self.error_boundarylayer = 1000
        self.v_FBL = {}
        self.v_FMI = {}
        self.v_ENP = {}
        self.v_MPI = {}
        for key in self.ions:
            self.v_FBL[key] = np.zeros(len(self.variables))
            self.v_FMI[key] = np.zeros(len(self.variables))
            self.v_ENP[key] = {}
            for j in range(self.N_nodes - 1):
                self.v_ENP[key][j] = np.zeros(len(self.variables))
            self.v_MPI[key] = np.zeros(len(self.variables))
        self.v_FBL['EN'] = np.zeros(len(self.variables))  # electroneutrality condition
        self.v_FMI['EN'] = np.zeros(len(self.variables))
        self.v_MPI['EN'] = np.zeros(len(self.variables))
        self.v_ENP['EN'] = {}
        for j in range(self.N_nodes - 1):
            self.v_ENP['EN'][j] = np.zeros(len(self.variables))
        self.A_FBL = {}
        self.B_FBL = {}
        self.C_FBL = {}
        self.D_FBL = {}
        self.A_FMI = {}
        self.B_FMI = {}
        self.A_ENP = {}
        self.B_ENP = {}
        self.C_ENP = {}
        self.D_ENP = {}
        self.E_ENP = {}
        self.F_ENP = {}
        self.A_MPI = {}
        self.B_MPI = {}
        self.C_MPI = {}
        for key in self.ions:
            self.A_ENP[key] = np.zeros(len(range(self.N_nodes)))
            self.B_ENP[key] = np.zeros(len(range(self.N_nodes)))
            self.C_ENP[key] = np.zeros(len(range(self.N_nodes)))
            self.D_ENP[key] = np.zeros(len(range(self.N_nodes)))
            self.E_ENP[key] = np.zeros(len(range(self.N_nodes)))
            self.F_ENP[key] = np.zeros(len(range(self.N_nodes)))
        self.v_noti = []
        self.matrix_equations = []
        self.F_RT = self.config['F'] / (self.config['R'] * self.config['T'])
        self.gamma_permeate = {}
        self.gamma_pore_p = {}
        self.gamma_pore_b = {}
        self.gamma_bm = {}
        self.I_bm = 0
        self.I_permeate = 0
        self.I_pore_b = 0
        self.I_pore_p = 0

    def iterative_solving_procedure_DSPMDE(self):
        i = 0
        while self.error_ENP_tot > 1e-4 or self.error_interface > 1e-4\
                or self.error_elecroneutrality > 1e-4 or self.error_boundarylayer > 1e-4:
            self.psi_m_first = self.psi_m * 1
            self.psi_p_first = self.psi_p * 1
            for key in self.ions:
                self.Cm_first[key] = self.C_m[key] * 1
                self.Cp_first[key] = self.C_p[key] * 1
            '''estimating the coefficients'''
            for key in self.ions:
                self.A_FBL[key] = ls.coeff_FBL_A(self.k_c_prime[key], self.Jv)
                self.B_FBL[key] = ls.coeff_FBL_B(self.Jv)
                self.C_FBL[key] = ls.coeff_FBL_C(self.z[key],self.C_bm[key], self.D_inf[key], self.config)
                self.D_FBL[key] = ls.coeff_FBL_D(self.k_c_prime[key], self.C_b[key])
                self.A_FMI[key] = ls.coeff_FMI_A(self.z[key], self.C_bm[key], self.phi_steric[key], self.phi_DE[key], self.psi_m, self.config)
                self.B_FMI[key] = ls.coeff_FMI_B(self.z[key], self.C_bm[key], self.phi_steric[key], self.phi_DE[key], self.psi_m, self.config)
                self.A_MPI[key] = ls.coeff_MPI_A(self.z[key], self.phi_steric[key], self.phi_DE[key], self.psi_m, self.psi_p, self.N_nodes, self.config)
                self.B_MPI[key] = ls.coeff_MPI_B(self.z[key], self.phi_steric[key], self.phi_DE[key], self.psi_m,
                                                 self.psi_p, self.N_nodes, self.C_p[key], self.config)
                self.C_MPI[key] = ls.coeff_MPI_C(self.z[key], self.phi_steric[key], self.phi_DE[key], self.psi_m,
                                                 self.psi_p, self.N_nodes, self.C_p[key], self.config)
                for j in range(self.N_nodes-1):
                    self.A_ENP[key][j] = ls.coeff_ENP_A(j, self.D_p[key], self.dx, self.k_c_pore[key], self.Jv, self.z[key], self.psi_m, self.config)
                    self.B_ENP[key][j] = ls.coeff_ENP_B(j, self.D_p[key], self.dx, self.k_c_pore[key], self.Jv, self.z[key], self.psi_m, self.config)
                    self.C_ENP[key][j] = ls.coeff_ENP_C(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.config)
                    self.D_ENP[key][j] = ls.coeff_ENP_D(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.config)
                    self.E_ENP[key][j] = ls.coeff_ENP_E(self.Jv)
                    self.F_ENP[key][j] = ls.coeff_ENP_F(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.psi_m, self.config)

            '''linear system'''
            #FBL equations: A * C_bm + B * Cp + C * csi = D
            # sum (z * C_bm) = 0
            for key in self.ions:
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key])] = self.A_FBL[key]
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes) + self.pos[key]] = self.B_FBL[key]
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes + 2*self.Ncomp)] = self.C_FBL[key]
                self.v_FBL['EN'][int((self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key])] = self.z[key]
                # FMI equations: Cm[0] + A * psi_m[0] = B (1, 2, 3)
                # sum(z * Cm[0]) = -Cx
                self.v_FMI[key][self.pos[key] * self.N_nodes] = 1
                self.v_FMI[key][int(self.Ncomp*self.N_nodes)] = self.A_FMI[key]
                self.v_FMI['EN'][self.pos[key] * self.N_nodes] = self.z[key]
                # ENP equations: A * Cm[j] + B * Cm[j+1] + C * psi_m[j] + D * psi_m[j+1] + E * Cp = F (1, 2, 3)
                # sum(z * Cm[j+1] = -Cx
                for j in range(self.N_nodes-1):
                    self.v_ENP[key][j][self.pos[key] * self.N_nodes + j] = self.A_ENP[key][j]
                    self.v_ENP[key][j][self.pos[key] * self.N_nodes + j+1] = self.B_ENP[key][j]
                    self.v_ENP[key][j][int(self.Ncomp * self.N_nodes + j)] = self.C_ENP[key][j]
                    self.v_ENP[key][j][int(self.Ncomp * self.N_nodes + j + 1)] = self.D_ENP[key][j]
                    self.v_ENP[key][j][int((self.Ncomp+1)*self.N_nodes + self.pos[key])] = self.E_ENP[key][j]
                    self.v_ENP['EN'][j][int(self.pos[key] * self.N_nodes + j + 1)] = self.z[key]
                # MPI equations: Cm[N_nodes-1] - A * Cp + B * psi_m[N_nodes-1] - B * psi_p = C (1, 2, 3)
                # sum(z * Cp) = 0
                self.v_MPI[key][int((self.pos[key] + 1) * self.N_nodes - 1)] = 1
                self.v_MPI[key][int((self.Ncomp + 1) * self.N_nodes - 1)] = self.B_MPI[key]
                self.v_MPI[key][int((self.Ncomp + 1) * self.N_nodes + self.pos[key])] = - self.A_MPI[key]
                self.v_MPI[key][-1] = - self.B_MPI[key]
                self.v_MPI['EN'][int((self.Ncomp+1)*self.N_nodes + self.pos[key])] = self.z[key]

            self.v_noti = []
            for key in self.ordered_ions:
                self.v_noti.append(self.D_FBL[key])
            self.v_noti.append(0)
            for key in self.ordered_ions:
                self.v_noti.append(self.B_FMI[key])
            self.v_noti.append(- self.config['charge_density'])
            for key in self.ordered_ions:
                for j in range(self.N_nodes-1):
                    self.v_noti.append(self.F_ENP[key][j])
            for j in range(self.N_nodes-1):
                self.v_noti.append(- self.config['charge_density'])
            for key in self.ordered_ions:
                self.v_noti.append(self.C_MPI[key])
            self.v_noti.append(0)
            # print(v_noti, len(v_noti))
            self.matrix_equations = []
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_FBL[key])
            self.matrix_equations.append(self.v_FBL['EN'])
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_FMI[key])
            self.matrix_equations.append(self.v_FMI['EN'])
            for key in self.ordered_ions:
                for j in range(self.N_nodes-1):
                    self.matrix_equations.append(self.v_ENP[key][j])
            for j in range(self.N_nodes-1):
                self.matrix_equations.append(self.v_ENP['EN'][j])
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_MPI[key])
            self.matrix_equations.append(self.v_MPI['EN'])

            self.matrix_equations = np.array(self.matrix_equations)

            self.sol = np.linalg.solve(self.matrix_equations, self.v_noti)
            # print(sol)
            for key in self.ions:
                self.C_m[key] = self.sol[self.pos[key] * self.N_nodes : (self.pos[key] + 1) * self.N_nodes]
            self.psi_m = self.sol[self.Ncomp * self.N_nodes: (self.Ncomp+1) * self.N_nodes]
            for key in self.ions:
                self.C_p[key] = self.sol[(self.Ncomp+1) * self.N_nodes + self.pos[key]]
                self.C_bm[key] = self.sol[(self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key]]
            self.csi = self.sol[(self.Ncomp + 1) * self.N_nodes + 2 * self.Ncomp]
            self.psi_p = self.sol[-1]
            # print(C_m)
            # print(C_p)
            # print(psi_m, psi_p)
            self.res = np.dot(self.matrix_equations, self.sol) - self.v_noti
            self.res_bool = np.allclose(np.dot(self.matrix_equations, self.sol), self.v_noti)
            # print('verifica sistema lineare', np.sum(self.res), self.res_bool)

            '''under-relaxing the solutions'''
            self.psi_m = self.psi_m_first + self.config['eta_rel_pot'] * (self.psi_m - self.psi_m_first)
            self.psi_p = self.psi_p_first + self.config['eta_rel_pot'] * (self.psi_p - self.psi_p_first)
            error_Cp_square = 0
            for key in self.ions:
                self.Delta_Cp[key] = np.minimum(abs(self.Cp_first[key] / (self.C_p[key] - self.Cp_first[key])), 1) * \
                                     (self.C_p[key] - self.Cp_first[key]) / self.Cp_first[key]
                self.C_p[key] = self.Cp_first[key] * (1 + self.config['eta_rel_conc'] * self.Delta_Cp[key])
                for j in range(self.N_nodes):
                    self.Delta_Cm[key][j] = np.minimum(abs(self.Cm_first[key][j] / (self.C_m[key][j] - self.Cm_first[key][j])), 1)\
                                            * (self.C_m[key][j] - self.Cm_first[key][j]) / self.Cm_first[key][j]
                    self.C_m[key][j] = self.Cm_first[key][j] * (1 + self.config['eta_rel_conc'] * self.Delta_Cm[key][j])
                    error_Cp_square =+ (self.C_p[key] - self.Cp_first[key])**2
            self.error_Cp = np.sqrt(error_Cp_square)
            self.error_ENP_ion = {}
            for key in self.ions:
                self.error_ENP_ion[key] = []
            self.error_elecroneutrality = 0
            self.error_ENP_tot = 0
            self.error_interface = 0
            self.error_boundarylayer = 0
            for key in self.ions:
                for j in range(self.N_nodes - 1):
                    self.error_ENP_ion[key].append(np.abs(-self.D_p[key] * (self.C_m[key][j+1] - self.C_m[key][j]) / self.dx - 0.5 * self.z[key] *
                                                    (self.C_m[key][j+1] + self.C_m[key][j]) * self.D_p[key] * self.F_RT * (self.psi_m[j+1] - self.psi_m[j]) / self.dx +
                                                    0.5 * self.k_c_pore[key] * (self.C_m[key][j + 1] + self.C_m[key][j]) * self.Jv - self.Jv * self.C_p[key]))
                self.error_ENP_tot += (np.sum(self.error_ENP_ion[key]) / (np.abs(self.Jv * self.C_p[key])))

            for key in self.ions:
                self.error_interface += abs(self.C_m[key][0] / (self.C_bm[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * self.psi_m[0]) * self.phi_DE[key]) - 1) +\
                              abs(self.C_m[key][self.N_nodes-1] / (self.C_p[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * (self.psi_m[self.N_nodes-1] - self.psi_p))
                                                                   * self.phi_DE[key]) - 1)
                self.error_boundarylayer += abs(-self.k_c_prime[key] * (self.C_bm[key] - self.C_b[key]) + self.Jv * self.C_bm[key] - self.z[key] * self.C_bm[key] *
                                              self.D_inf[key] * self.F_RT * self.csi - self.Jv * self.C_p[key]) / abs(self.Jv * self.C_p[key])
            charge_m_elem = 0
            charge_m = 0
            charge_p = 0
            charge_p_abs = 0
            for j in range(self.N_nodes-1):
                charge_m_elem = 0
                for key in self.ions:
                    charge_m_elem += self.z[key] * self.C_m[key][j]
                charge_m += abs(charge_m_elem + self.config['charge_density'])
            for key in self.ions:
                charge_p += self.z[key] * self.C_p[key]
                charge_p_abs += abs(self.z[key] * self.C_p[key])
            charge_p = abs(charge_p)
            self.error_elecroneutrality = charge_m + charge_p / (abs(self.config['charge_density']) + charge_p_abs)
            # print('difference Cp', self.error_Cp)

            i += 1
            if i >= 500:
                print('too high number of iteration')
                break
        # print(self.error_ENP_tot, self.error_interface, self.error_elecroneutrality, self.error_boundarylayer)
        #
        for key in self.ions:
            self.R_ion[key] = (self.C_b[key] - self.C_p[key]) / self.C_b[key]
        # print('{} iterations'.format(i-1))

    def iterative_solving_procedure_DSPMDE_activitycoeff(self):
        i = 0
        while self.error_ENP_tot > 1e-4 or self.error_interface > 1e-4\
                or self.error_elecroneutrality > 1e-4 or self.error_boundarylayer > 1e-4:
            self.psi_m_first = self.psi_m * 1
            self.psi_p_first = self.psi_p * 1
            for key in self.ions:
                self.Cm_first[key] = self.C_m[key] * 1
                self.Cp_first[key] = self.C_p[key] * 1
            '''estimating the coefficients'''
            self.I_bm = 0
            self.I_permeate = 0
            self.I_pore_b = 0
            self.I_pore_p = 0
            for key in self.ions:
                self.I_bm += self.z[key]**2 * self.C_bm[key]
                self.I_permeate += self.z[key]**2 * self.C_p[key]
                self.I_pore_b += self.z[key]**2 * self.C_m[key][0]
                self.I_pore_p += self.z[key]**2 * self.C_m[key][self.N_nodes-1]
            self.I_bm = self.I_bm * 0.5
            self.I_permeate = self.I_permeate * 0.5
            self.I_pore_b = self.I_pore_b * 0.5
            self.I_pore_p = self.I_pore_p * 0.5
            for key in self.ions:
                self.gamma_bm[key] = mt.activity_coefficient(mt.A_coefficient_bulk(self.config['T'], self.config), self.z[key], self.I_bm)
                self.gamma_pore_b[key] = mt.activity_coefficient(mt.A_coefficient_pore(self.config['T'], self.config), self.z[key], self.I_pore_b)
                self.gamma_permeate[key] = mt.activity_coefficient(mt.A_coefficient_bulk(self.config['T'], self.config), self.z[key], self.I_permeate)
                self.gamma_pore_p[key] = mt.activity_coefficient(mt.A_coefficient_pore(self.config['T'], self.config), self.z[key], self.I_pore_p)
                self.A_FBL[key] = ls.coeff_FBL_A(self.k_c_prime[key], self.Jv)
                self.B_FBL[key] = ls.coeff_FBL_B(self.Jv)
                self.C_FBL[key] = ls.coeff_FBL_C(self.z[key],self.C_bm[key], self.D_inf[key], self.config)
                self.D_FBL[key] = ls.coeff_FBL_D(self.k_c_prime[key], self.C_b[key])
                self.A_FMI[key] = ls.coeff_FMI_A_activitycoeff(self.z[key], self.C_bm[key], self.phi_steric[key],
                                                               self.phi_DE[key], self.psi_m, self.config, self.gamma_bm[key])
                self.B_FMI[key] = ls.coeff_FMI_B_activitycoeff(self.z[key], self.C_bm[key], self.phi_steric[key],
                                                               self.phi_DE[key], self.psi_m, self.config, self.gamma_bm[key])
                self.A_MPI[key] = ls.coeff_MPI_A_activitycoeff(self.z[key], self.phi_steric[key], self.phi_DE[key],
                                                               self.psi_m, self.psi_p, self.N_nodes, self.config, self.gamma_permeate[key])
                self.B_MPI[key] = ls.coeff_MPI_B_activitycoeff(self.z[key], self.phi_steric[key], self.phi_DE[key], self.psi_m,
                                                 self.psi_p, self.N_nodes, self.C_p[key], self.config, self.gamma_permeate[key])
                self.C_MPI[key] = ls.coeff_MPI_C_activitycoeff(self.z[key], self.phi_steric[key], self.phi_DE[key], self.psi_m,
                                                 self.psi_p, self.N_nodes, self.C_p[key], self.config, self.gamma_permeate[key])
                for j in range(self.N_nodes-1):
                    self.A_ENP[key][j] = ls.coeff_ENP_A(j, self.D_p[key], self.dx, self.k_c_pore[key], self.Jv, self.z[key], self.psi_m, self.config)
                    self.B_ENP[key][j] = ls.coeff_ENP_B(j, self.D_p[key], self.dx, self.k_c_pore[key], self.Jv, self.z[key], self.psi_m, self.config)
                    self.C_ENP[key][j] = ls.coeff_ENP_C(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.config)
                    self.D_ENP[key][j] = ls.coeff_ENP_D(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.config)
                    self.E_ENP[key][j] = ls.coeff_ENP_E(self.Jv)
                    self.F_ENP[key][j] = ls.coeff_ENP_F(j, self.D_p[key], self.dx, self.z[key], self.C_m[key], self.psi_m, self.config)

            '''linear system'''
            #FBL equations: A * C_bm + B * Cp + C * csi = D
            # sum (z * C_bm) = 0
            for key in self.ions:
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key])] = self.A_FBL[key]
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes) + self.pos[key]] = self.B_FBL[key]
                self.v_FBL[key][int((self.Ncomp+1) * self.N_nodes + 2*self.Ncomp)] = self.C_FBL[key]
                self.v_FBL['EN'][int((self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key])] = self.z[key]
                # FMI equations: gamma_pore_b * Cm[0] + A * psi_m[0] = B (1, 2, 3)
                # sum(z * Cm[0]) = -Cx
                self.v_FMI[key][self.pos[key] * self.N_nodes] = self.gamma_pore_b[key]
                self.v_FMI[key][int(self.Ncomp*self.N_nodes)] = self.A_FMI[key]
                self.v_FMI['EN'][self.pos[key] * self.N_nodes] = self.z[key]
                # ENP equations: A * Cm[j] + B * Cm[j+1] + C * psi_m[j] + D * psi_m[j+1] + E * Cp = F (1, 2, 3)
                # sum(z * Cm[j+1] = -Cx
                for j in range(self.N_nodes-1):
                    self.v_ENP[key][j][self.pos[key] * self.N_nodes + j] = self.A_ENP[key][j]
                    self.v_ENP[key][j][self.pos[key] * self.N_nodes + j+1] = self.B_ENP[key][j]
                    self.v_ENP[key][j][int(self.Ncomp * self.N_nodes + j)] = self.C_ENP[key][j]
                    self.v_ENP[key][j][int(self.Ncomp * self.N_nodes + j + 1)] = self.D_ENP[key][j]
                    self.v_ENP[key][j][int((self.Ncomp+1)*self.N_nodes + self.pos[key])] = self.E_ENP[key][j]
                    self.v_ENP['EN'][j][int(self.pos[key] * self.N_nodes + j + 1)] = self.z[key]
                # MPI equations: gamma_pore_p * Cm[N_nodes-1] - A * Cp + B * psi_m[N_nodes-1] - B * psi_p = C (1, 2, 3)
                # sum(z * Cp) = 0
                self.v_MPI[key][int((self.pos[key] + 1) * self.N_nodes - 1)] = self.gamma_pore_p[key]
                self.v_MPI[key][int((self.Ncomp + 1) * self.N_nodes - 1)] = self.B_MPI[key]
                self.v_MPI[key][int((self.Ncomp + 1) * self.N_nodes + self.pos[key])] = - self.A_MPI[key]
                self.v_MPI[key][-1] = - self.B_MPI[key]
                self.v_MPI['EN'][int((self.Ncomp+1)*self.N_nodes + self.pos[key])] = self.z[key]

            self.v_noti = []
            for key in self.ordered_ions:
                self.v_noti.append(self.D_FBL[key])
            self.v_noti.append(0)
            for key in self.ordered_ions:
                self.v_noti.append(self.B_FMI[key])
            self.v_noti.append(- self.config['charge_density'])
            for key in self.ordered_ions:
                for j in range(self.N_nodes-1):
                    self.v_noti.append(self.F_ENP[key][j])
            for j in range(self.N_nodes-1):
                self.v_noti.append(- self.config['charge_density'])
            for key in self.ordered_ions:
                self.v_noti.append(self.C_MPI[key])
            self.v_noti.append(0)
            # print(v_noti, len(v_noti))
            self.matrix_equations = []
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_FBL[key])
            self.matrix_equations.append(self.v_FBL['EN'])
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_FMI[key])
            self.matrix_equations.append(self.v_FMI['EN'])
            for key in self.ordered_ions:
                for j in range(self.N_nodes-1):
                    self.matrix_equations.append(self.v_ENP[key][j])
            for j in range(self.N_nodes-1):
                self.matrix_equations.append(self.v_ENP['EN'][j])
            for key in self.ordered_ions:
                self.matrix_equations.append(self.v_MPI[key])
            self.matrix_equations.append(self.v_MPI['EN'])

            self.matrix_equations = np.array(self.matrix_equations)

            self.sol = np.linalg.solve(self.matrix_equations, self.v_noti)
            # print(sol)
            for key in self.ions:
                self.C_m[key] = self.sol[self.pos[key] * self.N_nodes : (self.pos[key] + 1) * self.N_nodes]
            self.psi_m = self.sol[self.Ncomp * self.N_nodes: (self.Ncomp+1) * self.N_nodes]
            for key in self.ions:
                self.C_p[key] = self.sol[(self.Ncomp+1) * self.N_nodes + self.pos[key]]
                self.C_bm[key] = self.sol[(self.Ncomp+1) * self.N_nodes + self.Ncomp + self.pos[key]]
            self.csi = self.sol[(self.Ncomp + 1) * self.N_nodes + 2 * self.Ncomp]
            self.psi_p = self.sol[-1]
            # print(C_m)
            # print(C_p)
            # print(psi_m, psi_p)
            self.res = np.dot(self.matrix_equations, self.sol) - self.v_noti
            self.res_bool = np.allclose(np.dot(self.matrix_equations, self.sol), self.v_noti)
            # print('verifica sistema lineare', np.sum(self.res), self.res_bool)

            '''under-relaxing the solutions'''
            self.psi_m = self.psi_m_first + self.config['eta_rel_pot'] * (self.psi_m - self.psi_m_first)
            self.psi_p = self.psi_p_first + self.config['eta_rel_pot'] * (self.psi_p - self.psi_p_first)
            error_Cp_square = 0
            for key in self.ions:
                self.Delta_Cp[key] = np.minimum(abs(self.Cp_first[key] / (self.C_p[key] - self.Cp_first[key])), 1) * \
                                     (self.C_p[key] - self.Cp_first[key]) / self.Cp_first[key]
                self.C_p[key] = self.Cp_first[key] * (1 + self.config['eta_rel_conc'] * self.Delta_Cp[key])
                for j in range(self.N_nodes):
                    self.Delta_Cm[key][j] = np.minimum(abs(self.Cm_first[key][j] / (self.C_m[key][j] - self.Cm_first[key][j])), 1)\
                                            * (self.C_m[key][j] - self.Cm_first[key][j]) / self.Cm_first[key][j]
                    self.C_m[key][j] = self.Cm_first[key][j] * (1 + self.config['eta_rel_conc'] * self.Delta_Cm[key][j])
                    error_Cp_square =+ (self.C_p[key] - self.Cp_first[key])**2
            self.error_Cp = np.sqrt(error_Cp_square)
            self.error_ENP_ion = {}
            for key in self.ions:
                self.error_ENP_ion[key] = []
            self.error_elecroneutrality = 0
            self.error_ENP_tot = 0
            self.error_interface = 0
            error_int_b = 0
            error_int_p = 0
            self.error_boundarylayer = 0
            for key in self.ions:
                for j in range(self.N_nodes - 1):
                    self.error_ENP_ion[key].append(np.abs(-self.D_p[key] * (self.C_m[key][j+1] - self.C_m[key][j]) / self.dx - 0.5 * self.z[key] *
                                                    (self.C_m[key][j+1] + self.C_m[key][j]) * self.D_p[key] * self.F_RT * (self.psi_m[j+1] - self.psi_m[j]) / self.dx +
                                                    0.5 * self.k_c_pore[key] * (self.C_m[key][j + 1] + self.C_m[key][j]) * self.Jv - self.Jv * self.C_p[key]))
                self.error_ENP_tot += (np.sum(self.error_ENP_ion[key]) / (np.abs(self.Jv * self.C_p[key])))

            for key in self.ions:
                error_int_b += abs(self.C_m[key][0] * self.gamma_pore_b[key] / (self.gamma_bm[key] * self.C_bm[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * self.psi_m[0]) * self.phi_DE[key]) - 1)
                error_int_p += abs(self.C_m[key][self.N_nodes-1] * self.gamma_pore_p[key] / (self.gamma_permeate[key] * self.C_p[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * (self.psi_m[self.N_nodes-1] - self.psi_p))
                                                                   * self.phi_DE[key]) - 1)
                self.error_interface += abs(self.C_m[key][0] * self.gamma_pore_b[key] / (self.gamma_bm[key] * self.C_bm[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * self.psi_m[0]) * self.phi_DE[key]) - 1) +\
                              abs(self.C_m[key][self.N_nodes-1] * self.gamma_pore_p[key] / (self.gamma_permeate[key] * self.C_p[key] * self.phi_steric[key] * np.exp(-self.z[key] * self.F_RT * (self.psi_m[self.N_nodes-1] - self.psi_p))
                                                                   * self.phi_DE[key]) - 1)
                self.error_boundarylayer += abs(-self.k_c_prime[key] * (self.C_bm[key] - self.C_b[key]) + self.Jv * self.C_bm[key] - self.z[key] * self.C_bm[key] *
                                              self.D_inf[key] * self.F_RT * self.csi - self.Jv * self.C_p[key]) / abs(self.Jv * self.C_p[key])
            charge_m_elem = 0
            charge_m = 0
            charge_p = 0
            charge_p_abs = 0
            for j in range(self.N_nodes-1):
                charge_m_elem = 0
                for key in self.ions:
                    charge_m_elem += self.z[key] * self.C_m[key][j]
                charge_m += abs(charge_m_elem + self.config['charge_density'])
            for key in self.ions:
                charge_p += self.z[key] * self.C_p[key]
                charge_p_abs += abs(self.z[key] * self.C_p[key])
            charge_p = abs(charge_p)
            self.error_elecroneutrality = charge_m + charge_p / (abs(self.config['charge_density']) + charge_p_abs)
            # print('bulk int', error_int_b, 'permeate int', error_int_p)

            i += 1
            if i >= 500:
                print('too high number of iteration')
                break
        # print(self.error_ENP_tot, self.error_interface, self.error_elecroneutrality, self.error_boundarylayer)
        #
        for key in self.ions:
            self.R_ion[key] = (self.C_b[key] - self.C_p[key]) / self.C_b[key]
        # print('{} iterations'.format(i-1))
