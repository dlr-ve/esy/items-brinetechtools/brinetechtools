class ResultList:
    """List of key to Quantity mappings for the purposes of output."""
    def __init__(self, results, title: str = None, xls_sheet="results", xls_x0=0, xls_y0=0):
        """Initialize a new ResultList

        Parameters:
        - **results** (dict|list(tuple)): Can be either a dictionary or a list of key/value pairs. Values can be
                                          Quantities, individual values or tuples.
        - **title** (str, optional): specifies a title
        - **xls_sheet** (str, optional): In case this result list is put into an xlsx, this is the sheet name for it
        - **xls_x0**, **xls_y0** (int, optional): Starting row and column indices for xlsx output"""
        self.results = dict(results)
        self.title = title
        self.xls_sheet = xls_sheet
        self.xls_x0 = xls_x0
        self.xls_y0 = xls_y0

    def __len__(self):
        return len(self.results)

    def __getitem__(self, key):
        return self.results[key]

    def __iter__(self):
        return iter(self.results.items())

    def __str__(self):
        max_key_len = max(map(len, self.results.keys()))
        formatted_items = [(k, ' '.join(map(str, v)) if isinstance(v, tuple) else str(v))
                           for k, v in self.results.items()]
        return "\n".join([
            f'# {self.title}'
        ] + [
            f"{key:>{max_key_len+2}}: {value}" for key, value in formatted_items
        ])
