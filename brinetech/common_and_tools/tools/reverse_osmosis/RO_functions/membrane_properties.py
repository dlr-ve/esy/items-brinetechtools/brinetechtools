from . import tools
import numpy as np

def calculation_Amembr_nom_sw(config):  # [kg/(s m2 bar)]
    Posm_test = config['osmotic_coeff'] * config['test_feedsalinity_sw'] * 1e-4
    Amembr_nom = (config['test_permeateflow_sw'] *1000 / 24 / 3600) / (config['membrane_area_sw'] * (config['Pfeed_sw'] - Posm_test))
    return Amembr_nom

def calculation_Bmembr_nom_sw(config, Amembr_nom):  # [kg/(s m2)]
    Posm_test = config['osmotic_coeff'] * config['test_feedsalinity_sw'] * 1e-4
    Bmembr_nom = (1 - config['salt_rejection_sw']) / (config['salt_rejection_sw']) * Amembr_nom * (config['Pfeed_sw'] - Posm_test)
    return Bmembr_nom

def calculation_Amembr_nom_bw(config):  # [kg/(s m2 bar)]
    Posm_test = config['osmotic_coeff'] * config['test_feedsalinity_bw'] * 1e-4
    Amembr_nom = (config['test_permeateflow_bw'] *1000 / 24 / 3600) / (config['membrane_area_bw'] * (config['Pfeed_bw'] - Posm_test))
    return Amembr_nom

def calculation_Bmembr_nom_bw(config, Amembr_nom):  # [kg/(s m2)]
    Posm_test = config['osmotic_coeff'] * config['test_feedsalinity_bw'] * 1e-4
    Bmembr_nom = (1 - config['salt_rejection_bw']) / (config['salt_rejection_bw']) * Amembr_nom * (config['Pfeed_bw'] - Posm_test)
    return Bmembr_nom

def temperature_factor(config):
    TCF = 1/np.exp(config['Cmembr'] * (1/(273+config['Feed_temperature']) - 1/298))
    return TCF

def ageing_factor(config):
    DFw = np.power((1-config['DFw_coeff']), config['membrane_age'])
    DFs = 1 + config['DFs_coeff'] * config['membrane_age']
    return DFw, DFs

def calculation_Amembr_real(Amembr_nom, TCF, DFw):
    Amembr_real = Amembr_nom * TCF * DFw
    return Amembr_real

def calculation_Bmembr_real(Bmembr_nom, TCF, DFs):
    Bmembr_real = Bmembr_nom * TCF * DFs
    return Bmembr_real

def concentration_polarization_factor(r_el):
    CPF = 1.05 + (1.2-1.05)/(0.25-0.05)*(r_el-0.05)
    return CPF
