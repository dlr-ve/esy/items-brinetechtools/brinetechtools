
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

df_output = pd.read_excel('./results/oaro/oaro_two_stage/arabian_gulf_kuwait/average_tds/results_qfeedm3h_train=173.36.xls')

df_output

#%%
df_output.iloc[3,1]

#%%


## Flow rates
no_of_trains = float(df_output.iloc[0,1])
recovery_oaro = float(df_output.iloc[5,1])
Qretent_m3h_tot = float(df_output.iloc[1,6]) * no_of_trains

## Flow concentrations
Xretent_NaCl_ppm = float(df_output.iloc[4,1])

print(f"no_of_trains, {no_of_trains}, recovery_oaro: {recovery_oaro}, Qretent_m3h_tot: {Qretent_m3h_tot}, Xretent_NaCl_ppm: {Xretent_NaCl_ppm}")


#%%
float(df_output.iloc[6,17])

#%%
## Economic and energy requirements (same cells for single and double stage)
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
annual_fixed_cost_tot = float(df_output.iloc[0,17]) * no_of_trains  # [USD/y] annualized capital + fixed opex
var_opex_tot = float(df_output.iloc[2,17]) * no_of_trains  # [USD/y]
annual_costs_tot_oaro = annual_fixed_cost_tot + var_opex_tot
annual_electric_energy_req_tot = float(df_output.iloc[3,8]) * no_of_trains  # kWh_el/a
Mbrine_annual_tot = float(df_output.iloc[6,17]) * no_of_trains  # [m3/y]

print(f"annual_fixed_cost_tot: {annual_fixed_cost_tot}, var_opex_tot: {var_opex_tot}, annual_costs_tot_oaro: {annual_costs_tot_oaro}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}, Mbrine_annual_tot: {Mbrine_annual_tot}")


# %%
