
#%%

import pandas as pd
from common_and_tools.common import density_mixtures
from common_and_tools.common import density_seawater_liquid

df_output = pd.read_excel('./results/electrodialysis/arabian_gulf_kuwait/average_tds/results_mfeedkgh_train=1026.78_xf_40050.0_xco_204675.0_dcr_15.95_i_250.0.xls')

df_output

#%%
df_output.iloc[18,3]

#%%
Qfeed_m3h_per_train = df_output.iloc[1,1] * 3600
no_of_trains = df_output.iloc[2,1]
Qfeed_m3h_tot = Qfeed_m3h_per_train * no_of_trains
Qfeed_m3h_tot

#%%


## Flow rates
# train
Mretent_kgh_per_train = float(df_output.iloc[14,3])
Mperm_kgh_per_train = float(df_output.iloc[14,4])
Qretent_m3h_per_train = float(df_output.iloc[16,3])
Qperm_m3h_per_train = float(df_output.iloc[16,4])

# total
Mretent_kgh_tot = Mretent_kgh_per_train * no_of_trains
Mperm_kgh_tot = Mperm_kgh_per_train * no_of_trains
Qretent_m3h_tot = Qretent_m3h_per_train * no_of_trains
Qperm_m3h_tot = Qperm_m3h_per_train * no_of_trains 


## Flow concentrations
Xretent_NaCl_ppm = float(df_output.iloc[18,3])
Xperm_NaCl_ppm = float(df_output.iloc[18,4])


print(f"Mretent_kgh_tot: {Mretent_kgh_tot}, Xretent_NaCl_ppm: {Xretent_NaCl_ppm}")
print(f"Xretent_NaCl_ppm: {Xretent_NaCl_ppm}, Xperm_NaCl_ppm: {Xperm_NaCl_ppm}")


#%%
df_output.iloc[7,7]

#%%
## Economic and energy requirements (same cells for single and double stage)
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
total_annual_cost = float(df_output.iloc[13,11])  # [USD/y] annualized capital + opex (fixed+variable)
electric_power_req_tot = float(df_output.iloc[7,7])   # kW_el
annual_electric_energy_req_tot = float(df_output.iloc[8,7])   # kWh_el/a

print(f"total_annual_cost: {total_annual_cost} electric_power_req_tot: {electric_power_req_tot}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}")


# %%
