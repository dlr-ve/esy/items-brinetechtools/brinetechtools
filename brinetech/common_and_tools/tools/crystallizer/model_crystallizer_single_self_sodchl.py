import numpy as np
import pandas as pd

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_mixtures, density_seawater_liquid, get_density_nacl_solution, latent_heat, convert_ppm_to_mol_m3, convert_mol_m3_to_ppm, calculate_nacl_concentration_in_ppm, calculate_nacl_concentration_in_mol_m3, calculate_residual_concentration_in_ppm, sum_impurities_and_convert_to_g_L
from ...common import calculate_pump_efficiency
from ...common.iotools import ResultList


def crystallizer_nacl_model_electric(tech_config_and_params, proj_config_and_params, df_nacl_densities, feed_ion_conc_ppm_unordered_dict, num_trains, feed_mass_flow_per_train_kgh, electricity_cost):
    """
    Calculates the adjusted capital cost and operational expenses for an MVR-based NaCl crystallizer based on the given 
    parameters, considering the concentration of NaCl and impurities in the feed, effective pressure ratio, and other 
    process-specific factors.

    All calculations are based on:
    Nayar, K. G., et al. (2019). Cost and energy requirements of hybrid RO and ED brine concentration systems for salt production. 
    Desalination, 456, 97–120. DOI: 10.1016/j.desal.2018.11.018.

    Args:
        tech_config_and_params (dict): Technical parameters for the crystallizer, including:
            - 'ref_purge_ratio': Reference purge ratio.
            - 'ref_feed_nacl_conc_g_kg': Reference NaCl concentration in g/kg for solid feed.
            - 'ref_conc_impurities_g_L': Reference impurity concentration in g/L.
            - 'ref_nacl_plant': Reference plant details, including:
                - 'cap_cost_mil_usd': Capital cost in million USD.
                - 'annual_capacity_t': Annual production capacity in tons.
                - 'cost_year': The year of the reference cost.
            - 'plant_availability': Plant availability factor.
            - 'depreciation_time_cryst': Depreciation time for the crystallizer.
            - 'labor': Labor-related parameters, including:
                - 'max_plant_annual_capacity_t': Maximum plant annual capacity for current labor calculations.
                - 'chemical_plant_operator': Details of plant operators, including:
                    - 'nos': Number of plant operators.
                    - 'salary': Dictionary of salary based on country.
                - 'process_chemist': Details of process chemists, including:
                    - 'nos': Number of process chemists.
                    - 'salary': Dictionary of salary based on country.
        proj_config_and_params (dict): Project-specific parameters, including:
            - 'pos': Dictionary defining the order of ions.
            - 'CEPCI': Dictionary with Chemical Engineering Plant Cost Index values.
            - 'country': Dictionary for country-specific parameters.
            - 'sea': Sea index or identifier.
            - 'spec_price_commodity': Specification for commodity prices.
            - 'labor': Labor cost year.
            - 'interest_rate': Interest rate for calculating annual depreciation.
        feed_ion_conc_ppm_unordered_dict (dict): Concentrations of various ions in ppm.
        num_trains (int): Number of crystallizer trains.
        feed_mass_flow_per_train_kgh (float): Mass flow rate of the feed in kg/h.
        electricity_cost (float): Cost of electricity (USD/kWh).

    Returns:
        list: A list of ResultList objects containing:
            - Crystallizer inputs.
            - Crystallizer results, including flow rates and economic metrics.
            - Economic results, including capital investment, OPEX, and revenue.
    """

    # Maintain the order of ions as specified in the project configuration
    ion_positions = proj_config_and_params["pos"]
    feed_ion_conc_ppm_dict = {k: feed_ion_conc_ppm_unordered_dict[k] for k in sorted(ion_positions, key=ion_positions.get)}

    ## Volumetric feed flow rate
    #feed_density = density_mixtures(tech_config_and_params, feed_ion_conc_mol_m3)  # [kg/m3]
    # using seawater density, as density_mixtures is giving very low density (eg. 999 kg/m3 for feed with 78 g/L salt)
    #feed_density = density_seawater_liquid(25, sum(feed_ion_conc_ppm_dict.values()))  # [kg/m3]    
    #df_nacl_densities = pd.read_excel(r"../../../inputs/nacl_densities_perry.xlsx", index_col=0)
    #df_nacl_densities = df_nacl_densities.T
    
    feed_ion_conc_perc_tot = sum(feed_ion_conc_ppm_dict.values()) / 10000
    feed_density = get_density_nacl_solution(df_nacl_densities, 25, feed_ion_conc_perc_tot)  # [kg/m3]    
    feed_vol_flow_per_train_m3h = feed_mass_flow_per_train_kgh / feed_density  # [m3/h]
    feed_vol_flow_m3day = feed_vol_flow_per_train_m3h * 24  # [m3/day]

    # Convert ion concentrations from ppm to mol/m³
    feed_ion_conc_mol_m3 = convert_ppm_to_mol_m3(feed_ion_conc_ppm_dict, feed_density, tech_config_and_params)

    # Calculate NaCl concentration in both ppm and mol/m³
    feed_nacl_conc_ppm = calculate_nacl_concentration_in_ppm(feed_ion_conc_ppm_dict, tech_config_and_params)
    feed_nacl_conc_g_kg = feed_nacl_conc_ppm / 1000  # Convert NaCl concentration to g/kg
    feed_nacl_conc_mol_m3_dict = calculate_nacl_concentration_in_mol_m3(feed_ion_conc_ppm_dict, tech_config_and_params)

    # Calculate the total concentration of impurities in g/L
    total_impurities_g_L = sum_impurities_and_convert_to_g_L(feed_ion_conc_ppm_dict)

    # Calculate the effective pressure ratio based on NaCl and impurity concentrations
    effective_purge_ratio = tech_config_and_params['ref_purge_ratio'] * (feed_nacl_conc_g_kg / tech_config_and_params['ref_feed_nacl_conc_g_kg']) * (total_impurities_g_L / tech_config_and_params['ref_conc_impurities_g_L'])

    # Calculate the mass flow rates of pure salt, product water, and purge streams in kg/h
    purge_flow_kgh = feed_mass_flow_per_train_kgh * effective_purge_ratio
    purge_nacl_conc_g_kg = tech_config_and_params['ref_feed_nacl_conc_g_kg']  # Assumed purge salinity
    nacl_mass_flow_kgh = feed_mass_flow_per_train_kgh * (feed_nacl_conc_g_kg / 1000) - purge_flow_kgh * (purge_nacl_conc_g_kg / 1000)
    water_mass_flow_kgh = feed_mass_flow_per_train_kgh - purge_flow_kgh - nacl_mass_flow_kgh
    water_mass_flow_m3h = water_mass_flow_kgh / 1000

    # Annual salt and water production in tons (or m3) per year, considering plant availability
    nacl_production_capacity_tday = nacl_mass_flow_kgh / 1000 * 24  # this does not include plant availability, since we are talking about capacity
    nacl_production_rate_ta = nacl_mass_flow_kgh / 1000 * 8760 * tech_config_and_params['plant_availability']
    annual_water_production_m3a = water_mass_flow_kgh / 1000 * 8760 * tech_config_and_params['plant_availability']

    # Crystallizer Salinity Scale-up Factor (CSSF)
    cssf_numerator = (1000 / feed_nacl_conc_g_kg) * ((1 - effective_purge_ratio) / (1 - (effective_purge_ratio * purge_nacl_conc_g_kg / feed_nacl_conc_g_kg))) - 1
    cssf_denominator = (1000 / purge_nacl_conc_g_kg) * ((1 - tech_config_and_params['ref_purge_ratio']) / (1 - (tech_config_and_params['ref_purge_ratio'] * purge_nacl_conc_g_kg / feed_nacl_conc_g_kg))) - 1
    cssf = cssf_numerator / cssf_denominator

    ## Capital cost of the NaCl crystallizer
    capital_cost = tech_config_and_params['ref_nacl_plant']['cap_cost_mil_usd'] * 10**6 / tech_config_and_params['ref_nacl_plant']['annual_capacity_t'] * nacl_production_rate_ta * cssf
    cost_year_ref_nacl_plant = str(tech_config_and_params['ref_nacl_plant']['cost_year'])
    adjusted_capital_cost = capital_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year_ref_nacl_plant]  # Adjusted to current USD
    # annualization
    annual_capex = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'],
                                           tech_config_and_params['depreciation_time_cryst'], adjusted_capital_cost)  # [USD_current/a]


    ## Operational expenses, consisting of expenses on i) electricity, ii) parts, maintenance, and chemicals and iii) labor
    # i) electricity
    spec_elec_consumption = tech_config_and_params['ref_nacl_plant']['spec_elec_consumption'] * cssf  # [kWh_el/t_nacl]
    annual_elec_consumption = spec_elec_consumption * nacl_production_rate_ta  # [kWh_el/a]
    hourly_elec_consumption = spec_elec_consumption * nacl_mass_flow_kgh / 1000  # [kWh_el/a]
    annual_opex_elec = annual_elec_consumption * electricity_cost  # [USD_current/a] 

    # ii) parts, maintenance, and chemicals
    cost_year_opex = str(tech_config_and_params['spec_annual_opex_m3day']['cost_year'])
    adjusted_annual_opex_parts = tech_config_and_params['spec_annual_opex_m3day']['parts'] * feed_vol_flow_m3day * tech_config_and_params['plant_availability'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year_opex]
    adjusted_annual_opex_maint = tech_config_and_params['spec_annual_opex_m3day']['maintenance'] * feed_vol_flow_m3day * tech_config_and_params['plant_availability'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year_opex]
    adjusted_annual_opex_chem = tech_config_and_params['spec_annual_opex_m3day']['chemicals'] * feed_vol_flow_m3day * tech_config_and_params['plant_availability'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year_opex]

    # iii) Labor
    # Check if the annual salt production is within the allowed range
    if nacl_production_rate_ta <= tech_config_and_params['labor_nos']['max_plant_annual_capacity_t']:
        country = proj_config_and_params['country'][proj_config_and_params['sea']]

        # Plant operator
        num_plant_operators = tech_config_and_params['labor_nos']['chemical_plant_operator']
        cost_year_labor = str(proj_config_and_params['labor_salary']['cost_year'])
        adj_salary_plant_operator = (
            proj_config_and_params['labor_salary']['chemical_plant_operator'][country]
            * proj_config_and_params['CEPCI']['current']
            / proj_config_and_params['CEPCI'][cost_year_labor]
        )

        # Process chemist
        num_process_chemists = tech_config_and_params['labor_nos']['process_chemist']
        adj_salary_process_chemist = (
            proj_config_and_params['labor_salary']['chemical_plant_operator'][country]
            * proj_config_and_params['labor_salary']['process_chemist']['salary_over_operator']
            * proj_config_and_params['CEPCI']['current']
            / proj_config_and_params['CEPCI'][cost_year_labor]
        )

        # Calculate the annual labor cost
        adj_annual_opex_labor = (
            num_plant_operators * adj_salary_plant_operator
            + num_process_chemists * adj_salary_process_chemist
        )  # [USD_current/a]

    else:
        raise ValueError(
            "Annual salt production exceeds 900,000 tons/a. The current labor calculation is only applicable to plants with an annual capacity of up to 900,000 tons. "
            "For higher capacities, the script and associated YAML configuration need to be adjusted."
        )

    adjusted_annual_opex_tot = annual_opex_elec + adjusted_annual_opex_parts + adjusted_annual_opex_maint + adjusted_annual_opex_chem + adj_annual_opex_labor  # [USD_current/a]


    revenue_NaCl = proj_config_and_params['spec_price_commodity']['NaCl'] * nacl_production_rate_ta  # [USD_current/a]
    revenue_water = proj_config_and_params['spec_price_commodity']['water'] * annual_water_production_m3a  # [USD_current/a]

    crystallizer_inputs = ResultList(
        title="crystallizer inputs", xls_sheet="NaCl crystallizer", results=[
         ('Crystallizer type', ('MVR-based', '-')),
         ('No. of trains', (num_trains, '-')),
         ('feed mass flow rate per train', (feed_mass_flow_per_train_kgh, 'kg/h')),
         ('feed volume flow rate per train', (feed_vol_flow_per_train_m3h, 'm3/h')),
         ('rho feed', (feed_density, 'kg/m3')),
         ('Cfeed_ppm (all ions)', (sum(feed_ion_conc_ppm_dict.values()), 'ppm')),
         ('Cfeed_ppm (only NaCl)', (feed_nacl_conc_ppm, 'ppm')),
         ('Cfeed_molm3 (only NaCl)', (feed_nacl_conc_mol_m3_dict, 'mol/m3')),
         ('electricity_cost', (electricity_cost, 'USD/kWh_el')),
         ('cost year', (proj_config_and_params['CEPCI_current_time'], '-')),
         ('ion', ('C [mol/m3', 'C [ppm]')),
         ] + [(key, (feed_ion_conc_mol_m3[key], feed_ion_conc_ppm_dict[key])) for key in feed_ion_conc_ppm_dict.keys()]
        )

    crystallizer_results_flows = ResultList(
        title="crystallizer results per train", xls_sheet="NaCl crystallizer", xls_y0=4, results=[
         ('Total impurities', (total_impurities_g_L, 'g/L')),
         ('Purge ratio (effective)', (effective_purge_ratio*100, '%')),
         ('Mpurge per train', (purge_flow_kgh, 'kg/h')),
         ('Nacl production rate (hourly)', (nacl_mass_flow_kgh, 'kg/h')),
         ('Nacl production rate (annual)', (nacl_production_rate_ta, 't/a')),
         ('Water production rate (hourly)', (water_mass_flow_m3h, 'm3/h')),
         ('Water production rate (annual)', (annual_water_production_m3a, 't/a')),
         ('Water recovery rate (mass based)', (water_mass_flow_kgh / feed_mass_flow_per_train_kgh, '-')),
         ('Electricity consumption', ('kWh_el/t_salt', 'kWh_el/h', 'kWh_el/a', 'kWh_el/m3_feed')),
         ('', (spec_elec_consumption, hourly_elec_consumption, annual_elec_consumption, hourly_elec_consumption/feed_vol_flow_per_train_m3h)),
         ('Crystallizer Salinity Scale-up Factor (CSSF)', (cssf, '-')),
         ]
         )

    crystallizer_results_economic = ResultList(title="Economic results per train", xls_sheet="NaCl crystallizer", xls_y0=10, results=[
         ('Capital investment', ('$/y', 'levelized ($/t_salt)', '$', '$/t_salt_capacity/day', )),
         ('capital cost', (annual_capex, annual_capex/nacl_production_rate_ta, adjusted_capital_cost, adjusted_capital_cost/nacl_production_capacity_tday)),
         ('opex', ('$/y', 'variable or levelized ($/t_salt)', '', '$/m3_feed/day')),
         ('Electricity', (annual_opex_elec, annual_opex_elec/nacl_production_rate_ta, '', annual_opex_elec/(feed_vol_flow_m3day * 365 * tech_config_and_params['plant_availability']))),
         ('Parts', (adjusted_annual_opex_parts, adjusted_annual_opex_parts/nacl_production_rate_ta, '', adjusted_annual_opex_parts/(feed_vol_flow_m3day * tech_config_and_params['plant_availability']))),
         ('Maintenance', (adjusted_annual_opex_maint, adjusted_annual_opex_maint/nacl_production_rate_ta, '', adjusted_annual_opex_maint/(feed_vol_flow_m3day * tech_config_and_params['plant_availability']))),
         ('Chemicals', (adjusted_annual_opex_chem, adjusted_annual_opex_chem/nacl_production_rate_ta, '', adjusted_annual_opex_chem/(feed_vol_flow_m3day * tech_config_and_params['plant_availability']))),
         ('Labor', (adj_annual_opex_labor, adj_annual_opex_labor/nacl_production_rate_ta, '', adj_annual_opex_labor/(feed_vol_flow_m3day * tech_config_and_params['plant_availability']))),
         ('Total OPEX', (adjusted_annual_opex_tot, adjusted_annual_opex_tot/nacl_production_rate_ta, '', adjusted_annual_opex_tot/(feed_vol_flow_m3day * tech_config_and_params['plant_availability']))),
         ('Total levelized costs', ('', ((annual_capex+adjusted_annual_opex_tot)/nacl_production_rate_ta))),
         ('Revenues', ('$/y')),
         ('NaCl revenue', (revenue_NaCl)),
         ('Water revenue', (revenue_water)),
         ]
         )

    return [crystallizer_inputs, crystallizer_results_flows, crystallizer_results_economic]


def crystallizer_nacl_model_thermal(tech_config_and_params, proj_config_and_params, df_nacl_densities, feed_ion_conc_ppm_unordered_dict, N_trains, feed_mass_flow_per_train_kgh, electricity_cost, heat_cost, T_in):
    """
    Simulates the thermal performance and economic analysis of a NaCl crystallizer based on given parameters and feed conditions.

    This function calculates various parameters related to the crystallization process, including the volumetric feed flow rate, concentration conversions, thermal and economic requirements, and the results of the crystallization process.

    Parameters:
        tech_config_and_params (dict): Technical configuration and parameters including molecular weights, saturation concentrations, operating temperatures, etc.
        proj_config_and_params (dict): Project configuration and parameters, including ion positions and economic indicators.
        feed_ion_conc_ppm_unordered_dict (dict): Unordered dictionary of ion concentrations in ppm (mg/L).
        N_trains (int): Number of crystallizer trains.
        feed_mass_flow_per_train_kgh (float): Feed mass flow rate in kg/h.
        electricity_cost (float): Cost of electricity in USD per kWh.
        heat_cost (float): Cost of heat in USD per kWh.
        T_in (float): Feed inlet temperature in °C.

    Returns:
        list: A list containing three `ResultList` objects with:
            1. Crystallizer inputs, including feed concentrations and costs.
            2. Crystallizer results, including flow rates, thermal requirements, and output concentrations.
            3. Economic results, including energy costs, annual capital and operational expenses, and NaCl revenue.

    Example:
        Given:
        tech_config_and_params = {'Cp_NaCl_sat_sol': 3.5, 'T_operating_cryst': 80, 'C_NaCl_sat': 60000, 'MW_NaCl': 58.44, 'N_effect': 3, 'depreciation_time_cryst': 10}
        proj_config_and_params = {'pos': {'Na': 0, 'Cl': 1, 'Mg': 2, 'Ca': 3, 'SO4': 4, 'CO3': 5}, 'CEPCI': {'current': 600}, 'spec_price_commodity': {'NaCl': 100}}
        feed_ion_conc_ppm_unordered_dict = {'Na': 30895.062, 'Cl': 51341.669, 'Mg': 943.13, 'Ca': 634.513, 'SO4': 304.915, 'CO3': 3e-05}
        N_trains = 2
        feed_mass_flow_per_train_kgh = 1000
        electricity_cost = 0.1
        heat_cost = 0.05
        T_in = 25

        The function will return:
        [ResultList(...), ResultList(...), ResultList(...)]
    """

    # Order ion concentrations according to their positions
    pos = proj_config_and_params["pos"]
    feed_ion_conc_ppm_dict = {k: feed_ion_conc_ppm_unordered_dict[k] for k in sorted(pos, key=pos.get)}

    # Calculate feed density using seawater density as a proxy
    #feed_density = density_seawater_liquid(25, sum(feed_ion_conc_ppm_dict.values()))  # [kg/m3]
    feed_ion_conc_perc_tot = sum(feed_ion_conc_ppm_dict.values()) / 10000
    feed_density = get_density_nacl_solution(df_nacl_densities, 25, feed_ion_conc_perc_tot)  # [kg/m3]    
    feed_vol_flow_per_train_m3h = feed_mass_flow_per_train_kgh / feed_density  # [m3/h]
    feed_vol_flow_m3day = feed_vol_flow_per_train_m3h * 24  # [m3/day]

    # Convert ion concentrations from ppm to mol/m³
    feed_ion_conc_mol_m3 = convert_ppm_to_mol_m3(feed_ion_conc_ppm_dict, feed_density, tech_config_and_params)

    # Calculate NaCl concentration in ppm and mol/m³
    feed_nacl_conc_ppm = calculate_nacl_concentration_in_ppm(feed_ion_conc_ppm_dict, tech_config_and_params)
    feed_nacl_conc_mol_m3_dict = calculate_nacl_concentration_in_mol_m3(feed_ion_conc_ppm_dict, tech_config_and_params)

    # Calculate residual ion concentrations and convert to mol/m³
    residual_ion_conc_ppm_dict = calculate_residual_concentration_in_ppm(feed_ion_conc_ppm_dict, feed_nacl_conc_mol_m3_dict, tech_config_and_params)
    residual_ion_conc_mol_m3_dict = convert_ppm_to_mol_m3(residual_ion_conc_ppm_dict, feed_density, tech_config_and_params)

    # Calculate thermal requirements and flow rates
    Qrisc_sens = feed_mass_flow_per_train_kgh * tech_config_and_params['Cp_NaCl_sat_sol'] * (tech_config_and_params['T_operating_cryst'] - T_in) / 3600  # [kW]
    Wout_1_sat = feed_mass_flow_per_train_kgh * feed_nacl_conc_ppm / tech_config_and_params['C_NaCl_sat']
    V1 = feed_mass_flow_per_train_kgh - Wout_1_sat
    Q_out_min_SO4 = feed_vol_flow_per_train_m3h * residual_ion_conc_mol_m3_dict['SO4'] / tech_config_and_params['max_conc_Na2SO4']
    
    # densities
    feed_nacl_density = get_density_nacl_solution(df_nacl_densities, 25, feed_nacl_conc_ppm/10000)  # [kg/m3]   
    saturated_nacl_density = get_density_nacl_solution(df_nacl_densities, tech_config_and_params['T_operating_cryst'], tech_config_and_params['C_NaCl_sat']/10000)  # [kg/m3]   
    
    C_feed_Ca_Mg_wt = (residual_ion_conc_mol_m3_dict['Mg'] * tech_config_and_params['MW_MgCl2'] + residual_ion_conc_mol_m3_dict['Ca']
                       * tech_config_and_params['MW_CaCl2']) / 1000 / feed_nacl_density * 100
    Q_out_min_CaMg = feed_vol_flow_per_train_m3h * C_feed_Ca_Mg_wt / tech_config_and_params['max_conc_CaCl2_MgCl2']
    Q_out = max(Q_out_min_SO4, Q_out_min_CaMg)
    Wout_2_sat = Q_out * saturated_nacl_density
    Wsalt_out = (feed_mass_flow_per_train_kgh * feed_nacl_conc_ppm - Wout_2_sat * tech_config_and_params['C_NaCl_sat']) * 1e-6  # [kg/h]
    Wsalt_out_mol = Wsalt_out / tech_config_and_params['MW_NaCl'] * 1000  # [mol/h]

    # Calculate output ion concentrations in ppm and mol/m³
    out_ion_conc_ppm_dict = {}
    out_ion_conc_mol_m3_dict = {}
    for key in feed_ion_conc_ppm_dict:
        if key in ['Na', 'Cl']:
            out_ion_conc_ppm_dict[key] = (feed_mass_flow_per_train_kgh * feed_ion_conc_ppm_dict[key] - Wsalt_out_mol * tech_config_and_params[f'MW_{key}'] * 1e3) / Wout_2_sat
        else:
            out_ion_conc_ppm_dict[key] = feed_mass_flow_per_train_kgh * feed_ion_conc_ppm_dict[key] / Wout_2_sat
    for key in out_ion_conc_ppm_dict:
        out_ion_conc_mol_m3_dict[key] = out_ion_conc_ppm_dict[key] * 1e-3 / tech_config_and_params[f'MW_{key}'] * density_seawater_liquid(100, tech_config_and_params['C_NaCl_sat'])

    # Calculate evaporative volume and thermal energy requirements
    V2 = Wout_1_sat - Wout_2_sat
    Vtot_evap = V1 + V2
    Qevap = Vtot_evap * latent_heat(tech_config_and_params['T_operating_cryst']) / 3600  # [kW]
    Qtot = Qrisc_sens + Qevap
    thermal_req = Qtot / tech_config_and_params['N_effect']

    # Calculate annual costs and revenues
    Vol_sol = feed_mass_flow_per_train_kgh * tech_config_and_params['tau_cryst'] / saturated_nacl_density  # [m3]
    cryst_volume = Vol_sol / tech_config_and_params['filling_ratio']
    Cp_0_cryst = 10 ** (tech_config_and_params['k1_cryst_batch'] + tech_config_and_params['k2_cryst_batch'] * np.log10(cryst_volume)
                        + tech_config_and_params['k3_cryst_batch'] * np.log10(cryst_volume) ** 2)
    Cp_0_cryst_current = Cp_0_cryst * proj_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI_reference']
    Cbm_cryst = Cp_0_cryst_current * (tech_config_and_params['b1_cryst_batch'] + tech_config_and_params['b2_cryst_batch'] *
                                      tech_config_and_params['Fm_cryst_batch'] * tech_config_and_params['Fp_cryst_batch'])
    total_annual_capex = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'], Cbm_cryst)  # [$/y]

    pump_efficiency = calculate_pump_efficiency(feed_vol_flow_per_train_m3h * 24)
    pump_power = 1 * 1e5 * feed_vol_flow_per_train_m3h / 3600 / pump_efficiency * 1e-3  # [kW] (based on volumetric flow rate)
    electric_energy_cost = pump_power * electricity_cost * 8760 * tech_config_and_params['plant_availability']
    thermal_energy_cost = thermal_req * 8760 * tech_config_and_params['plant_availability'] * heat_cost  # [$/y]
    disposal_brine_cost = proj_config_and_params['spec_cost_commodity']['brine_disposal'] * Wout_2_sat / density_seawater_liquid(100, 400000) * 8760 * tech_config_and_params['plant_availability']  # [$/y]
    revenue_NaCl = proj_config_and_params['spec_price_commodity']['NaCl'] * Wsalt_out / 1000 * 8760 * tech_config_and_params['plant_availability']  # [$/y]

    total_annual_opex = electric_energy_cost + thermal_energy_cost + disposal_brine_cost

    # Compile results into structured output
    crystallizer_inputs = ResultList(
        title="Crystallizer Inputs",
        xls_sheet="NaCl Crystallizer",
        results=[
            ('No. of trains', (N_trains, '-')),
            ('Feed mass flow rate per train', (feed_mass_flow_per_train_kgh, 'kg/h')),
            ('Feed volumetric flow rate per train', (feed_vol_flow_per_train_m3h, 'm³/h')),
            ('Feed density', (feed_density, 'kg/m³')),
            ('Feed ion concentration (total)', (sum(feed_ion_conc_ppm_dict.values()), 'ppm')),
            ('Feed NaCl concentration', (feed_nacl_conc_ppm, 'ppm')),
            ('Feed NaCl concentration (mol/m³)', (feed_nacl_conc_mol_m3_dict, 'mol/m³')),
            ('Saturation concentration of NaCl', (tech_config_and_params['C_NaCl_sat'], 'ppm')),
            ('Inlet temperature', (T_in, '°C')),
            ('Operating temperature', (tech_config_and_params['T_operating_cryst'], '°C')),
            ('Number of effects', (tech_config_and_params['N_effect'], '-')),
            ('Electricity cost', (electricity_cost, 'USD/kWh')),
            ('cost year', (proj_config_and_params['CEPCI_current_time'], '-')),
            ('ion', ('C [mol/m3', 'C [ppm]')),
        ] + [(key, (feed_ion_conc_mol_m3[key], feed_ion_conc_ppm_dict[key])) for key in feed_ion_conc_ppm_dict.keys()
            #('Ion concentrations (mol/m³ and ppm)', [(key, (feed_ion_conc_mol_m3[key], feed_ion_conc_ppm_dict[key])) for key in feed_ion_conc_ppm_dict.keys()])
        ]
    )

    crystallizer_results = ResultList(
        title="Crystallizer Results",
        xls_sheet="NaCl Crystallizer",
        xls_y0=4,
        results=[
            ('V1 (kg/h)', (V1, 'kg/h')),
            ('Wout_1_sat (kg/h)', (Wout_1_sat, 'kg/h')),
            ('V2 (kg/h)', (V2, 'kg/h')),
            ('Wout_2_sat (kg/h)', (Wout_2_sat, 'kg/h')),
            ('Qrisc_sens (kW)', (Qrisc_sens, 'kW')),
            ('Qrisc_evap (kW)', (Qevap, 'kW')),
            ('Total thermal requirement (kW)', (Qtot, 'kW')),
            ('Salt output (kg/h)', (Wsalt_out, 'kg/h')),
            ('Saturation concentration of NaCl (ppm)', (tech_config_and_params['C_NaCl_sat'], 'ppm')),
            ('ion', ('C out [ppm]', 'C out [mol/m3]')),
                ] + [(key, (out_ion_conc_ppm_dict[key], out_ion_conc_mol_m3_dict[key])) for key in feed_ion_conc_ppm_dict.keys()
                ]
    )

            #('Ion concentrations (out, ppm and mol/m³)', [(key, (out_ion_conc_ppm_dict[key], out_ion_conc_mol_m3_dict[key])) for key in feed_ion_conc_ppm_dict.keys()])
        #]
    #)

    others = ResultList(
        title="Economic Results",
        xls_sheet="NaCl Crystallizer",
        xls_y0=9,
        results=[
            ('Electricity consumption (kW)', (pump_power, 'kW')),
            ('Thermal consumption (kW)', (thermal_req, 'kW')),
            ('Annual electricity cost', (electric_energy_cost, '$/y')),
            ('Annual heat cost', (thermal_energy_cost, '$/y')),
            ('Annual brine disposal cost', (disposal_brine_cost, '$/y')),
            ('Capital cost of crystallizer', (Cbm_cryst, '$')),
            ('Annual capital expenditure', (total_annual_capex, '$/y')),
            ('Annual operational expenditure', (total_annual_opex, '$/y')),
            ('Annual NaCl revenue', (revenue_NaCl, '$/y'))
        ]
    )

    return [crystallizer_inputs, crystallizer_results, others]


# old script (not ued anymore):
"""
def crystallizer_nacl_model_thermal(tech_config_and_params, proj_config_and_params, feed_ion_conc_ppm_unordered_dict, N_trains, feed_mass_flow_per_train_kgh, electricity_cost, heat_cost, T_in):
    
    # ions position or sequence (to maintain consistency in excel, while referring to their results in RCE)
    pos = proj_config_and_params["pos"]
    # Creating a new dictionary Cfeed_plant with keys ordered based on the provided order
    feed_ion_conc_ppm_dict = {k: feed_ion_conc_ppm_unordered_dict[k] for k in sorted(pos, key=pos.get)}

    ## Volumetric feed flow rate
    #feed_density = density_mixtures(tech_config_and_params, feed_ion_conc_mol_m3)  # [kg/m3]
    # using seawater density, as density_mixtures is giving very low density (eg. 999 kg/m3 for feed with 78 g/L salt)
    feed_density = density_seawater_liquid(25, sum(feed_ion_conc_ppm_dict.values()))  # [kg/m3]    
    feed_vol_flow_per_train_m3h = feed_mass_flow_per_train_kgh / feed_density  # [m3/h]
    feed_vol_flow_m3day = feed_vol_flow_per_train_m3h * 24  # [m3/day]

    # Convert ion concentrations from ppm to mol/m³
    feed_ion_conc_mol_m3 = convert_ppm_to_mol_m3(feed_ion_conc_ppm_dict, feed_density, tech_config_and_params)

    # Calculate NaCl concentration in both ppm and mol/m³
    feed_nacl_conc_ppm = calculate_nacl_concentration_in_ppm(feed_ion_conc_ppm_dict, tech_config_and_params)
    feed_nacl_conc_g_kg = feed_nacl_conc_ppm / 1000  # Convert NaCl concentration to g/kg
    feed_nacl_conc_mol_m3_dict = calculate_nacl_concentration_in_mol_m3(feed_ion_conc_ppm_dict, tech_config_and_params)
    
    # concentration of residual ions
    residual_ion_conc_ppm_dict = calculate_residual_concentration_in_ppm(feed_ion_conc_ppm_dict, feed_nacl_conc_mol_m3_dict, tech_config_and_params)
    residual_ion_conc_mol_m3_dict = convert_ppm_to_mol_m3(residual_ion_conc_ppm_dict, feed_density, tech_config_and_params)  # [mol/m3]


    Qrisc_sens = feed_mass_flow_per_train_kgh * tech_config_and_params['Cp_NaCl_sat_sol'] * (tech_config_and_params['T_operating_cryst'] - T_in) / 3600  # [kW]
    Wout_1_sat = feed_mass_flow_per_train_kgh * feed_nacl_conc_ppm / tech_config_and_params['C_NaCl_sat']
    V1 = feed_mass_flow_per_train_kgh - Wout_1_sat
    Q_out_min_SO4 = feed_vol_flow_per_train_m3h * residual_ion_conc_mol_m3_dict['SO4'] / tech_config_and_params['max_conc_Na2SO4']
    C_feed_Ca_Mg_wt = (residual_ion_conc_mol_m3_dict['Mg'] * tech_config_and_params['MW_MgCl2'] + residual_ion_conc_mol_m3_dict['Ca']
                       * tech_config_and_params['MW_CaCl2']) / 1000 / feed_nacl_density * 100
    Q_out_min_CaMg = feed_vol_flow_per_train_m3h * C_feed_Ca_Mg_wt / tech_config_and_params['max_conc_CaCl2_MgCl2']
    Q_out = max(Q_out_min_SO4, Q_out_min_CaMg)
    Wout_2_sat = Q_out * saturated_nacl_density
    Wsalt_out = (feed_mass_flow_per_train_kgh * feed_nacl_conc_ppm - Wout_2_sat * tech_config_and_params['C_NaCl_sat']) * 1e-6  # [kg/h]
    Wsalt_out_mol = Wsalt_out / tech_config_and_params['MW_NaCl'] * 1000  # [mol/h]
    out_ion_conc_ppm_dict = {}
    out_ion_conc_mol_m3_dict = {}
    for key in feed_ion_conc_ppm_dict:
        if key == 'Na' or key == 'Cl':
            out_ion_conc_ppm_dict[key] = (feed_mass_flow_per_train_kgh * feed_ion_conc_ppm_dict[key] - Wsalt_out_mol * tech_config_and_params['MW_'+key] * 1e3) / Wout_2_sat
        else:
            out_ion_conc_ppm_dict[key] = feed_mass_flow_per_train_kgh * feed_ion_conc_ppm_dict[key] / Wout_2_sat
    for key in out_ion_conc_ppm_dict:
        out_ion_conc_mol_m3_dict[key] = out_ion_conc_ppm_dict[key] * 1e-3 / tech_config_and_params['MW_'+key] * density_seawater_liquid(100, tech_config_and_params['C_NaCl_sat'])
    # Wsalt_out = tech_config_and_params['ratio_saltin_saltout'] * (feed_mass_flow_per_train_kgh * feed_nacl_conc_ppm * 1e-6)
    # Wout_2_sat = (Wout_1_sat * tech_config_and_params['C_NaCl_sat'] - Wsalt_out * 1e6) / tech_config_and_params['C_NaCl_sat']
    V2 = Wout_1_sat - Wout_2_sat
    Vtot_evap = V1 + V2
    Qevap = Vtot_evap * latent_heat(tech_config_and_params['T_operating_cryst']) / 3600  # [kW]
    Qtot = Qrisc_sens + Qevap
    thermal_req = Qtot / tech_config_and_params['N_effect']

    Vol_sol = feed_mass_flow_per_train_kgh * tech_config_and_params['tau_cryst'] / density_seawater_liquid(tech_config_and_params['T_operating_cryst'],
                                                                              tech_config_and_params['C_NaCl_sat'])  # [m3]
    cryst_volume = Vol_sol / tech_config_and_params['filling_ratio']
    Cp_0_cryst = 10 ** (tech_config_and_params['k1_cryst_batch'] + tech_config_and_params['k2_cryst_batch'] * np.log10(cryst_volume)
                        + tech_config_and_params['k3_cryst_batch'] * np.log10(cryst_volume) ** 2)
    Cp_0_cryst_current = Cp_0_cryst * proj_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI_reference']
    Cbm_cryst = Cp_0_cryst_current * (tech_config_and_params['b1_cryst_batch'] + tech_config_and_params['b2_cryst_batch'] *
                                      tech_config_and_params['Fm_cryst_batch'] * tech_config_and_params['Fp_cryst_batch'])
    total_annual_capex = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'],
                                           tech_config_and_params['depreciation_time_cryst'], Cbm_cryst)  # [$/y]
    
    pump_efficiency = calculate_pump_efficiency(feed_vol_flow_per_train_m3h*24)
    pump_power = 1 * 1e5 * feed_vol_flow_per_train_m3h / 3600 / pump_efficiency *1e-3  # [kW]  operating pressure: 1 bar #! previously was based on feed_mass_flow_per_train_kgh [kg/h], which apparently was a mistake
    electric_energy_cost = pump_power * electricity_cost * 8760 * tech_config_and_params['plant_availability']
    thermal_energy_cost = thermal_req * 8760 * tech_config_and_params['plant_availability'] * heat_cost  # [$/y]
    disposal_brine_cost = tech_config_and_params['spec_cost_disposal'] *  Wout_2_sat / density_seawater_liquid(100, 400000)\
                          * 8760 * tech_config_and_params['plant_availability']  # [$/y]
    revenue_NaCl = proj_config_and_params['spec_price_commodity']['NaCl'] * Wsalt_out / 1000 * 8760 * tech_config_and_params['plant_availability']  # [$/y]

    total_annual_opex = electric_energy_cost + thermal_energy_cost + disposal_brine_cost

    crystallizer_inputs = ResultList(
        title="crystallizer inputs", xls_sheet="NaCl crystallizer", results=[
        ('No. of trains', (N_trains, '-')),
        ('feed_mass_flow_per_train_kgh per train', (feed_mass_flow_per_train_kgh, 'kg/h')),
        ('feed_mass_flow_per_train_kgh per train', (feed_mass_flow_per_train_kgh, 'kg/h')),
        ('feed_vol_flow_per_train_m3h per train', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('rho feed', (feed_density, 'kg/m3')),
        ('Cfeed_ppm (all ions)', (sum(feed_ion_conc_ppm_dict.values()), 'ppm')),
        ('Cfeed_ppm (only NaCl)', (feed_nacl_conc_ppm, 'ppm')),
        ('Cfeed_molm3 (only NaCl)', (feed_nacl_conc_mol_m3_dict, 'mol/m3')),
        ('C NaCl sat', (tech_config_and_params['C_NaCl_sat'], 'ppm')),
        ('T in', (T_in, '°C')),
        ('T operating', (tech_config_and_params['T_operating_cryst'], '°C')),
        ('N effects', (tech_config_and_params['N_effect'], '-')),
        ('electricity_cost', (electricity_cost, 'USD/kWh_el')),
        ('cost year', (proj_config_and_params['CEPCI_current_time'], '-')),
        ('ion', ('C [mol/m3', 'C [ppm]')),
         ] + [(key, (feed_ion_conc_mol_m3[key], feed_ion_conc_ppm_dict[key])) for key in feed_ion_conc_ppm_dict.keys()]
        )

    crystallizer_results = ResultList(title="crystallizer results", xls_sheet="NaCl crystallizer", xls_y0=4, results=[
         ('V1', (V1, 'kg/h')),
         ('Wout_1_sat', (Wout_1_sat, 'kg/h')),
         ('V2', (V2, 'kg/h')),
         ('Wout_2_sat', (Wout_2_sat, 'kg/h')),
         ('Qrisc_sens', (Qrisc_sens, 'kW')),
         ('Qrisc_evap', (Qevap, 'kW')),
         ('Qtot', (Qtot, 'kW')),
         ('Wsalt_out', (Wsalt_out, 'kg/h')),
         ('Cout_2_sat', (tech_config_and_params['C_NaCl_sat'], 'ppm')),
         ('ion', ('C out [ppm]', 'C out [mol/m3]')),
         ] + [(key, (out_ion_conc_ppm_dict[key], out_ion_conc_mol_m3_dict[key])) for key in feed_ion_conc_ppm_dict.keys()])

    others = ResultList(title="Economic results", xls_sheet="NaCl crystallizer", xls_y0=9, results=[
         ('electric cons cryst', (pump_power, 'kW el')),
         ('thermal cons cryst', (thermal_req, 'kW th')),
         ('cost electricity cons', (electric_energy_cost, '$/y')),
         ('cost heat cons', (thermal_energy_cost, '$/y')),
         ('cost brine disposal', (disposal_brine_cost, '$/y')),
         ('cost cryst', (Cbm_cryst, '$')),
         ('annual capex', (total_annual_capex, '$/y')),
         ('annual opex', (total_annual_opex, '$/y')),
         ('annual NaCl revenue', (revenue_NaCl, '$/y'))])

    return [crystallizer_inputs, crystallizer_results, others]
    """