import logging
import math
import numpy as np
from . import h2oprop, naclprops, pressure, heattransfer, tools, tvc
from scipy.optimize import minimize
from .modelparams import fixed_DeltaTcond

logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)

''' functions for the design problem'''


def global_balance_mdist(m_dist, xf, xn):  # Mdist in [kg/s]
    m_brine = m_dist * xf / (xn - xf)
    m_feed = m_brine + m_dist
    return m_brine, m_feed


def global_balance_mfeed(m_feed, xf, xn):
    m_brine = m_feed * xf / xn
    m_dist = m_feed - m_brine
    return m_brine, m_dist


def calc_tvsat(tbrine, xb, config):
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime = h2oprop.sat_pressure(tvsat) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                      config['delta_w']) * config['thickness'] * 1E-3 * 1E-3
    tvsat_prime = h2oprop.sat_temp(pv_prime)
    if tvsat_prime > tvsat:
        return pv_prime, tvsat
    return pv_prime, tvsat_prime


def deltat_losses(m_vap, tbrine, xb, config):  # Mvap in [kg/s], Tbrine in [°C]
    tbrine = float(tbrine)
    xb = float(xb)
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime, tvsat_prime = calc_tvsat(tbrine, xb, config)
    pc_prime = pv_prime - pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tvsat_prime),
                                                    config['d_tube']) * 1E-3 * config['L_tube']
    tc_prime = h2oprop.sat_temp(pc_prime)
    if tc_prime > tvsat_prime:
        tc_prime = tvsat_prime
    return tvsat, tvsat_prime, tc_prime

def deltat_cond(m_vap, tc_prime, config, n_tubes):
    dp_cond = 0.99 * pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tc_prime, config['epsilon']) * 1E-3 -\
              (pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime),
                                                  h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'],
                                                  config['L_evap']) * 1E-3)
    pc = h2oprop.sat_pressure(tc_prime) - dp_cond
    # pc = pc_prime - pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tbrine,
    #                                 config['epsilon']) * 1E-3
    tc = h2oprop.sat_temp(pc)
    if tc > tc_prime:
        tc = tc_prime
    return tc

def mvap_effectbalance(i_effect, m_steam, t_steam, Mfeed, t_preh, alfa_c, tv_prime, t, m_fb, mb, xb, N, Xf):
    if i_effect == 0:
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) + Mfeed * naclprops.enthalpy_seawater(t_preh[i_effect + 1], Xf)
                 + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                 - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) \
             / ((1 - alfa_c[i_effect]) * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                + alfa_c[i_effect] * h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]))
    elif i_effect != 0 and i_effect != N-1:  # in this case m_steam = (1-alfa(iEffect-1))*Mvap(iEffect-1) and t_steam is Tc(iEffect-1)
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) +
             + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
             + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1])
             - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) / \
             (h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
        # m_vap = (m_steam * h2oprop.latent_heat(t_steam) + Mfeed * naclprops.cp_seawater(
        #     (t_preh[i_effect + 1] + t_preh[i_effect]) / 2, Xb) * (t_preh[i_effect + 1] - t_preh[i_effect])
        #          + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
        #          + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1])
        #          - mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) / \
        #         ((1 - alfa_c[i_effect]) * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]) + alfa_c[
        #             i_effect] * h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]))
    elif i_effect == N-1:  # i_effect == N-1
        m_vap = (m_steam * h2oprop.latent_heat(t_steam) + m_fb[i_effect] * h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
                 + mb[i_effect - 1] * naclprops.enthalpy_seawater(t[i_effect - 1], xb[i_effect - 1]) -
                 mb[i_effect] * naclprops.enthalpy_seawater(t[i_effect], xb[i_effect])) / h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])
    return m_vap


def tbrineout_fromtcout(tc_n, tv_prime, xb_out, m_vap, config, tbrine):
    tc_n = float(tc_n)
    xb_out = float(xb_out)
    pc_prime = h2oprop.sat_pressure(tc_n)
    pv_prime = pc_prime + pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tv_prime), config['d_tube']) * 1E-3 * config['L_tube']
    deltat_lines = h2oprop.sat_temp(pv_prime) - tc_n
    if deltat_lines < 0:
        deltat_lines = 0
    pv = pv_prime + pressure.demister_deltap(config['rho_d'], config['v_vap'], config['delta_w']) * 1E-3
    deltat_dem = h2oprop.sat_temp(pv) - h2oprop.sat_temp(pv_prime)
    if deltat_dem < 0:
        deltat_dem = 0
    tbrine = tc_n + naclprops.bpe_brine_act(tbrine, xb_out) + deltat_dem + deltat_lines
    return tbrine


def tpreh_profile_old(t_preh_next, alfa_c, m_vap, tvsat_prime, mfeed, t_mean, Xf):
    t_preh = t_preh_next + (alfa_c * m_vap * h2oprop.latent_heat(tvsat_prime)) / (
    mfeed * naclprops.cp_seawater(t_mean, Xf))
    return t_preh


def tpreh_profile(t_0, DTT_preh, tpreh_n, n, deltat_ph = [], T_preh = []):
    T_preh.append(t_0 - DTT_preh)
    for i in range(0, n-1):
        deltat_ph.append((T_preh[0]-tpreh_n)/(n-1))
    for i in range(1, n):
        T_preh.append(T_preh[i-1] - deltat_ph[i-1])
    return deltat_ph, T_preh


def alfa_preheater(mfeed, i_effect, t_preh, Xf, m_vap, tvsat_prime):
    alfa_c = mfeed * naclprops.cp_seawater((t_preh[i_effect] + t_preh[i_effect + 1]) / 2, Xf) * (
    t_preh[i_effect] - t_preh[i_effect + 1]) \
             / (m_vap[i_effect] * h2oprop.latent_heat(tvsat_prime[i_effect]))
    return alfa_c


def first_flashbox(alfa_c, m_vap, tc, t_fb, tv):  # not used
    m_fb = alfa_c * m_vap * (h2oprop.enthalpy_satwat_liquid(tc) - h2oprop.enthalpy_satwat_liquid(t_fb)) / (
    h2oprop.enthalpy_satwat_vapor(tv) - h2oprop.enthalpy_satwat_liquid(t_fb))
    mc = alfa_c * m_vap - m_fb
    return m_fb, mc


def generic_flashbox(i_effect, mfb_previous, tv_prime, tc, m_vap, alfa_c):
    mc = (alfa_c[i_effect] * m_vap[i_effect] * (
    h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) -
                                                              h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (
             h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = alfa_c[i_effect] * m_vap[i_effect] + (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def last_flashbox(i_effect, alfa_cond, mfb_previous, tv_prime, tc, m_vap, m_vap_out, tc_prime):
    mc = (m_vap_out * (h2oprop.enthalpy_satwat_liquid(tc_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(
              tv_prime[i_effect]))) / (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = m_vap_out + (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def brine_flash(i_effect, t, mb, xb):
    #logger.info('i_effect-1 (brine_flash in design_equalAph_DTpreh value is: "' + str((i_effect - 1)) + '"')
    #logger.info('xb (brine_flash in design_equalAph_DTpreh value is: "' + str(xb[i_effect - 1]) + '"')
    tbrine_fe = t[i_effect] + naclprops.non_equilibrium_allowance(t[i_effect - 1], t[i_effect], xb[i_effect - 1])
    m_fbrine = mb[i_effect - 1] * naclprops.cp_seawater((t[i_effect - 1] + tbrine_fe) / 2, xb[i_effect - 1]) * \
               (t[i_effect - 1] - tbrine_fe) / naclprops.latent_heat_seawater(tbrine_fe, xb[i_effect-1])
    #(h2oprop.latent_heat(tbrine_fe)*(1-xb[i_effect - 1]*1e-6))
    return m_fbrine


def area_hx(i_effect, mfeed, xf, t, t_preh, m_d, ts, alfa_cond, m_vap, tc):
    if i_effect == 0:
        return (mfeed * naclprops.cp_seawater((t[i_effect] + t_preh[i_effect]) / 2, xf) * (
        t[i_effect] - t_preh[i_effect]) +
                m_d[0] * naclprops.latent_heat_seawater(t[i_effect] - naclprops.bpe_brine_act(t[i_effect], xf), xf)) / (
               heattransfer.u_evaporator(t[i_effect]) * (ts - t[i_effect]))

    return ((1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1]) * h2oprop.latent_heat(tc[i_effect - 1]) / (
    heattransfer.u_evaporator(t[i_effect]) * (tc[i_effect - 1] - t[i_effect]))


def deltat_logmean(i_effect, tv_prime, t_preh):
    return ((tv_prime[i_effect] - t_preh[i_effect + 1]) - (tv_prime[i_effect] - t_preh[i_effect])) / \
           np.log((tv_prime[i_effect] - t_preh[i_effect + 1]) / (tv_prime[i_effect] - t_preh[i_effect]))


def area_preh(i_effect, alfa_c, m_vap, tv_prime, DTLM):
    return alfa_c[i_effect] * m_vap[i_effect] * h2oprop.latent_heat(tv_prime[i_effect]) / (
        heattransfer.u_condenser(tv_prime[i_effect]) * DTLM[i_effect])


def coolingwater_flowrate(mvap_out, tc_prime, xf, tcw_out, tcw_in):
    return mvap_out * h2oprop.latent_heat(tc_prime) / (naclprops.cp_seawater((tcw_out + tcw_in) / 2, xf) * (tcw_out - tcw_in))


def last_alfapreh(mb, t, xb, m_vap, tv_prime, m_fb, tc, N):
    m_steam = (mb[N - 1] * naclprops.enthalpy_seawater(t[N - 1], xb[N - 1]) + m_vap[
        N - 1] * h2oprop.enthalpy_satwat_vapor(tv_prime[N - 1]) - m_fb[N - 1] * h2oprop.enthalpy_satwat_vapor(
        tv_prime[N - 1]) -
               mb[N - 2] * naclprops.enthalpy_seawater(t[N - 2], xb[N - 2])) / h2oprop.latent_heat(tc[N - 1])
    return 1 - (m_steam / m_vap[N - 1])


def area_condenser(m_cw, tcw_in, tcw_out, tvap, xf):
    deltat_ln = (tcw_out - tcw_in) / np.log((tvap - tcw_in) / (tvap - tcw_out))
    area_c = m_cw * naclprops.cp_seawater((tcw_in + tcw_out) / 2, xf) * (tcw_out - tcw_in) / (
        deltat_ln * heattransfer.u_condenser(tvap))
    return area_c


class MedSolver:
    def __init__(self, N, DeltaT, DeltaT_tot, DeltaT_ph, T, Ts, Tn, Tvsat, Tvsat_prime, Tc_prime, Tc, T_preh, Tcw_in, DeltaT_cond, Tcw_out, Mb, Md, M_fbrine,
                 M_fb, Mvap,
                 Xb, Mbrine, Mdist, Mfeed, Xf, Xb_out, M_cond_ph, alfa_cond, DTT_preh, DTT_cond, h_preh, h_vprime, h_cprime, h_b, Ms,
                 TVC_on, Mms, Pm, Mss, ER, Mc, A_hx, A_preh,
                 Msteam, X_max, A_av, DeltaA, SumSquareErrorArea, Mdist_real, Error_Mdist, config, DTLM, n_tubes, T_dist_fb):
        self.N = N
        self.config = config
        self.DeltaT = DeltaT
        self.DeltaT_tot = DeltaT_tot
        self.DeltaT_ph = DeltaT_ph
        self.T = T
        self.Ts = Ts
        self.Tn = Tn
        self.Tvsat = Tvsat
        self.Tvsat_prime = Tvsat_prime
        self.Tc_prime = Tc_prime
        self.Tc = Tc
        self.T_preh = T_preh
        self.Tcw_in = Tcw_in
        self.DeltaT_cond = DeltaT_cond
        self.Tcw_out = Tcw_out
        self.Mb = Mb
        self.Md = Md
        self.M_fbrine = M_fbrine
        self.M_fb = M_fb
        self.Mvap = Mvap
        self.Xb = Xb
        self.Mbrine = Mbrine
        self.Mdist = Mdist
        self.Mfeed = Mfeed
        self.Xf = Xf
        self.Xb_out = Xb_out
        self.M_cond_ph = M_cond_ph
        self.alfa_cond = alfa_cond
        self.DTT_preh = DTT_preh
        if naclprops.bpe_brine_act(self.config['Tsw_design'], self.Xb_out) < 1.5:
            self.DTT_cond = DTT_cond
        else:
            self.DTT_cond = naclprops.bpe_brine_act(self.config['Tsw_design'], self.Xb_out) * 2
        self.h_preh = h_preh
        self.h_vprime = h_vprime
        self.h_cprime = h_cprime
        self.h_b = h_b
        self.Ms = Ms
        self.TVC_on = TVC_on
        self.Mms = Mms
        self.Pm = Pm
        self.Mss = Mss
        self.ER = ER
        self.Mc = Mc
        self.A_hx = A_hx
        self.A_preh = A_preh
        self.Msteam = Msteam
        self.X_max = X_max
        self.A_av = A_av
        self.DeltaA = DeltaA
        self.SumSquareArea = SumSquareErrorArea
        self.Mdist_real = Mdist_real
        self.Error_Mdist = Error_Mdist
        self.DTLM = DTLM
        self.n_tubes = n_tubes
        self.T_dist_fb = T_dist_fb

    def areahxcalculation(self, x):
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT[i_effect] = x[i_effect]
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                              self.Xb[i_effect], self.config)

        for i_effect in tools.itereffects(0, self.N-1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes)
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5
            #logger.info('self.Tc[i_effect] value in areahxcalculation.design_equalAphpreh.py is: "' + str(self.Tc[i_effect]) + '"')

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N-1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
            self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect-1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N-1]
        # self.T_preh = self.T - 5
        # self.Tcw_out = self.Tcw_in + self.DeltaT_cond
        # self.T_preh[self.N-1] = self.Tcw_out
        # for i_effect in range(self.N-2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # self.Tc_prime[self.N-1] = self.T_preh[self.N-1] + self.DTT_cond
        # self.Tn = tbrineout_fromtcout(self.Tc_prime[self.N-1], self.Tvsat_prime[self.N-1], self.Xb_out, (self.Mvap[self.N-1]-self.Mss), self.config, self.Tn)

        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - self.DTT_cond
        self.Tcw_out = self.T_preh[self.N - 1]

        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
            self.M_cond_ph[i_effect] = self.Mfeed * naclprops.cp_seawater(
                (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                self.T_preh[i_effect] - self.T_preh[i_effect + 1]) / h2oprop.latent_heat(self.Tvsat_prime[i_effect])
            self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]

        # first effect
        self.Mb[0] = self.Mfeed - self.Md[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0]))/h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mfeed, self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                       self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mfeed * naclprops.cp_seawater(
        #     (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / naclprops.latent_heat_seawater(self.T[0], self.Xb)
        #(h2oprop.latent_heat(self.T[0])*(1-self.Xb*1e-6))
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb)) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0]+self.M_fb[0]
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond, self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1], self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            # DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
            #     self.T[i_effect], self.Xb[i_effect])
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) /\
            #                     (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            #(h2oprop.latent_heat(self.T[i_effect])*(1-self.Xb[i_effect-1]*1e-6))
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect-1]) + self.M_fbrine[i_effect] *
                                 (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]) -
                                  h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]))
                                 + self.Mb[i_effect] * (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1])-
                                                        naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])))\
                                /(h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]) - naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]))
            # self.Mvap[i_effect] = mvap_effectbalance(i_effect, self.Msteam, self.Tc[i_effect-1], self.Mfeed, self.T_preh, self.alfa_cond,
            #                                self.Tvsat_prime,
            #                                self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xb)
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]
            #logger.info('Mvap (intermediate effect) value in design_equalAphpreh.py is: "' + str(self.Mvap[i_effect]) + '"')
            # self.Md[i_effect] = self.Mvap[i_effect] - self.M_fb[i_effect] - self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)
        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / naclprops.latent_heat_seawater(self.T[self.N-1], self.Xb[self.N-2])
        # (h2oprop.latent_heat(self.T[self.N - 1])*(1-self.Xb[self.N-2]*1e-6))
        self.Mvap[self.N-1] = mvap_effectbalance(self.N-1, self.Msteam, self.Tc[self.N-2], self.Mfeed, self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                       self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        # self.Md[self.N-1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) + self.M_fbrine[self.N-1] *
        #                      (naclprops.enthalpy_seawater(self.T[self.N-2], self.Xb[self.N-2]) -
        #                       h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N-1]))
        #                      + self.Mb[self.N-1] * (
        #                      naclprops.enthalpy_seawater(self.T[self.N-2], self.Xb[self.N-2]) -
        #                      naclprops.enthalpy_seawater(self.T[self.N-1], self.Xb[self.N-1]))) \
        #                     / (h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N-1]) - naclprops.enthalpy_seawater(
        #     self.T[self.N-2], self.Xb[self.N-2]))
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Md[self.N-1] = self.Mvap[self.N-1] - self.M_fbrine[self.N-1] - self.M_fb[self.N-1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)
        self.A_av = np.sum(self.A_hx) / self.N
        # self.n_tubes = self.A_hx[self.N - 1] / (np.pi * self.config['d_evap'] * self.config['L_evap'])
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        self.SumSquareErrorArea = (np.sum(self.DeltaA ** 2)) ** 0.5
        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        Atot_av = self.A_av + Aph_av
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        Tot_error = self.SumSquareErrorArea * (self.A_av/Atot_av) + SumSquareErrorAreaph * (Aph_av/Atot_av)
        return self.SumSquareErrorArea


    def areaphcalculation(self, z):
        for i_effect in tools.itereffects(0, self.N-1):
            self.DeltaT_ph[i_effect] = z[i_effect]

        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                              self.Xb[i_effect], self.config)

        for i_effect in tools.itereffects(0, self.N - 1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes)
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N - 1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
            self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect - 1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N - 1]
        # self.T_preh = self.T - 5
        # self.Tcw_out = self.Tcw_in + self.DeltaT_cond
        # self.T_preh[self.N-1] = self.Tcw_out
        # for i_effect in range(self.N-2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # self.Tc_prime[self.N-1] = self.T_preh[self.N-1] + self.DTT_cond
        # self.Tn = tbrineout_fromtcout(self.Tc_prime[self.N-1], self.Tvsat_prime[self.N-1], self.Xb_out, (self.Mvap[self.N-1]-self.Mss), self.config, self.Tn)

        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - self.DTT_cond
        self.Tcw_out = self.T_preh[self.N - 1]
        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
            self.M_cond_ph[i_effect] = self.Mfeed * naclprops.cp_seawater(
                (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                                           self.T_preh[i_effect] - self.T_preh[i_effect + 1]) / h2oprop.latent_heat(
                self.Tvsat_prime[i_effect])
            self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]

        # first effect
        self.Mb[0] = self.Mfeed - self.Md[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[
            0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(
            self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mfeed, self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mfeed * naclprops.cp_seawater(
        #     (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / naclprops.latent_heat_seawater(self.T[0], self.Xb)
        # (h2oprop.latent_heat(self.T[0])*(1-self.Xb*1e-6))
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb)) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0]+self.M_fb[0]
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond,
                               self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1],
                                                                      self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            # DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
            #     self.T[i_effect], self.Xb[i_effect])
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) / \
            #                     (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            # (h2oprop.latent_heat(self.T[i_effect])*(1-self.Xb[i_effect-1]*1e-6))
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect-1]) + self.M_fbrine[i_effect] *
                                 (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]) -
                                  h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]))
                                 + self.Mb[i_effect] * (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1])-
                                                        naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])))\
                                /(h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]) - naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]))
            # self.Mvap[i_effect] = mvap_effectbalance(i_effect, self.Msteam, self.Tc[i_effect-1], self.Mfeed, self.T_preh, self.alfa_cond,
            #                                self.Tvsat_prime,
            #                                self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xb)
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]

            # self.Md[i_effect] = self.Mvap[i_effect] - self.M_fb[i_effect] - self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max
            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)
        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)

        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / naclprops.latent_heat_seawater(self.T[self.N-1], self.Xb[self.N-2])
        # (h2oprop.latent_heat(self.T[self.N - 1])*(1-self.Xb[self.N-2]*1e-6))
        self.Mvap[self.N-1] = mvap_effectbalance(self.N-1, self.Msteam, self.Tc[self.N-2], self.Mfeed, self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                       self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) + self.M_fbrine[self.N - 1] *
        #                        (naclprops.enthalpy_seawater(self.T[self.N - 2], self.Xb[self.N - 2]) -
        #                         h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N - 1]))
        #                        + self.Mb[self.N - 1] * (
        #                            naclprops.enthalpy_seawater(self.T[self.N - 2], self.Xb[self.N - 2]) -
        #                            naclprops.enthalpy_seawater(self.T[self.N - 1], self.Xb[self.N - 1]))) \
        #                       / (h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N - 1]) - naclprops.enthalpy_seawater(
        #                           self.T[self.N - 2], self.Xb[self.N - 2]))
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Md[self.N-1] = self.Mvap[self.N-1] - self.M_fbrine[self.N-1] - self.M_fb[self.N-1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)
        # self.n_tubes = self.A_hx[self.N-1] / (np.pi * self.config['d_evap'] * self.config['L_evap'])
        Aph_av = np.sum(self.A_preh) / (self.N - 1)
        DeltaAph = np.abs(self.A_preh - Aph_av)
        # MaxError = np.max(DeltaA) - np.min(DeltaA)
        SumSquareErrorAreaph = (np.sum(DeltaAph ** 2)) ** 0.5
        return SumSquareErrorAreaph

    def steamcalculation(self, y):
        self.Ms = y
        if self.TVC_on:
            self.ER = tvc.tvc_entrainment_ratio(self.Pm, self.Ts, self.Tn, self.Xb_out, self.config)
            # self.Ms, self.Mss = tvc.tvc_steam(self.ER, self.Ms)
            self.Mms = self.Ms / (1 + 1 / self.ER)
            self.Mss = self.Ms - self.Mms
        else:
            self.Mms = self.Ms
        # T profile calculations
        self.DeltaT_tot = 0
        for i_effect in tools.itereffects(0, self.N):
            self.DeltaT_tot += self.DeltaT[i_effect]
        self.T[0] = self.Ts - self.DeltaT[0]
        for i_effect in tools.itereffects(1, self.N):
            self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]
        if abs(self.T[self.N - 1] - self.Tn) > 0.05:
            for i_effect in tools.itereffects(0, self.N):
                self.DeltaT[i_effect] = self.DeltaT[i_effect] * (self.Ts - self.Tn) / self.DeltaT_tot
            self.T[0] = self.Ts - self.DeltaT[0]
            for i_effect in tools.itereffects(1, self.N):
                self.T[i_effect] = self.T[i_effect - 1] - self.DeltaT[i_effect]

        for i_effect in tools.itereffects(0, self.N):
            self.Tvsat[i_effect], self.Tvsat_prime[i_effect], self.Tc_prime[i_effect] = \
                deltat_losses(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]), self.T[i_effect],
                              self.Xb[i_effect], self.config)

        for i_effect in tools.itereffects(0, self.N - 1):
            self.Tc[i_effect] = deltat_cond(((1 - self.alfa_cond[i_effect]) * self.Mvap[i_effect]),
                                            self.Tc_prime[i_effect], self.config, self.n_tubes)
            if self.Tc[i_effect] < self.T[i_effect + 1]:
                self.Tc[i_effect] = self.T[i_effect + 1] + 0.5
        ##logger.info('self.alfa_cond[i_effect] value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##        self.alfa_cond) + '"')
        #logger.info('self.Mvap[i_effect] value in steamcalculation.design_equalAphpreh.py is: "' + str(
        #    self.Mvap) + '"')
        ##logger.info('self.Tc_prime value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##    self.Tc_prime) + '"')
        #logger.info('self.n_tubes value in steamcalculation.design_equalAphpreh.py is: "' + str(
         #   self.n_tubes) + '"')
        ##logger.info('self.Tc value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##    self.Tc) + '"')

        # T preheater profile calculation
        DeltaT_totph = 0
        for i_effect in tools.itereffects(0, self.N - 1):
            DeltaT_totph += self.DeltaT_ph[i_effect]
        self.T_preh[0] = self.T[0] - self.DTT_preh
        for i_effect in tools.itereffects(1, self.N):
            self.T_preh[i_effect] = self.T_preh[i_effect - 1] - self.DeltaT_ph[i_effect - 1]
        self.Tn = self.config['Tn_design']
        self.Tcw_out = self.T_preh[self.N - 1]
        # self.T_preh = self.T - 5
        # self.Tcw_out = self.Tcw_in + self.DeltaT_cond
        # self.T_preh[self.N-1] = self.Tcw_out
        # for i_effect in range(self.N-2, -1, -1):
        #     self.T_preh[i_effect] = self.T_preh[i_effect + 1] + self.DeltaT_ph[i_effect]
        # self.Tc_prime[self.N-1] = self.T_preh[self.N-1] + self.DTT_cond
        # self.Tn = tbrineout_fromtcout(self.Tc_prime[self.N-1], self.Tvsat_prime[self.N-1], self.Xb_out, (self.Mvap[self.N-1]-self.Mss), self.config, self.Tn)

        for iEffect in range(0, self.N):
            if self.T_preh[iEffect] > self.Tvsat_prime[iEffect]:
                self.T_preh[iEffect] = self.T[iEffect] - self.DTT_cond
        self.Tcw_out = self.T_preh[self.N - 1]
        for i_effect in tools.itereffects(0, (self.N - 1)):
            self.DTLM[i_effect] = deltat_logmean(i_effect, self.Tvsat_prime, self.T_preh)
            self.M_cond_ph[i_effect] = self.Mfeed * naclprops.cp_seawater(
                (self.T_preh[i_effect] + self.T_preh[i_effect + 1]) / 2, self.Xf) * (
                                           self.T_preh[i_effect] - self.T_preh[i_effect + 1]) / h2oprop.latent_heat(
                self.Tvsat_prime[i_effect])
            self.alfa_cond[i_effect] = self.M_cond_ph[i_effect] / self.Mvap[i_effect]
        ##logger.info('self.M_cond_ph value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##    self.M_cond_ph) + '"')
        ##logger.info('self.Mvap value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##    self.Mvap) + '"')
        ##logger.info('self.alfa_cond value in steamcalculation.design_equalAphpreh.py is: "' + str(
        ##    self.alfa_cond) + '"')

        # first effect
        self.Mb[0] = self.Mfeed - self.Md[0]
        self.Xb[0] = self.Mfeed * self.Xf / self.Mb[0]
        ##logger.info('Mb (first effect) value in design_equalAph_DTpreh is: "' + str(self.Mb[0]) + '"')
        ##logger.info('Xb (first effect) value in design_equalAph_DTpreh is: "' + str(self.Xb[0]) + '"')
        self.h_preh[1] = naclprops.cp_seawater(self.T_preh[1], self.Xf) * self.T_preh[1]
        self.h_vprime[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.h_cprime[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime[0])
        self.h_b[0] = naclprops.cp_seawater(self.T[0], self.Xb[0]) * self.T[0]
        self.Mc[0] = self.M_cond_ph[0] + (self.Ms - self.Mms) - self.M_fb[0]
        self.M_fb[0] = ((self.Ms - self.Mms) * h2oprop.enthalpy_satwat_liquid(self.Ts) + self.M_cond_ph[
            0] * h2oprop.enthalpy_satwat_liquid(self.Tvsat_prime[0])
                        - self.Mc[0] * h2oprop.enthalpy_satwat_liquid(
            self.Tvsat_prime[0])) / h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[0])
        self.M_fbrine[0] = 0
        self.Mvap[0] = mvap_effectbalance(0, self.Ms, self.Ts, self.Mfeed, self.T_preh, self.alfa_cond,
                                          self.Tvsat_prime,
                                          self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        #logger.info('Mvap (first effect) value in design_equalAphpreh.py is: "' + str(self.Mvap[0]) + '"')
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb) - self.Mfeed * naclprops.cp_seawater(
        #     (self.T_preh[0] + self.T[0]) / 2, self.Xb) * (self.T[0] - self.T_preh[0])) / naclprops.latent_heat_seawater(self.T[0], self.Xb)
        # (h2oprop.latent_heat(self.T[0])*(1-self.Xb*1e-6))
        # self.Md[0] = (self.Ms * h2oprop.latent_heat(self.Xb)) / h2oprop.latent_heat(self.T[0])
        # self.Mvap[0] = self.Md[0]+self.M_fb[0]
        self.Md[0] = self.Mvap[0] - self.M_fb[0]
        self.A_hx[0] = area_hx(0, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts, self.alfa_cond,
                               self.Mvap,
                               self.Tc)
        self.A_preh[0] = area_preh(0, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        # intermediate effects (2 to (N-1))
        for i_effect in tools.itereffects(1, (self.N - 1)):
            self.M_fbrine[i_effect] = brine_flash(i_effect, self.T, self.Mb, self.Xb)
            #logger.info('M_fbrine[i_effect] value in design_equalAphpreh.py is: "' + str(self.M_fbrine[i_effect]) + '"')
            self.Mc[i_effect], self.M_fb[i_effect] = generic_flashbox(i_effect, self.Mc[i_effect - 1],
                                                                      self.Tvsat_prime,
                                                                      self.Tc, self.Mvap, self.alfa_cond)
            self.Msteam = self.Mvap[i_effect - 1] - self.M_cond_ph[i_effect - 1]
            #logger.info('self.Tc[i_effect-1] value in design_equalAphpreh.py is: "' + str(self.Tc[i_effect-1]) + '"')
            #logger.info('M_fbrine[i_effect] value in design_equalAphpreh.py is: "' + str(
            #    self.M_fbrine[i_effect]) + '"')
            #logger.info('Mb[i_effect] value in design_equalAphpreh.py is: "' + str(self.Mb[i_effect]) + '"')
            # DeltaH = h2oprop.enthalpy_satwat_liquid(self.T[i_effect]) - naclprops.enthalpy_seawater(
            #     self.T[i_effect], self.Xb[i_effect])
            # self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect - 1])) / \
            #                     (h2oprop.latent_heat(self.T[i_effect]) + DeltaH)
            # (h2oprop.latent_heat(self.T[i_effect])*(1-self.Xb[i_effect-1]*1e-6))
            self.Md[i_effect] = (self.Msteam * h2oprop.latent_heat(self.Tc[i_effect-1]) + self.M_fbrine[i_effect] *
                                 (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]) -
                                  h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]))
                                 + self.Mb[i_effect] * (naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1])-
                                                        naclprops.enthalpy_seawater(self.T[i_effect], self.Xb[i_effect])))\
                                /(h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[i_effect]) - naclprops.enthalpy_seawater(self.T[i_effect-1], self.Xb[i_effect-1]))
            # self.Mvap[i_effect] = mvap_effectbalance(i_effect, self.Msteam, self.Tc[i_effect-1], self.Mfeed, self.T_preh, self.alfa_cond,
            #                                self.Tvsat_prime,
            #                                self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xb)
            self.Mvap[i_effect] = self.Md[i_effect] + self.M_fb[i_effect] + self.M_fbrine[i_effect]

            # self.Md[i_effect] = self.Mvap[i_effect] - self.M_fb[i_effect] - self.M_fbrine[i_effect]
            self.Mb[i_effect] = self.Mb[i_effect - 1] - self.Md[i_effect] - self.M_fbrine[i_effect]


            #logger.info('M_fbrine[i_effect] value in design_equalAphpreh.py is: "' + str(self.M_fbrine[i_effect]) + '"')
            #logger.info('Md (intermediate effect) value is: "' + str(self.Md[i_effect]) + '"')
            #logger.info('M_fbrine (intermediate effect) value is: "' + str(self.M_fbrine[i_effect]) + '"')
            self.Xb[i_effect] = self.Mfeed * self.Xf / self.Mb[i_effect]

            #logger.info('Mfeed (intermediate effect) value is: "' + str(self.Mfeed) + '"')
            #logger.info('Xf (intermediate effect) value is: "' + str(self.Xf) + '"')
            #logger.info('Xb (intermediate effect) value is: "' + str(self.Xb[i_effect]) + '"')
            if self.Xb[i_effect] > self.X_max:
                self.Xb[i_effect] = self.X_max

            self.A_hx[i_effect] = area_hx(i_effect, self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                          self.alfa_cond, self.Mvap, self.Tc)
            self.A_preh[i_effect] = area_preh(i_effect, self.alfa_cond, self.Mvap, self.Tvsat_prime, self.DTLM)

        #logger.info('Md (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.Md) + '"')
        #logger.info('M_fbrine (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.M_fbrine) + '"')
        #logger.info('Xb value (intermediate effects) in design_equalAph_DTpreh is: "' + str(self.Xb) + '"')
        #logger.info('self.Mfeed value (intermediate effects) in design_equalAph_DTpreh is: "' + str(self.Mfeed) + '"')
        #logger.info('self.Xf value (intermediate effects) in design_equalAph_DTpreh is: "' + str(self.Xf) + '"')
        #logger.info('self.Mb value (intermediate effects) in design_equalAph_DTpreh is: "' + str(self.Mb) + '"')
        ##logger.info('Msteam (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.Msteam) + '"')
        ##logger.info('Tc (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.Tc) + '"')
        ##logger.info('T (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.T) + '"')
        ##logger.info('Tvsat_prime (intermediate effects) value in design_equalAphpreh.py is: "' + str(self.Tvsat_prime) + '"')

        # last effect and end condenser
        self.alfa_cond[self.N - 1] = 0
        #logger.info('brine_f value is: "' + str(t) + '"')
        self.M_fbrine[self.N - 1] = brine_flash(self.N - 1, self.T, self.Mb, self.Xb)
        ##logger.info('M_fbrine (last effect and condenser) value is: "' + str(self.M_fbrine[self.N - 1]) + '"')
        Mvap_last = self.Mvap[self.N - 1] - self.Mss
        self.Mc[self.N - 1], self.M_fb[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond, self.Mc[self.N - 2],
                                                                   self.Tvsat_prime, self.Tc, self.Mvap, Mvap_last, self.Tc_prime)

        self.Msteam = self.Mvap[self.N - 2] - self.M_cond_ph[self.N - 2]
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2])) / naclprops.latent_heat_seawater(self.T[self.N-1], self.Xb[self.N-2])
        # (h2oprop.latent_heat(self.T[self.N - 1])*(1-self.Xb[self.N-2]*1e-6))
        self.Mvap[self.N-1] = mvap_effectbalance(self.N-1, self.Msteam, self.Tc[self.N-2], self.Mfeed, self.T_preh, self.alfa_cond, self.Tvsat_prime,
                                       self.T, self.M_fb, self.Mb, self.Xb, self.N, self.Xf)
        # self.Md[self.N - 1] = (self.Msteam * h2oprop.latent_heat(self.Tc[self.N - 2]) + self.M_fbrine[self.N - 1] *
        #                        (naclprops.enthalpy_seawater(self.T[self.N - 2], self.Xb[self.N - 2]) -
        #                         h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N - 1]))
        #                        + self.Mb[self.N - 1] * (
        #                            naclprops.enthalpy_seawater(self.T[self.N - 2], self.Xb[self.N - 2]) -
        #                            naclprops.enthalpy_seawater(self.T[self.N - 1], self.Xb[self.N - 1]))) \
        #                       / (h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime[self.N - 1]) - naclprops.enthalpy_seawater(
        #                           self.T[self.N - 2], self.Xb[self.N - 2]))
        # self.Mvap[self.N - 1] = self.Md[self.N - 1] + self.M_fb[self.N - 1] + self.M_fbrine[self.N - 1]
        self.Md[self.N-1] = self.Mvap[self.N-1] - self.M_fbrine[self.N-1] - self.M_fb[self.N-1]
        self.Mb[self.N - 1] = self.Mb[self.N - 2] - self.Md[self.N - 1] - self.M_fbrine[self.N - 1]
        ##logger.info('Mb (last effect) value in design_equalAph_DTpreh is: "' + str(self.Mb[self.N - 1]) + '"')
        self.A_hx[self.N - 1] = area_hx((self.N - 1), self.Mfeed, self.Xf, self.T, self.T_preh, self.Md, self.Ts,
                                        self.alfa_cond, self.Mvap, self.Tc)
        # self.n_tubes = self.A_hx[self.N - 1] / (np.pi * self.config['d_evap'] * self.config['L_evap'])
        self.A_av = np.sum(self.A_hx) / self.N
        self.DeltaA = np.abs(self.A_hx - self.A_av)
        self.Mdist_real = sum(self.Md) + sum(self.M_fbrine)
        self.Error_Mdist = abs(self.Mdist_real - self.Mdist)
        Xb_real = (self.Mfeed * self.Xf / self.Mb[self.N - 1])
        #logger.info('Xb_real value is: "' + str(Xb_real) + '"')
        Error_Xb = abs(Xb_real - self.Xb_out)
        # tot_error = self.Error_Mdist + Error_Xb
        return self.Error_Mdist


