import numpy as np

def estimation_elements_cost(N, Amembr_tot, tech_config_and_params, proj_config_and_params):
    tot_no_of_elements = np.zeros(N)
    elements_cost = np.zeros(N)
    installed_elements_cost = np.zeros(N)
    for iN in range(N):
        # total no. of elements in a given stage
        tot_no_of_elements[iN] = Amembr_tot[iN] /(tech_config_and_params['membrane_area_sw'])
        #if iN == 0:
            #elements_cost[iN] = nelem[iN] * tech_config_and_params['price_memb_element']["2018"]["SWRO"]["inch_8"] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2005']
        elements_cost[iN] = tot_no_of_elements[iN] * tech_config_and_params['price_memb_element']["2018"]["SWRO"]["inch_8"] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        installed_elements_cost[iN] = elements_cost[iN] * (1 + tech_config_and_params['f_inst'])
        #else:  # why BWRO for 2nd stage? It will be applicable to 2nd pass, not 2nd stage of SWRO. Changed it to SWRO (Nikhil)
            #elements_cost[iN] = nelem[iN] * tech_config_and_params['price_memb_element']["2018"]["SWRO"]["inch_8"] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2005']
          #  installed_elements_cost[iN] = elements_cost[iN] * (1 + tech_config_and_params['f_inst'])
    print("tot_no_of_elements: ", tot_no_of_elements)
    print("elements_cost", elements_cost)
    print("installed_elements_cost", installed_elements_cost)
    
    return tot_no_of_elements, np.sum(installed_elements_cost)

def calculate_annual_chemical_cost(Volume_m3h, needed_conc_mg_per_l, commercial_conc_perc, operation_time, specific_cost):
    """
    Calculates the chemical cost for a given post-treatment process based on operational parameters.

    Parameters:
        Volume_m3h (float): The flow rate of permeate in cubic meters per hour (m³/h).
        needed_conc_mg_per_l (float): The required concentration of the chemical in milligrams per liter (mg/L).
        commercial_conc_perc (float): The commercial concentration of the chemical as a percentage (%).
        operation_time (float): The operational time factor (typically expressed as a fraction or percentage of the total operation time).
        specific_cost (float): The specific cost of the chemical in the chosen currency (e.g., currency per unit mass or volume).

    Returns:
        float: The estimated annual chemical cost.

    Formula:
        The formula used is:
        cost = Volume_m3h * 8760 * 1e-6 * (needed_conc_mg_per_l / commercial_conc_perc * operation_time * specific_cost)

    Explanation:
        - `8760` represents the number of hours in a year (365 days * 24 hours).
        - `1e-6` converts milligrams per liter (mg/L) to tons per cubic meter (t/m³).
        - The chemical consumption is adjusted by the ratio of needed concentration to commercial concentration.
        - The final product gives the annual cost based on the flow rate, chemical concentration, operation time, and specific cost.

    Example Usage:
        >>> calculate_annual_chemical_cost(100, 50, 5, 0.8, 1.2)
        840.96
    """
    return Volume_m3h * 8760 * 1e-6 * (needed_conc_mg_per_l / commercial_conc_perc * operation_time * specific_cost)


def estimation_pressurevessels_cost(N, nelem, Amembr_tot, tech_config_and_params, proj_config_and_params):
    NPV = np.zeros(N)
    pressurevessels_cost = np.zeros(N)
    installed_vessels_cost = np.zeros(N)
    for iN in range(N):
        #if iN == 0:
        NPV[iN] = Amembr_tot[iN] /(tech_config_and_params['membrane_area_sw'] * nelem[iN])
        #pressurevessels_cost[iN] = NPV[iN] * tech_config_and_params['price_SWRO_pressvessel'] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2005']
        pressurevessels_cost[iN] = NPV[iN] * tech_config_and_params['price_pressure_vessel']["2018"]["SWRO"]["inch_8"] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        installed_vessels_cost[iN] = pressurevessels_cost[iN] * (1 + tech_config_and_params['f_inst'])
        #else:  # why BWRO for 2nd stage? Changed it to SWRO (Nikhil)            
         #   NPV[iN] = Amembr_tot[iN] / (tech_config_and_params['membrane_area_bw'] * nelem[iN])
          #  pressurevessels_cost[iN] = NPV[iN] * tech_config_and_params['price_BWRO_pressvessel'] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2005']
           # installed_vessels_cost[iN] = pressurevessels_cost[iN] * (1 + tech_config_and_params['f_inst'])
    return NPV, np.sum(installed_vessels_cost)

def estimation_piping_trainsupporting_cost(N, technical_train_size, tech_config_and_params, proj_config_and_params):
    # define empty arrays of size of stages (2, if 2 stages)
    inst_piping_cost = np.zeros(N)
    inst_train_support_cost = np.zeros(N)
    inst_train_instrum_cost = np.zeros(N)

    for iN in range(N):
        inst_piping_cost[iN] = tech_config_and_params['price_train_piping']["2018"][technical_train_size[iN]] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'] * (1 + tech_config_and_params['f_inst'])
        inst_train_support_cost[iN] = tech_config_and_params['price_train_support']["2018"][technical_train_size[iN]] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'] * (1 + tech_config_and_params['f_inst'])
        inst_train_instrum_cost[iN] = tech_config_and_params['price_train_I_and_C']["2018"][technical_train_size[iN]] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'] * (1 + tech_config_and_params['f_inst'])
    
    print("array of inst_piping_cost, inst_train_support_cost, inst_train_instrum_cost", inst_piping_cost, inst_train_support_cost, inst_train_instrum_cost)

    return (np.sum(inst_piping_cost) + np.sum(inst_train_support_cost) + np.sum(inst_train_instrum_cost))

def estimation_HPpump_cost_single(Vfeed, pressure_delta, tech_config_and_params, proj_config_and_params):
    #if Vfeed == 450:
    #    pump_cost = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * Pfeed_1
    if 200 < Vfeed <= 450:
        #pump_cost = tech_config_and_params['coeffA_catB_HPP'] * np.power((Pfeed_1 * Vfeed), tech_config_and_params['coeffB_catB_HPP'])
        pump_cost = tech_config_and_params['coeffA_catB_HPP'] * (pressure_delta * Vfeed)**tech_config_and_params['coeffB_catB_HPP']
    else:  # Vfeed < 200
        pump_cost = tech_config_and_params['coeffA_catC_HPP'] * pressure_delta * Vfeed

    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_HPpump_cost_parallpumps(Vfeed, pressure_delta, tech_config_and_params, proj_config_and_params):
    Vfeed_1 = 450
    pump_cost_1 = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * pressure_delta
    Vfeed_2 = Vfeed - Vfeed_1
    if Vfeed_2 <= 450:
        pump_cost_2 = estimation_HPpump_cost_single(Vfeed_2, pressure_delta, tech_config_and_params, proj_config_and_params)
    else:
        pump_cost_2 = estimation_HPpump_cost_parallpumps(Vfeed_2, pressure_delta, tech_config_and_params, proj_config_and_params)
    pump_cost = pump_cost_1 + pump_cost_2
    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_cost_erd_px(Vretentate, proj_config_and_params):
    """
    Estimate the cost of a Pressure Exchanger (PX) type Energy Recovery Device (ERD) in a SWRO or NF plant.

    This function calculates the cost of a ERD-PX,
    based on the retentate volume of the SWRO/NF system entering the PX at high pressure, and adjusts for current economic conditions
    using the Chemical Engineering Plant Cost Index (CEPCI).

    Parameters:
    tech_config_and_params (dict): Configuration dictionary containing CEPCI indices with keys 'current' and '2018'.
    Vretentate (float): Retentate volume flow rate in cubic meters per hour (m3/h).

    Returns:
    float: Estimated cost of the ERD-PX in current USD.
    
    References:
    Lu, Yan-Yue; Hu, Yang-Dong; Zhang, Xiu-Ling; Wu, Lian-Ying; Liu, Qing-Zhi (2007): Optimum design of 
    reverse osmosis system under different feed concentration and product specification. In 
    Journal of Membrane Science 287 (2), pp. 219-229. 
    DOI: 10.1016/j.memsci.2006.10.037.
    """

    erd_cost = 3134.7 * Vretentate**0.58 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2007']  # [USD_current]

    return erd_cost

def estimation_LPpump_cost(power, Pfeed, tech_config_and_params, proj_config_and_params):
    Cp_0 = np.power(10, (tech_config_and_params['k1_LPpump'] + tech_config_and_params['k2_LPpump'] * np.log10(power) + tech_config_and_params['k3_LPpump'] * (np.log10(power)) ** 2))
    Cp_0 = Cp_0 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']
    if Pfeed < 10:
        Fp = 1
    elif 10 <= Pfeed <= 100:
        Fp = np.power(10, (tech_config_and_params['c1_LPpump'] + tech_config_and_params['c2_LPpump'] * np.log10(Pfeed-1) + tech_config_and_params['c3_LPpump'] * (np.log10(Pfeed-1))**2))
    else:
        Fp = 1
    Cbm = Cp_0 * (tech_config_and_params['b1_LPpump'] + tech_config_and_params['b2_LPpump'] * tech_config_and_params['Fm_LPpump'] * Fp)
    return Cbm

def estimation_intake_pretreat_cost(Vfeed, tech_config_and_params, proj_config_and_params):
    Vfeed_m3d = Vfeed * 24
    swip_cost = tech_config_and_params['coeffA_intake_pretreat'] * Vfeed_m3d**tech_config_and_params['coeffB_intake_pretreat']
    return swip_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_intake_pretreat_cost_updated(Vfeed, proj_config_and_params):
    """
    Estimate the intake and pretreatment cost for a reverse osmosis (RO) desalination plant.

    This function calculates the cost of the seawater intake and pretreatment (SWIP) process using a scaling factor based on feed flow rate and cost adjustment indexes. The calculation follows the methodology described by Du et al. (2015).

    Reference:
        Du, Yawei; Xie, Lixin; Liu, Yan; Zhang, Shaofeng; Xu, Yingjun (2015): Optimization of reverse osmosis networks with split partial second pass design. In Desalination 365, pp. 365-380. DOI: 10.1016/j.desal.2015.03.019.

    Parameters:
    -----------
    Vfeed : float
        Feed flow rate in cubic meters per hour (m³/h).
    tech_config_and_params : dict
        Configuration dictionary containing:
            - 'CEPCI': A dictionary with the Chemical Engineering Plant Cost Index (CEPCI) for both the current year and the base year (2015).

            Example:
            tech_config_and_params = {
                'CEPCI': {
                    'current': 600,  # Current CEPCI value
                    '2015': 556.4    # CEPCI value for the year 2015
                }
            }

    Returns:
    --------
    swip_cost : float
        Estimated SWIP cost in current currency units.

    Calculation Steps:
    ------------------
    1. Convert feed flow rate from m³/h to m³/day.
    2. Estimate the SWIP cost using the formula:
       swip_cost = 996 * (Vfeed_m3d)**0.8 * (current_CEPCI / 2015_CEPCI)

    Notes:
    ------
    - The cost model is derived from empirical data and should be used for estimation purposes.
    - Adjustments may be needed depending on specific plant conditions and technology changes.
    """
    Vfeed_m3d = Vfeed * 24
    swip_cost = 996 * (Vfeed_m3d)**0.8 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2015']
    
    return swip_cost



def estimation_labour_cost(Vfeed, tech_config_and_params):  # Vfeed is the feed flow rate in [m3/h]
    pers_cost = tech_config_and_params['staff'] * tech_config_and_params['av_personnel_cost_year'] / tech_config_and_params['design_desplant_feed'] * Vfeed
    return pers_cost

def estimation_labour_cost_updated(Vpermeate, tech_config_and_params):  # Vpermeate is the permeate flow rate in [m3/h]
    # ref: Du, Yawei; Xie, Lixin; Liu, Yan; Zhang, Shaofeng; Xu, Yingjun (2015): Optimization of reverse osmosis networks with split partial second pass design. In Desalination 365, pp. 365-380. DOI: 10.1016/j.desal.2015.03.019.
    
    pers_cost = Vpermeate * 24 * 365 * tech_config_and_params['plant_availability'] * 0.01
    return pers_cost  #[US$/y]

def estimation_maintenance_cost(equipment_cost, labour_cost_annual, tech_config_and_params):
    maint_cost = tech_config_and_params['perc_maint_CAPEX'] * equipment_cost + tech_config_and_params['perc_mant_labour'] * labour_cost_annual
    return maint_cost

def estimation_electric_cost(spec_el_consumption, Mdist, electricity_cost, tech_config_and_params):  #Mdist in kg/s, spec_el_con in kWh/m3
    operating_hours = 8760 * tech_config_and_params['plant_availability']
    electric_consumption = spec_el_consumption * (Mdist * 3600 / 1000 * operating_hours) # [kWh/y]
    annual_el_cost = electric_consumption * electricity_cost # [US$/y]
    #spec_el_cost = spec_el_consumption * electricity_cost # [US$/m3]
    return annual_el_cost

def estimation_membrane_replac_cost(N, NPV, nelem, with_preceding_nf, tech_config_and_params, proj_config_and_params):

    if with_preceding_nf:
        replacement_rate = tech_config_and_params['replacement_rate']['with_preceding_nf']
    else:
        replacement_rate = tech_config_and_params['replacement_rate']['standalone_plant']

    annual_cost_replacement = np.zeros(N)
    for iN in range(N):
        #if iN == 0:
        annual_cost_replacement[iN] = NPV[iN] * nelem[iN] * replacement_rate * tech_config_and_params['price_memb_element']["2018"]["SWRO"]["inch_8"] *\
                                        proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2005']
        #else:
         #   annual_cost_replacement[iN] = NPV[iN] * nelem[iN] * tech_config_and_params['replacement_rate_2ndstage'] * tech_config_and_params['price_memb_element']["2018"]["SWRO"]["inch_8"] * \
          #                                tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2005']
    return np.sum(annual_cost_replacement)

def estimation_chemical_cost(Vfeed, Vpermeate, N, tech_config_and_params):
    cost_chlorination = Vfeed * 8760 * 1e-6 * tech_config_and_params['needed_perc_chlorination'] \
                        / tech_config_and_params['comm_conc_chlorination'] * tech_config_and_params['operation_time_chlorination'] * tech_config_and_params['specific_cost_chlorination']
    cost_flocculation = Vfeed * 8760 * 1e-6 * (tech_config_and_params['needed_perc_coagulant'] \
                        / tech_config_and_params['comm_conc_coagulant'] * tech_config_and_params['operation_time_coagulant'] * tech_config_and_params['specific_cost_coagulant']
                        + tech_config_and_params['needed_perc_floacculant'] / tech_config_and_params['comm_conc_floacculant'] * tech_config_and_params['operation_time_floacculant']
                                                        * tech_config_and_params['specific_cost_floacculant'] + tech_config_and_params['needed_perc_sulphacid'] \
                        / tech_config_and_params['comm_conc_sulphacid'] * tech_config_and_params['operation_time_sulphacid'] * tech_config_and_params['specific_cost_sulphacid'])
    cost_filtration = Vfeed * 8760 * 1e-6 * tech_config_and_params['needed_perc_filtration'] \
                        / tech_config_and_params['comm_conc_filtration'] * tech_config_and_params['operation_time_filtration'] * tech_config_and_params['specific_cost_filtration']
    if N > 1:
        cost_antiscalant_stage = np.zeros(N)
        cost_antiscalant_stage[0] = Vfeed * 8760 * 1e-6 * (tech_config_and_params['needed_perc_antiscalant_1stpass'] \
                        / tech_config_and_params['comm_conc_antiscalant_1stpass'] * tech_config_and_params['operation_time_antiscalant_1stpass'] * tech_config_and_params['specific_cost_antiscalant_1stpass']
                        + tech_config_and_params['needed_perc_bisulphite_1stpass'] / tech_config_and_params['comm_conc_bisulphite_1stpass'] * tech_config_and_params['operation_time_bisulphite_1stpass']
                                                        * tech_config_and_params['specific_cost_bisulphite_1stpass'])
        for iN in range(1, N):
            cost_antiscalant_stage[iN] = Vpermeate[iN-1] * 8760 * 1e-6 * (tech_config_and_params['needed_perc_antiscalant_2ndpass'] \
                        / tech_config_and_params['comm_conc_antiscalant_2ndpass'] * tech_config_and_params['operation_time_antiscalant_2ndpass'] * tech_config_and_params['specific_cost_antiscalant_2ndpass']
                        + tech_config_and_params['needed_perc_causticsoda_2ndpass'] / tech_config_and_params['comm_conc_causticsoda_2ndpass'] * tech_config_and_params['operation_time_causticsoda_2ndpass']
                                                        * tech_config_and_params['specific_cost_causticsoda_2ndpass'])
        cost_antiscalant = np.sum(cost_antiscalant_stage)
    else:
        cost_antiscalant = Vfeed * 8760 * 1e-6 * (tech_config_and_params['needed_perc_antiscalant_1stpass'] \
                        / tech_config_and_params['comm_conc_antiscalant_1stpass'] * tech_config_and_params['operation_time_antiscalant_1stpass'] * tech_config_and_params['specific_cost_antiscalant_1stpass']
                        + tech_config_and_params['needed_perc_bisulphite_1stpass'] / tech_config_and_params['comm_conc_bisulphite_1stpass'] * tech_config_and_params['operation_time_bisulphite_1stpass']
                                                        * tech_config_and_params['specific_cost_bisulphite_1stpass'])
    if N > 1:
        cost_posttreatment = Vpermeate[N-1] * 8760 * 1e-6 * (tech_config_and_params['needed_perc_Ca(OH)2'] \
                        / tech_config_and_params['comm_conc_Ca(OH)2'] * tech_config_and_params['operation_time_Ca(OH)2'] * tech_config_and_params['specific_cost_Ca(OH)2']
                        + tech_config_and_params['needed_perc_CO2'] / tech_config_and_params['comm_conc_CO2'] * tech_config_and_params['operation_time_CO2']
                                                        * tech_config_and_params['specific_cost_CO2'] + tech_config_and_params['needed_perc_NaHypochlorite'] \
                        / tech_config_and_params['comm_conc_NaHypochlorite'] * tech_config_and_params['operation_time_NaHypochlorite'] * tech_config_and_params['specific_cost_NaHypochlorite'])
    else:
        cost_posttreatment = Vpermeate * 8760 * 1e-6 * (tech_config_and_params['needed_perc_Ca(OH)2'] \
                        / tech_config_and_params['comm_conc_Ca(OH)2'] * tech_config_and_params['operation_time_Ca(OH)2'] * tech_config_and_params['specific_cost_Ca(OH)2']
                        + tech_config_and_params['needed_perc_CO2'] / tech_config_and_params['comm_conc_CO2'] * tech_config_and_params['operation_time_CO2']
                                                        * tech_config_and_params['specific_cost_CO2'] + tech_config_and_params['needed_perc_NaHypochlorite'] \
                        / tech_config_and_params['comm_conc_NaHypochlorite'] * tech_config_and_params['operation_time_NaHypochlorite'] * tech_config_and_params['specific_cost_NaHypochlorite'])
    chem_cost = cost_chlorination + cost_flocculation + cost_filtration + cost_antiscalant + cost_posttreatment
    return chem_cost

def estimation_power_and_cost_intake_system(Vfeed_m3h, tech_config_and_params, proj_config_and_params):

    ## Capital Cost
    # Construction cost varies between 90-120 USD_2018/m3_perm/day (avg. 105) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    # Assuming 50% recovery rate for the reference SWRO cost, their spec. cost should be divided by 2 to get USD_2018/m3_feed/day
    Vfeed_m3day = Vfeed_m3h * 24
    if Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["upper_limit"] / 2 * Vfeed_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["average"] / 2 * Vfeed_m3day
    elif Vfeed_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["lower_limit"] / 2 * Vfeed_m3day



    ## consists of three components: 1] intake structure; 2] pumping station; 3] auxilliaries. The capital cost equations are based on the graphs from Nikolay Project estimation book (2018)   
    
    """
    # Set default intake type/subtype based on seawater, unless explicitly specified by the user
    intake_structure_type = proj_config_and_params.get("intake_structure_type", tech_config_and_params["intake_type_subtype_default"][proj_config_and_params["sea"]]["type"])
    intake_structure_subtype = proj_config_and_params.get("intake_structure_subtype", tech_config_and_params["intake_type_subtype_default"][proj_config_and_params["sea"]]["subtype"])    
    print("intake_structure_type, intake_structure_subtype: ",intake_structure_type, intake_structure_subtype)
    
    ### 1] intake structure 
    if intake_structure_subtype == "offshore":
        intake_material_if_offshore = proj_config_and_params["intake_material_if_offshore"]
        offshore_intake_length_m = tech_config_and_params["offshore_intake_length_km"][proj_config_and_params["sea"]] * 1000
        if intake_material_if_offshore == "HDPE":
            capital_cost_intake_structure = ((-1e-21*Vperm_m3day**4) + (1e-15*Vperm_m3day**3) - (5e-10*Vperm_m3day**2) + (0.0002*Vperm_m3day) + 0.3415) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        elif intake_material_if_offshore == "concrete_tunnel":
            capital_cost_intake_structure = ((-4e-21*Vperm_m3day**4) + (4e-15*Vperm_m3day**3) - (2e-9*Vperm_m3day**2) + (0.0005*v) + 1.8519) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif intake_structure_subtype == "onshore":
        capital_cost_intake_structure = ((4e-14*Vperm_m3day**3) - (4e-8*Vperm_m3day**2) + (0.0519*Vperm_m3day) + 125.31) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    
    ### 2] pumping station
    intake_pumping_type = proj_config_and_params["intake_pumping_type"]
    ## capital cost
    if intake_pumping_type == "dry":
        capital_cost_intake_pumping_station = ((1e-14*Vperm_m3day**3) - (1e-8*Vperm_m3day**2) + (0.0169*Vperm_m3day) + 83.825) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif intake_pumping_type == "wet":
        capital_cost_intake_pumping_station = ((5e-15*Vperm_m3day**3) - (5e-9*Vperm_m3day**2) + (0.0098*Vperm_m3day) + 32.067) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    print("capital_cost_intake_pumping_station", capital_cost_intake_pumping_station)
    
    ### 3] auxilliaries
    # It's capital cost is assumed to be 1.7 times that of 1] + 2], which approximately gives their sum to lie between 90 and 120 USD_2018/m3/day, which is total intake cost (source: Nikolay (2018))
    capital_cost_intake_auxilliaries = 1.7 * (capital_cost_intake_structure + capital_cost_intake_pumping_station)

   
    # total capital cost
    capital_cost_intake_system = capital_cost_intake_structure + capital_cost_intake_pumping_station + capital_cost_intake_auxilliaries
    """

    # power 
    power_intake_pump = tech_config_and_params["intake_pump_pressure"] * Vfeed_m3h / tech_config_and_params['eff_intake_pump'] * 1e5 * 1e-3 / 3600  # kW


    return power_intake_pump, capital_cost_intake_system * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']


def estimation_cost_pretreatment_updated(Vfeed_m3h, tech_config_and_params, proj_config_and_params):

    ## consists of physical and chemical pretreatment
    
    ## Capital cost
    # Construction cost varies between 150-230 USD_2018/m3_perm/day (avg. 190) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    # Assuming 50% recovery rate for the reference SWRO cost, their spec. cost should be divided by 2 to get USD_2018/m3_feed/day
    Vfeed_m3day = Vfeed_m3h * 24
    if Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["pretreatment_spec_capital_cost"]["upper_limit"] / 2 * Vfeed_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["pretreatment_spec_capital_cost"]["average"] / 2 * Vfeed_m3day
    elif Vfeed_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["pretreatment_spec_capital_cost"]["lower_limit"] / 2 * Vfeed_m3day
    
    # Annual costs for various treatments
    cost_chlorination = calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_chlorination'], tech_config_and_params['comm_conc_chlorination'],
                                       tech_config_and_params['operation_time_chlorination'], tech_config_and_params['specific_cost_chlorination'])

    cost_flocculation = calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_coagulant'], tech_config_and_params['comm_conc_coagulant'],
                                       tech_config_and_params['operation_time_coagulant'], tech_config_and_params['specific_cost_coagulant']) + \
                        calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_floacculant'], tech_config_and_params['comm_conc_floacculant'],
                                       tech_config_and_params['operation_time_floacculant'], tech_config_and_params['specific_cost_floacculant']) + \
                        calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_sulphacid'], tech_config_and_params['comm_conc_sulphacid'],
                                       tech_config_and_params['operation_time_sulphacid'], tech_config_and_params['specific_cost_sulphacid'])

    cost_filtration = calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_filtration'], tech_config_and_params['comm_conc_filtration'],
                                     tech_config_and_params['operation_time_filtration'], tech_config_and_params['specific_cost_filtration'])

    # Costs for antiscalants
    first_pass_cost = (
        calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_antiscalant_1stpass'], tech_config_and_params['comm_conc_antiscalant_1stpass'],
                       tech_config_and_params['operation_time_antiscalant_1stpass'], tech_config_and_params['specific_cost_antiscalant_1stpass']) +
        calculate_annual_chemical_cost(Vfeed_m3h, tech_config_and_params['needed_perc_bisulphite_1stpass'], tech_config_and_params['comm_conc_bisulphite_1stpass'],
                       tech_config_and_params['operation_time_bisulphite_1stpass'], tech_config_and_params['specific_cost_bisulphite_1stpass'])
    )

    
    cost_antiscalant = first_pass_cost

    # Total annual pretreatment chemical cost
    annual_pretreatment_chem_cost = cost_chlorination + cost_flocculation + cost_filtration + cost_antiscalant

    return capital_cost_physical_and_chemical_pretreatment * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'], annual_pretreatment_chem_cost

def estimation_cost_posttreatment_updated(Vpermeate_m3h, tech_config_and_params, proj_config_and_params):
    """
    Estimates both the capital and annual chemical costs for post-treatment processes based on permeate volume, project configuration, and economic parameters.

    Parameters:
        Vpermeate_m3h (float, list, or np.ndarray): The permeate flow rate in cubic meters per hour (m³/h). Can be a single value or a list/array representing multiple values.
        project_config (dict): Configuration dictionary containing details on post-treatment processes (e.g., stabilization and disinfection methods).
        tech_config_and_params (dict): Dictionary of technical parameters such as required chemical concentrations, commercial concentrations, operation times, and specific costs.
        proj_config_and_params (dict): Contains tech_config_and_params settings like post-treatment processes (e.g., stabilization and disinfection methods). Among params, contains economic parameters including CEPCI (Chemical Engineering Plant Cost Index) for current and base years.

    Returns:
        tuple: Contains the following two elements:
            - total_capital_cost_post_treatment (float): Total capital cost for permeate stabilization and disinfection [in USD].
            - total_annual_cost_post_treatment (float): Total annual operating cost for permeate stabilization and disinfection [in USD].

    Raises:
        SystemExit: If the formulation for certain treatments (e.g., annual chemical cost for calcite with CO2 or chlorine dioxide) is missing.

    Notes:
        - The capital cost estimation is adjusted based on the current and reference CEPCI values.
        - The stabilization methods supported are "lime_with_co2" and "calcite_with_co2".
        - The disinfection methods supported are "NaHypochlorite" and "chlorine_dioxide".
        - The calculations are based on formulas derived from cost estimation models, adjusted for the relevant scale and CEPCI index.

    Example Usage:
        >>> estimation_cost_posttreatment_updated(100, project_config, tech_config_and_params, eco_param)
        (total_capital_cost_post_treatment, total_annual_cost_post_treatment)
    """

    total_Vpermeate_m3h = sum(Vpermeate_m3h) if isinstance(Vpermeate_m3h, (list, np.ndarray)) else Vpermeate_m3h
    total_Vpermeate_m3day = total_Vpermeate_m3h * 24
    
    ## Capital cost
    # Construction cost varies between 150-230 USD_2018/m3_perm/day (avg. 190) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    if total_Vpermeate_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_posttreatment = tech_config_and_params["posttreatment_spec_capital_cost"]["upper_limit"] * total_Vpermeate_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= total_Vpermeate_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_posttreatment = tech_config_and_params["posttreatment_spec_capital_cost"]["average"] * total_Vpermeate_m3day
    elif total_Vpermeate_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_posttreatment = tech_config_and_params["posttreatment_spec_capital_cost"]["lower_limit"] * total_Vpermeate_m3day

    
        ### Operational cost of chemicals
    # modify Vperm based on the share of it being treated (based on treatment method)
    stabilization_method = proj_config_and_params["posttreatment"]["permeate_stabilization"]
    total_Vpermeate_treated_stabil_m3day = total_Vpermeate_m3day * tech_config_and_params["perm_flow_share_stabilization"][stabilization_method]

    # Determine permeate stabilization method and calculate costs
    if stabilization_method == "lime_with_co2":
        annual_cost_permeate_stabilization = (
            calculate_annual_chemical_cost(total_Vpermeate_treated_stabil_m3day, tech_config_and_params['needed_perc_Ca(OH)2'], tech_config_and_params['comm_conc_Ca(OH)2'], tech_config_and_params['operation_time_Ca(OH)2'], tech_config_and_params['specific_cost_Ca(OH)2']) +
            calculate_annual_chemical_cost(total_Vpermeate_treated_stabil_m3day, tech_config_and_params['needed_perc_CO2'], tech_config_and_params['comm_conc_CO2'], tech_config_and_params['operation_time_CO2'], tech_config_and_params['specific_cost_CO2'])
        )
        #capital_cost_permeate_stabilization = ((-1e-17*total_Vpermeate_treated_stabil_m3day**4) + (5e-12*total_Vpermeate_treated_stabil_m3day**3) - (1e-6*total_Vpermeate_treated_stabil_m3day**2) + (0.1136*total_Vpermeate_treated_stabil_m3day) + 390.28) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif stabilization_method == "calcite_with_co2":
        
        #capital_cost_permeate_stabilization = ((-6e-18*total_Vpermeate_treated_stabil_m3day**4) + (3e-12*total_Vpermeate_treated_stabil_m3day**3) - (5e-7*total_Vpermeate_treated_stabil_m3day**2) + (0.0591*total_Vpermeate_treated_stabil_m3day) + 219.99) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        raise SystemExit("Error: Formulation for annual chemical cost using calcite with CO2 for permeate stabilization is missing. Script terminated.")

    # Determine disinfection method and calculate costs
    disinfectant_method = proj_config_and_params["posttreatment"]["disinfectant"]
    if disinfectant_method == "NaHypochlorite":
        annual_cost_permeate_disinfectant = calculate_annual_chemical_cost(
            total_Vpermeate_m3h, 
            tech_config_and_params['needed_perc_NaHypochlorite'], 
            tech_config_and_params['comm_conc_NaHypochlorite'], 
            tech_config_and_params['operation_time_NaHypochlorite'], 
            tech_config_and_params['specific_cost_NaHypochlorite']
        )
        #capital_cost_permeate_disinfectant = ((-1e-18*total_Vpermeate_m3day**4) + (5e-13*total_Vpermeate_m3day**3) - (8e-8*total_Vpermeate_m3day**2) + (0.009*total_Vpermeate_m3day) + 34.285) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif disinfectant_method == "chlorine_dioxide":
        #capital_cost_permeate_disinfectant = ((-6e-18*total_Vpermeate_m3day**4) + (2e-12*total_Vpermeate_m3day**3) - (2e-7*total_Vpermeate_m3day**2) + (0.0165*total_Vpermeate_m3day) + 39.919) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        raise SystemExit("Error: Formulation for annual chemical cost using chlorine_dioxide for permeate disinfection is missing. Script terminated.")

    # Calculate total capital and annual costs
    #capital_cost_posttreatment = capital_cost_permeate_stabilization + capital_cost_permeate_disinfectant
    annual_posttreatment_chem_cost = annual_cost_permeate_stabilization + annual_cost_permeate_disinfectant

    return capital_cost_posttreatment * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'], annual_posttreatment_chem_cost


def estimation_total_costs(tech_config_and_params, N, nelem, Vfeed, Vpermeate, Pfeed_1, Mpermeate_calc, Amembr_tot_calc,
                           spec_el_consumption, LP_power, Pfeed_2, electricity_cost):
    elements_cost = estimation_elements_cost(N, nelem, tech_config_and_params)
    NPV, vessels_cost = estimation_pressurevessels_cost(N, nelem, Amembr_tot_calc, tech_config_and_params)
    piping_instrum_cost = estimation_piping_trainsupporting_cost(N, tech_config_and_params)
    if Vfeed <= 450:
        pump_cost = estimation_HPpump_cost_single(tech_config_and_params, Vfeed, Pfeed_1)
    else:
        pump_cost = estimation_HPpump_cost_parallpumps(tech_config_and_params, Vfeed, Pfeed_1)
    if N > 1:
        LPpump_cost = estimation_LPpump_cost(tech_config_and_params, LP_power, Pfeed_2)
    else:
        LPpump_cost = 0
    intake_pretreat_cost = estimation_intake_pretreat_cost(Vfeed, tech_config_and_params)
    equipment_cost = elements_cost + vessels_cost + piping_instrum_cost + pump_cost + intake_pretreat_cost + LPpump_cost
    site_cost = tech_config_and_params['perc_site_equip'] * equipment_cost
    total_direct_cost = equipment_cost + site_cost
    total_indirect_cost = tech_config_and_params['perc_TIndCC_TDCC'] * total_direct_cost
    total_fixed_cost = total_direct_cost + total_indirect_cost
    annual_fixed_cost = annualize_capital_cost(tech_config_and_params, total_fixed_cost)
    volume_fixed_cost = annual_fixed_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    ROstructure_cost = elements_cost + vessels_cost + piping_instrum_cost
    annual_ROstructure_cost = annualize_capital_cost(tech_config_and_params, ROstructure_cost)
    volume_ROstructure_cost = annual_ROstructure_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_SWIP_cost = annualize_capital_cost(tech_config_and_params, intake_pretreat_cost)
    volume_SWIP_cost = annual_SWIP_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_pump_cost = annualize_capital_cost(tech_config_and_params, (pump_cost + LPpump_cost))
    volume_pump_cost = annual_pump_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])

    annual_personnel_cost = estimation_labour_cost(tech_config_and_params, Vfeed)
    volume_personnel_cost = annual_personnel_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_maintenance_cost = estimation_maintenance_cost(tech_config_and_params, total_fixed_cost, annual_personnel_cost)
    volume_maintenance_cost = annual_maintenance_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_electric_cost, volume_electric_cost = estimation_electric_cost(tech_config_and_params, spec_el_consumption, Mpermeate_calc, electricity_cost)
    annual_membr_replacement_cost = estimation_membrane_replac_cost(tech_config_and_params, N, NPV, nelem)
    volume_membr_replacement_cost = annual_membr_replacement_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_chemical_cost = estimation_chemical_cost(tech_config_and_params, Vfeed, Vpermeate, N)
    volume_chemical_cost = annual_chemical_cost / (Mpermeate_calc * 3600 / 1000 * 24 * 365 * tech_config_and_params['plant_availability'])
    annual_operating_cost = annual_personnel_cost + annual_maintenance_cost + annual_electric_cost + annual_membr_replacement_cost + annual_chemical_cost
    volume_operating_cost = volume_personnel_cost + volume_maintenance_cost + volume_electric_cost + volume_membr_replacement_cost + volume_chemical_cost
    annual_total_cost = annual_fixed_cost + annual_operating_cost
    volume_total_cost = volume_fixed_cost + volume_operating_cost
    return annual_fixed_cost, annual_operating_cost, annual_total_cost
