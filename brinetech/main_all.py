from argparse import ArgumentParser
from sys import argv
import os
import yaml
import pandas as pd

# Try to import CLoader from PyYAML for performance improvement; fallback to standard Loader if unavailable
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

# Import required functions and models from common_and_tools
from common_and_tools.common import save_resultlists
from common_and_tools.tools import model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd
from common_and_tools.tools import model_crystallizer_double_dol_maghyd_self_calsul
from common_and_tools.tools import model_crystallizer_double_dol_maghyd_sodcar_calcar
from common_and_tools.tools import model_crystallizer_single_sodhyd_maghyd
from common_and_tools.tools import model_crystallizer_single_self_sodchl_thermal
from common_and_tools.tools import model_crystallizer_single_self_sodchl_electric
from common_and_tools.tools import model_crystallizer_single_sodcar_calcar
from common_and_tools.tools import model_nanofiltration_rejection_given
from common_and_tools.tools import model_reverse_osmosis
from common_and_tools.tools import model_hpro
from common_and_tools.tools import model_oaro_two_stage
from common_and_tools.tools import model_pretreatment
from common_and_tools.tools import model_posttreatment
from common_and_tools.tools import model_intake
from common_and_tools.tools import model_levelized_costs_plant_config_1
from common_and_tools.tools import model_multi_effect_distillation
from common_and_tools.tools import model_electrodialysis
from common_and_tools.tools import model_secondpass_ro_with_ix

def driver(model, programconfigpath, tech_config_params_filepath, proj_config_params_filepath, Cfeed_filepath, nacl_densities_excel_filepath, Pfeed, Pfeed_pump, recovery_req, N_trains, with_pretreatment_cost, with_swip_and_posttreatment, with_preceding_nf, Qfeed, Mfeed, electricity_cost,
           heat_cost, T_in, Rionfilepath, nNF, with_erd, erd_pressure_in_hp, erd_feed_q_in_hp_train, nelem_1, nelem_2, Xfeed, Xret_out, N_effects, Ts_design, i_j, Mdial_con_ratio, following_tech, 
           recovery_techs_filepath, Cannual_costs_techs_with_elec_wo_brine_filepath, Qupw_requirement_tot_m3h, Qfeed_water_mgcryst_tot_m3h, Qeffluent_mgcryst_tot_m3h, Mmgoh2_mgcryst_tot_tonh, Mnacl_naclcryst_tot_tonh,
           output_directory, output_namefile, output_filename):

    """
    Runs the specified simulation model with the given parameters and saves the results.

    Parameters
    ----------
    model : function
        The simulation model to be used.
    programconfigpath : str
        Path to the program configuration file (used by specific models like multi-effect distillation).
    tech_config_params_filepath : str
        Path to the parameter configuration file.
    Cfeed_filepath : str
        Path to the concentration feed file.
    Pfeed : float
        Feed pressure (used by nanofiltration).
    recovery_req : float
        Required recovery rate (used by nanofiltration).
    Qfeed : float
        Feed volumetric flow rate (used by crystallizers and nanofiltration).
    Mfeed : float
        Feed mass flow rate (used by crystallizers, reverse osmosis, multi-effect distillation, and electrodialysis).
    electricity_cost : float
        Cost of electricity in $/kWh.
    heat_cost : float
        Cost of heat in $/kWh_th (used by crystallizers and multi-effect distillation).
    T_in : float
        Temperature in °C (used by crystallizers and multi-effect distillation).
    Rionfilepath : str
        Path to the ion rejection file (used by nanofiltration).
    nNF : int
        Number of nanofiltration units in the treatment chain.
    nelem_1 : int
        Number of elements in the first stage of reverse osmosis.
    nelem_2 : int
        Number of elements in the second stage of reverse osmosis.
    Xfeed : float
        Salinity of the feed stream in ppm (used by reverse osmosis, multi-effect distillation, and electrodialysis).
    Xret_out : float
        Desired salinity of the retentate stream in ppm (used by reverse osmosis, multi-effect distillation, and electrodialysis).
    N_effects : int
        Number of effects (used by multi-effect distillation).
    Ts_design : float
        Design temperature in °C (used by multi-effect distillation).
    i_j : float
        Current density in A/m² (used by electrodialysis).
    Mdial_con_ratio : float
        Dialuate-to-conc splitting ratio (used by electrodialysis).
    output_directory : str
        Directory where the output files will be saved.
    output_namefile : str
        Name of the file that stores the output filename.
    output_filename : str
        Name of the output file.

    Returns
    -------
    None
        The function saves the results to a specified directory.
    """

    # load yaml files
    if "levelized_costs" not in str(model):
        with open(tech_config_params_filepath, "r") as file_params:
            tech_config_and_params = yaml.load(file_params, Loader=Loader)
    with open(proj_config_params_filepath, "r") as file_params:
        proj_config_and_params = yaml.load(file_params, Loader=Loader)
    if "nanofiltration" in str(model) or "crystallizer" in str(model):
        with open(Cfeed_filepath, "r") as file_conc:
            Cfeed = yaml.load(file_conc, Loader=Loader)
    if "crystallizer_nacl" in str(model):
        df_nacl_densities = pd.read_excel(nacl_densities_excel_filepath, index_col=0)
        df_nacl_densities = df_nacl_densities.T
    if "levelized_costs" in str(model):
        with open(recovery_techs_filepath, "r") as file_params:
            recovery_techs = yaml.load(file_params, Loader=Loader)
        with open(Cannual_costs_techs_with_elec_wo_brine_filepath, "r") as file_params:
            Cannual_costs_techs_with_elec_wo_brine = yaml.load(file_params, Loader=Loader)
            # recovery_techs_filepath, Cannual_costs_techs_with_elec_wo_brine_filepath


    ## result lists
    if "nanofiltration" in str(model):
        with open(Rionfilepath, "r") as file_rej:
            R_ion_given = yaml.load(file_rej, Loader=Loader)
            print(model)
        result_lists = model(tech_config_and_params, proj_config_and_params, Cfeed, Pfeed, Pfeed_pump, recovery_req, N_trains, with_pretreatment_cost, Qfeed, electricity_cost, R_ion_given, with_erd, erd_pressure_in_hp, erd_feed_q_in_hp_train)
    elif "crystallizer_model" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, Cfeed, N_trains, Qfeed, electricity_cost)
    elif "crystallizer_nacl_model_thermal" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, df_nacl_densities, Cfeed, N_trains, Mfeed, electricity_cost, heat_cost, T_in)
    elif "crystallizer_nacl_model_electric" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, df_nacl_densities, Cfeed, N_trains, Mfeed, electricity_cost)
    elif "reverse_osmosis_model" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, nelem_1, nelem_2, N_trains, with_swip_and_posttreatment, with_preceding_nf, Mfeed, Xfeed, Xret_out, electricity_cost, with_erd)
        stage = "single" if "simulation-single" in str(result_lists[0]) else "double"
        if with_erd:
            output_filename = f"results_{stage}_stage_mfeedkgs_train={round(Mfeed, 2)}_with_erd.xls"
        else:
            output_filename = f"results_{stage}_stage_mfeedkgs_train={round(Mfeed, 2)}_wo_erd.xls"
    elif "hpro_model" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, Pfeed_pump, Xfeed, electricity_cost, with_erd, following_tech)
    elif "two_stage_oaro_model" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, Xfeed, electricity_cost)
    elif "pretreatment" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, electricity_cost)
    elif "posttreatment" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, Xfeed, electricity_cost)
    elif "intake" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, electricity_cost)
    elif "levelized_cost" in str(model):
        result_lists = model(proj_config_and_params, Qfeed, recovery_techs, Cannual_costs_techs_with_elec_wo_brine, Qupw_requirement_tot_m3h, Qfeed_water_mgcryst_tot_m3h, Qeffluent_mgcryst_tot_m3h, Mmgoh2_mgcryst_tot_tonh, Mnacl_naclcryst_tot_tonh)
    elif "multi_effect_distillation" in str(model):
        with open(programconfigpath, "r") as file_program:
            program_config = yaml.load(file_program, Loader=Loader)
        result_lists = model(program_config, tech_config_and_params, proj_config_and_params, N_effects, N_trains, Mfeed, Xfeed, Xret_out, Ts_design, electricity_cost, heat_cost)
    elif "electrodialysis" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, Xfeed, Xret_out, i_j, N_trains, Mfeed, Mdial_con_ratio, electricity_cost)
    elif "secondpass_ro_with_ix_model" in str(model):
        result_lists = model(tech_config_and_params, proj_config_and_params, N_trains, Qfeed, Xfeed, electricity_cost)
        
    for result_list in result_lists:
        print(result_list)

    print("output_directory: ", output_directory)
    print("output_namefile: ", output_namefile)
    print("output_filename: ", output_filename)

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    """
    Parses command-line arguments, sets up paths and configuration, and runs the specified simulation model.

    Parameters
    ----------
    argv : list of str
        Command-line arguments passed to the script.

    Returns
    -------
    None
        The function executes the driver function and saves the results.
    """    
    models = {
        'crystallizer_double_sodhyd_maghyd_sodhyd_calhyd': model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd,
        'crystallizer_double_dol_maghyd_self_calsul': model_crystallizer_double_dol_maghyd_self_calsul,
        'crystallizer_double_dol_maghyd_sodcar_calcar': model_crystallizer_double_dol_maghyd_sodcar_calcar,
        'crystallizer_single_sodhyd_maghyd': model_crystallizer_single_sodhyd_maghyd,
        'crystallizer_single_self_sodchl_thermal': model_crystallizer_single_self_sodchl_thermal,
        'crystallizer_single_self_sodchl_electric': model_crystallizer_single_self_sodchl_electric,
        'crystallizer_single_sodcar_calcar': model_crystallizer_single_sodcar_calcar,
        'nanofiltration': model_nanofiltration_rejection_given,
        'reverse_osmosis': model_reverse_osmosis,
        'hpro': model_hpro,
        'oaro_two_stage': model_oaro_two_stage,
        'pretreatment': model_pretreatment,
        'posttreatment': model_posttreatment,
        'intake_system': model_intake,
        'levelized_costs': model_levelized_costs_plant_config_1,
        'multi_effect_distillation': model_multi_effect_distillation,
        'electrodialysis': model_electrodialysis,
        'secondpass_ro_with_ix': model_secondpass_ro_with_ix,
        }

    # Create top-level parcer for command-line arguments
    parser = ArgumentParser(prog='brinetech_tools', description='NF, crystallizers, RO and MED simulation tools')
    parser.add_argument('-m', '--model', choices=models.keys())
    #parser.add_argument('-e', '--electricity_cost', type=float, default=0.1035, help="Cost of electricity in $/kWh")
    parser.add_argument('-e', '--electricity_cost', type=float, help="Cost of electricity in $/kWh")
    parser.add_argument('-sea', '--sea_name', type=str, help="Name of the chosen seawater ['arabian_gulf_kuwait', 'typical_seawater']")
    parser.add_argument('-tds', '--tds_cat', type=str, help="Chosen TDS category for the sea ['average_tds', 'max_tds', 'min_tds']")
    parser.add_argument('-cc', '--rce_conc_file', type=str, help="Path of yaml file for feed concentration [mol/m3 for tools except NaCl cryst, where it is ppm (this option is used mainly in RCE tool)")
    parser.add_argument('-trains', '--no_of_trains', type=int, help="Number of trains of a given desal tech")
    subparsers = parser.add_subparsers(dest='subcommand', help='sub-command help')

    # create a parser for crystallizer models
    parser_crystallizer = subparsers.add_parser('crystallizer', help='Crystallizer help for crystallizers other than NaCl')
    parser_crystallizer.add_argument('-q', '--feed-q', type=float, required=True, help="Feed volumetric flow rate Q in m³/h")

    # create a parser for NaCl crystallizer model
    parser_nacl = subparsers.add_parser('nacl_crystallizer', help='Crystallizer help for nacl crystallizer')
    parser_nacl.add_argument('-fm', '--feed-m', type=float, required=True, help="Total feed mass flow rate in ALL trains in kg/h")
    parser_nacl.add_argument('-he', '--heat-cost', type=float, default=0.01, help="Cost of heat in $/kWh_th")
    parser_nacl.add_argument('-t', '--t-n-design', type=float, default=25, help="Tn_design temperature in °C")
    

    # create a parser for nanofiltration
    parser_nanofiltration = subparsers.add_parser('nanofiltration', help='Nanofiltration help')
    parser_nanofiltration.add_argument('-q', '--feed-q', type=float, required=True, help="Total feed volumetric flow rate in ALL trains in m³/h")
    parser_nanofiltration.add_argument('-rr', '--req-rec', type=float, required=True, help="required recovery in [-]")
    parser_nanofiltration.add_argument('-p', '--feed-p', type=float, required=True, help="Feed pressure P in bar")
    parser_nanofiltration.add_argument('-nnf', '--nNF', type=int, required=True, help="Number of nanofiltration in treatment chain [-]")
    parser_nanofiltration.add_argument('-pp', '--pump_feed_pressure', type=float, help="Pump feed pressure in bar")
    parser_nanofiltration.add_argument('-pretreat', '--with_pretreatment_cost', action='store_true', help="Includes chemical pretreatment cost in the economic model. If not provided, it is taken care of separately.")
    parser_nanofiltration.add_argument('-erd', '--with_erd', action='store_true', default=False, help="If provided, considers the effect of ERD (installed at NF2 retentate) on the energy and costing of NF1. Default is false, when the flag is not provided")
    parser_nanofiltration.add_argument('-p_in_hp', '--erd_pressure_in_hp', type=float, required=False, help="Pressure [bar] of high-pressure retentate of NF2 that is sent to ERD. This is an additional parameter when ERD is considered.")
    parser_nanofiltration.add_argument('-q_in_hp_train', '--erd_feed_q_in_hp_train', type=float, required=False, help="Volumetric flow rate [m³/h] of high-pressure retentate of NF2 train that is sent to ERD. This is an additional parameter when ERD is considered.")
    # optional arguments, when -erd is provided
    # First pass: parse the known arguments
    #args, unknown = parser.parse_known_args()
    #if args.subcommand == 'nanofiltration' and args.with_erd:
    #    parser_nanofiltration.add_argument('-p_in_hp', '--erd_pressure_in_hp', type=float, required=True, help="Pressure [bar] of high-pressure retentate of NF2 that is sent to ERD. This is an additional parameter when ERD is considered.")
    #    parser_nanofiltration.add_argument('-q_in_hp_train', '--erd_feed_q_in_hp_train', type=float, required=True, help="Volumetric flow rate [m³/h] of high-pressure retentate of NF2 that is sent to ERD. This is an additional parameter when ERD is considered.")

    # create a parser for reverse osmosis
    parser_reverse_osmosis = subparsers.add_parser('reverse_osmosis', help='Reverse osmosis help')
    parser_reverse_osmosis.add_argument('-fm', '--feed-m', type=float, required=True, help="Total feed mass flow rate in ALL trains in kg/s")
    parser_reverse_osmosis.add_argument('-n1', '--n1-elem', type=int, required=True, help="Number of elements in first stage [-]")
    parser_reverse_osmosis.add_argument('-n2', '--n2-elem', type=int, required=True, help="Number of elements in second stage [-]")
    parser_reverse_osmosis.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser_reverse_osmosis.add_argument('-xro', '--x-ret-out', type=float, required=True, help="Desired salinity of the retentate stream [ppm]")
    parser_reverse_osmosis.add_argument('-swip_and_postt', '--with_swip_and_posttreat', action='store_true', help="Includes intake+pretreatment+posttreatment system cost and energy consumption in the economic model. If not provided, it is to be taken care of separately.")
    parser_reverse_osmosis.add_argument('-nf', '--with_preceding_nf', action='store_true', help="Includes the cost effect of using nanofiltration (NF) as pretreatment to SWRO feed, if provided (which itself is preceded by a chemical pretreatment). If not provided, it assumes that SWRO feed is only preceded by a chemical pretreatment")
    parser_reverse_osmosis.add_argument('-erd', '--with_erd', action='store_true', default=False, help="If provided, considers the effect of ERD (installed at the retentate of last RO stage) on the energy and costing of SWRO. Default is false, when the flag is not provided")

    # create a parser for high pressure reverse osmosis (HPRO)
    parser_hpro = subparsers.add_parser('hpro', help='High pressure reverse osmosis (HPRO) help')
    parser_hpro.add_argument('-q', '--feed-q', type=float, required=True, help="Total feed volumetric flow rate in m³/h")
    parser_hpro.add_argument('-pp', '--pump_feed_pressure', type=float, help="Pump feed pressure in bar (is equal to that of high-pressure retentate of SWRO)")
    parser_hpro.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser_hpro.add_argument('-erd', '--with_erd', action='store_true', default=False, help="If provided, considers the effect of ERD (installed at SWRO retentate) on the energy and costing of HPRO.")
    parser_hpro.add_argument('-tech', '--foll-tech', type=str, required=True, choices=['oaro', 'ed'], 
                            help="Tool that follows HPRO. It should be either 'ed' for electrodialysis or 'oaro'.")
    
    # create a parser for osmotically assisted reverse osmosis (OARO) with two stage
    parser_oaro = subparsers.add_parser('oaro', help='Help for osmotically assisted reverse osmosis (OARO) with two stage')
    parser_oaro.add_argument('-q', '--feed-q', type=float, required=True, help="Total feed volumetric flow rate in m³/h")
    parser_oaro.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    
    # create a parser for pretreatment model
    parser_pretreatment = subparsers.add_parser('pretreatment', help='Pretreatment help')
    parser_pretreatment.add_argument('-q', '--feed-q', type=float, required=True, help="Feed volumetric flow rate Q in m³/h")

    # create a parser for posttreatment model
    parser_posttreatment = subparsers.add_parser('posttreatment', help='Posttreatment help')
    parser_posttreatment.add_argument('-q', '--feed-q', type=float, required=True, help="Feed volumetric flow rate Q in m³/h")
    parser_posttreatment.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")

    # create a parser for intake system model
    parser_intake = subparsers.add_parser('intake_system', help='Intake system help')
    parser_intake.add_argument('-q', '--feed-q', type=float, required=True, help="Feed volumetric flow rate Q in m³/h")

    # create a parser for levelized cost model
    parser_levelized_costs = subparsers.add_parser('levelized_costs', help='Levelized cost calculation help')
    parser_levelized_costs.add_argument('-q', '--feed-q', type=float, required=True, help="Desalination plant's total feed volumetric flow rate Q in m³/h")
    parser_levelized_costs.add_argument('-rtd', '--recovery-techs', type=str, required=True, help="Path to YAML file containing recovery rates, representing permeate recovery for all technologies except OARO, where it represents concentrate recovery")
    parser_levelized_costs.add_argument('-ctd', '--annual-costs-techs', type=str, required=True, help="Path to YAML file containing annual costs (USD_current/annum), which is the sum of CAPEX and OPEX (fixed and variable, including electricity cost, but excluding brine disposal cost) representing permeate recovery for all technologies except OARO, where it represents concentrate recovery")
    parser_levelized_costs.add_argument('-qupw', '--upw-req-q', type=float, required=True, help="Total volumetric flow rate required for ultrapure water (UPW) to be produced by the desalination plant in m³/h")
    parser_levelized_costs.add_argument('-qwmg', '--w-req-mgcryst-q', type=float, required=True, help="Total volumetric flow rate of potable water required by Mg Crystallizer for the preparation of NaOH solution in m³/h")
    parser_levelized_costs.add_argument('-qemg', '--eff-mgcryst-q', type=float, required=True, help="Total volumetric flow rate of effluent produced by Mg Crystallizer in m³/h")
    parser_levelized_costs.add_argument('-mmg', '--mg-mgcryst-m', type=float, required=True, help="Total mass flow rate of Mg(OH)2 produced by Mg Crystallizer in ton/h")
    parser_levelized_costs.add_argument('-mnacl', '--nacl-naclcryst-m', type=float, required=True, help="Total mass flow rate of NaCl produced by NaCl Crystallizer in ton/h")


    # create a parser for Multi-effect distillation (MED)
    parser_med = subparsers.add_parser('multi_effect_distillation', help='Multi-effect distillation (MED) help')
    parser_med.add_argument('-fm', '--feed-m', type=float, required=True, help="Total feed mass flow rate in ALL trains in kg/s")
    parser_med.add_argument('-ne', '--num-of-effects', type=int, required=True, help="Number of effects [-]")
    parser_med.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser_med.add_argument('-xro', '--x-ret-out', type=float, required=True, help="Desired salinity of the retentate stream [ppm]")
    parser_med.add_argument('-ts', '--ts-design', type=float,required=True, help="Feed steam temperature [°C]")
    parser_med.add_argument('-he', '--heat-cost', type=float, default=0.01, required=True, help="Cost of heat in $/kWh_th")
    
    # create a parser for electrodialysis (ED)
    parser_ed = subparsers.add_parser('electrodialysis', help='Electrodialysis (ED) help')
    parser_ed.add_argument('-fm', '--feed-m', type=float, required=True, help="Total feed mass flow rate in ALL trains in kg/s")
    parser_ed.add_argument('-i', '--i-j', type=float, required=True, help="Current density [A/m**2]")
    parser_ed.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser_ed.add_argument('-xro', '--x-ret-out', type=float, required=True, help="Desired salinity of the retentate stream [ppm]")
    parser_ed.add_argument('-dcr', '--dil-to-conc-ratio', type=float, required=True, help="Dialuate-to-conc mass-splitting ratio (DCR) at ED inlet [-]")

    # create a parser for 2nd pass SWRO with ion exchange
    parser_2ndpass_ro_with_ix = subparsers.add_parser('secondpass_ro_with_ix', help='Help for 2nd pass SWRO with ion exchange')
    parser_2ndpass_ro_with_ix.add_argument('-q', '--feed-q', type=float, required=True, help="Total feed volumetric flow rate in m³/h")
    parser_2ndpass_ro_with_ix.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    
    args = parser.parse_args(argv)

    ## getting paths for parameters.yml and concentration_ion.yml for each tool:
    main_tool_name = ""
    sub_tool_name = ""
    model_name = args.model
    base_path = os.path.dirname(os.path.realpath(__file__))

    # Determine main and sub model names
#    main_tool_name = model_name.split("_", 1)[0] if 'crystallizer' in model_name else model_name
    main_tool_name = model_name.split("_", 1)[0] if any(tool in model_name for tool in ['crystallizer', 'oaro']) else model_name
    #sub_tool_name = model_name if 'crystallizer' in model_name else None
    sub_tool_name = model_name if any(tool in model_name for tool in ['crystallizer', 'oaro']) else None
    #sub_tool_name_path = [model_name] if 'crystallizer' in model_name else []
    sub_tool_name_path = [model_name] if any(tool in model_name for tool in ['crystallizer', 'oaro']) else []

    # Specific paths based on model_name
    program_config_path = os.path.join(base_path, "inputs", main_tool_name, 'program_config.yml') if model_name == 'multi_effect_distillation' else None
    nnf_path = [f"NF{args.nNF}"] if model_name == 'nanofiltration' else []


    # parameters file path (parameters.yml is saved in the respective tech folder in the inputs folder)
    tech_config_and_params_path = os.path.join(base_path, "inputs", main_tool_name, 'parameters.yml')
    proj_config_and_params_path = os.path.join(base_path, "inputs", 'proj_config_and_params.yml')
    nacl_densities_excel_filepath = os.path.join(base_path, "inputs", 'nacl_densities_perry.xlsx')
    #proj_config_path = os.path.join(base_path, "inputs", 'project_config.yml')
    # open project params and config, and nacl densities excel
    with open(proj_config_and_params_path, "r") as file_params:
        proj_config_and_params = yaml.load(file_params, Loader=Loader)
    if main_tool_name != "levelized_costs":
        with open(tech_config_and_params_path, "r") as file_params:
            tech_config_and_params = yaml.load(file_params, Loader=Loader)
    if main_tool_name == "crystallizer" and "crystallizer_single_self_sodchl" in sub_tool_name:
        df_nacl_densities = pd.read_excel(nacl_densities_excel_filepath, index_col=0)
        df_nacl_densities = df_nacl_densities.T
        
    # no. of trains
    if args.no_of_trains:
        no_of_trains = args.no_of_trains
    elif main_tool_name == "nanofiltration":
        no_of_trains = proj_config_and_params["train_nos_in_scenarios"][proj_config_and_params["train_scenario"]][main_tool_name][f"NF{args.nNF}"]
    #elif main_tool_name == "reverse_osmosis":
    else:
        no_of_trains = proj_config_and_params["train_nos_in_scenarios"][proj_config_and_params["train_scenario"]][main_tool_name]
    #else:
    #    no_of_trains = 1

    # pump feed pressure (more relevant in case of NF2, which takes in retentate of NF1). NF1 retent pressure is assumed to be same as feed of NF1, except a small head loss (2 bar). Ref.: Ma, Hong-Peng et al. (2022)
    if main_tool_name == "nanofiltration":
        if args.nNF == 1:
            if args.pump_feed_pressure is None:
                args.pump_feed_pressure = 0.0  # assumes 0 bar(g) as feed pressure for the HPP of NF1
        elif args.nNF == 2:
            if args.pump_feed_pressure is None:
                parser.error("When -nnf is 2 or more, -pp (pump feed pressure) must be provided, as it could depend, based on chosen configuration, on the retentate pressure of preceding NF.")
    if main_tool_name == "hpro":
        if args.pump_feed_pressure is None:
            parser.error("In case of HPRO, -pp (pump feed pressure) must be provided, as it could depend, based on chosen configuration, on the retentate pressure of preceding SWRO.")

    # ERD parameters (more relevant in case of NF2, which takes in retentate of NF1). NF1 retent pressure is assumed to be same as feed of NF1, except a small head loss (2 bar). Ref.: Ma, Hong-Peng et al. (2022)
    if main_tool_name == "nanofiltration":
        if args.with_erd:
            if args.erd_pressure_in_hp is None:
                parser.error("When -erd is given, -erd_pressure_in_hp (NF2 retentate pressure, as high-pressure feed for ERD) must be provided.")
            if args.erd_feed_q_in_hp_train is None:
                parser.error("When -erd is given, -q_in_hp_train (NF2 retentate volumetric flow rate, as high-pressure feed for ERD) must be provided.")
        else:
            args.erd_pressure_in_hp = 0
            args.erd_feed_q_in_hp_train = 0

    # sea name and tds category (either both should be provided or none)
    if (args.sea_name and args.tds_cat):
        sea_name = args.sea_name
        tds_cat = args.tds_cat
    else:
        sea_name = proj_config_and_params["sea"]
        tds_cat = proj_config_and_params["tds_cat"]
    

    # Electricity cost
    electricity_cost = args.electricity_cost or proj_config_and_params["elec_cost"][sea_name]
    
    ## concentration file path, output directory & output file name
    sea_path = [sea_name, tds_cat] if sea_name else []
    tool_path = [main_tool_name, sub_tool_name] if main_tool_name == 'crystallizer' else [main_tool_name]
    if main_tool_name == 'hpro':
        output_directory = os.path.join(base_path, "results", main_tool_name,
        *sub_tool_name_path, *sea_path, args.foll_tech)
    else:
        output_directory = os.path.join(base_path, "results", main_tool_name,
            *sub_tool_name_path, *sea_path, *nnf_path)
    output_namefile = "file_name.dat"

    print(f"main tool: {main_tool_name}, sub-tool: {sub_tool_name}, no_of_trains: {no_of_trains}, sea_name: {sea_name}, tds_cat: {tds_cat}")

    if main_tool_name == "crystallizer":
        c_feed_path = args.rce_conc_file or os.path.join(base_path, "inputs", *tool_path, *sea_path, 'concentration_ion.yml')
        if sub_tool_name == 'crystallizer_single_self_sodchl_thermal':
            output_filename = f"results_mfeedkgh_train_{round(args.feed_m/no_of_trains, 2)}_thermal.xls"
            heat_cost = args.heat_cost or proj_config_and_params["heat_cost"][sea_name]
            t_n_design = args.t_n_design or tech_config_and_params["t_n_design"]
        elif sub_tool_name == 'crystallizer_single_self_sodchl_electric':
            output_filename = f"results_mfeedkgh_train_{round(args.feed_m/no_of_trains, 2)}_electric.xls"
        else:
            output_filename = f"results_qfeedm3h_train_{round(args.feed_q/no_of_trains, 2)}.xls"
            
    elif main_tool_name == "nanofiltration":
        c_feed_path = args.rce_conc_file if args.nNF != 1 and args.rce_conc_file else os.path.join(base_path, "inputs", *tool_path, *sea_path, f"NF{args.nNF}", 'concentration_ion.yml')
        output_filename = f"results_Rgiven_noCorrFactor_qfeedm3h_train={round(args.feed_q/no_of_trains, 2)}_{'with' if args.with_erd else 'wo'}_erd.xls"
        rej_feed_path = os.path.join(base_path, "inputs", *tool_path, *sea_path, f"NF{args.nNF}", 'rejection_ion.yml')
        
    elif main_tool_name == "hpro":
        output_filename = f"results_qfeedm3h_train={round(args.feed_q/no_of_trains, 2)}_{'with' if args.with_erd else 'wo'}_erd.xls"

    elif main_tool_name == "electrodialysis":
        output_filename = f"results_mfeedkgh_train={round(args.feed_m/no_of_trains*3600, 2)}_xf_{round(args.x_feed, 2)}_xco_{round(args.x_ret_out, 2)}_dcr_{round(args.dil_to_conc_ratio, 2)}_i_{args.i_j}.xls"

    elif main_tool_name in ["oaro", "secondpass_ro_with_ix", "pretreatment", "posttreatment", "intake_system", "levelized_costs"]:
        output_filename = f"results_qfeedm3h_train={round(args.feed_q/no_of_trains, 2)}.xls"

    elif main_tool_name == "multi_effect_distillation":
        output_filename = f"results_N_{args.num_of_effects}_Xin_{args.x_feed}_Xout_{args.x_ret_out}_mfeedkgh_train={round(args.feed_m/no_of_trains*3600, 2)}.xls"

    dummy_recovery_dict = {
    "intake": 1,
    "pretreat": 0.9,
    "nf1": 0.5,
    "nf2": 0.6,
    "mgcryst": 0,
    "swro": 0.6,
    "hpro": 0.25,
    "oaro": 0.195,
    "naclcryst": 0.91,
    "secondpass_ro_ix": 0.75,
    "posttreat": 0.95
}

    dummy_Cannual_costs_tot_dict = {
        "intake": 100,
        "pretreat": 100,
        "nf1": 100,
        "nf2": 100,
        "mgcryst": 100,
        "swro": 100,
        "hpro": 100,
        "oaro": 100,
        "naclcryst": 100,
        "secondpass_ro_ix": 100,
        "posttreat": 100
    }

    ## input arguments to the tool
    #! simplify this below part, too much redundancy
    if main_tool_name == 'nanofiltration':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=c_feed_path,
            nacl_densities_excel_filepath=None,
            Pfeed=args.feed_p,
            Pfeed_pump=args.pump_feed_pressure,
            recovery_req=args.req_rec,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=args.with_pretreatment_cost,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=rej_feed_path,
            nNF=args.nNF,
            with_erd=args.with_erd,
            erd_pressure_in_hp=args.erd_pressure_in_hp,
            erd_feed_q_in_hp_train=args.erd_feed_q_in_hp_train,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'crystallizer' and sub_tool_name not in ['crystallizer_single_self_sodchl_thermal', 'crystallizer_single_self_sodchl_electric']:
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=c_feed_path,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'crystallizer' and sub_tool_name == 'crystallizer_single_self_sodchl_thermal':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=c_feed_path,
            nacl_densities_excel_filepath=nacl_densities_excel_filepath,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=None,
            Mfeed=args.feed_m/no_of_trains,
            electricity_cost=electricity_cost,
            heat_cost=heat_cost,
            T_in=t_n_design,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'crystallizer' and sub_tool_name == 'crystallizer_single_self_sodchl_electric':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=c_feed_path,
            nacl_densities_excel_filepath=nacl_densities_excel_filepath,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=None,
            Mfeed=args.feed_m/no_of_trains,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'reverse_osmosis':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=args.with_swip_and_posttreat,
            with_pretreatment_cost=None,
            with_preceding_nf=args.with_preceding_nf,
            Qfeed=None,
            Mfeed=args.feed_m/no_of_trains,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=args.with_erd,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=args.n1_elem,
            nelem_2=args.n2_elem,
            Xfeed=args.x_feed,
            Xret_out=args.x_ret_out,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=None,
            )
    elif main_tool_name == 'hpro':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=args.pump_feed_pressure,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=args.with_erd,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=args.x_feed,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=args.foll_tech,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'oaro' and sub_tool_name == 'oaro_two_stage':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=args.x_feed,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'pretreatment':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'posttreatment':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=args.x_feed,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'intake_system':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'levelized_costs':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=None,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=None,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=None,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = args.recovery_techs, #dummy_recovery_dict, #args.recovery_techs,
            Cannual_costs_techs_with_elec_wo_brine_filepath = args.annual_costs_techs, # args.annual_costs_techs, #dummy_Cannual_costs_tot_dict, #args.annual_costs_techs,
            Qupw_requirement_tot_m3h = args.upw_req_q,
            Qfeed_water_mgcryst_tot_m3h = args.w_req_mgcryst_q,
            Qeffluent_mgcryst_tot_m3h = args.eff_mgcryst_q,
            Mmgoh2_mgcryst_tot_tonh = args.mg_mgcryst_m,
            Mnacl_naclcryst_tot_tonh = args.nacl_naclcryst_m,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'multi_effect_distillation':
        return driver(
            model=models[args.model],
            programconfigpath=program_config_path,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            Qfeed=None,
            Mfeed=args.feed_m/no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            electricity_cost=electricity_cost,
            heat_cost=heat_cost,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            i_j=None,
            Mdial_con_ratio=None,
            Xfeed=args.x_feed,
            Xret_out=args.x_ret_out,
            N_effects=args.num_of_effects,
            Ts_design=args.ts_design,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'electrodialysis':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            Qfeed=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Mfeed=args.feed_m/no_of_trains,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=args.x_feed,
            Xret_out=args.x_ret_out,
            N_effects=None,
            Ts_design=None,
            i_j=args.i_j,
            Mdial_con_ratio=args.dil_to_conc_ratio,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
    elif main_tool_name == 'secondpass_ro_with_ix':
        return driver(
            model=models[args.model],
            programconfigpath=None,
            tech_config_params_filepath=tech_config_and_params_path,
            proj_config_params_filepath=proj_config_and_params_path,
            Cfeed_filepath=None,
            nacl_densities_excel_filepath=None,
            Pfeed=None,
            Pfeed_pump=None,
            recovery_req=None,
            N_trains=no_of_trains,
            with_swip_and_posttreatment=None,
            with_pretreatment_cost=None,
            with_preceding_nf=None,
            Qfeed=args.feed_q/no_of_trains,
            Mfeed=None,
            electricity_cost=electricity_cost,
            heat_cost=None,
            T_in=None,
            Rionfilepath=None,
            nNF=None,
            with_erd=None,
            erd_pressure_in_hp=None,
            erd_feed_q_in_hp_train=None,
            nelem_1=None,
            nelem_2=None,
            Xfeed=args.x_feed,
            Xret_out=None,
            N_effects=None,
            Ts_design=None,
            i_j=None,
            Mdial_con_ratio=None,
            following_tech=None,
            recovery_techs_filepath = None,
            Cannual_costs_techs_with_elec_wo_brine_filepath = None,
            Qupw_requirement_tot_m3h = None,
            Qfeed_water_mgcryst_tot_m3h = None,
            Qeffluent_mgcryst_tot_m3h = None,
            Mmgoh2_mgcryst_tot_tonh = None,
            Mnacl_naclcryst_tot_tonh = None,
            output_directory=output_directory,
            output_namefile=output_namefile,
            output_filename=output_filename,
            )
            

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()


"""
## model_nanofiltration_rejection_given
python main_nanofiltration.py -m "nanofiltration" -q 10 -e 0.06 -rr 0.65 -p 20 -nnf 1
python main_all.py -m "nanofiltration" -e 0.06 "nanofiltration" -q 0.0165 -rr 0.8 -p 50 -nnf 2
python main_all.py -m "nanofiltration" -e 0.06 "nanofiltration" -q 0.0165 -rr 0.60 -p 23 -nnf 1

python main_all.py -m "nanofiltration" -e 0.06 -sea "arabian_gulf_kuwait" -tds "average_tds" "nanofiltration" -q 400 -rr 0.54 -p 28 -nnf 1 -pretreat   #RR=0.54 here, so we come to the required value (0.5) in results
python main_all.py -m "nanofiltration" -e 0.06 -sea "arabian_gulf_kuwait" -tds "average_tds" "nanofiltration" -q 400 -rr 0.54 -p 28 -nnf 1 -erd -p_in_hp 10 -q_in_hp_train 50

python main_all.py -m "nanofiltration" -e 0.06 -sea "arabian_gulf_kuwait" -tds "average_tds" "nanofiltration" -q 198.8712 -rr 0.722 -p 50 -nnf 2 -pp 26 -pretreat   #RR=0.722 here, so we come to the required value (0.701879) in results, such that NF1+NF2 overall recovery is 85%
python main_all.py -m "nanofiltration" -e 0.06 -trains 2 -sea "arabian_gulf_kuwait" -tds "average_tds" "nanofiltration" -q 198.8712 -rr 0.722 -p 50 -nnf 2 -pp 26 -pretreat   #RR=0.722 here, so we come to the required value (0.701879) in results, such that NF1+NF2 overall recovery is 85%
python main_all.py -m "nanofiltration" -e 0.06 -sea "arabian_gulf_kuwait" -tds "average_tds" "nanofiltration" -q 198.8712 -rr 0.722 -p 50 -nnf 2 -pp 26 -pretreat   #RR=0.722 here, so we come to the required value (0.701879) in results, such that NF1+NF2 overall recovery is 85%
python main_all.py -m "nanofiltration" -e 0.06 -sea "arabian_gulf_kuwait" -tds "average_tds" -cc "./concentration_ion.yml" "nanofiltration" -q 198.8712 -rr 0.722 -p 50 -nnf 2 -pp 26 -pretreat   #RR=0.722 here, so we come to the required value (0.701879) in results, such that NF1+NF2 overall recovery is 85%

## model_reverse_osmosis (single stage)
python main_all.py -m "reverse_osmosis" -e 0.103 "reverse_osmosis" -n1 8 -n2 6 -fm 200 -xf 26455.82 -xro 40000
python main_all.py -m "reverse_osmosis" -sea "arabian_gulf_kuwait" -tds "average_tds" "reverse_osmosis" -n1 8 -n2 6 -fm 200 -xf 26455.82 -xro 40000

## model_reverse_osmosis (double stage)
python main_all.py -m "reverse_osmosis" -e 0.103 "reverse_osmosis" -n1 8 -n2 6 -fm 17.3458 -xf 26455.82 -xro 70000 -nf -swip_and_postt
python main_all.py -m "reverse_osmosis" -e 0.103 "reverse_osmosis" -n1 8 -n2 6 -fm 253.54 -xf 35880 -xro 74000 -nf -swip_and_postt  # partial mimic of first stage SWRO from Al-Amoudi A.S. et al (2023). Mass flow rate of 3651 t/h or 1014.166 kg/s is divided into 4 trains
python main_all.py -m "reverse_osmosis" -e 0.103 -sea "arabian_gulf_kuwait" -tds "average_tds" "reverse_osmosis" -n1 8 -n2 6 -fm 253.54 -xf 35880 -xro 74000  # partial mimic of first stage SWRO from Al-Amoudi A.S. et al (2023). Mass flow rate of 3651 t/h or 1014.166 kg/s is divided into 4 trains
python main_all.py -m "reverse_osmosis" -e 0.103 -trains 2 -sea "arabian_gulf_kuwait" -tds "average_tds" "reverse_osmosis" -n1 8 -n2 6 -fm 253.54 -xf 35880 -xro 74000 -nf -swip_and_postt -erd  # partial mimic of first stage SWRO from Al-Amoudi A.S. et al (2023). Mass flow rate of 3651 t/h or 1014.166 kg/s is divided into 4 trains

## model_hpro
python main_all.py -m "hpro" -e 0.103 -sea "arabian_gulf_kuwait" -tds "average_tds" "hpro" -q 3278 -xf 74000 -pp 70 -erd -tech oaro  # partial mimic of first stage SWRO from Al-Amoudi A.S. et al (2023). Mass flow rate of 3651 t/h or 1014.166 kg/s is divided into 4 trains
python main_all.py -m "hpro" -e 0.103 -sea "arabian_gulf_kuwait" -tds "average_tds" "hpro" -q 3278 -xf 74000 -pp 70 -erd -tech ed

## model_oaro_two_stage
python main_all.py -m "oaro_two_stage" -e 0.103 -sea "arabian_gulf_kuwait" -tds "average_tds" "oaro" -q 3838 -xf 110000  # partial mimic of first stage SWRO from Al-Amoudi A.S. et al (2023).

## model_pretreatment
python main_all.py -m "pretreatment" -sea "arabian_gulf_kuwait" -tds "average_tds" "pretreatment" -q 200

## model_posttreatment
python main_all.py -m "posttreatment" -sea "arabian_gulf_kuwait" -tds "average_tds" "posttreatment" -q 200 -xf 500

## model_intake
python main_all.py -m "intake_system" -sea "arabian_gulf_kuwait" -tds "average_tds" "intake_system" -q 200

## model_crystallizer_double_sodhyd_maghyd_sodhyd_calhyd
python main_all.py -m "crystallizer_double_sodhyd_maghyd_sodhyd_calhyd" -e 0.3 "crystallizer" -q 0.02777778

## model_crystallizer_double_dol_maghyd_self_calsul
python main_all.py -m "crystallizer_double_dol_maghyd_self_calsul" -e 0.1035 "crystallizer" -q 0.000048333

## model_crystallizer_double_dol_maghyd_sodcar_calcar
python main_all.py -m "crystallizer_double_dol_maghyd_sodcar_calcar" -e 0.06 "crystallizer" -q 0.011666666

## model_crystallizer_single_sodhyd_maghyd
python main_all.py -m "crystallizer_single_sodhyd_maghyd" -e 0.06 "crystallizer" -q 0.000277777
python main_all.py -m "crystallizer_single_sodhyd_maghyd" "crystallizer" -q 513.2578  # based on RCE simulation of for a 3-train Mg crystallizer taking in retentate from NF2

## model_crystallizer_single_self_sodchl_thermal
python main_all.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 7668.8 -he 0.01 -t 25
python main_all.py -m "crystallizer_single_self_sodchl" -e 0.06 "nacl_crystallizer" -fm 570012
python main_all.py -m "crystallizer_single_self_sodchl_thermal" -sea "arabian_gulf_kuwait" -tds "average_tds"  -cc "./concentration_ion.yml" "nacl_crystallizer" -fm 570012
python main_all.py -m "crystallizer_single_self_sodchl_thermal" -sea "arabian_gulf_kuwait" -tds "average_tds" "nacl_crystallizer" -fm 570012

## model_crystallizer_single_self_sodchl_electric
python main_all.py -m "crystallizer_single_self_sodchl_electric" -sea "arabian_gulf_kuwait" -tds "average_tds" "nacl_crystallizer" -fm 570012

## model_crystallizer_single_sodcar_calcar
python main_all.py -m "crystallizer_single_sodcar_calcar" -e 0.06 "crystallizer" -q 2.777777e-4

## model_multi_effect_distillation
python main_all.py -m "multi_effect_distillation" -e 0.103 "multi_effect_distillation" -ne 6 -fm 4.52 -xf 26209 -xro 90000 -ts 100 -he 0.01

## model_electrodialysis
python main_all.py -m "electrodialysis" -e 0.1035 -trains 1 "electrodialysis" -xf 40050 -xro 204675 -i 250 -fm 0.285218 -dcr 15.94915  ## Nayar et al. 2019 industrial ed

## 2ndpass_ro_with_ix
python main_all.py -m "secondpass_ro_with_ix" -e 0.103 -sea "arabian_gulf_kuwait" -tds "average_tds" "secondpass_ro_with_ix" -q 500 -xf 300



## levelized_costs
python main_all.py -m "levelized_costs" "levelized_costs" -q 500 -rtd dummy_recovery_dict -ctd dummy_Cannual_costs_tot_dict -qupw 1 -qwmg 2 -qemg 1 -mmg 0.5 -mnacl 0.7 


"""