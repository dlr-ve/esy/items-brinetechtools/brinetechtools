import logging
import math
import numpy as np
from medmodel import h2oprop, naclprops, pressure, heattransfer, tools, tvc

logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)


''' functions to identify the operating conditions given the heat exchanger areas'''


def global_balance_mdist(m_dist, xf, xn):  # Mdist in [kg/s]
    m_brine = m_dist * xf / (xn - xf)
    m_feed = m_brine + m_dist
    return m_brine, m_feed


def global_balance_mfeed(m_feed, xf, xn):
    m_brine = m_feed * xf / xn
    m_dist = m_feed - m_brine
    return m_brine, m_dist


def calc_tvsat(tbrine, xb, config):
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime = h2oprop.sat_pressure(tvsat) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                      config['delta_w']) * config['thickness'] * 1E-3 * 1E-3
    tvsat_prime = h2oprop.sat_temp(pv_prime)
    if tvsat_prime > tvsat:
        return pv_prime, tvsat
    return pv_prime, tvsat_prime

def deltat_losses(m_vap, tbrine, xb, config):  # Mvap in [kg/s], Tbrine in [°C]
    tbrine = float(tbrine)
    xb = float(xb)
    tvsat = tbrine - naclprops.bpe_brine_act(tbrine, xb)
    pv_prime, tvsat_prime = calc_tvsat(tbrine, xb, config)
    pc_prime = pv_prime - pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tvsat_prime),
                                                    config['d_tube']) * 1E-3 * config['L_tube']
    tc_prime = h2oprop.sat_temp(pc_prime)
    if tc_prime > tvsat_prime:
        tc_prime = tvsat_prime
    return tvsat, tvsat_prime, tc_prime

def deltat_cond(m_vap, tc_prime, config, n_tubes):
    dp_cond = 0.99 * pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tc_prime, config['epsilon']) * 1E-3 -\
              (pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime),
                                                  h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'],
                                                  config['L_evap']) * 1E-3)
    pc = h2oprop.sat_pressure(tc_prime) - dp_cond
    # pc = pc_prime - pressure.friction_deltap(m_vap, config['d_evap'], n_tubes, config['L_evap'], tbrine,
    #                                 config['epsilon']) * 1E-3
    tc = h2oprop.sat_temp(pc)
    if tc > tc_prime:
        tc = tc_prime
    return tc

def tbrineout_fromtcout(tc_n, tc_prime, tv_prime, xb_out, m_vap, config, tbrine):
    tc_n = float(tc_n)
    xb_out = float(xb_out)
    pc = h2oprop.sat_pressure(tc_n)
    pc_prime = pc + pressure.gravitational_deltap(h2oprop.density_satwat_vapor(tc_prime), h2oprop.density_satwat_liquid(tc_prime), 0.5, config['theta'], config['L_evap']) * 1E-3 + \
               pressure.acceleration_deltap(m_vap, config['A_cross'], h2oprop.density_satwat_vapor(tc_prime), h2oprop.density_satwat_liquid(tc_prime),
                                            h2oprop.density_satwat_vapor(tc_prime), h2oprop.density_satwat_liquid(tc_prime)) * 1E-3
    tc_prime = h2oprop.sat_temp(pc_prime)
    deltat_evap = tc_prime - tc_n
    pv_prime = pc_prime + pressure.connlines_deltap(m_vap, config['L_tube'], h2oprop.density_satwat_vapor(tv_prime), config['d_tube']) * 1E-3 * config['L_tube']
    deltat_lines = h2oprop.sat_temp(pv_prime) - tc_prime
    if deltat_lines < 0:
        deltat_lines = 0
    pv = pv_prime + pressure.demister_deltap(config['rho_d'], config['v_vap'], config['delta_w']) * 1E-3
    deltat_dem = h2oprop.sat_temp(pv) - h2oprop.sat_temp(pv_prime)
    if deltat_dem < 0:
        deltat_dem = 0
    tbrine = tc_n + naclprops.bpe_brine_act(tbrine, xb_out) + deltat_dem + deltat_lines + deltat_evap
    return tbrine


def tpreh_profile(t_preh_next, alfa_c, m_vap, tvsat_prime, mfeed, t_mean, Xf):
    t_preh = t_preh_next + (alfa_c * m_vap * h2oprop.latent_heat(tvsat_prime)) / (mfeed * naclprops.cp_seawater(t_mean, Xf))
    return t_preh


def alfa_preheater(mfeed, i_effect, t_preh, Xf, m_vap, tvsat_prime):
    alfa_c = mfeed * naclprops.cp_seawater((t_preh[i_effect] + t_preh[i_effect + 1]) / 2, Xf) * (t_preh[i_effect] - t_preh[i_effect + 1]) \
             / (m_vap[i_effect] * h2oprop.latent_heat(tvsat_prime[i_effect]))
    return alfa_c


def first_flashbox(alfa_c, m_vap, tc, t_fb, tv):  # not used
    m_fb = alfa_c * m_vap * (h2oprop.enthalpy_satwat_liquid(tc) - h2oprop.enthalpy_satwat_liquid(t_fb)) / (h2oprop.enthalpy_satwat_vapor(tv) - h2oprop.enthalpy_satwat_liquid(t_fb))
    mc = alfa_c * m_vap - m_fb
    return m_fb, mc


def generic_flashbox(i_effect, mfb_previous, tv_prime, tc, m_vap, alfa_c):
    mc = (alfa_c[i_effect] * m_vap[i_effect] * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) -
                                                              h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))) / (
        h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = alfa_c[i_effect] * m_vap[i_effect] + (1 - alfa_c[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def last_flashbox(i_effect, alfa_cond, mfb_previous, tv_prime, tc, m_vap):
    mc = (m_vap[i_effect] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] * (h2oprop.enthalpy_satwat_liquid(tc[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect])) +
          mfb_previous * (h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect - 1]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))) / (
        h2oprop.enthalpy_satwat_liquid(tv_prime[i_effect]) - h2oprop.enthalpy_satwat_vapor(tv_prime[i_effect]))
    m_fb = m_vap[i_effect] + (1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1] + mfb_previous - mc
    return mc, m_fb


def brine_flash(i_effect, t, mb, xb):
    tbrine_fe = t[i_effect] + naclprops.non_equilibrium_allowance(t[i_effect - 1], t[i_effect], xb[i_effect - 1])
    m_fbrine = mb[i_effect - 1] * naclprops.cp_seawater((t[i_effect - 1] - tbrine_fe) / 2, xb[i_effect - 1]) * \
               (t[i_effect - 1] - tbrine_fe) / h2oprop.latent_heat(tbrine_fe)
    return m_fbrine


def t_fromhxarea(i_effect, mfeed, xf, area_hx, t, t_preh, m_d, ts, alfa_cond, m_vap, tc):
    if i_effect == 0:
        return ts - ((mfeed * naclprops.cp_seawater((t[i_effect] + t_preh[i_effect]) / 2, xf) * (t[i_effect] - t_preh[i_effect]) +
                m_d[0] * h2oprop.latent_heat(t[i_effect] - naclprops.bpe_brine_act(t[i_effect], xf))) / (heattransfer.u_evaporator(t[i_effect]) * area_hx[i_effect]))

    return tc[i_effect - 1] - (((1 - alfa_cond[i_effect - 1]) * m_vap[i_effect - 1]) * h2oprop.latent_heat(tc[i_effect - 1]) / (heattransfer.u_evaporator(t[i_effect]) * area_hx[i_effect]))


def deltat_logmean(i_effect, tv_prime, t_preh):
    return ((tv_prime[i_effect] - t_preh[i_effect + 1]) - (tv_prime[i_effect] - t_preh[i_effect])) / \
           np.log((tv_prime[i_effect] - t_preh[i_effect + 1]) / (tv_prime[i_effect] - t_preh[i_effect]))


def mcond_frompharea(i_effect, area_ph, tv_prime, t_preh):
    return area_ph * (heattransfer.u_condenser(tv_prime[i_effect]) * deltat_logmean(i_effect, tv_prime, t_preh)) / (h2oprop.latent_heat(tv_prime[i_effect]))


def tpreh_fromareaph_tprec(i_effect, area_ph, tv_prime, t_preh, m_cond):
    return t_preh[i_effect-1] - ((m_cond[i_effect-1] * h2oprop.latent_heat(tv_prime[i_effect-1]))
                                 /(area_ph[i_effect-1] * heattransfer.u_condenser(tv_prime[i_effect-1]))
                                 * np.log((tv_prime[i_effect-1] - t_preh[i_effect]) / (tv_prime[i_effect-1] - t_preh[i_effect-1])))


def tpreh_fromareaph_tsucc(i_effect, area_ph, tv_prime, t_preh, m_cond):
    return t_preh[i_effect+1] + ((m_cond[i_effect] * h2oprop.latent_heat(tv_prime[i_effect]))
                                 /(area_ph[i_effect] * heattransfer.u_condenser(tv_prime[i_effect]))
                                 * np.log((tv_prime[i_effect] - t_preh[i_effect+1]) / (tv_prime[i_effect] - t_preh[i_effect])))


def mcw_fromcondarea(a_cond, tcw_in, tcw_out, tvap, xf):
    deltat_ln = (tcw_out - tcw_in) / np.log((tvap - tcw_in) / (tvap - tcw_out))
    m_cw = a_cond * (deltat_ln * heattransfer.u_condenser(tvap)) / (naclprops.cp_seawater((tcw_in + tcw_out)/2, xf) * (tcw_out - tcw_in))
    return m_cw


def last_alfapreh(mb, t, xb, m_vap, tv_prime, m_fb, tc, N):
    m_steam = (mb[N - 1] * naclprops.enthalpy_seawater(t[N - 1], xb[N - 1]) + m_vap[N - 1] * h2oprop.enthalpy_satwat_vapor(tv_prime[N - 1]) - m_fb[N - 1] * h2oprop.enthalpy_satwat_vapor(tv_prime[N - 1]) -
               mb[N - 2] * naclprops.enthalpy_seawater(t[N - 2], xb[N - 2])) / h2oprop.latent_heat(tc[N - 1])
    return 1 - (m_steam / m_vap[N - 1])



class MedSolver_operation:
    def __init__(self, N, DeltaT_op, DeltaT_tot_op, T_op, Ts_op, Tn_op, DTT_cond, Tvsat_op, Tvsat_prime_op, Tc_prime_op, Tc_op, T_preh_op, Mb_op, Md_op, M_fbrine_op, M_fb_op, Mvap_op,
                 Xb_op, Xb_out_op, Mbrine_op, Mdist_op, Mfeed_op, Xf_op, M_cond_ph_op, alfa_cond_op, h_preh_op, h_vprime_op, h_cprime_op, h_b_op, Ms_op, Mc_op, A_hx, A_preh, A_cond,
                 Msteam_op, X_max, config, Tsw_op, Tcw_out_op, Mdist_real_op, Error_mdist_op, DTLM_op, Mcw_op, Mcw_op_des, Xf_des, Tn_des, Tcn_des, Xb_out_op_real,
                 DTLM_cond_des, DTLM_cond_op, TVC_on, Pm_op, ER_op, Mms_op, Mss_op, n_tubes):
        self.N = N
        self.DeltaT_op = DeltaT_op
        self.DeltaT_tot_op = DeltaT_tot_op
        self.T_op = T_op
        self.Ts_op = Ts_op
        self.Tn_op = Tn_op
        self.DTT_cond = DTT_cond
        self.Tvsat_op = Tvsat_op
        self.Tvsat_prime_op = Tvsat_prime_op
        self.Tc_prime_op = Tc_prime_op
        self.Tc_op = Tc_op
        self.T_preh_op = T_preh_op
        self.Mb_op = Mb_op
        self.Md_op = Md_op
        self.M_fbrine_op = M_fbrine_op
        self.M_fb_op = M_fb_op
        self.Mvap_op = Mvap_op
        self.Xb_op = Xb_op
        self.Xb_out_op = Xb_out_op
        self.Mbrine_op = Mbrine_op
        self.Mdist_op = Mdist_op
        self.Mfeed_op = Mfeed_op
        self.Xf_op = Xf_op
        self.M_cond_ph_op = M_cond_ph_op
        self.alfa_cond_op = alfa_cond_op
        self.h_preh_op = h_preh_op
        self.h_vprime_op = h_vprime_op
        self.h_cprime_op = h_cprime_op
        self.h_b_op = h_b_op
        self.Ms_op = Ms_op
        self.Mc_op = Mc_op
        self.A_hx = A_hx
        self.A_preh = A_preh
        self.A_cond = A_cond
        self.Msteam_op = Msteam_op
        self.X_max = X_max
        self.config = config
        self.Tsw_op = Tsw_op
        self.Tcw_out_op = Tcw_out_op
        self.Mdist_real_op = Mdist_real_op
        self.Error_mdist_op = Error_mdist_op
        self.DTLM_op = DTLM_op
        self.Mcw_op = Mcw_op
        self.Mcw_op_des = Mcw_op_des
        self.Xf_des = Xf_des
        self.Tn_des = Tn_des
        self.Tcn_des = Tcn_des
        self.Xb_out_op_real = Xb_out_op_real
        self.DTLM_cond_des = DTLM_cond_des
        self.DTLM_cond_op = DTLM_cond_op
        self.TVC_on = TVC_on
        self.Pm_op = Pm_op
        self.ER_op = ER_op
        self.Mms_op = Mms_op
        self.Mss_op = Mss_op
        self.n_tubes = n_tubes

    def operating_calculation(self):  # for the minimisation add the the argument x, for the manual updating self is the only argument
        # self.Ms_op = x   # comment this line for the case of manual updating
        # self.Mfeed_op = x[1]
        self.Mbrine_op, self.Mdist_op = global_balance_mfeed(self.Mfeed_op, self.Xf_op, self.Xb_out_op)
        if self.TVC_on:
            self.ER_op = tvc.tvc_entrainment_ratio(self.Pm_op, self.Ts_op, self.Tn_op, self.Xb_out_op, self.config)
            self.Ms_op, self.Mss_op = tvc.tvc_steam(self.ER_op, self.Mms_op)
        else:
            self.Ms_op = self.Mms_op
        for i_effect in tools.itereffects(0, self.N):
            self.Tvsat_op[i_effect], self.Tvsat_prime_op[i_effect], self.Tc_prime_op[i_effect] = \
                deltat_losses(((1 - self.alfa_cond_op[i_effect]) * self.Mvap_op[i_effect]), self.T_op[i_effect],
                              self.Xb_op[i_effect], self.config)

        for i_effect in tools.itereffects(0, self.N - 1):
            self.Tc_op[i_effect] = deltat_cond(((1 - self.alfa_cond_op[i_effect]) * self.Mvap_op[i_effect]),
                                            self.Tc_prime_op[i_effect], self.config, self.n_tubes)
            if self.Tc_op[i_effect] < self.T_op[i_effect + 1]:
                self.Tc_op[i_effect] = self.T_op[i_effect + 1] + 0.5

        for iEffect in range(0, (self.N - 1)):
            self.DTLM_op[iEffect] = self.M_cond_ph_op[iEffect] * h2oprop.latent_heat(self.Tvsat_prime_op[iEffect]) /\
                                    (heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect])

        for iEffect in range(self.N - 2, -1, -1):
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect] or self.T_preh_op[iEffect] < self.T_preh_op[iEffect+1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 0.5
            self.T_preh_op[iEffect] = self.T_preh_op[iEffect+1] + (self.DTLM_op[iEffect] * heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect] /
                                    (self.Mfeed_op * naclprops.cp_seawater((self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2, self.Xf_op)))
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect]or self.T_preh_op[iEffect] < self.T_preh_op[iEffect+1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 0.5
        for iEffect in range(0, self.N-1):
            self.M_cond_ph_op[iEffect] = self.Mfeed_op * naclprops.cp_seawater(
                         (self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2, self.Xf_op) * (self.T_preh_op[iEffect] - self.T_preh_op[iEffect + 1]) / \
                        h2oprop.latent_heat(self.Tvsat_prime_op[iEffect])
            self.alfa_cond_op[iEffect] = self.M_cond_ph_op[iEffect] / self.Mvap_op[iEffect]

        self.Mb_op[0] = self.Mfeed_op - self.Mvap_op[0]
        self.Xb_op[0] = self.Mfeed_op * self.Xf_op / self.Mb_op[0]
        self.h_preh_op[1] = naclprops.cp_seawater(self.T_preh_op[1], self.Xf_op) * self.T_preh_op[1]
        self.h_vprime_op[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime_op[0])
        self.h_cprime_op[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime_op[0])
        self.h_b_op[0] = naclprops.cp_seawater(self.T_op[0], self.Xb_op[0]) * self.T_op[0]
        self.M_fb_op[0] = 0
        self.M_fbrine_op[0] = 0
        self.Md_op[0] = (self.Ms_op * h2oprop.latent_heat(self.Ts_op) - self.Mfeed_op * naclprops.cp_seawater((self.T_preh_op[0] + self.T_op[0]) / 2, self.Xf_op) * (self.T_op[0] - self.T_preh_op[0])) / h2oprop.latent_heat(self.T_op[0])
        self.T_op[0] = t_fromhxarea(0, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op, self.T_preh_op, self.Md_op,
                                    self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
        self.Mvap_op[0] = self.Md_op[0]
        self.Mc_op[0] = self.M_cond_ph_op[0]

        # intermediate effects (2 to (N-1))
        for iEffect in range(1, (self.N - 1)):
            self.M_fbrine_op[iEffect] = brine_flash(iEffect, self.T_op, self.Mb_op, self.Xb_op)
            self.Mc_op[iEffect], self.M_fb_op[iEffect] = generic_flashbox(iEffect, self.Mc_op[iEffect - 1], self.Tvsat_prime_op,
                                                                    self.Tc_op, self.Mvap_op, self.alfa_cond_op)
            self.Msteam_op = self.Mvap_op[iEffect - 1] - self.M_cond_ph_op[iEffect - 1]
            if self.Msteam_op < 0:
                self.Msteam_op = self.Mvap_op[iEffect-1] * 0.9
            self.Md_op[iEffect] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[iEffect - 1])) / h2oprop.latent_heat(self.T_op[iEffect])
            self.Mvap_op[iEffect] = self.Md_op[iEffect] + self.M_fb_op[iEffect] + self.M_fbrine_op[iEffect]
            self.T_op[iEffect] = t_fromhxarea(iEffect, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op, self.T_preh_op, self.Md_op,
                                        self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
            self.Mb_op[iEffect] = self.Mb_op[iEffect - 1] - self.Md_op[iEffect] - self.M_fbrine_op[iEffect]
            self.Xb_op[iEffect] = self.Mfeed_op * self.Xf_op / self.Mb_op[iEffect]
            if self.Xb_op[iEffect] > self.X_max:
                self.Xb_op[iEffect] = self.X_max

        # last effect and end condenser
        self.alfa_cond_op[self.N - 1] = 0
        self.M_fbrine_op[self.N - 1] = brine_flash(self.N - 1, self.T_op, self.Mb_op, self.Xb_op)
        self.Mc_op[self.N - 1], self.M_fb_op[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond_op, self.Mc_op[self.N - 2], self.Tvsat_prime_op, self.Tc_op, self.Mvap_op)
        self.Msteam_op = self.Mvap_op[self.N - 2] - self.M_cond_ph_op[self.N - 2]
        self.Md_op[self.N - 1] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[self.N - 2])) / h2oprop.latent_heat(self.T_op[self.N - 1])
        self.Mvap_op[self.N - 1] = self.Md_op[self.N - 1] + self.M_fb_op[self.N - 1] + self.M_fbrine_op[self.N - 1]
        self.T_op[self.N-1] = t_fromhxarea(self.N-1, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op, self.T_preh_op, self.Md_op,
                                    self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
        self.Tn_op = self.T_op[self.N-1]
        self.Mb_op[self.N - 1] = self.Mb_op[self.N - 2] - self.Md_op[self.N - 1] - self.M_fbrine_op[self.N - 1]

        if self.Tcw_out_op > self.Tc_prime_op[self.N-1]:
            self.Tcw_out_op = self.Tc_prime_op[self.N-1] - 0.5
        DTLM_cond_op = (self.Tcw_out_op - self.Tsw_op) / np.log((self.Tc_prime_op[self.N-1] - self.Tsw_op)/(self.Tc_prime_op[self.N-1] - self.Tcw_out_op))
        DeltaT_cond_op = (heattransfer.u_condenser(self.Tc_prime_op[self.N-1]) * self.A_cond * DTLM_cond_op) / (self.Mcw_op * naclprops.cp_seawater((self.Tcw_out_op + self.Tsw_op)/2, self.Xf_op))
        if DeltaT_cond_op < 3:
            DeltaT_cond_op = 3
            logging.warning('The DeltaT in the condenser is too low, a DeltaT of 3K has been fixed, Mfeed should be changed')
        if DeltaT_cond_op > 13:
            DeltaT_cond_op = 13
            logging.warning('The DeltaT in the condenser is too high, a DeltaT of 13K has been fixed, Mfeed should be changed')
        self.Mcw_op = self.Mvap_op[self.N-1] * h2oprop.latent_heat(self.Tc_prime_op[self.N-1]) /(naclprops.cp_seawater((self.Tcw_out_op + self.Tsw_op)/2, self.Xf_op) * DeltaT_cond_op)
        self.Tcw_out_op = self.Tsw_op + DeltaT_cond_op
        self.T_preh_op[self.N - 1] = self.Tcw_out_op
        if self.T_preh_op[self.N-1] > self.Tc_prime_op[self.N-1]:
            self.T_preh_op[self.N-1] = self.Tc_prime_op[self.N-1] - 0.5
            self.Tcw_out_op = self.T_preh_op[self.N-1]

        # self.Mvap_last_op = self.Mvap_last_des * h2oprop.latent_heat(self.Tcn_des) * heattransfer.u_condenser(self.Tc_prime_op[self.N-1]) / \
        #                 (h2oprop.latent_heat(self.Tc_prime_op[self.N-1]) * heattransfer.u_condenser(self.Tcn_des))
        self.Mdist_real_op = sum(self.Md_op) + sum(self.M_fbrine_op)
        self.Xb_out_op = self.Mfeed_op * self.Xf_op / self.Mb_op[self.N - 1]
        self.Xb_op[self.N-1] = self.Xb_out_op
        self.Error_mdist_op = abs(self.Mdist_real_op - self.Mdist_op)
        self.Mms_op = self.Mms_op * (self.Mdist_op / self.Mdist_real_op)**0.7  # comment this line for the case of minimisation
        # Error_Mvap = abs(self.Mvap_last_op - self.Mvap_op[self.N-1])
        return self.Error_mdist_op


    def operating_calculation_Tsw(self, x):  # for the minimisation add the the argument x, for the manual updating self is the only argument
        self.Mms_op = x[0]  # comment this line for the case of manual updating
        self.Mfeed_op = x[1]
        if self.TVC_on:
            self.ER_op = tvc.tvc_entrainment_ratio(self.Pm_op, self.Ts_op, self.Tn_op, self.Xb_op, self.config)
            self.Ms_op, self.Mss_op = tvc.tvc_steam(self.ER_op, self.Mms_op)
        else:
            self.Ms_op = self.Mms_op
        self.Mbrine_op, self.Mdist_op = global_balance_mfeed(self.Mfeed_op, self.Xf_op, self.Xb_out_op)
        for i_effect in tools.itereffects(0, self.N):
            self.Tvsat_op[i_effect], self.Tvsat_prime_op[i_effect], self.Tc_prime_op[i_effect] = \
                deltat_losses(((1 - self.alfa_cond_op[i_effect]) * self.Mvap_op[i_effect]), self.T_op[i_effect],
                              self.Xb_op[i_effect], self.config)

        for i_effect in tools.itereffects(0, self.N - 1):
            self.Tc_op[i_effect] = deltat_cond(((1 - self.alfa_cond_op[i_effect]) * self.Mvap_op[i_effect]),
                                            self.Tc_prime_op[i_effect], self.config, self.n_tubes)
            if self.Tc_op[i_effect] < self.T_op[i_effect + 1]:
                self.Tc_op[i_effect] = self.T_op[i_effect + 1] + 0.5

        for iEffect in range(0, (self.N - 1)):
            self.DTLM_op[iEffect] = self.M_cond_ph_op[iEffect] * h2oprop.latent_heat(self.Tvsat_prime_op[iEffect]) / \
                                    (heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect])

        for iEffect in range(self.N - 2, -1, -1):
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect] or self.T_preh_op[iEffect] < self.T_preh_op[iEffect + 1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 5
            self.T_preh_op[iEffect] = self.T_preh_op[iEffect + 1] + (
            self.DTLM_op[iEffect] * heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect] /
            (self.Mfeed_op * naclprops.cp_seawater((self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2,
                                                   self.Xf_op)))
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect] or self.T_preh_op[iEffect] < self.T_preh_op[
                        iEffect + 1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 5
        for iEffect in range(0, self.N - 1):
            self.M_cond_ph_op[iEffect] = self.Mfeed_op * naclprops.cp_seawater(
                (self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2, self.Xf_op) * (
                                         self.T_preh_op[iEffect] - self.T_preh_op[iEffect + 1]) / \
                                         h2oprop.latent_heat(self.Tvsat_prime_op[iEffect])
            self.alfa_cond_op[iEffect] = self.M_cond_ph_op[iEffect] / self.Mvap_op[iEffect]

        self.Mb_op[0] = self.Mfeed_op - self.Mvap_op[0]
        self.Xb_op[0] = self.Mfeed_op * self.Xf_op / self.Mb_op[0]
        self.h_preh_op[1] = naclprops.cp_seawater(self.T_preh_op[1], self.Xf_op) * self.T_preh_op[1]
        self.h_vprime_op[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime_op[0])
        self.h_cprime_op[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime_op[0])
        self.h_b_op[0] = naclprops.cp_seawater(self.T_op[0], self.Xb_op[0]) * self.T_op[0]
        self.M_fb_op[0] = 0
        self.M_fbrine_op[0] = 0
        self.Md_op[0] = (self.Ms_op * h2oprop.latent_heat(self.Ts_op) - self.Mfeed_op * naclprops.cp_seawater(
            (self.T_preh_op[0] + self.T_op[0]) / 2, self.Xf_op) * (
                         self.T_op[0] - self.T_preh_op[0])) / h2oprop.latent_heat(self.T_op[0])
        self.T_op[0] = t_fromhxarea(0, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op, self.T_preh_op, self.Md_op,
                                    self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
        self.Mvap_op[0] = self.Md_op[0]
        self.Mc_op[0] = self.M_cond_ph_op[0]

        # intermediate effects (2 to (N-1))
        for iEffect in range(1, (self.N - 1)):
            self.M_fbrine_op[iEffect] = brine_flash(iEffect, self.T_op, self.Mb_op, self.Xb_op)
            self.Mc_op[iEffect], self.M_fb_op[iEffect] = generic_flashbox(iEffect, self.Mc_op[iEffect - 1],
                                                                          self.Tvsat_prime_op,
                                                                          self.Tc_op, self.Mvap_op, self.alfa_cond_op)
            self.Msteam_op = self.Mvap_op[iEffect - 1] - self.M_cond_ph_op[iEffect - 1]
            if self.Msteam_op < 0:
                self.Msteam_op = self.Mvap_op[iEffect - 1] * 0.9
            self.Md_op[iEffect] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[iEffect - 1])) / h2oprop.latent_heat(
                self.T_op[iEffect])
            self.Mvap_op[iEffect] = self.Md_op[iEffect] + self.M_fb_op[iEffect] + self.M_fbrine_op[iEffect]
            self.T_op[iEffect] = t_fromhxarea(iEffect, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op, self.T_preh_op,
                                              self.Md_op,
                                              self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
            self.Mb_op[iEffect] = self.Mb_op[iEffect - 1] - self.Md_op[iEffect] - self.M_fbrine_op[iEffect]
            self.Xb_op[iEffect] = self.Mfeed_op * self.Xf_op / self.Mb_op[iEffect]
            if self.Xb_op[iEffect] > self.X_max:
                self.Xb_op[iEffect] = self.X_max

        # last effect and end condenser
        self.alfa_cond_op[self.N - 1] = 0
        self.M_fbrine_op[self.N - 1] = brine_flash(self.N - 1, self.T_op, self.Mb_op, self.Xb_op)
        self.Mc_op[self.N - 1], self.M_fb_op[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond_op, self.Mc_op[self.N - 2], self.Tvsat_prime_op,
                                                                         self.Tc_op, self.Mvap_op)
        self.Msteam_op = self.Mvap_op[self.N - 2] - self.M_cond_ph_op[self.N - 2]
        self.Md_op[self.N - 1] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[self.N - 2])) / h2oprop.latent_heat(self.T_op[self.N - 1])
        self.Mvap_op[self.N - 1] = self.Md_op[self.N - 1] + self.M_fb_op[self.N - 1] + self.M_fbrine_op[self.N - 1]
        self.T_op[self.N - 1] = t_fromhxarea(self.N - 1, self.Mfeed_op, self.Xf_op, self.A_hx, self.T_op,
                                             self.T_preh_op, self.Md_op,
                                             self.Ts_op, self.alfa_cond_op, self.Mvap_op, self.Tc_op)
        self.Tn_op = self.T_op[self.N - 1]
        self.Mb_op[self.N - 1] = self.Mb_op[self.N - 2] - self.Md_op[self.N - 1] - self.M_fbrine_op[self.N - 1]

        if self.Tcw_out_op > self.Tc_prime_op[self.N - 1]:
            self.Tcw_out_op = self.Tc_prime_op[self.N - 1] - 0.5
        DTLM_cond_op = (self.Tcw_out_op - self.Tsw_op) / np.log((self.Tc_prime_op[self.N - 1] - self.Tsw_op) / (self.Tc_prime_op[self.N - 1] - self.Tcw_out_op))
        # self.Tcw_out_op = self.Tn_op - self.DTT_cond
        DeltaT_cond_op = self.Tcw_out_op - self.Tsw_op
        if DeltaT_cond_op < 3:
            DeltaT_cond_op = 3
            logging.warning(
                'The DeltaT in the condenser is too low, a DeltaT of 3K has been fixed, Mfeed should be changed')
        if DeltaT_cond_op > 13:
            DeltaT_cond_op = 13
            logging.warning(
                'The DeltaT in the condenser is too high, a DeltaT of 13K has been fixed, Mfeed should be changed')
        self.Mcw_op = self.Mvap_op[self.N - 1] * h2oprop.latent_heat(self.Tc_prime_op[self.N - 1]) / (
        naclprops.cp_seawater((self.Tcw_out_op + self.Tsw_op) / 2, self.Xf_op) * DeltaT_cond_op)
        self.T_preh_op[self.N - 1] = self.Tcw_out_op
        if self.T_preh_op[self.N - 1] > self.Tc_prime_op[self.N - 1]:
            self.T_preh_op[self.N - 1] = self.Tc_prime_op[self.N - 1] - 0.5
            self.Tcw_out_op = self.T_preh_op[self.N - 1]
        self.DTLM_cond_op = DeltaT_cond_op / np.log((self.Tc_prime_op[self.N-1] - self.Tsw_op) / (self.Tc_prime_op[self.N-1] - self.Tcw_out_op))
        self.Mvap_last_op = self.Mvap_last_des * h2oprop.latent_heat(self.Tcn_des) * heattransfer.u_condenser(
            self.Tc_prime_op[self.N - 1] * self.DTLM_cond_op) / \
                            (h2oprop.latent_heat(self.Tc_prime_op[self.N - 1]) * heattransfer.u_condenser(
                                self.Tcn_des) * self.DTLM_cond_des)
        self.Mdist_real_op = sum(self.Md_op) + sum(self.M_fbrine_op)
        self.Xb_out_op_real = self.Mfeed_op * self.Xf_op / self.Mb_op[self.N - 1]
        self.Error_mdist_op = abs(self.Mdist_real_op - self.Mdist_op)
        self.Mms_op = self.Mms_op * (
                                  self.Mdist_op / self.Mdist_real_op) ** 0.7  # comment this line for the case of minimisation
        Error_Mvap = abs(self.Mvap_last_op - self.Mvap_op[self.N - 1])
        Tot_err = Error_Mvap + self.Error_mdist_op
        return Tot_err

    def operating_calculation_Mfeedvariable(self, x):  # for the minimisation add the the argument x, for the manual updating self is the only argument
        self.Mms_op = x[0]   # comment this line for the case of manual updating
        self.Mfeed_op = x[1]
        if self.TVC_on:
            self.ER_op = tvc.tvc_entrainment_ratio(self.Pm_op, self.Ts_op, self.Tn_op)
            self.Ms_op, self.Mss_op = tvc.tvc_steam(self.ER_op, self.Mms_op)
        else:
            self.Ms_op = self.Mms_op
        self.Mbrine_op, self.Mdist_op = global_balance_mfeed(self.Mfeed_op, self.Xf_op, self.Xb_out_op)
        for iEffect in range(0, (self.N - 1)):
            self.DTLM_op[iEffect] = self.M_cond_ph_op[iEffect] * h2oprop.latent_heat(self.Tvsat_prime_op[iEffect]) /\
                                    (heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect])

        for iEffect in range(self.N - 2, -1, -1):
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect] or self.T_preh_op[iEffect] < self.T_preh_op[iEffect+1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 5
            self.T_preh_op[iEffect] = self.T_preh_op[iEffect+1] + (self.DTLM_op[iEffect] * heattransfer.u_condenser(self.Tvsat_prime_op[iEffect]) * self.A_preh[iEffect] /
                                    (self.Mfeed_op * naclprops.cp_seawater((self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2, self.Xf_op)))
            if self.T_preh_op[iEffect] > self.Tvsat_prime_op[iEffect]or self.T_preh_op[iEffect] < self.T_preh_op[iEffect+1]:
                self.T_preh_op[iEffect] = self.T_op[iEffect] - 5
        for iEffect in range(0, self.N-1):
            self.M_cond_ph_op[iEffect] = self.Mfeed_op * naclprops.cp_seawater(
                         (self.T_preh_op[iEffect] + self.T_preh_op[iEffect + 1]) / 2, self.Xf_op) * (self.T_preh_op[iEffect] - self.T_preh_op[iEffect + 1]) / \
                        h2oprop.latent_heat(self.Tvsat_prime_op[iEffect])
            self.alfa_cond_op[iEffect] = self.M_cond_ph_op[iEffect] / self.Mvap_op[iEffect]

        self.Mb_op[0] = self.Mfeed_op - self.Mvap_op[0]
        self.Xb_op[0] = self.Mfeed_op * self.Xf_op / self.Mb_op[0]
        self.h_preh_op[1] = naclprops.cp_seawater(self.T_preh_op[1], self.Xf_op) * self.T_preh_op[1]
        self.h_vprime_op[0] = h2oprop.enthalpy_satwat_vapor(self.Tvsat_prime_op[0])
        self.h_cprime_op[0] = h2oprop.enthalpy_satwat_liquid(self.Tc_prime_op[0])
        self.h_b_op[0] = naclprops.cp_seawater(self.T_op[0], self.Xb_op[0]) * self.T_op[0]
        self.M_fb_op[0] = 0
        self.M_fbrine_op[0] = 0
        self.Md_op[0] = (self.Ms_op * h2oprop.latent_heat(self.Ts_op) - self.Mfeed_op * naclprops.cp_seawater((self.T_preh_op[0] + self.T_op[0]) / 2, self.Xf_op) * (self.T_op[0] - self.T_preh_op[0])) / h2oprop.latent_heat(self.T_op[0])
        self.Mvap_op[0] = self.Md_op[0]
        self.Mc_op[0] = self.M_cond_ph_op[0]

        # intermediate effects (2 to (N-1))
        for iEffect in range(1, (self.N - 1)):
            self.M_fbrine_op[iEffect] = brine_flash(iEffect, self.T_op, self.Mb_op, self.Xb_op)
            self.Mc_op[iEffect], self.M_fb_op[iEffect] = generic_flashbox(iEffect, self.Mc_op[iEffect - 1], self.Tvsat_prime_op,
                                                                    self.Tc_op, self.Mvap_op, self.alfa_cond_op)
            self.Msteam_op = self.Mvap_op[iEffect - 1] - self.M_cond_ph_op[iEffect - 1]
            if self.Msteam_op < 0:
                self.Msteam_op = self.Mvap_op[iEffect-1] * 0.9
            self.Md_op[iEffect] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[iEffect - 1])) / h2oprop.latent_heat(self.T_op[iEffect])
            self.Mvap_op[iEffect] = self.Md_op[iEffect] + self.M_fb_op[iEffect] + self.M_fbrine_op[iEffect]
            self.Mb_op[iEffect] = self.Mb_op[iEffect - 1] - self.Md_op[iEffect] - self.M_fbrine_op[iEffect]
            self.Xb_op[iEffect] = self.Mfeed_op * self.Xf_op / self.Mb_op[iEffect]
            if self.Xb_op[iEffect] > self.X_max:
                self.Xb_op[iEffect] = self.X_max

        # last effect and end condenser
        self.alfa_cond_op[self.N - 1] = 0
        self.M_fbrine_op[self.N - 1] = brine_flash(self.N - 1, self.T_op, self.Mb_op, self.Xb_op)
        self.Mc_op[self.N - 1], self.M_fb_op[self.N - 1] = last_flashbox(self.N - 1, self.alfa_cond_op, self.Mc_op[self.N - 2], self.Tvsat_prime_op, self.Tc_op, self.Mvap_op)
        self.Msteam_op = self.Mvap_op[self.N - 2] - self.M_cond_ph_op[self.N - 2]
        self.Md_op[self.N - 1] = (self.Msteam_op * h2oprop.latent_heat(self.Tc_op[self.N - 2])) / h2oprop.latent_heat(self.T_op[self.N - 1])
        self.Mvap_op[self.N - 1] = self.Md_op[self.N - 1] + self.M_fb_op[self.N - 1] + self.M_fbrine_op[self.N - 1]
        self.Mb_op[self.N - 1] = self.Mb_op[self.N - 2] - self.Md_op[self.N - 1] - self.M_fbrine_op[self.N - 1]

        self.T_preh_op[self.N - 1] = self.Tcw_out_op
        if self.T_preh_op[self.N-1] > self.Tc_prime_op[self.N-1]:
            self.T_preh_op[self.N-1] = self.Tc_prime_op[self.N-1] - 0.5

        self.Mvap_last_op = self.Mvap_last_des * h2oprop.latent_heat(self.Tcn_des) * heattransfer.u_condenser(self.Tc_prime_op[self.N-1] * self.DTLM_cond_op) / \
                         (h2oprop.latent_heat(self.Tc_prime_op[self.N-1]) * heattransfer.u_condenser(self.Tcn_des) * self.DTLM_cond_des)
        self.Mdist_real_op = sum(self.Md_op) + sum(self.M_fbrine_op)
        self.Xb_out_op_real = self.Mfeed_op * self.Xf_op / self.Mb_op[self.N - 1]
        self.Error_mdist_op = abs(self.Mdist_real_op - self.Mdist_op)
        # self.Mmms_op = self.Mms_op * (self.Mdist_op / self.Mdist_real_op)**0.7  # comment this line for the case of minimisation
        Error_Mvap = abs(self.Mvap_last_op - self.Mvap_op[self.N-1])
        Tot_err = self.Error_mdist_op + Error_Mvap
        return Tot_err

