import numpy as np
from scipy import interpolate


def calculate_transport_permeability(Sconc_gm_per_kg, Sdial_gm_per_kg):
    """
    Calculates transport numbers and membrane permeability for salt and water based on empirical relations.
    Ref.: McGovern, R. K., et al. (2014). "On the cost of electrodialysis for desalination of high salinity feeds."
    
    :param Sconc_gm_per_kg: Concentrate salinity [g_salt/kg_solution]
    :param Sdial_gm_per_kg: Diluate salinity [g_salt/kg_solution]

    :return salt_transport_number: Salt transport number [-]
    :return water_transport_number: Water transport number [-]
    :return Psalt_molm2sbar: Membrane salt permeability [m²/s]
    :return Pwater_molm2sbar: Membrane water permeability [mol/m².s.bar]
    """
    
    salt_transport_number = -4e-6 * Sdial_gm_per_kg ** 2 + 4e-5 * Sdial_gm_per_kg + 0.92
    water_transport_number = -4e-5 * Sconc_gm_per_kg ** 2 - 0.019 * Sconc_gm_per_kg + 10.6
    
    Psalt_molm2sbar = min(
        2e-12 * Sdial_gm_per_kg ** 2 - 3e-10 * Sdial_gm_per_kg + 6e-8,
        2e-12 * Sconc_gm_per_kg ** 2 - 3e-10 * Sconc_gm_per_kg + 6e-8
    ) - 6e-9
    
    Pwater_molm2sbar = 5e-4 * Sconc_gm_per_kg ** (-0.416) - 2e-5

    return salt_transport_number, water_transport_number, Psalt_molm2sbar, Pwater_molm2sbar



def calculate_osmotic_coefficient_nacl(molarity_molL):
    """
    Linearly interpolates the osmotic coefficient for NaCl at 25°C based on known data.
    Ref.: Robinson, R. A. and R. H. Stokes (1949). "Tables of osmotic and activity coefficients of
    electrolytes in aqueous solutions at 25°C."
    
    :param molarity_molL: molarity_molL of the NaCl solution [mol/L]
    :return: Osmotic coefficient for NaCl [-]
    """
    
    # Known molarity_molL values and corresponding osmotic coefficients
    molarity_values = np.concatenate((np.arange(0.1, 1.1, 0.1), np.arange(1.2, 2.1, 0.2), np.arange(2.5, 6.5, 0.5)))
    osmotic_coefficients = np.array([0.932, 0.925, 0.922, 0.920, 0.921, 0.923, 0.926, 0.929, 0.932, 
                                     0.936, 0.943, 0.951, 0.962, 0.972, 0.983, 1.013, 1.045, 1.080, 
                                     1.116, 1.153, 1.192, 1.231, 1.271])
    
    # Linear interpolation function
    interpolate_function = interpolate.interp1d(molarity_values, osmotic_coefficients, kind='linear')
    
    # Calculate the osmotic coefficient for the given molarity_molL
    osmotic_coefficient = interpolate_function(molarity_molL).item()

    return osmotic_coefficient


def calculate_chemical_potential_nacl_water(molality_molkg):
    """
    Interpolates the chemical potentials of NaCl and water at a given molality.
    Ref.: Zoltan, M. and Z. P. Athanassios (2015). "Mean ionic activity coefficients in aqueous NaCl solutions
    from molecular dynamics simulations."

    :param molality_molkg: Molality of NaCl solution [mol/kg_solvent]
    :return: mu_nacl [J/mol], mu_water [J/mol]
    """

    # Data for NaCl chemical potential [kJ/mol]
    molality_values_nacl_molkg = [0.01, 0.06, 0.56, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00]
    chemical_potential_nacl_kJmol = [-414.6, -407.3, -396.2, -393.1, -389.3, -386.0, -384.4, -380.7, -378.7]

    # Interpolating NaCl chemical potential
    interpolator_nacl = interpolate.interp1d(molality_values_nacl_molkg, chemical_potential_nacl_kJmol, kind='linear')
    mu_nacl_Jmol = interpolator_nacl(molality_molkg).item() * 1000  # Convert from kJ/mol to J/mol

    # Data for water chemical potential [kJ/mol]
    molality_values_water_molkg = [0, 0.44, 1.00, 1.67, 3.00, 4.00, 4.77, 6.00]
    chemical_potential_water_kJmol = [-240.20, -240.26, -240.32, -240.39, -240.47, -240.70, -240.84, -241.06]

    # Interpolating water chemical potential
    interpolator_water = interpolate.interp1d(molality_values_water_molkg, chemical_potential_water_kJmol, kind='linear')
    mu_water_Jmol = interpolator_water(molality_molkg).item() * 1000  # Convert from kJ/mol to J/mol

    return mu_nacl_Jmol, mu_water_Jmol



def calculate_equivalent_conductivity_nacl(molarity_molL):
    """
    Interpolates the equivalent conductivity of NaCl at 25°C based on molarity.
    Ref.: Chambers, J. F., et al. (1956). "Conductances of concentrated aqueous sodium and potassium chloride
    solutions at 25°C."

    :param molarity_molL: Molarity of NaCl solution [mol/L]
    :return: Equivalent conductivity of NaCl [S.m²/mol]
    """

    # Molarity values [mol/L] and their corresponding equivalent conductivities [S.cm²/mol]
    molarity_values_molL = [0.15621, 0.20998, 0.24584, 0.33998, 0.49935, 0.68703, 1.05142,
                            1.3948, 1.5193, 1.9279, 2.3793, 2.7439, 3.1431, 3.5037, 4.5199, 5.3540]
    
    equivalent_conductivities_Scm2mol = [103.66, 101.36, 100.02, 97.29, 93.66, 90.26, 85.12,
                                         81.02, 79.65, 75.41, 71.14, 67.82, 64.36, 61.30, 53.13, 46.83]
    
    # Linear interpolation for equivalent conductivity based on molarity
    interpolator = interpolate.interp1d(molarity_values_molL, equivalent_conductivities_Scm2mol, kind='linear')

    # Calculate equivalent conductivity [S.m²/mol] (conversion from S.cm²/mol to S.m²/mol is by multiplying by 10^-4)
    equi_cond_nacl_Sm2mol = interpolator(molarity_molL).item() * 10**-4

    return equi_cond_nacl_Sm2mol



def calculate_partial_molar_volume_water(molality_molkg):
    """
    Calculates the partial molar volume of water in an NaCl solution based on molality.
    Ref.: Michaels, M. (2018). "Solution Density and Partial Molar Volume as Functions of Concentration."
    Source: https://www.odinity.com/solution-density-partial-molar-volume-functions-concentration/.

    :param molality_molkg: Molality of the NaCl solution [mol/kg]
    :return: Partial molar volume of water [m^3/mol]
    """

    Qpart_m3mol = (-0.0132 * molality_molkg**5 + 0.0732 * molality_molkg**4 - 0.1074 * molality_molkg**3 +
                            0.0798 * molality_molkg**2 + 0.0786 * molality_molkg + 18.037) / 10**6

    return Qpart_m3mol


def calculate_pumping_power(Sconc_gm_per_kg, Sdial_gm_per_kg, Dconc_kgm3, Ddial_kgm3, vel_conc_m_per_s, vel_dial_m_per_s, tech_config_and_params):
    """
    Calculates the pressure drop in the concentrate and diluate channels for pumping power.
    Ref.: Chehayeb, K. M., et al. (2017). "Optimal design and operation of electrodialysis for brackish-water
    desalination and for high-salinity brine concentration."

    :param Sconc_gm_per_kg: Salinity of concentrate [g/kg]
    :param Sdial_gm_per_kg: Salinity of diluate [g/kg]
    :param Dconc_kgm3: Density of concentrate [kg/m³]
    :param Ddial_kgm3: Density of diluate [kg/m³]
    :param vel_conc_m_per_s: Flow velocity of concentrate [m/s]
    :param vel_dial_m_per_s: Flow velocity of diluate [m/s]
    :param tech_config_and_params: Dictionary containing tech configuration and parameters (MW_nacl, h, epsilon_s, k_f, L_stack)
    :return: pr_drop_conc_pa: Pressure drop in concentrate channel [Pa]
    :return: pr_drop_dial_pa: Pressure drop in diluate channel [Pa]
    """

    # 1. Dynamic viscosity of NaCl solution
    # 1.1 Bulk concentration in concentrate and diluate
    Cconc_mol_per_m3 = Sconc_gm_per_kg * Dconc_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m³]
    Cdial_mol_per_m3 = Sdial_gm_per_kg * Ddial_kgm3 / tech_config_and_params['MW_nacl']  # [mol/m³]

    # 1.2 Molality [mol/kg]
    Cconc_mol_per_kg = Cconc_mol_per_m3 / Dconc_kgm3
    Cdial_mol_per_kg = Cdial_mol_per_m3 / Ddial_kgm3

    # 1.3 Dynamic viscosity [Pa.s] at 0.1 MPa and 25°C
    molality_values = np.arange(0, 6.5, 0.5)
    viscosity_values_micro_pa_s = [890.1, 928.6, 972.0, 1020.5, 1074.5, 1134.1, 1199.8, 1271.7, 1350.4, 
                                   1436.0, 1528.9, 1629.3, 1737.5]
    viscosity_interpolator = interpolate.interp1d(molality_values, viscosity_values_micro_pa_s, kind='linear')

    # Convert micro Pa.s to Pa.s
    viscosity_conc_pa_s = viscosity_interpolator(Cconc_mol_per_kg).item() * 10**-6
    viscosity_dial_pa_s = viscosity_interpolator(Cdial_mol_per_kg).item() * 10**-6

    # 2. Reynolds number
    Re_conc = 2 * Dconc_kgm3 * vel_conc_m_per_s * tech_config_and_params['h'] / viscosity_conc_pa_s
    Re_dial = 2 * Ddial_kgm3 * vel_dial_m_per_s * tech_config_and_params['h'] / viscosity_dial_pa_s

    # 3. Pressure drop
    effective_diameter_m = 2 * tech_config_and_params['h'] * (1 - tech_config_and_params['epsilon_s'])  # [m]
    friction_conc = tech_config_and_params['k_f'] * Re_conc**(-0.5)  # Friction factor for concentrate
    friction_dial = tech_config_and_params['k_f'] * Re_dial**(-0.5)  # Friction factor for diluate
    pr_drop_conc_pa = 4 * friction_conc * tech_config_and_params['L_stack'] * Dconc_kgm3 * vel_conc_m_per_s**2 / (2 * effective_diameter_m)  # [Pa]
    pr_drop_dial_pa = 4 * friction_dial * tech_config_and_params['L_stack'] * Ddial_kgm3 * vel_dial_m_per_s**2 / (2 * effective_diameter_m)  # [Pa]

    return pr_drop_conc_pa, pr_drop_dial_pa
