
#from .densities import density_seawater_liquid

def convert_ppm_to_mol_m3(feed_ion_conc_ppm_dict, feed_density, params_dict):
    """
    Converts concentrations from ppm (mg/L) to mol/m³ for each ion in the given feed_ion_conc_ppm_dict dictionary.

    Parameters:
        feed_ion_conc_ppm_dict (dict): A dictionary where keys are ion names (e.g., 'Na', 'Cl') and values are concentrations in ppm (mg/L).
        params_dict (dict): A dictionary containing the molecular weights of the ions, where the key format is 'MW_<ion>'.

    Returns:
        dict: A dictionary where keys are ion names and values are concentrations in mol/m³.

    Example:
        Given:
        feed_ion_conc_ppm_dict = {'Na': 29350.63481826556, 'Cl': 48775.127792378225}
        params_dict = {'MW_Na': 22.99, 'MW_Cl': 35.45}

        The function will return:
        {'Na': 1276.09, 'Cl': 1376.33}
    """
    feed_ion_conc_mol_m3_dict = {}
    for ion, concentration_ppm in feed_ion_conc_ppm_dict.items():
        # Convert ppm (mg/L) to mol/m³
        feed_ion_conc_mol_m3_dict[ion] = concentration_ppm * 1e-3 / params_dict[f'MW_{ion}'] * feed_density  # mg/L to g/L then g/L to mol/m³
    return feed_ion_conc_mol_m3_dict

def convert_mol_m3_to_ppm(feed_ion_conc_mol_m3_dict, params_dict):
    """
    Converts concentrations from mol/m³ to ppm (g/m3 or mg/L) for each ion in the given feed_ion_conc_mol_m3_dict dictionary.

    Parameters:
        feed_ion_conc_mol_m3_dict (dict): A dictionary where keys are ion names (e.g., 'Na', 'Cl') and values are concentrations in mol/m³.
        params_dict (dict): A dictionary containing the molecular weights of the ions, where the key format is 'MW_<ion>'.

    Returns:
        dict: A dictionary where keys are ion names and values are concentrations in ppm (g/m3 or mg/L).

    Example:
        Given:
        feed_ion_conc_mol_m3_dict = {'Na': 1276.09, 'Cl': 1376.33}
        params_dict = {'MW_Na': 22.99, 'MW_Cl': 35.45}

        The function will return:
        {'Na': 29350.63, 'Cl': 48775.13}
    """
    feed_ion_conc_ppm_dict = {}
    for ion, concentration_mol_m3 in feed_ion_conc_mol_m3_dict.items():
        MW = params_dict[f'MW_{ion}']  # Get molecular weight from the config dictionary
        # Convert mol/m³ to ppm (mg/L)
        feed_ion_conc_ppm_dict[ion] = concentration_mol_m3 * MW  # mol/m³ to g/m³ or mg/L
    return feed_ion_conc_ppm_dict


def calculate_nacl_concentration_in_ppm(feed_ion_conc_ppm_dict, tech_config_and_params):
    """
    Calculates the concentration of sodium chloride (NaCl) in ppm from individual ion concentrations in ppm (mg/L).

    The function converts concentrations from ppm (mg/L) to mol/L for sodium (Na) and chloride (Cl),
    determines the minimum concentration of Na and Cl, and then calculates the concentration of NaCl in ppm.

    Parameters:
        feed_ion_conc_ppm_dict (dict): A dictionary where keys are ion names (e.g., 'Na', 'Cl') and values are concentrations in ppm (mg/L).
        tech_config_and_params (dict): A dictionary containing the molecular weights of the ions, where the key format is 'MW_<ion>'.

    Returns:
        float: The concentration of NaCl in ppm (mg/L).

    Formulae:
        1. Convert ppm to mol/L:
           - mol/L = (ppm / 1000) / MW
           - ppm to g/L: ppm / 1000
           - g/L to mol/L: g/L / MW

        2. Calculate the minimum of the molar concentrations of Na and Cl:
           - C_feed_NaCl_mol = min(C_feed_Na_mol, C_feed_Cl_mol)

        3. Convert mol/L to ppm:
           - ppm = mol/L * MW * 1000

    Example:
        Given:
        feed_ion_conc_ppm_dict = {'Na': 29350.63481826556, 'Cl': 48775.127792378225}
        tech_config_and_params = {'MW_Na': 22.99, 'MW_Cl': 35.45, 'MW_NaCl': 58.44}

        The function will return:
        49378.43881077991
    """
    # Convert concentrations from ppm (mg/L) to mol/L
    Cfeed_Na_mol_L = feed_ion_conc_ppm_dict['Na'] / 1000 / tech_config_and_params['MW_Na']
    Cfeed_Cl_mol_L = feed_ion_conc_ppm_dict['Cl'] / 1000 / tech_config_and_params['MW_Cl']
    
    # Determine the limiting concentration of NaCl
    Cfeed_NaCl_mol_L = min(Cfeed_Na_mol_L, Cfeed_Cl_mol_L)
    
    # Convert mol/L to ppm
    Cfeed_NaCl_ppm = Cfeed_NaCl_mol_L * tech_config_and_params['MW_NaCl'] * 1000

    return Cfeed_NaCl_ppm

def calculate_nacl_concentration_in_mol_m3(feed_ion_conc_ppm_dict, tech_config_and_params):
    """
    Calculates the concentration of sodium chloride (NaCl) in mol/m3 from individual ion concentrations in ppm (mg/L).

    The function converts concentrations from ppm (mg/L) to mol/L for sodium (Na) and chloride (Cl),
    determines the minimum concentration of Na and Cl, and then calculates the concentration of NaCl in ppm.

    Parameters:
        feed_ion_conc_ppm_dict (dict): A dictionary where keys are ion names (e.g., 'Na', 'Cl') and values are concentrations in ppm (mg/L).
        tech_config_and_params (dict): A dictionary containing the molecular weights of the ions, where the key format is 'MW_<ion>'.

    Returns:
        float: The concentration of NaCl in ppm (mg/L).

    Formulae:
        1. Convert ppm to mol/L:
           - mol/L = (ppm / 1000) / MW
           - ppm to g/L: ppm / 1000
           - g/L to mol/L: g/L / MW

        2. Calculate the minimum of the molar concentrations of Na and Cl:
           - C_feed_NaCl_mol = min(C_feed_Na_mol, C_feed_Cl_mol)

        3. Convert mol/L to mol/m3:
           - mol/m3 = mol/L * 1000

    Example:
        Given:
        feed_ion_conc_ppm_dict = {'Na': 29350.63481826556, 'Cl': 48775.127792378225}
        tech_config_and_params = {'MW_Na': 22.99, 'MW_Cl': 35.45, 'MW_NaCl': 58.44}

        The function will return the concentration of NaCl in mol/m³.
    """
    # Convert concentrations from ppm (mg/L) to mol/L
    Cfeed_Na_mol_L = feed_ion_conc_ppm_dict['Na'] / 1000 / tech_config_and_params['MW_Na']
    Cfeed_Cl_mol_L = feed_ion_conc_ppm_dict['Cl'] / 1000 / tech_config_and_params['MW_Cl']
    
    # Determine the limiting concentration of NaCl
    Cfeed_NaCl_mol_L = min(Cfeed_Na_mol_L, Cfeed_Cl_mol_L)

    # Convert mol/L to mol/m³
    Cfeed_NaCl_mol_m3 = Cfeed_NaCl_mol_L * 1000
    

    return Cfeed_NaCl_mol_m3


def sum_impurities_and_convert_to_g_L(feed_ion_conc_ppm_dict):
    """
    Sums the concentrations of ions other than Na and Cl from a dictionary and converts the total to g/L.

    Parameters:
        feed_ion_conc_ppm_dict (dict): A dictionary containing the concentrations of various ions in ppm (mg/L). 
                            Expected keys include 'Na' and 'Cl' which are to be excluded from the sum.

    Returns:
        float: The total concentration of impurities (all ions except Na and Cl) in g/L.

    Formula:
        - Sum the concentrations of ions excluding Na and Cl.
        - Convert the total from ppm to g/L by multiplying by 0.001.

    Example:
        Given:
        feed_ion_conc_ppm_dict = {'CO3': 2.9774614484331e-05, 'Ca': 602.7944662300827, 'Cl': 48775.127792378225, 
                       'Mg': 895.9848734037511, 'Na': 29350.63481826556, 'SO4': 286.8224377388289}

        The function will return:
        1.8 (sum of CO3, Ca, Mg, and SO4 converted to g/L)
    """
    # Filter out Na and Cl from the dictionary
    impurities = {ion: concentration for ion, concentration in feed_ion_conc_ppm_dict.items() if ion not in ['Na', 'Cl']}
    
    # Sum the concentrations of impurities
    total_ppm = sum(impurities.values())
    
    # Convert the total concentration from ppm to g/L
    total_g_per_L = total_ppm * 0.001
    
    return total_g_per_L


def calculate_residual_concentration_in_ppm(feed_ion_conc_ppm_dict, feed_nacl_conc_mol_m3_dict, tech_config_and_params):
    """
    Calculates the residual concentration of ions, including sodium (Na) and chloride (Cl), in ppm (mg/L) after accounting for the concentration of NaCl in the feed.

    The function adjusts the concentrations of Na and Cl by subtracting the concentration of NaCl (converted to ppm) from the original ion concentrations in ppm.
    For other ions, the original concentrations in ppm are retained.

    Parameters:
        feed_ion_conc_ppm_dict (dict): A dictionary where keys are ion names (e.g., 'Na', 'Cl') and values are concentrations in ppm (mg/L).
        feed_nacl_conc_mol_m3_dict (float): The concentration of NaCl in the feed, in mol/m³.
        tech_config_and_params (dict): A dictionary containing the molecular weights of the ions, where the key format is 'MW_<ion>' (e.g., 'MW_Na' for sodium).

    Returns:
        dict: A dictionary where keys are ion names and values are the residual concentrations in ppm (mg/L).

    Example:
        Given:
        feed_ion_conc_ppm_dict = {'Na': 29350.63481826556, 'Cl': 48775.127792378225, 'SO4': 304.915}
        feed_nacl_conc_mol_m3_dict = 0.5  # mol/m³
        tech_config_and_params = {'MW_Na': 22.99, 'MW_Cl': 35.45, 'MW_NaCl': 58.44}

        The function will return:
        {'Na': 17778.81826556, 'Cl': 30500.127792378225, 'SO4': 304.915}
    """

    residual_ion_conc_ppm_dict = {}  # Residual concentrations of ions in ppm after accounting for NaCl

    for ion, concentration_ppm in feed_ion_conc_ppm_dict.items():
        if ion == 'Na' or ion == 'Cl':
            # Subtract the NaCl contribution from the Na and Cl concentrations
            residual_ion_conc_ppm_dict[ion] = concentration_ppm - feed_nacl_conc_mol_m3_dict * tech_config_and_params[f'MW_{ion}'] * 1000
        else:
            # Retain original concentration for other ions
            residual_ion_conc_ppm_dict[ion] = concentration_ppm

    return residual_ion_conc_ppm_dict
