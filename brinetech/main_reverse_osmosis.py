from argparse import ArgumentParser
from sys import argv
import os
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from common_and_tools import save_resultlists
from common_and_tools import model_reverse_osmosis

def driver(model, parameterconfigfilepath, nelem_1, nelem_2, Mfeed, Xfeed, Xret_out, electricity_cost,
           output_directory, output_namefile, output_filename):
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)

    result_lists = model(parameter_config, nelem_1, nelem_2, Mfeed, Xfeed, Xret_out, electricity_cost)

    for result_list in result_lists:
        print(result_list)

    if 'simulation-single' in str(result_lists[0]):
        output_filename = f"results_single_stage_mfeed={round(Mfeed, 2)}.xls"
    else:
        output_filename = f"results_double_stage_mfeed={round(Mfeed, 2)}.xls"

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
    'reverse_osmosis': model_reverse_osmosis
    }
    parser = ArgumentParser(prog='brinetech_reverse_osmosis', description='Reverse osmosis simulation tool')
    parser.add_argument('-m', '--model')  # should be 'reverse_osmosis'
    parser.add_argument('-n1', '--n1-elem', type=int, required=True, help="Number of elements in first stage [-]")
    parser.add_argument('-n2', '--n2-elem', type=int, required=True, help="Number of elements in second stage [-]")
    parser.add_argument('-fm', '--feed-m', type=float, required=True, help="Feed flow rate M in kg/s")
    parser.add_argument('-xf', '--x-feed', type=float, required=True, help="Salinity of the feed stream [ppm]")
    parser.add_argument('-xro', '--x-ret-out', type=float, required=True, help="Desired salinity of the retentate stream [ppm]")
    parser.add_argument('-e', '--electricity-cost', type=float, default=0.1035, help="Cost of electricity in $/kWh")    
    args = parser.parse_args(argv)

    ## getting path for the parameters.yml:
    main_tool_name = ""
    main_tool_name = args.model
    # parameters file path, output directory & output file name
    param_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'parameters.yml')
    output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name)
    # name of the dat. file to store output file name:
    output_namefile = "fila_name.dat"

    return driver(
        model=models[args.model],
        parameterconfigfilepath=param_path,
        nelem_1=args.n1_elem,
        nelem_2=args.n2_elem,
        Mfeed=args.feed_m,
        electricity_cost=args.electricity_cost,
        Xfeed=args.x_feed,
        Xret_out=args.x_ret_out,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=None,
    )

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()

"""
## model_reverse_osmosis (single stage)
python main_reverse_osmosis.py -m "reverse_osmosis" -n1 8 -n2 6 -fm 17.3458 -xf 26455.82 -xro 40000 -e 0.103

## model_reverse_osmosis (double stage)
python main_reverse_osmosis.py -m "reverse_osmosis" -n1 8 -n2 6 -fm 17.3458 -xf 26455.82 -xro 70000 -e 0.103
"""