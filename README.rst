===============
Brine Tech Tools: Introductory information
===============

This software package consists of various thermal- and membrane-based technology tools used for the 
treatment of seawater / industrial effluents for the recovery of water and minerals. Following 
tools are included in the package: nanofiltration (NF), reverse osmosis (RO), multi-effect-distillation (MED), 
electrodialysis (ED), along with crystallizer tools tailored to the minerals/salts being recovered. 
These Python-based tools can be used independently or integrated to simulate a treatment chain on a suitable 
integration platform, like RCE (Remote Component Environment, https://rcenvironment.de/). This work was performed
within the ZERO BRINE project (ZERO BRINE – Industrial Desalination – Resource Recovery – Circular Economy),
funded by the European Union's Horizon 2020 research and innovation program under grant agreement No 730390. www.zerobrine.eu).
For more information about the source code or integration of these tools to simulate treatment chains, you can contact the team at
brinetechtools@dlr.de.

Package structure
------------------
- The folder 'brinetech' contains the main software package that houses the Python scripts for all the 
technology tools (main_crystallizer.py, main_electrolysis.py and so on). The file main_all.py combines all these technology tools into
one file. At the end of each Python script, we have given a sample executable line for your conda terminal, that will simulate the tool
and save the results in the 'results' folder. The values in the executable line can be modified to suit the user needs.

- The folder 'inputs' contains the inputs parameters, input ion concentration, etc. for each of the tools. These need to be modified by
the user to suit a particular application

- The folder 'common_and_tools' has a folder 'tools' that houses the source code for the modeling of the technologies. The folder
'common', as the name implies, houses some commonly used economic, physical and input-output functions accessible for all the tools.

- The folder 'model_documentation' contains some basic information about each tools.

Naming convention for crystallizer tools
------------------
- Firstly, we distinguish the crystallizers into two categories: single stage (that is, only one mineral is precipitated using, if required,
an alkaline solution as a reactant) and double stage (that is, two minerals are precipitated one after the other using the same or different alkaline
solutions as reactants). Thus we use the following convention:
	- Single stage: crystallizer_single_{reactant name or self}_{precipitate name}
	- Double stage: crystallizer_double_{reactant1 name or self}_{precipitate1 name}_{reactant2 name or self}_{precipitate2 name}
	
- We use the following short-hand names for the reactants and precipitates:
	- Reactants:
		1. Sodium hydroxide (NaOH): sodhyd
		2. Dolime solution (Ca(OH)2.Mg(OH)2): dol
		3. Sodium carbonate (Na2CO3): sodcar
	- Precipitate:
		1. Magnesium hydroxide (Mg(OH)2): maghyd
		2. Calcium hydroxide (Ca(OH)2): calhyd
		3. Calcium sulphate (CaSO4): calsul
		4. Calcium carbonate(CaCO3): calcar

- For example, the tool name 'crystallizer_double_dol_maghyd_self_calsul' would indicate a double stage crystallizer, where using dolime solution magnesium hydroxide is precipitated
 followed by a self precipitation (no reactant solution) of calcium sulphate
 
 How to use the tools
------------------
- First ensure that the Python dependencies mentioned in the 'poetry.lock' are installed in your desired Python environment.

- Instruction for all tools except nanofiltration:
	- Each of the tools work independently of each other. To use the tools, open the python file starting with 'main_' in the folder 'brinetech/brinetech' followed by the name of your
	desired tool, for example, 'main_reverse_osmosis.py'. The 'main_all.py' combines all the tools into one file. The driver function in each of these files is the main script that
	simulates the tool. Certain inputs, necessary to execute these tools, are saved in the 'brinetech/brinetech/inputs/{tool name}' folder. In this case, a 'parameters.yml' file is saved
	in the 'brinetech/brinetech/inputs/reverse_osmosis' folder. The user may want to modify the values, as necessary. Once all the inputs are correctly chosen, the script in the 
	'main_reverse_osmosis.py' can be executed. An exemplary command prompt-based execution line is given at the end of these Python scripts. In this case (for single stage RO): 
	python main_reverse_osmosis.py -m "reverse_osmosis" -n1 8 -n2 6 -fm 17.3458 -xf 26455.82 -xro 40000 -e 0.103
	
- Instructions for nanofiltration tool:
	- To use this tool (main_nanofiltration.py), a pre-processing step is required. This is because, the ion rejection file, which is used an input in the tool,
	is at membrane level and not at NF plant level, as is available from site measurements. This means, that the plant-level rejection first needs to translated to
	its membrane-level format. This is done using the file ‘main_nanofiltration_membrane_rejection.py’. The output, which is a membrane-level ion rejection is saved in the
	inputs folder of the tool. Now the main tool (main_nanofiltration.py) can be used, which will automatically access the membrane-level ion rejection (provided their nNF numbering 
	is the same). For more details, refer to the document provided in the 'model_documentation' folder

- On successful execution, the results, in the form of an excel file, will then be saved in the 'brinetech/brinetech/inputs/{tool name}' folder

- For more tool-specific information, refer to the documents provided in the 'model_documentation' folder

- For detailed information about the modeling assumptions for these tools, refer to:
1. Micari, M. (2020). Integration of Desalination and Purification processes for the Treatment and Valorization of Industrial Brines. Institute for Building Energetics, Thermotechnology and Energy Storage (IGTE). Stuttgart, University of Stuttgart, Dissertation.
2. Pawar, Nikhil Dilip; Harris, Steve; Mitko, Krzysztof; Korevaar, Gijsbert (2022): Valorization of coal mine effluents - Challenges and economic opportunities. In: Water Resources and Industry 28, S. 100179. DOI: 10.1016/j.wri.2022.100179.

