import numpy as np

def civil_investment_cost(n_modules, Qfeed, proj_config_and_params):  # Qfeed in m3/s, cost in dollars
    Qfeed = Qfeed * 3600
    C_civil = 862 * Qfeed + 1239 * n_modules
    C_civil = C_civil * proj_config_and_params['change_euro_dollar'] * proj_config_and_params['CEPCI']['current']/ proj_config_and_params['CEPCI']['2001']
    return C_civil

def mechanical_cost(n_modules, Qfeed, proj_config_and_params): # Qfeed in m3/s, cost in dollars
    Qfeed = Qfeed * 3600
    C_mech = 3608 * np.power(Qfeed, 0.85) + 908 * n_modules
    C_mech = C_mech * proj_config_and_params['change_euro_dollar'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2001']
    return C_mech

def electrotechnic_cost_outdated(Pfeed, Qfeed, proj_config_and_params): # Pfeed in bar, Qfeed in m3/s, cost in dollars
    # outdated, because it does not consider pump cost separately, instead, combines it with filter and piping
    # separate pump sizing and cost is necessary to account for reduced HPP sizing and energy, when ERD is integrated
    Qfeed = Qfeed * 3600
    C_electro = 1.4 * 1e6 + 54 * Pfeed * Qfeed
    C_electro = C_electro * proj_config_and_params['change_euro_dollar'] * proj_config_and_params['CEPCI']['current']/ proj_config_and_params['CEPCI']['2001']
    return C_electro


def electrotechnic_cost_only_filter_and_piping(Qperm_m3h, proj_config_and_params): # Pfeed in bar, Qfeed in m3/s, cost in dollars
    C_electro = 1.4 * 1e6 * (Qperm_m3h / 2000)  # The source, B. Van der Bruggen et al. (2001) estimates 1.4*1e6 for a 2000 m3/h permeate plant
    C_electro = C_electro * proj_config_and_params['change_euro_dollar'] * proj_config_and_params['CEPCI']['current']/ proj_config_and_params['CEPCI']['2001']
    return C_electro

def estimation_HPpump_cost_single(Qfeed, pressure_delta, tech_config_and_params, proj_config_and_params):  # Qfeed in m3/s
    Qfeed_m3h = Qfeed * 3600
    if Qfeed_m3h == 450:
        pump_cost = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * pressure_delta
    elif 200 < Qfeed < 450:
        pump_cost = tech_config_and_params['coeffA_catB_HPP'] * np.power((pressure_delta * Qfeed_m3h), tech_config_and_params['coeffB_catB_HPP'])
    else:  # Qfeed_m3h < 200
        pump_cost = tech_config_and_params['coeffA_catC_HPP'] * pressure_delta * Qfeed_m3h
    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_HPpump_cost_parallpumps(Qfeed, pressure_delta, tech_config_and_params, proj_config_and_params):
    Qfeed_1 = 450
    pump_cost_1 = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * pressure_delta
    Qfeed_2 = Qfeed - Qfeed_1
    if Qfeed_2 <= 450:
        pump_cost_2 = estimation_HPpump_cost_single(tech_config_and_params, Qfeed_2, pressure_delta)
    else:
        pump_cost_2 = estimation_HPpump_cost_parallpumps(tech_config_and_params, Qfeed_2, pressure_delta)
    pump_cost = pump_cost_1 + pump_cost_2
    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_cost_erd_px(Vretentate, proj_config_and_params):
    """
    Estimate the cost of a Pressure Exchanger (PX) type Energy Recovery Device (ERD) in a SWRO or NF plant.

    This function calculates the cost of a ERD-PX,
    based on the retentate volume of the SWRO/NF system entering the PX at high pressure, and adjusts for current economic conditions
    using the Chemical Engineering Plant Cost Index (CEPCI).

    Parameters:
    tech_config_and_params (dict): Configuration dictionary containing CEPCI indices with keys 'current' and '2018'.
    Vretentate (float): Retentate volume flow rate in cubic meters per hour (m3/h).

    Returns:
    float: Estimated cost of the ERD-PX in current USD.
    
    References:
    Lu, Yan-Yue; Hu, Yang-Dong; Zhang, Xiu-Ling; Wu, Lian-Ying; Liu, Qing-Zhi (2007): Optimum design of 
    reverse osmosis system under different feed concentration and product specification. In 
    Journal of Membrane Science 287 (2), pp. 219-229. 
    DOI: 10.1016/j.memsci.2006.10.037.
    """

    erd_cost = 3134.7 * Vretentate**0.58 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2007']  # [USD_current]

    return erd_cost

def membrane_cost(n_modules, tech_config_and_params, proj_config_and_params): # cost in dollars
    # if module prices are "latest" (assumed 2022 in this case), we use it directly. Or else, if it is from 2001, we use the current CEPCI to bring it to current year (if data is available, it could also be 2023)
    C_membrane = tech_config_and_params['cost_membrane_module'] * n_modules * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params["cost_membrane_module_year"]]
    #if ((tech_config_and_params["cost_membrane_module_year"] == 2022) & (tech_config_and_params["cost_membrane_module_currency"] == "USD")):
    #    C_membrane = C_membrane * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2022']
    #elif ((tech_config_and_params["cost_membrane_module_year"] == 2022) & (tech_config_and_params["cost_membrane_module_currency"] == "EUR")):
    #    C_membrane = C_membrane * tech_config_and_params['change_euro_dollar'] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2022']
    #elif ((tech_config_and_params["cost_membrane_module_year"] == 2001) & (tech_config_and_params["cost_membrane_module_currency"] == "EUR")):
    #    C_membrane = C_membrane * tech_config_and_params['change_euro_dollar'] * tech_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI']['2001']
    return C_membrane

def capital_cost(C_civil, C_mech, C_electro, C_membrane):  # cost in dollars
    C_capital = C_civil + C_mech + C_electro + C_membrane
    return C_capital


def manteinance_cost(C_cap_tot, tech_config_and_params): # dollars/y  C_cap_tot is the total investment cost=Cciv+Cmech+Celectrotech+Cmembrane [dollars]
    C_mant = tech_config_and_params['perc_mant_cap'] * C_cap_tot
    return C_mant

def quality_control_cost(C_cap_tot, tech_config_and_params): # dollars/y
    C_quality = tech_config_and_params['perc_quality_cap'] * C_cap_tot
    return C_quality

def installation_cost(C_cap_tot, tech_config_and_params): # dollars/y
    C_install = tech_config_and_params['perc_install_cap'] * C_cap_tot
    return C_install

def chemical_cost(Mpermeate, tech_config_and_params, proj_config_and_params): # dollars/y
    operating_hours = 8760 * tech_config_and_params['plant_availability'] # h/y
    Mpermeate = Mpermeate * 3600 # m3/h
    C_chem = tech_config_and_params['chemicals_cost'] * proj_config_and_params['change_euro_dollar'] * Mpermeate * operating_hours # euro/h * h/y = euro/y
    return C_chem

def energy_cost(Qfeed, pressure_delta, electricity_cost, tech_config_and_params): # Qfeed in m3/s, Pfeed in bar, cost in dollar/y
    operating_hours = 8760 * tech_config_and_params['plant_availability']  # h/y
    C_energy = electricity_cost / 1000 * (tech_config_and_params['cons_membrane_system'] * Qfeed * 3600 / tech_config_and_params['energy_efficiency'] +
                                                    pressure_delta * 1e5 * Qfeed / tech_config_and_params['energy_efficiency']) * operating_hours # $/h * h/y = $/y
    return C_energy


