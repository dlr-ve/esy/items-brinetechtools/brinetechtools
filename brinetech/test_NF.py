
#%%

import pandas as pd
from common_and_tools.common import density_mixtures
import yaml
import os
# Try to import CLoader from PyYAML for performance improvement; fallback to standard Loader if unavailable
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

#%%
base_path = os.path.dirname(os.path.realpath(__file__))
main_tool_name = "nanofiltration"

parameterconfigfilepath = os.path.join(base_path, "inputs", main_tool_name, 'parameters.yml')
with open(parameterconfigfilepath, "r") as file_params:
        parameter_dict = yaml.load(file_params, Loader=Loader)

# project param
proj_param_and_config_path = os.path.join(base_path, "inputs", 'proj_config_and_params.yml')
with open(proj_param_and_config_path, "r") as file_params:
        proj_config_params_dict = yaml.load(file_params, Loader=Loader)

#%%
############ NF1 #####################
current_tool_name = 'nanofiltration'
sub_tool_category = 'NF1'
df_output = pd.read_excel('./results/nanofiltration/arabian_gulf_kuwait/average_tds/NF1/results_Rgiven_noCorrFactor_qfeedm3h_train=475.0_with_erd.xls')
#df_nf2_output = pd.read_excel('./results/nanofiltration/arabian_gulf_kuwait/average_tds/NF2/results_Rgiven_noCorrFactor_qfeed_train=339.67.xls')
df_output

#%%
## no. of trains and corrected mass flow rate for upcoming tool
no_of_trains = proj_config_params_dict["train_nos_in_scenarios"][proj_config_params_dict["train_scenario"]][current_tool_name][sub_tool_category]
print("no_of_trains in NF1: ", no_of_trains)

#%%

## Mass and volume flow rates
## pressures and densities
density_perm_kgm3 = df_output.iloc[6, 19]
rhofeed_chain = float(df_output.iloc[7, 1])
Pfeed_BF = df_output.iloc[2, 19]  # for determining feed pressure of BPP of NF2
#Pretent = Pfeed - parameter_dict['retentate_head_loss']  # feed pressure of BPP of NF2
Pfeed_feed_pump = df_output.iloc[3, 19]  # for ERD

## per-train mass flow rates for upcoming tool
Qperm_m3s_per_train = float(df_output.iloc[1, 19])
Qretent_m3s_per_train = float(df_output.iloc[0, 19])
Mfeed_chain_kgs_per_train = float(df_output.iloc[8, 1])
recovery_nf1 = float(df_output.iloc[6, 19])

print(f"density_perm_kgm3: {density_perm_kgm3}, Qperm_m3s_per_train: {Qperm_m3s_per_train}, Qretent_m3s_per_train: {Qretent_m3s_per_train}, recovery_nf1: {recovery_nf1}")


#%%
## Total total mass flow rates for upcoming tool
# perm
Qperm_m3s_tot = Qperm_m3s_per_train *  no_of_trains
Qperm_m3h_tot = Qperm_m3s_tot*3600
Mperm_kgs_tot = Qperm_m3s_tot * density_perm_kgm3
# retent
Qretent_m3s_tot = Qretent_m3s_per_train *  no_of_trains
Qretent_m3h_tot = Qretent_m3s_tot*3600
# feed
Mfeed_chain_kgs_tot = Mfeed_chain_kgs_per_train *  no_of_trains

print(f"Qperm_m3s_tot: {Qperm_m3s_tot}, Qretent_m3s_tot: {Qretent_m3s_tot}")


#%%
## Flow concentrations
Cperm={}
Cperm['Na']=df_output.iloc[10, 21]
Cperm['Cl']=df_output.iloc[11, 21]
Cperm['Mg']=df_output.iloc[12, 21]
Cperm['Ca']=df_output.iloc[13, 21]
Cperm['SO4']=df_output.iloc[14, 21]
Cperm['CO3']=df_output.iloc[15, 21]

Cperm

#%%

Cretent = {}
Cretent['Na']=df_output.iloc[10, 20]
Cretent['Cl']=df_output.iloc[11, 20]
Cretent['Mg']=df_output.iloc[12, 20]
Cretent['Ca']=df_output.iloc[13, 20]
Cretent['SO4']=df_output.iloc[14, 20]
Cretent['CO3']=df_output.iloc[15, 20]

Cretent


#%%
capital_per_train=float(df_output.iloc[6, 24])
capex_per_train=float(df_output.iloc[6, 25])
opex_per_train=float(df_output.iloc[13, 25])
electric_power_req_per_train=float(df_output.iloc[20, 24])+float(df_output.iloc[21, 24])

print(capital_per_train, capex_per_train, opex_per_train, electric_power_req_per_train)


#%%
############ NF2 #####################
current_tool_name = 'nanofiltration'
sub_tool_category = 'NF2'
df_output = pd.read_excel('./results/nanofiltration/arabian_gulf_kuwait/average_tds/NF2/results_Rgiven_noCorrFactor_qfeedm3h_train=229.83_wo_erd.xls')
df_output

#%%

## no. of trains and corrected mass flow rate for upcoming tool
no_of_trains = proj_config_params_dict["train_nos_in_scenarios"][proj_config_params_dict["train_scenario"]][current_tool_name][sub_tool_category]
print("no_of_trains in NF2: ", no_of_trains)

#%%

## pressures and densities
density_perm_kgm3 = df_output.iloc[6, 19]
rhofeed_chain = float(df_output.iloc[7, 1])
Pfeed_NF2 = df_output.iloc[2, 19]  # for ERD
Pfeed_feed_pump = df_output.iloc[3, 19]  # for ERD

## per-train mass flow rates for upcoming tool
Qperm_m3s_per_train = df_output.iloc[1, 19]
Qretent_m3s_per_train = df_output.iloc[0, 19]
Mfeed_chain_kgs_per_train = float(df_output.iloc[8, 1])
recovery_nf2 = float(df_output.iloc[6, 19])

print(f"density_perm_kgm3: {density_perm_kgm3}, Qperm_m3s_per_train: {Qperm_m3s_per_train}, Qretent_m3s_per_train: {Qretent_m3s_per_train}, recovery_nf2: {recovery_nf2}")

#%%
## Total total mass flow rates for upcoming tool
# perm
Qperm_m3s_tot = Qperm_m3s_per_train *  no_of_trains
Qperm_m3h_tot = Qperm_m3s_tot*3600
Mperm_kgs_tot = Qperm_m3s_tot * density_perm_kgm3
# retent
Qretent_m3s_tot = Qretent_m3s_per_train *  no_of_trains
Qretent_m3h_tot = Qretent_m3s_tot*3600

print(f"Qperm_m3s_tot: {Qperm_m3s_tot}, Qretent_m3s_tot: {Qretent_m3s_tot}")


#%%

Cperm={}
Cperm['Na']=df_output.iloc[10, 21]
Cperm['Cl']=df_output.iloc[11, 21]
Cperm['Mg']=df_output.iloc[12, 21]
Cperm['Ca']=df_output.iloc[13, 21]
Cperm['SO4']=df_output.iloc[14, 21]
Cperm['CO3']=df_output.iloc[15, 21]

Cperm

#%%

Cretent = {}
Cretent['Na']=df_output.iloc[10, 20]
Cretent['Cl']=df_output.iloc[11, 20]
Cretent['Mg']=df_output.iloc[12, 20]
Cretent['Ca']=df_output.iloc[13, 20]
Cretent['SO4']=df_output.iloc[14, 20]
Cretent['CO3']=df_output.iloc[15, 20]

Cretent


#%%
## ERD

# allowable portion of Vfeed into ERD for pressure increase: calculated using ERD efficiency formula (ref: Sanz Miguel A.; Stover R. 2007) and expected outlet pressure for Vfeed_erd, based on assumed booster pump pressure delta
#Vfeed_erd = parameter_config['erd_type']['PX']['efficiency'] * (P_brine_out * Vbrine_out) / (stage.Pfeed[0] - parameter_config['booster_pump']['pressure_delta']) # [m3/h]
P_in_ERD_HP = Pfeed_NF2  # [bar]  Assuming NF2 retentate has same pressure as feed
V_in_ERD_HP_m3s = Qretent_m3s_NF2   # [m3/s]  NF2 retentate
energy_in_ERD = P_in_ERD_HP * V_in_ERD_HP_m3s
Vfeed_erd_m3s = parameter_config['erd_type']['PX']['efficiency'] * energy_in_ERD / Pfeed_NF # [m3/s]
print(Qfeed_NF_m3s, Vfeed_erd_m3s, P_in_ERD_HP)

# %%
