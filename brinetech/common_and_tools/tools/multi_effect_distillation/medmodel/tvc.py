from . import h2oprop, naclprops, pressure, tools
from .logger import logger
import numpy as np
from iapws import iapws95, iapws97, IAPWS97

'''functions to evaluate TVC parameters'''

def tvc_entrainment_ratio(pm, ts, tn, xb, config):  # Pm in [kPa], all the p are corrected from kPa to bar. T in [°C]
    ps = h2oprop.sat_pressure(ts)  # [kPa]
    p_ev = h2oprop.sat_pressure(tn-naclprops.bpe_brine_act(tn, xb)) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                      config['delta_w']) * config['thickness'] * 1E-3 * 1E-3   # vapor extracted from the last stage [kPa]
    pcf = 3 * 1e-7 * (pm*100)**2 - 0.0009 * (pm*100) + 1.6101
    tcf = 2 * 1e-8 * tn**2 - 0.0006*tn + 1.0047
    ra = 0.296 * ps**1.19 / p_ev**1.04 * (pm/p_ev)**0.015 * pcf / tcf
    return ra


def tvc_entrainment_ratio_hassan(pm, ts, tn, xb, config): # Hassan and Darwish correlation
    ps = h2oprop.sat_pressure(ts)  # [kPa]
    p_ev = h2oprop.sat_pressure(tn - naclprops.bpe_brine_act(tn, xb)) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
                                                                                                 config['delta_w']) * \
                                                                        config['thickness'] * 1E-3 * 1E-3  # vapor extracted from the last stage [kPa]
    Cr = ps / p_ev
    Er = (pm * 100) / p_ev
    if 2 <= Er <= 10:
        ra = - 1.61061763080868 + 11.0331387899116 * np.log(Cr) + 13.5281254171601/Er - 14.9338191429307 * (np.log(Cr))**2 \
              - 34.4397376531113/ Er**2 - 48.4767172051364 * np.log(Cr)/Er + 6.46223679313751 * (np.log(Cr))**3 + \
              29.9699902855834 / Er**3 + 70.8113406477665 * np.log(Cr) / Er**2 + 46.9590107717394 * (np.log(Cr)**2)/Er
    elif 10 <= Er <= 100:
        ra = -3.20842210618164 + 3.93335312452389 * Cr + 27.2360043794853 /Er - 1.19206948677452 * Cr**2 - 141.423288255019/Er**2\
             - 22.5455184193569 * Cr/Er + 0.125812687624122 * Cr**3 + 348.506574704109 / Er**3 + 41.7960967174647 * Cr/Er**2 \
             + 6.43992939366982 * Cr**2 / Er
    elif Er > 100:
        ra = -1.93422581403321 + 2.152523807931 * Cr + 113.490932154749/Er - 0.522221061154973 * Cr**2 - 14735.9653361836 / (Er**2) \
             - 31.8519701023059 * Cr/Er + 0.047506773195604 * Cr**3 + 900786.044551787 / Er**3 - 495.581541338594 * Cr / Er**2 \
             + 10.0251265889018 * (Cr**2) / Er
    return ra


def tvc_entrainment_ratio_gebel(pm, ts, tn, xb, config):
    tm = iapws97._TSat_P(pm * 0.1)
    h1 = iapws97._Region2(tm, pm * 0.1)['h']
    s1 = iapws97._Region2(tm, pm * 0.1)['s']
    pss = iapws97._PSat_T(tn + 273)
    cond2_prime = iapws95.IAPWS95_Ps(pss, s1)
    h2_prime = cond2_prime.h
    h0 = iapws97._Region2(tn + 273, pss)['h']
    ps = iapws97._PSat_T(ts + 273)
    h4_star = h0 + (ps - pss) / (pm * 0.1 - pss) * (h1 - h0)
    t4_star = iapws97._Backward2_T_Ph(ps, h4_star)
    s4_star = iapws97._Region2(t4_star, ps)['s']
    cond3_star = iapws95.IAPWS95_Ps(pss, s4_star)
    h3_star = cond3_star.h
    # print(h1, h2_prime, h4_star, h3_star)
    # tm = h2oprop.sat_temp(pm*100) + 273
    # print(tm)
    # cond1 = IAPWS97(P=pm*0.1, T=tm)
    # h1 = cond1.h0
    # s1 = cond1.s0
    # print(s1)
    # pss = (h2oprop.sat_pressure(tn - naclprops.bpe_brine_act(tn, xb)) - pressure.demister_deltap(config['rho_d'], config['v_vap'],
    #                     config['delta_w']) * config['thickness'] * 1E-3 * 1E-3) * 1e-3
    # cond2_prime = IAPWS97(P=pss, s=s1)
    # print(pss)
    # h2_prime = cond2_prime.h0
    # cond0 = IAPWS97(P=pss, T=(tn - naclprops.bpe_brine_act(tn, xb) + 273))
    # h0 = cond0.h0
    # ps = h2oprop.sat_pressure(ts) * 1e-3
    # print(ps)
    # h4_star = h0 + (ps - pss)/ (pm * 0.1 - pss) * (h1 - h0)
    # cond4_star = IAPWS97(P=ps, h=h4_star)
    # s4_star = cond4_star.s0
    # cond3_star = IAPWS97(P=pss, s=s4_star)
    # h3_star = cond3_star.h0
    # print(h1, h2_prime, h4_star, h3_star)
    w = np.sqrt(config['eta_nozzle'] * config['eta_diff'] * (h1 - h2_prime)/(h4_star-h3_star)) - 1
    ra = 1/w
    return ra

def tvc_steam(ra, m_motive):  # not used, as Ms is given as an input, so Mm is derived
    m_steam = m_motive * (1 + 1 / ra)
    m_ev = m_steam - m_motive
    return m_steam, m_ev

