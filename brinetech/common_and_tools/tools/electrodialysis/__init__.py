from .model_electrodialysis import electrodialysis_model as model_electrodialysis

__all__ = [
    "model_electrodialysis",
]
