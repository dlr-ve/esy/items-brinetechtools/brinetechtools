from .intake_functions import estimation_power_and_cost_intake_system, estimation_electric_cost_for_vol

from ...common import calculate_annual_depreciation_cost
from ...common import ResultList


#%%

def intake_model(tech_config_and_params, proj_config_and_params, N_trains, feed_vol_flow_per_train_m3h, electricity_cost):
    
    lst_tech_names = proj_config_and_params['lst_tech_names']
    print("lst_tech_names: ", lst_tech_names)
    
    # flow rate of Vfeed_out
    feed_vol_out_flow_per_train_m3h = feed_vol_flow_per_train_m3h * tech_config_and_params['intake_recovery_rate']
    feed_vol_out_flow_per_train_m3a = feed_vol_out_flow_per_train_m3h * 8760 * tech_config_and_params['plant_availability']  # [m3 of feed_out/year]
    feed_vol_out_capacity_per_train_m3d = feed_vol_out_flow_per_train_m3h * 24  # [capacity of m3_perm/day]

    # Pump power and capital cost
    power_intake_pump, capital_cost_intake_system = estimation_power_and_cost_intake_system(feed_vol_out_flow_per_train_m3h, tech_config_and_params, proj_config_and_params)
    # Annualization of costs (if not already annual)
    ## get the depreciation period based on open_ocean or subsurface
    depr_period = tech_config_and_params['depr_period'][proj_config_and_params["intake_structure_type"]]
    annualized_intake_system_cost = calculate_annual_depreciation_cost(proj_config_and_params["interest_rate"], depr_period, capital_cost_intake_system)
    
    # Energy consumption
    spec_elec_consumption = power_intake_pump/feed_vol_out_flow_per_train_m3h  # [kWh/m3 of feed_out]
    annual_electric_consumption = spec_elec_consumption * feed_vol_out_flow_per_train_m3a  # [kWh/a]
    
    ## OPEX
    # fixed (% of capital cost)
    fixed_opex_cost = capital_cost_intake_system * tech_config_and_params['perc_maint_CAPEX']  # [$/a]
    # variable (apart from annual chemical cost)
    annual_electric_cost = estimation_electric_cost_for_vol(spec_elec_consumption, feed_vol_flow_per_train_m3h, electricity_cost, tech_config_and_params)  # [$/a]
    # total
    annual_operating_cost = fixed_opex_cost + annual_electric_cost

    ## total (fixed + variable operating)
    annual_total_cost = annualized_intake_system_cost + annual_operating_cost  # [$/a]

    intake_inputs = ResultList(title="Intake system", xls_sheet="intake", results=[
        #('input and parameters', ('', '')),
        ('Number of trains', (N_trains, '-')),
        ('Qfeed per train', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('Recovery rate', (tech_config_and_params['intake_recovery_rate'], '-')),
        ('specific electricity consumption', (spec_elec_consumption, 'kWh/m3_feed')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ])

    intake_results = ResultList(title="Technical results per train", xls_sheet="intake", xls_y0=4, results=[
        ('Qfeed_out per train', (feed_vol_out_flow_per_train_m3h, 'm3/h')),
        ('power and electricity consumption', ('kW', 'kWh/m3_feed_out')),
        ('Intake pump', (power_intake_pump, spec_elec_consumption)),
        ('annual electricity consumption', (annual_electric_consumption, 'kWh/a')),
        ])
    
    intake_results_economic = ResultList(title="economic results per train", xls_sheet="intake", xls_y0=8, results=[
        ('capex', ('$/y', '$', '$/m3_feed_out_capacity/day')),
        ('Intake system', (annualized_intake_system_cost, capital_cost_intake_system, capital_cost_intake_system/feed_vol_out_capacity_per_train_m3d)),
        ('fixed opex', ('$/y', '' , '$/m3_feed_out_capacity/day')),
        ('fixed', (fixed_opex_cost, '', fixed_opex_cost/feed_vol_out_capacity_per_train_m3d)),
        ('variable opex', ('$/y', '', '$/m3_feed_out')),
        ('electric energy', (annual_electric_cost, '', annual_electric_cost/feed_vol_out_flow_per_train_m3a)),
        ('annual opex (fixed+variable) cost', (annual_operating_cost, '', annual_operating_cost/feed_vol_out_flow_per_train_m3a)),
        ('levelized or total annual cost (capex+opex)', (annual_total_cost, '',  annual_total_cost/feed_vol_out_flow_per_train_m3a)),
        ])

    return [intake_inputs, intake_results, intake_results_economic]
