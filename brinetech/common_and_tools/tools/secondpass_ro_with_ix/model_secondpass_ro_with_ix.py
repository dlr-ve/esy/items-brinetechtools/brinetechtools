import os
import yaml

import numpy as np
from colorama import Fore, Style

from .secondpass_ro_with_ix_functions import economic_function

from ...common import calculate_annual_depreciation_cost
from ...common import ResultList

def validate_feed_concentration(feed_ion_conc_ppm_tot, tech_config_and_params):
    """
    Validates that the total feed ion concentration for secondpass_ro_ix is within a 5% (or given) error margin
    of the required concentration.

    Parameters:
        feed_ion_conc_ppm_tot (float): Total ion concentration in ppm.
        tech_config_and_params (dict): A dictionary containing the reference feed concentration under the key 'ref_feed_conc_ppm',
                                      as well as %-factor of allowable variation under the key 'allowable_feed_upper_ppm_variation'.

    Raises:
        ValueError: If the concentrations are not within a 5% (or given) error margin.
    """
    feed_ion_required_conc_ppm_tot = tech_config_and_params['ref_feed_conc_ppm']

    # Calculate the acceptable margin of error
    allowed_upper_margin = tech_config_and_params['allowable_feed_upper_ppm_variation'] * feed_ion_required_conc_ppm_tot

    # Check if the concentration difference is within the allowed margin
    if (feed_ion_conc_ppm_tot - feed_ion_required_conc_ppm_tot) > allowed_upper_margin:
        raise ValueError(f"Error: The feed ion concentration {feed_ion_conc_ppm_tot} ppm is not within the \
                         allowed {tech_config_and_params['allowable_feed_upper_ppm_variation']*100}% upper limit of the required concentration {feed_ion_required_conc_ppm_tot} ppm.")
    else:
        print(f"Validation successful: Feed ion concentration is within the allowed {tech_config_and_params['allowable_feed_upper_ppm_variation']*100}% upper error margin.")


def secondpass_ro_with_ix_model(tech_config_and_params, proj_config_and_params, N_trains, feed_vol_flow_per_train_m3h, feed_ion_conc_ppm_tot, electricity_cost):

    # feed ppm validation (since OARO costs and energy consumption are based on a specific OARO from literature)
    
    #feed_ion_required_conc_ppm_tot = tech_config_and_params['ref_feed_conc_ppm']
    #allowed_upper_margin = tech_config_and_params['allowable_feed_upper_ppm_variation'] * feed_ion_required_conc_ppm_tot
    #feed_ion_upper_limit_conc_ppm_tot = feed_ion_required_conc_ppm_tot + allowed_upper_margin

    validate_feed_concentration(feed_ion_conc_ppm_tot, tech_config_and_params)

    # read techno-economic params from dict
    Pfeed = tech_config_and_params['feed_pressure']  # [bar]
    recovery_rate_perm = tech_config_and_params['recovery_rate']  # [%-factor]
    spec_elec_consumption_perm = tech_config_and_params['spec_elec_consumption_perm']  # [kWh_el/m3 of perm] It is assumed that it considers the savings through the use of ERD in it
    spec_capital_cost_perm = tech_config_and_params['spec_capital_cost'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']] # [USD_current/(m3_capacity/day)]
    spec_fixed_opex_perm = tech_config_and_params['spec_fixed_opex'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']]  # [USD_current/year/(m3_capacity/day)]
    
    ## output flow rates
    # perm
    Qperm_m3h = feed_vol_flow_per_train_m3h * recovery_rate_perm
    Qperm_m3_per_annum = Qperm_m3h * 8760 * tech_config_and_params['plant_availability']
    Qperm_m3day_capacity = Qperm_m3h * 24  # [capacity of m3_perm/day]
    # retentate
    Qretent_m3h = feed_vol_flow_per_train_m3h * (1 - recovery_rate_perm)
    Qretent_m3_per_annum = Qretent_m3h * 8760 * tech_config_and_params['plant_availability']
    
    ## Xretent [ppm]
    # mass flow rate of salt in feed and perm
    Msalt_in_feed_kgh = (feed_vol_flow_per_train_m3h * 1000 * feed_ion_conc_ppm_tot) / 10**6  # ppm = mg/L
    perm_ion_conc_ppm_tot = tech_config_and_params['expectec_perm_conc_ppm']
    Msalt_in_perm_kgh = (Qperm_m3h * 1000 * perm_ion_conc_ppm_tot) / 10**6  # ppm = mg/L
    Msalt_in_retent_kgh = Msalt_in_feed_kgh - Msalt_in_perm_kgh
    retent_ion_conc_ppm_tot = (Msalt_in_retent_kgh / Qretent_m3h) * 1000  # 1 kg/m3 = 1000 mg/L = 1000 ppm

    # power
    power_total = spec_elec_consumption_perm * Qperm_m3h  # [kW_el] Total plant power
    # elec consumption
    annual_elec_consumption = spec_elec_consumption_perm * Qperm_m3_per_annum  # [kWh_el/a]

    # capital expenditures (USD)
    total_capital_cost = spec_capital_cost_perm * Qperm_m3day_capacity  # [USD_current]
    
    ## annualization of capital cost (USD_current/y)
    annualized_capital_cost = calculate_annual_depreciation_cost(proj_config_and_params["interest_rate"], tech_config_and_params["depr_period"], total_capital_cost)
    
    # annual fixed operating cost (USD_current/y)
    annual_fixed_opex = spec_fixed_opex_perm * Qperm_m3day_capacity  # [USD_current/y]

    # total annual cost
    total_annual_fixed_cost = annualized_capital_cost + annual_fixed_opex

    #levelized_fixed_cost_per_m3_dist = total_annual_fixed_cost / Qperm_m3_per_annum
    levelized_fixed_cost_per_m3_perm = total_annual_fixed_cost / Qperm_m3_per_annum

    #### variable operating costs (assumed to consist only of energy)
    # electricity
    annual_electric_cost = economic_function.estimation_electric_cost_from_m3h(spec_elec_consumption_perm, Qperm_m3h, electricity_cost, tech_config_and_params)
    annual_variable_operating_cost = annual_electric_cost

    #levelized_operating_cost_per_m3_dist = annual_electric_cost / Qperm_m3_per_annum
    levelized_var_operating_cost_per_m3_perm = annual_variable_operating_cost / Qperm_m3_per_annum
    
    ## total (fixed + variable operating)
    annual_total_cost = total_annual_fixed_cost + annual_variable_operating_cost
    levelized_cost_per_m3_perm = levelized_fixed_cost_per_m3_perm + levelized_var_operating_cost_per_m3_perm
    
    Qperm_m3_per_annum

    result_inputs = ResultList(title="Second_pass RO + IX inputs", xls_sheet="secondpass_ro_ix", results=[
        ('Number of trains', (N_trains, '-')),
        ('Feed vol. flow rate (per train)', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('Feed pressure', (Pfeed, 'bar')),
        ('Feed conc. (all ions)', (feed_ion_conc_ppm_tot, 'ppm')),
        #('Allowable upper limit of feed conc. (total)', (feed_ion_upper_limit_conc_ppm_tot, 'ppm')),
        ('Perm expected conc. (all ions)', (perm_ion_conc_ppm_tot, 'ppm')),
        ('Recovery rate of perm', (recovery_rate_perm, '-')),
        ('Spec. capital cost', (spec_capital_cost_perm, 'USD_current/(m3_perm_capacity/day)')),
        ('Spec. fixed opex', (spec_fixed_opex_perm, 'USD_current/year/(m3_perm_capacity/day)')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ('Plant availability', (tech_config_and_params['plant_availability'], '-')),
        ])

    result_technical = ResultList(title="Technical results per train", xls_sheet="secondpass_ro_ix", xls_y0=5, results=[
        ('Flow rates', ('m3/h', 'm3/a')),
        ('Permeate', (Qperm_m3h, Qperm_m3_per_annum)),
        ('Retentate', (Qretent_m3h, Qretent_m3_per_annum)),
        ('Retentate conc. (total)', (retent_ion_conc_ppm_tot, 'ppm')),
        ('Power and electricity consumption', ('kWh_el','kWh/m3_perm', 'kWh_el/y')),
        ('Total', (power_total, spec_elec_consumption_perm, annual_elec_consumption))
        ])
    
    result_eco = ResultList(title="Economic results per train", xls_sheet="secondpass_ro_ix", xls_y0=10, results=[
        ('capex', ('$/y', '$', '$/m3_perm_capacity/day')),
        ('Total capital', (annualized_capital_cost, total_capital_cost, total_capital_cost/Qperm_m3day_capacity)),
        ('opex', ('$/y', '$/m3_perm', '$/a/m3_perm_capacity/day')),
        ('fixed', (annual_fixed_opex, annual_fixed_opex/Qperm_m3_per_annum, annual_fixed_opex/Qperm_m3day_capacity)),
        ('variable', ('$/y', '$/m3_perm')),
        ('electric energy', (annual_electric_cost, annual_electric_cost/Qperm_m3_per_annum)),
        ])

    result_tot_cost = ResultList(title="Total costs per train", xls_sheet="secondpass_ro_ix", xls_y0=16, results=[
        ('total annual fixed cost', (total_annual_fixed_cost, '$/y')),
        ('levelized fixed cost (per m3 perm)', (levelized_fixed_cost_per_m3_perm, '$/m3_perm')),
        ('annual variable operating cost', (annual_variable_operating_cost, '$/y')),
        ('levelized variable operating cost (per m3 perm)', (levelized_var_operating_cost_per_m3_perm, '$/m3_perm')),
        ('total annual cost', (annual_total_cost, '$/y')),
        ('total levelized cost (per m3 perm)', (levelized_cost_per_m3_perm, '$/m3_perm')),
        ('Vol. of UPW annual', (Qperm_m3_per_annum, 'm3/y'))
        ])

    return [result_inputs, result_technical, result_eco, result_tot_cost]