
#import gsw
from scipy.interpolate import interp1d

#df_nacl_densities = pd.read_excel(r"../../../inputs/nacl_densities_perry.xlsx", index_col=0)
#df_nacl_densities = df_nacl_densities.T


def get_density_nacl_solution(df_nacl_densities, temp_deg_c, concentration_perc_wt):
    """Calculate the density of NaCl solution based on concentration and temperature using tabulated data.

    This function uses interpolation to estimate the density of NaCl solutions at specified concentrations and temperatures.
    The density values are based on the tabular data from Perry's Chemical Engineers' Handbook.

    Reference: Green, Don W.; Southard, Marylee Z. (2019): Perry's Chemical Engineers' Handbook, 
    9th Edition. 9th edition. 1 online resource (2352 pages) 650 illustrations. New York, N.Y: McGraw-Hill Education. 
    Available online at https://www.accessengineeringlibrary.com/content/book/9780071834087.

    Args:
        df_nacl_densities (pd.DataFrame): DataFrame containing the density values of NaCl solutions with 
                                           concentrations as columns and temperatures (with '_C' suffix) as index.
        temp_deg_c (int): Temperature in degrees Celsius at which the density is to be calculated. 
                            Must match one of the temperature indices in the DataFrame.
        concentration_perc_wt (float): Concentration of NaCl in weight percent (%). 
                                        Must be within the concentration range of the DataFrame.

    Raises:
        ValueError: If the specified temperature is not found in the DataFrame.
        ValueError: If the specified concentration is outside the range of available concentrations in the DataFrame.

    Returns:
        float: The interpolated density of the NaCl solution in kg/m³.
    """
    
    # Format the temp_deg_c to match the DataFrame index
    temperature_key = f"{temp_deg_c}_C"
    
    # Check if the formatted temp_deg_c is valid
    if temperature_key not in df_nacl_densities.index:
        raise ValueError(f"temp_deg_c {temp_deg_c}°C is not in the data.")

    # Check if the concentration_perc_wt is valid
    if concentration_perc_wt < df_nacl_densities.columns.min() or concentration_perc_wt > df_nacl_densities.columns.max():
        raise ValueError(f"concentration_perc_wt {concentration_perc_wt}% is outside the data range.")

    # Get densities for the given temp_deg_c
    densities = df_nacl_densities.loc[temperature_key].values  # Retrieve densities for the exact temp_deg_c

    # Create interpolation function based on available concentrations
    interpolation_func = interp1d(df_nacl_densities.columns.astype(float), densities, kind='linear', fill_value='extrapolate')

    # Interpolate the density value for the given concentration_perc_wt, and convert from kg/L to kg/m³
    density_kgm3 = interpolation_func(concentration_perc_wt) * 1000
    
    return density_kgm3



def density_seawater_liquid(t, C):   # rho_seawater in [kg/m3], C in [ppm] and T in [°C]
    """Calculate the density of seawater given its temp_deg_c and concentration_perc_wt (TODO: of what?)"""

    # TODO: Limits of t and C for which this function is valid.
    # TODO: Find sources for the function (no citations found in the documents)

    b = (2 * C / 1000 - 150) / 150
    g1 = 0.5
    g2 = b
    g3 = 2 * b**2 - 1
    a1 = 4.032219 * g1 + 0.115313 * g2 + 3.26 * 1e-4 * g3
    a2 = -0.108199 * g1 + 1.571 * 1e-3 * g2 - 4.23 * 1e-4 * g3
    a3 = -0.012247 * g1 + 1.74 * 1e-3 * g2 - 9 * 1e-6 * g3
    a4 = 6.92 * 1e-4 * g1 - 8.7 * 1e-5 * g2 - 5.3 * 1e-5 * g3
    a = (2 * t - 200)/160
    f1 = 0.5
    f2 = a
    f3 = 2 * a**2 - 1
    f4 = 4 * a**3 - 3 * a
    rho_seawater = 1e3 * (a1 * f1 + a2 * f2 + a3 * f3 + a4 * f4)

    return rho_seawater


#temp_deg_c = 25
#absolute_salinity_ppm = 74000
#absolute_salinity_kg_per_kg = absolute_salinity_ppm * 0.001
#sea_pressure = 10.1325 - 10.1325 # [dbar] Assuming 0m 
#print(density_seawater_liquid(temp_deg_c, absolute_salinity_ppm))
#print(gsw.density.rho(SA=absolute_salinity_kg_per_kg, CT=temp_deg_c, p=sea_pressure))


def density_mixtures(param_config, conc_dict, ignore_ions=()):
    """Returns the density of the mixture given by 'conc_dict' in [kg/m³].

    Uses a Sum (Example for LaTeX in the Documentation) $\\sum_{i=0}^N\\frac{f_i \\cdot MW_i}{Mw}$

    Parameters:
    - **param_config**: dict containing the molar volumes and weights as well as alpha of ions and water.
                         Molar volumes as well as alpha should be in [cm³/mol], molar weights in [g/mol]
    - **conc_dict**: dict containing the concentrations of ions in solution in [mol/m³]
    - **ignore_ions**: list of ions that are ignored (treated as water) for the purposes of density calculation
    """
    # TODO: Clarify what 'alpha' is in this context, provide references for the calculation
    # TODO: Clarify for what temp_deg_c the density is calculated

    # Filer out unwanted ions, don't consider them in the subsequent calculation
    ions = [ion for ion in conc_dict.keys() if ion not in ignore_ions]
    # Convert the input concentration_perc_wt from mol/m³ to mol/l, only considering relevant ions
    conc_molarity = {ion: conc_dict[ion] / 1000 for ion in ions}  # unit: mol/l

    # In the following section, we calculate how many moles of water are in one kilogram of mixture.
    # We need this to calculate the fraction that each ion has as part of the total number of molecules.

    # First, sum up the volume fractions of all ions in the mixture
    l_ions_lsol = sum(conc_molarity[ion] * param_config['v_molar_'+ion] / 1000 for ion in ions)  # unitless
    assert l_ions_lsol < 1, f"Fractional ion volumes add up to more than one or is NaN! ({l_ions_lsol})"
    # ...the remaining volume fraction is filled by water
    l_H2o_lsol = 1 - l_ions_lsol
    # ...and convert that volume fraction back to the number of molecules of water [in mol] per litre.
    # TODO: (Ebke): Change this conversion to v_molar_water, given that this is
    molarity_H2o = l_H2o_lsol * 1000 / param_config['MW_water']  # unit: mol/kg

    # Now we calculate the total number of molecules per kg of mixture (all types)
    mol_tot_lsol = molarity_H2o + sum(conc_molarity.values())  # unit: mol/kg OR mol/l (TODO: Fix!)
    # ...which enables us to calculate the molecular fraction per ion.
    mole_fraction = {ion: conc_molarity[ion] / mol_tot_lsol for ion in ions}  # unitless
    mole_fraction_ions = sum(mole_fraction[ion] for ion in ions)  # fraction of non-water molecules
    mole_fraction_water = 1 - mole_fraction_ions  # fraction of water molecules

    # Calculate mass of one mol of mixture
    ion_fractional_weight = sum(mole_fraction[ion] * param_config['MW_'+ion] for ion in ions)  # unit: g/mol
    mixture_mole_weight = ion_fractional_weight + param_config['MW_water'] * mole_fraction_water  # unit: g/mol

    # Calculate the volume of one mol of mixture
    ion_fractional_volume = sum(mole_fraction[ion] * param_config['v_molar_'+ion] for ion in ions)  # unit: cm³/mol
    ion_fractional_alpha = sum(mole_fraction[ion] * param_config['alpha_'+ion] for ion in ions)  # unit: cm³/mol
    water_fractional_volume = param_config['v_molar_water'] * mole_fraction_water
    mixture_mole_volume = ion_fractional_volume + water_fractional_volume + ion_fractional_alpha * mole_fraction_water

    # Finally, divide weight by volume and rescale from [g/cm³] to [kg/m³]
    rho_mixture = mixture_mole_weight / mixture_mole_volume * 1000

    return rho_mixture
