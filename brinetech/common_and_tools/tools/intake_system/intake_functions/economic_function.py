import numpy as np

def estimation_power_and_cost_intake_system(Vfeed_m3h, tech_config_and_params, proj_config_and_params):

    ## Capital Cost
    # Construction cost varies between 90-120 USD_2018/m3_perm/day (avg. 105) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    # Assuming 50% recovery rate for the reference SWRO cost, their spec. cost should be divided by 2 to get USD_2018/m3_feed/day
    Vfeed_m3day = Vfeed_m3h * 24
    if Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["upper_limit"] / 2 * Vfeed_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["average"] / 2 * Vfeed_m3day
    elif Vfeed_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_intake_system = tech_config_and_params["intake_spec_capital_cost"]["lower_limit"] / 2 * Vfeed_m3day



    ## consists of three components: 1] intake structure; 2] pumping station; 3] auxilliaries. The capital cost equations are based on the graphs from Nikolay Project estimation book (2018)   
    
    """
    # Set default intake type/subtype based on seawater, unless explicitly specified by the user
    intake_structure_type = proj_config_and_params.get("intake_structure_type", tech_config_and_params["intake_type_subtype_default"][proj_config_and_params["sea"]]["type"])
    intake_structure_subtype = proj_config_and_params.get("intake_structure_subtype", tech_config_and_params["intake_type_subtype_default"][proj_config_and_params["sea"]]["subtype"])    
    print("intake_structure_type, intake_structure_subtype: ",intake_structure_type, intake_structure_subtype)
    
    ### 1] intake structure 
    if intake_structure_subtype == "offshore":
        intake_material_if_offshore = proj_config_and_params["intake_material_if_offshore"]
        offshore_intake_length_m = tech_config_and_params["offshore_intake_length_km"][proj_config_and_params["sea"]] * 1000
        if intake_material_if_offshore == "HDPE":
            capital_cost_intake_structure = ((-1e-21*Vperm_m3day**4) + (1e-15*Vperm_m3day**3) - (5e-10*Vperm_m3day**2) + (0.0002*Vperm_m3day) + 0.3415) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        elif intake_material_if_offshore == "concrete_tunnel":
            capital_cost_intake_structure = ((-4e-21*Vperm_m3day**4) + (4e-15*Vperm_m3day**3) - (2e-9*Vperm_m3day**2) + (0.0005*v) + 1.8519) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif intake_structure_subtype == "onshore":
        capital_cost_intake_structure = ((4e-14*Vperm_m3day**3) - (4e-8*Vperm_m3day**2) + (0.0519*Vperm_m3day) + 125.31) * 1000 * offshore_intake_length_m * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    
    ### 2] pumping station
    intake_pumping_type = proj_config_and_params["intake_pumping_type"]
    ## capital cost
    if intake_pumping_type == "dry":
        capital_cost_intake_pumping_station = ((1e-14*Vperm_m3day**3) - (1e-8*Vperm_m3day**2) + (0.0169*Vperm_m3day) + 83.825) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif intake_pumping_type == "wet":
        capital_cost_intake_pumping_station = ((5e-15*Vperm_m3day**3) - (5e-9*Vperm_m3day**2) + (0.0098*Vperm_m3day) + 32.067) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    print("capital_cost_intake_pumping_station", capital_cost_intake_pumping_station)
    
    ### 3] auxilliaries
    # It's capital cost is assumed to be 1.7 times that of 1] + 2], which approximately gives their sum to lie between 90 and 120 USD_2018/m3/day, which is total intake cost (source: Nikolay (2018))
    capital_cost_intake_auxilliaries = 1.7 * (capital_cost_intake_structure + capital_cost_intake_pumping_station)

   
    # total capital cost
    capital_cost_intake_system = capital_cost_intake_structure + capital_cost_intake_pumping_station + capital_cost_intake_auxilliaries
    """

    # power 
    power_intake_pump = tech_config_and_params["intake_pump_pressure"] * Vfeed_m3h / tech_config_and_params['eff_intake_pump'] * 1e5 * 1e-3 / 3600  # kW


    return power_intake_pump, capital_cost_intake_system * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']


def estimation_electric_cost_for_vol(spec_el_consumption, Vfeed_m3h, electricity_cost, tech_config_and_params):  #Vfeed_m3h in m3/h, spec_el_consumption in kWh/m3
    operating_hours = 8760 * tech_config_and_params['plant_availability']
    electric_consumption = spec_el_consumption * Vfeed_m3h * operating_hours # [kWh/y]
    annual_el_cost = electric_consumption * electricity_cost # [US$/y]
    return annual_el_cost


def estimation_HPpump_cost_single(Vfeed, Pfeed_1, tech_config_and_params, proj_config_and_params):
    if Vfeed == 450:
        pump_cost = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * Pfeed_1
    elif 200 < Vfeed < 450:
        pump_cost = tech_config_and_params['coeffA_catB_HPP'] * np.power((Pfeed_1 * Vfeed), tech_config_and_params['coeffB_catB_HPP'])
    else:  # Vfeed < 200
        pump_cost = tech_config_and_params['coeffA_catC_HPP'] * Pfeed_1 * Vfeed
    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']

def estimation_HPpump_cost_parallpumps(Vfeed, Pfeed_1, tech_config_and_params, proj_config_and_params):
    Vfeed_1 = 450
    pump_cost_1 = tech_config_and_params['coeffA_catA_HPP'] + tech_config_and_params['coeffB_catA_HPP'] * Pfeed_1
    Vfeed_2 = Vfeed - Vfeed_1
    if Vfeed_2 <= 450:
        pump_cost_2 = estimation_HPpump_cost_single(Vfeed_2, Pfeed_1, tech_config_and_params, proj_config_and_params)
    else:
        pump_cost_2 = estimation_HPpump_cost_parallpumps(Vfeed_2, Pfeed_1, tech_config_and_params, proj_config_and_params)
    pump_cost = pump_cost_1 + pump_cost_2
    return pump_cost * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']


def estimation_LPpump_cost(power, Pfeed, tech_config_and_params, proj_config_and_params):
    Cp_0 = np.power(10, (tech_config_and_params['k1_LPpump'] + tech_config_and_params['k2_LPpump'] * np.log10(power) + tech_config_and_params['k3_LPpump'] * (np.log10(power)) ** 2))
    Cp_0 = Cp_0 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['1996']
    if Pfeed < 10:
        Fp = 1
    elif 10 <= Pfeed <= 100:
        Fp = np.power(10, (tech_config_and_params['c1_LPpump'] + tech_config_and_params['c2_LPpump'] * np.log10(Pfeed-1) + tech_config_and_params['c3_LPpump'] * (np.log10(Pfeed-1))**2))
    else:
        Fp = 1
    Cbm = Cp_0 * (tech_config_and_params['b1_LPpump'] + tech_config_and_params['b2_LPpump'] * tech_config_and_params['Fm_LPpump'] * Fp)
    return Cbm
