import numpy as np

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_mixtures
from ...common.iotools import ResultList


def crystallizer_model(parameter_config, Cfeed, Qfeed, electricity_cost):  # Qfeed in m3/h
    Qfeed_Mg_mol = Qfeed * Cfeed['Mg']  # mol/h
    rho_feed = density_mixtures(parameter_config, Cfeed)
    Cfeed_ppm = {}
    for ion in Cfeed.keys():
        Cfeed_ppm[ion] = Cfeed[ion] / rho_feed * parameter_config['MW_'+ion] * 1000

    '''first step: Mg(OH)2 precipitation with dolime'''
    Q_Ox_req_mol = Qfeed_Mg_mol * parameter_config['stoichiometric_coeff'] * 2  # mol/h
    # Each mole of hydrated dolime (Ca(OH)2.Mg(OH)2) contains 4 moles of oxygen, 1 mole of Mg and 1 of Ca
    Q_dolime_mol = Q_Ox_req_mol / 4  # mol/h
    C_M_dolime_sol = parameter_config['conc_dolime_sol_wt'] / parameter_config['MW_dolime'] / (
        parameter_config['conc_dolime_sol_wt'] / parameter_config['density_dolime'] +
        (100 - parameter_config['conc_dolime_sol_wt']) / 1000)
    C_dolime_sol_dict = {}  # mol/m3
    C_dolime_sol_dict['Mg'] = C_M_dolime_sol * 1 * 1000
    C_dolime_sol_dict['Ca'] = C_M_dolime_sol * 1 * 1000
    rho_dolime_sol = density_mixtures(parameter_config, C_dolime_sol_dict)
    Q_dolime_vol = Q_dolime_mol / C_M_dolime_sol / 1000  # m3/h
    M_dolime_kg_h = Q_dolime_vol * rho_dolime_sol  # kg/h
    Q_Mg_dolime_mol = Q_dolime_mol * 1  # mol/h
    C_dolime_ppm = {}
    C_dolime_ppm['Mg'] = C_dolime_sol_dict['Mg'] / rho_dolime_sol * parameter_config['MW_Mg'] * 1000
    C_dolime_ppm['Ca'] = C_dolime_sol_dict['Ca'] / rho_dolime_sol * parameter_config['MW_Ca'] * 1000
    print(C_dolime_ppm)
    Q_Mg_tot_mol = Qfeed_Mg_mol + Q_Mg_dolime_mol
    Q_MgHydro_out_mol = Q_Mg_tot_mol
    Q_MgHydro_out_vol = Q_MgHydro_out_mol * parameter_config['MW_MgOH2'] / parameter_config['MgOH2_density']
    Q_MgHydro_out_mass = Q_MgHydro_out_vol * parameter_config['MgOH2_density'] / 1000  # [kg/h]
    Mfeed_kg_h = Qfeed * rho_feed
    Meffluent_Mg_rec_kg_h = Mfeed_kg_h + M_dolime_kg_h - Q_MgHydro_out_mass
    C_effluent_Mg_rec_ppm = {}
    for ion in Cfeed.keys():
        if ion != 'Mg' or ion != 'Ca':
            C_effluent_Mg_rec_ppm[ion] = (Mfeed_kg_h * Cfeed_ppm[ion]) / Meffluent_Mg_rec_kg_h
    C_effluent_Mg_rec_ppm['Ca'] = (Mfeed_kg_h * Cfeed_ppm['Ca'] + M_dolime_kg_h * C_dolime_ppm['Ca']) \
        / Meffluent_Mg_rec_kg_h
    C_effluent_Mg_rec_ppm['Mg'] = 0
    print(C_effluent_Mg_rec_ppm)
    C_effluent_Mg_rec_mol = {}  # mol/m3
    rho_effluent_Mg_rec = (rho_feed * Mfeed_kg_h + rho_dolime_sol * M_dolime_kg_h) / (Mfeed_kg_h + M_dolime_kg_h)
    for ion in Cfeed.keys():
        C_effluent_Mg_rec_mol[ion] = C_effluent_Mg_rec_ppm[ion] * rho_effluent_Mg_rec / 1000 \
            / parameter_config['MW_'+ion]
    Q_effluent_Mg_rec = Meffluent_Mg_rec_kg_h / rho_effluent_Mg_rec  # [m3/h]
    # Q_Ca_effluent_Mg_rec_mol = Q_effluent_Mg_rec * C_effluent_Mg_rec_mol['Ca']  # [mol/h]
    # Q_SO4_effluent_Mg_rec_mol = Q_effluent_Mg_rec * C_effluent_Mg_rec_mol['SO4']  # [mol/h]

    '''2nd step: CaCO3 precipitation with NaCO3 solution'''
    Q_Ca_effluent_out_1st_mol = Q_effluent_Mg_rec * C_effluent_Mg_rec_mol['Ca']  # [mol/h]
    Q_CO3_req_mol = Q_Ca_effluent_out_1st_mol * 1  # [mol/h]
    Q_Na2CO3_sol_req_mol = Q_CO3_req_mol * 1  # [mol/h]
    Q_Na2CO3_sol_req_mass = Q_Na2CO3_sol_req_mol / parameter_config['conc_Na2CO3_sol'] / 1000 * \
        parameter_config['Na2CO3_sol_density']  # [kg/h]
    Q_Na2CO3_sol_req_vol = Q_Na2CO3_sol_req_mass / parameter_config['Na2CO3_sol_density']
    Q_CaCO3_prod_mol = Q_Ca_effluent_out_1st_mol * 1
    Q_CaCO3_prod_mass = Q_CaCO3_prod_mol * parameter_config['MW_CaCO3'] / 1000  # [kg/h]
    M_effluent_out = Meffluent_Mg_rec_kg_h + Q_Na2CO3_sol_req_mass - Q_CaCO3_prod_mass
    C_effluent_out_ppm = {}
    C_effluent_out_M = {}
    for ion in Cfeed.keys():
        if ion == 'Ca':
            C_effluent_out_ppm[ion] = (Meffluent_Mg_rec_kg_h * C_effluent_Mg_rec_ppm[ion] - Q_CaCO3_prod_mol
                                       * parameter_config['MW_' + ion] * 1000) / M_effluent_out
            if C_effluent_out_ppm[ion] < 0:
                C_effluent_out_ppm[ion] = 0
        elif ion == 'Na':
            C_effluent_out_ppm[ion] = (Meffluent_Mg_rec_kg_h * C_effluent_Mg_rec_ppm[ion]
                                       + Q_Na2CO3_sol_req_mol * 2 * parameter_config[
                                           'MW_' + ion] * 1000) / M_effluent_out
        else:
            C_effluent_out_ppm[ion] = (Meffluent_Mg_rec_kg_h * C_effluent_Mg_rec_ppm[ion]) / M_effluent_out
    for ion in Cfeed.keys():
        C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / parameter_config['MW_' + ion]
    Q_effluent_out = M_effluent_out / density_mixtures(parameter_config, C_effluent_out_M)
    Qeffl_out_calc = 0
    while abs(Q_effluent_out - Qeffl_out_calc) > 1e-4:
        Q_effluent_out = Qeffl_out_calc
        for ion in Cfeed.keys():
            C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / 1000 / parameter_config['MW_' + ion] * \
                                             density_mixtures(parameter_config, C_effluent_out_M)
        Qeffl_out_calc = M_effluent_out / density_mixtures(parameter_config, C_effluent_out_M)

    '''1st step costs'''
    spec_cost_dolime_cryst_mol = parameter_config['specific_cost_dolime_cryst'] \
        * 1e-6 * parameter_config['MW_dolime']  # [$/mol]
    spec_cost_water_l = parameter_config['specific_cost_water'] / 1000  # [$/l]
    specific_cost_dolime_sol = (spec_cost_dolime_cryst_mol * C_M_dolime_sol + spec_cost_water_l *
                                (1 - C_M_dolime_sol * parameter_config['MW_dolime'] /
                                 parameter_config['density_dolime'])) * 1000  # [$/m3]
    print('dolime sol', specific_cost_dolime_sol)
    cost_dolime_sol = specific_cost_dolime_sol * Q_dolime_vol * parameter_config['annual_hours'] \
        * parameter_config['plant_availability']
    revenue_MgHydro_cryst = parameter_config['price_MgHydro_cryst'] / 1e6 * parameter_config['MgOH2_density'] * \
        Q_MgHydro_out_vol * parameter_config['annual_hours'] * parameter_config['plant_availability']
    total_flow_rate_1 = Qfeed + Q_dolime_vol
    velocity_1 = parameter_config['fluid_vel'] * 3600
    section_area_1 = total_flow_rate_1 / velocity_1
    cryst_volume_1 = section_area_1 * parameter_config['reactor_length']
    Cp_0_cryst_Mg = 10**(
        parameter_config['k1_cryst_batch']
        + parameter_config['k2_cryst_batch'] * np.log10(cryst_volume_1)
        + parameter_config['k3_cryst_batch'] * np.log10(cryst_volume_1)**2)
    Cp_0_cryst_current_Mg = Cp_0_cryst_Mg * parameter_config['CEPCI_current'] / parameter_config['CEPCI_reference']
    Cbm_cryst_Mg = Cp_0_cryst_current_Mg * (parameter_config['b1_cryst_batch'] + parameter_config['b2_cryst_batch'] *
                                            parameter_config['Fm_cryst_batch'] * parameter_config['Fp_cryst_batch'])
    A_filter_Mg = parameter_config['design_ratio_filter'] * Q_MgHydro_out_vol * parameter_config['MgOH2_density'] / 1000
    A_filter = A_filter_Mg
    Cp_0_filter_Mg = 10**(parameter_config['k1_disc-drum_filter'] + parameter_config['k2_disc-drum_filter'] *
                          np.log10(A_filter) + parameter_config['k3_disc-drum_filter'] * np.log10(A_filter)**2)
    Cp_0_filter_current_Mg = Cp_0_filter_Mg * parameter_config['CEPCI_current'] / parameter_config['CEPCI_reference']
    Cbm_filter_Mg = Cp_0_filter_current_Mg * parameter_config['Fbm_disc-drum_filter']

    '''2nd (CaCO3) step costs'''
    total_flow_rate_2 = Q_effluent_Mg_rec
    velocity_2 = parameter_config['fluid_vel'] * 3600
    section_area_2 = total_flow_rate_2 / velocity_2
    cryst_volume_2 = section_area_2 * parameter_config['reactor_length']
    Cp_0_cryst_CaCO3 = 10 ** (parameter_config['k1_cryst_batch']
                              + parameter_config['k2_cryst_batch'] * np.log10(cryst_volume_2)
                              + parameter_config['k3_cryst_batch'] * np.log10(cryst_volume_2) ** 2)
    Cp_0_cryst_current_CaCO3 = Cp_0_cryst_CaCO3 * parameter_config['CEPCI_current'] / parameter_config[
                    'CEPCI_reference']
    Cbm_cryst_CaCO3 = Cp_0_cryst_current_CaCO3 * (
        parameter_config['b1_cryst_batch']
        + parameter_config['b2_cryst_batch'] * parameter_config['Fm_cryst_batch'] * parameter_config['Fp_cryst_batch']
    )
    spec_cost_Na2CO3_cryst_mol = parameter_config['specific_cost_Na2CO3_cryst'] * 1e-6 * parameter_config[
                    'MW_Na2CO3']  # [$/mol]
    spec_cost_water_l = parameter_config['specific_cost_water'] / 1000  # [$/l]
    specific_cost_Na2CO3_sol = (spec_cost_Na2CO3_cryst_mol * parameter_config['conc_Na2CO3_sol'] + spec_cost_water_l *
                                (1 - parameter_config['conc_Na2CO3_sol'] * parameter_config['MW_Na2CO3'] /
                                 (parameter_config['Na2CO3_density'] / 1000))) * 1000  # [$/m3]
    print('Na2CO3 sol', specific_cost_Na2CO3_sol)
    cost_Na2CO3_sol = specific_cost_Na2CO3_sol * Q_Na2CO3_sol_req_vol \
        * parameter_config['annual_hours'] * parameter_config['plant_availability']
    revenue_CaCO3_cryst = parameter_config['price_CaCO3_cryst'] / 1e3 * Q_CaCO3_prod_mass \
        * parameter_config['annual_hours'] * parameter_config['plant_availability']

    '''total costs'''
    Cbm_tot_cryst = Cbm_cryst_Mg + Cbm_cryst_CaCO3
    Cbm_tot_filter = Cbm_filter_Mg * 2

    total_annual_capex = calculate_annual_depreciation_cost(parameter_config['interest_rate'],
                                           parameter_config['depreciation_time_cryst'],
                                           (Cbm_tot_cryst + Cbm_tot_filter))

    pump_power_cryst = parameter_config['Pscarico_pump_cryst'] * 1e5 * (total_flow_rate_1 + total_flow_rate_2)\
        / 3600 / 0.8 * 1e-3  # [kW]
    flowrate_sedim_Mg = Q_MgHydro_out_mass / parameter_config['magma_density_sedim_Mg']  # [m3/h]
    power_Mg_filtration = parameter_config['ref_consumption_filter'] \
        / parameter_config['ref_flowrate_filter'] * flowrate_sedim_Mg
    power_filtration_tot = power_Mg_filtration * 2
    total_power = pump_power_cryst + power_filtration_tot
    cost_electric_energy_demand = total_power * electricity_cost * 8760 * parameter_config['plant_availability']
    total_opex = cost_dolime_sol + cost_Na2CO3_sol + cost_electric_energy_demand   # [$/y]
    total_revenue = revenue_MgHydro_cryst + revenue_CaCO3_cryst

    crystallizer_inputs = ResultList(title="crystallizer inputs", xls_sheet="crystallizer", results=[
        ('Qfeed', (Qfeed, 'm3/h')),
        ('rho feed', (rho_feed, 'kg/m3')),
        ('Mfeed', (Mfeed_kg_h, 'kg/h')),
        ('conc dolime sol', (C_M_dolime_sol, 'mol/l')),
        ('rho dolime sol', (rho_dolime_sol, 'kg/m3')),
        ('conc Na2CO3 sol', (parameter_config['conc_Na2CO3_sol'], 'mol/l')),
        ('rho Na2CO3 sol', (parameter_config['Na2CO3_sol_density'], 'kg/m3')),
        ('ion', ('C [mol/m3', 'C [ppm]')),
    ] + [(key, (Cfeed[key], Cfeed_ppm[key])) for key in Cfeed.keys()])

    crystallizer_results = ResultList(title="crystallizer results", xls_sheet="crystallizer", xls_y0=4, results=[
         ('Q Mg inlet mol', (Qfeed_Mg_mol, 'mol/h')),
         ('Q dolime tot mol', (Q_dolime_mol, 'mol/h')),
         ('Q dolime mass', (M_dolime_kg_h, 'kg/h')),
         ('Q Mg(OH)2 tot vol', (Q_MgHydro_out_vol, 'm3/h')),
         ('Q Mg(OH)2 mass', (Q_MgHydro_out_mass, 'kg/h')),
         ('Q effluent Mg rec', (Q_effluent_Mg_rec, 'm3/h')),
         ('M effluent Mg rec', (Meffluent_Mg_rec_kg_h, 'kg/h')),
         ('Q Ca effluent', (Q_Ca_effluent_out_1st_mol, 'mol/h')),
         ('Q Na2CO3 mol', (Q_Na2CO3_sol_req_mol, 'mol/h')),
         ('Q Na2CO3 mass', (Q_Na2CO3_sol_req_mass, 'kg/h')),
         ('Q CaCO3 tot mass', (Q_CaCO3_prod_mass, 'kg/h')),
         ('M effluent out', (M_effluent_out, 'kg/h')),
         ('rho effluent out', (density_mixtures(parameter_config, C_effluent_out_M), 'kg/m3')),
         ('Q effluent out', (Q_effluent_out, 'm3/h')),
         ('ion', (
             'C ppm out 1',
             'C mol/m3 out 1',
             '1',
             'C ppm out 2',
             'C mol/m3 out 2')),
    ] + [
         (key, (
             C_effluent_Mg_rec_ppm[key],
             C_effluent_Mg_rec_mol[key],
             '',
             C_effluent_out_ppm[key],
             C_effluent_out_M[key]
         )) for key in Cfeed.keys()
    ])

    cost_results = ResultList(title="cost results", xls_sheet="crystallizer", xls_y0=12, results=[
        ('dolime sol cost',      (cost_dolime_sol, '$/y')),
        ('Na2CO3 sol cost',      (cost_Na2CO3_sol, '$/y')),
        ('electric cons cryst',  (pump_power_cryst, 'kW')),
        ('electric cons filter', (power_filtration_tot, 'kW')),
        ('cost electricity',     (cost_electric_energy_demand, '$/y')),
        ('cost crystallizer',    (Cbm_tot_cryst, '$')),
        ('cost filter',          (Cbm_tot_filter, '$')),
        ('annual capex',         (total_annual_capex, '$/y')),
        ('annual opex',          (total_opex, '$/y')),
        ('revenue Mg(OH, 2)',    (revenue_MgHydro_cryst, '$/y')),
        ('', ()),
        ('revenue CaCO3',        (revenue_CaCO3_cryst,  '$/y')),
        ('total revenue',        (total_revenue,  '$/y')),
    ])

    return [crystallizer_inputs, crystallizer_results, cost_results]
