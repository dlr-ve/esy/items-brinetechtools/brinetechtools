from argparse import ArgumentParser
from sys import argv
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
#from brinetech.common.iotools import save_resultlists
from common.iotools import save_resultlists
from .crystallizer_model import crystallizer_model
from .crystallizer_model_NaOH import crystallizer_model as crystallizer_model_NaOH
from .crystallizer_model_coalmine_cacarbo import crystallizer_model as crystallizer_model_coalmine_cacarbo
from .crystallizer_model_coalmine_sulphate import crystallizer_model as crystallizer_model_coalmine_sulphate


def driver(model, parameterconfigfilepath, Cfeed_filepath, Qfeed, electricity_cost,
           output_directory, output_namefile, output_filename):
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)
    with open(Cfeed_filepath, "r") as file_conc:
        Cfeed = yaml.load(file_conc, Loader=Loader)

    result_lists = model(parameter_config, Cfeed, Qfeed, electricity_cost)

    for result_list in result_lists:
        print(result_list)

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
        'basic': crystallizer_model,
        'sulphate': crystallizer_model_coalmine_sulphate,
        'cacarbo': crystallizer_model_coalmine_cacarbo,
        'NaOH': crystallizer_model_NaOH,
    }
    parser = ArgumentParser(prog='brinetech_crystallizer', description='Precipitation simulation tool')
    parser.add_argument('-m', '--model', choices=models.keys())
    parser.add_argument('-p', '--parameter-file', default='./parameters.yml', help="Parameter file as YAML")
    parser.add_argument('-c', '--concentration-file', required=True, help="Concentrations of the main input feed")
    parser.add_argument('-q', '--feed-q', type=float, required=True, help="Feed flux Q in m³/h")
    parser.add_argument('-e', '--electricity-cost', type=float, required=True, help="Cost of electricity in $/kWh")
    parser.add_argument('-o', '--output-directory', required=True, help="Output directory for results")
    parser.add_argument('-n', '--output-namefile', required=True, help="File to write the output filename to")

    args = parser.parse_args(argv)

    # Note (JE): I've changed the output filenames to use feed_q for consistency.
    output_filename = {
        'basic': 'crystallizer_results.xls',
        'NaOH': f"precipitation_NaOH_MgHydro_results_feed_{round(args.feed_q, 3)}_m3h.xls",
        'sulphate': f"crystallizer_CaSulphate_MgHydro_results_feed_{round(args.feed_q, 3)}.xls",
        'cacarbo': f"crystallizer_dol_Na2CO3_results_feed_{round(args.feed_q, 3)}.xls",
    }

    return driver(
        model=models[args.model],
        parameterconfigfilepath=args.parameter_file,
        Cfeed_filepath=args.concentration_file,
        Qfeed=args.feed_q,
        electricity_cost=args.electricity_cost,
        output_directory=args.output_directory,
        output_namefile=args.output_namefile,
        output_filename=output_filename[args.model],
    )


def entry_point():
    return run(argv[1:])


if __name__ == "__main__":
    entry_point()
