from . import h2oprop, naclprops
from ....common.properties import density_seawater_liquid
import numpy as np
import logging

logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)

'''function to evaluate the overall heat transfer coefficient of a fouled evaporator and of a fouled condenser (El Dessouki 1998)'''

def u_evaporator_old(t):  # Uev in [kW/(m2 °C)] and T in [°C] (temperature of the effect as it is the evaporation temperature?)
    u_ev = 1e-3 * (1939.4 + 1.40562 * t - 0.0207525 * t**2 + 0.0023186 * t**3)
    return u_ev


def u_evaporator(t):
    u_ev = 1.9695 + 1.2057 * 1e-2 * t - 8.5989 * 1e-5 * t**2 + 2.5651 * 1e-7 * t**3
    # u_ev = u_ev / 10
    return u_ev


def u_condenser_Takada(t):  # Ucond in [kW/(m2 °C)] and T in [°C]
    u_cond = 0.8 * (3 + 0.05 * (t - 60))
    return u_cond


def u_condenser(t):  # Ucond in [kW/(m2 °C)] and T in [°C]
    u_cond = 1.7194 + 3.2063 * 1e-3 * t + 1.5971 * 1e-5 * t**2 - 1.9918 * 1e-7 * t**3
    return u_cond


def h_condensation_tubes(t, tc_previous, config): # in kW/(m2 K)
    h_tubes = 0.726 * np.power((h2oprop.latent_heat(tc_previous) * h2oprop.density_satwat_liquid(tc_previous)**2
                                * (h2oprop.thermal_conductivity_liquid(tc_previous)* 1e-3)**3 * 9.81
                       / ((tc_previous - t) * h2oprop.dynviscosity_satwat_liquid(tc_previous) * (config['d_evap'] - 2 * config['evap_tube_thickness']))), 1/4)
    # h_in = 1.59 * np.power((h2oprop.thermal_conductivity_vapor(t)**3 * h2oprop.density_satwat_vapor(t)**2 * 9.81
    #                         / h2oprop.dynviscosity_satwat_vapor(t)**2), (1/3)) *\
    #        np.power((4 * (mvap/(2 * config['L_evap']))/h2oprop.dynviscosity_satwat_vapor(t)), (-1/3))
    # h_out = 1.59 * np.power((h2oprop.thermal_conductivity_liquid(t) ** 3 * h2oprop.density_satwat_liquid(t) ** 2 * 9.81
    #                         / h2oprop.dynviscosity_satwat_liquid(t) ** 2), (1 / 3)) * \
    #        np.power((4 * (mvap / (2 * config['L_evap'])) / h2oprop.dynviscosity_satwat_liquid(t)), (-1 / 3))
    # h_tubes = (h_in + h_out)/2
    return h_tubes


def h_boiling_seawater(t, x, m_evapfeed, config, n_tubes): # in kW/(m2 K)
    h_boiling = (4/3)**(1/3) * (naclprops.conductivity_seawater(t, x) * 1e-3) * np.power((density_seawater_liquid(t, x)**2 * 9.81
                                   / naclprops.dynamic_viscosity_seawater(t, x)**2), (1/3)) \
                * np.power((4 * m_evapfeed / (2 * config['L_evap'] * n_tubes) / naclprops.dynamic_viscosity_seawater(t, x)), (-1/3))
    # h_boiling = 0.0147 * np.power((naclprops.conductivity_seawater(t, x)**3 * density_seawater_liquid(t, x)**2 * 9.81
    #                                / naclprops.dynamic_viscosity_seawater(t, x)**2), (1/3)) \
    #             * np.power((4 * m_evapfeed / (config['L_evap']) / naclprops.dynamic_viscosity_seawater(t, x)), (1/3)) \
    #             * np.power(config['D_evap'], (-1/3))
    return h_boiling


def U_overall(t, tc_previous, x, m_evapfeed, config, n_tubes):
    A_out = 4 * np.pi * config['d_evap'] * config['L_evap']
    A_in = 4 * np.pi * (config['d_evap'] - 2 * config['evap_tube_thickness']) * config['L_evap']
    A_av = 4 * np.pi * (config['d_evap'] + (config['d_evap'] - 2 * config['evap_tube_thickness'])) / 2 * config['L_evap']
    inv_U = (1/h_boiling_seawater(t, x, m_evapfeed, config, n_tubes) + 1/(config['h_fouling_outside'] *1e-3) +
             (1/h_condensation_tubes(t, tc_previous, config) + 1/(config['h_fouling_inside'] * 1e-3)) * A_out/A_in
             + config['evap_tube_thickness'] * A_out/(config['material_cond'][config['evap_tube_material']] * 1e-3 * A_av))
    U_tot = 1/inv_U
    return U_tot
