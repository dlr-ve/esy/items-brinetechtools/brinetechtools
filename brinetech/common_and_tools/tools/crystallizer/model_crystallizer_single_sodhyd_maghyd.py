import numpy as np

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_mixtures, density_seawater_liquid
from ...common import calculate_pump_efficiency
from ...common.iotools import ResultList
from .crystallization_process import crystallizer_start

def crystallizer_model(tech_config_and_params, proj_config_and_params, Cfeed_dict_molm3, N_trains, feed_vol_flow_per_train_m3h, electricity_cost):  # feed_vol_flow_per_train_m3h in m3/h
    feed_Mg_mol_flow_molh, feed_Ca_mol_flow_molh, rho_feed, Cfeed_dict_ppm, C_Na_NaOHsol_ppm, feed_NaOH_tot_mol_flow_molh = crystallizer_start(
        tech_config_and_params, Cfeed_dict_molm3, feed_vol_flow_per_train_m3h, use_NaOH_Mg_mol_as_tot_mol=True
    )

    # Pure NaOH mass flow rate
    feed_NaOH_tot_mol_flow_kgh = feed_NaOH_tot_mol_flow_molh * tech_config_and_params['MW_NaOH'] / 1000 # [kg of NaOH/h]

    # calculate volume of NaOH solution from moles of pure NaOH
    feed_NaOH_sol_tot_vol_flow_m3h = feed_NaOH_tot_mol_flow_molh / tech_config_and_params['NaOH_sol']['conc'] / 1000
    feed_NaOH_sol_tot_vol_flow_m3_annual = feed_NaOH_sol_tot_vol_flow_m3h * 8760 * tech_config_and_params['plant_availability']
    feed_water_in_NaOH_sol_tot_vol_flow_m3h = feed_NaOH_sol_tot_vol_flow_m3h  # [m3/h] assumed the same, as it is only a 1mol/L solution

    Q_MgHydro_out_mol = feed_Mg_mol_flow_molh
    Q_MgHydro_out_vol_m3h = Q_MgHydro_out_mol * tech_config_and_params['MW_MgOH2'] / tech_config_and_params['MgOH2_density_gm3']
    M_MgHydro_out_kgh = Q_MgHydro_out_vol_m3h * tech_config_and_params['MgOH2_density_gm3'] / 1000  # [kg/h]
    M_MgHydro_out_kgd_capacity = M_MgHydro_out_kgh * 24  # [kg_capacity/day]
    M_MgHydro_out_kg_annual = M_MgHydro_out_kgh * 8760 * tech_config_and_params['plant_availability']
    
    # Q_CaHydro_out_mol = feed_Ca_mol_flow_molh  # Nikhil
    # Q_CaHydro_out_vol = Q_CaHydro_out_mol * tech_config_and_params['MW_CaOH2'] / tech_config_and_params['CaOH2_density']  # Nikhil
    # Q_CaHydro_out_mass = Q_CaHydro_out_vol * tech_config_and_params['CaOH2_density'] / 1000  # [kg/h]  ## Nikhil
    feed_mass_flow_per_train_kgh = feed_vol_flow_per_train_m3h * rho_feed
    M_NaOH_kgh = feed_NaOH_sol_tot_vol_flow_m3h * tech_config_and_params['NaOH_sol']['density']
    # Meffluent_kgh = feed_mass_flow_per_train_kgh + M_NaOH_kgh - M_MgHydro_out_kgh - Q_CaHydro_out_mass  # Nikhil
    Meffluent_kgh = feed_mass_flow_per_train_kgh + M_NaOH_kgh - M_MgHydro_out_kgh  # Nikhil
    C_Na_effluent_ppm = (feed_mass_flow_per_train_kgh * Cfeed_dict_ppm['Na'] + M_NaOH_kgh * C_Na_NaOHsol_ppm) / Meffluent_kgh
    # TODO: The following variable was unused.
    # C_Cl_effluent_ppm = feed_mass_flow_per_train_kgh * Cfeed_dict_ppm['Cl'] / Meffluent_kgh
    C_Na_effluent_mol_kg = C_Na_effluent_ppm / 1000 / tech_config_and_params['MW_Na']
    C_NaCl_effluent_mol_kg = C_Na_effluent_mol_kg * 1
    C_NaCl_effluent_ppm = C_NaCl_effluent_mol_kg * 1000 * (tech_config_and_params['MW_Na'] + tech_config_and_params['MW_Cl'])
    rho_effluent = density_seawater_liquid(tech_config_and_params['T'] - 273, C_NaCl_effluent_ppm)
    Q_effluent_m3h = Meffluent_kgh / rho_effluent  # [m3/h]
    C_NaCl_effluent_molm3 = C_NaCl_effluent_ppm * rho_effluent / 1000 \
        / (tech_config_and_params['MW_Na'] + tech_config_and_params['MW_Cl'])

    # new added by Nikhil to get ion conc. of the whole effluent, not just Na and Cl
    ###########################
    C_effluent_out_ppm = {}
    C_effluent_out_M = {}
    # In below for loop, Nikhil replaced Meffluent_Mg_rec_kg_h with feed_mass_flow_per_train_kgh and C_effluent_Mg_rec_ppm with Cfeed_dict_ppm
    for ion in Cfeed_dict_molm3.keys():
        if ion == 'Mg':
            C_effluent_out_ppm[ion] = (feed_mass_flow_per_train_kgh * Cfeed_dict_ppm[ion] - Q_MgHydro_out_mol
                                       * tech_config_and_params['MW_' + ion] * 1000) / Meffluent_kgh
            if C_effluent_out_ppm[ion] < 0:
                C_effluent_out_ppm[ion] = 0
        elif ion == 'Na':
            C_effluent_out_ppm[ion] = (feed_mass_flow_per_train_kgh * Cfeed_dict_ppm[ion]
                                       + M_NaOH_kgh * C_Na_NaOHsol_ppm) / Meffluent_kgh
        else:
            C_effluent_out_ppm[ion] = (feed_mass_flow_per_train_kgh * Cfeed_dict_ppm[ion]) / Meffluent_kgh
    for ion in Cfeed_dict_molm3.keys():
        C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / tech_config_and_params['MW_' + ion]  # what is this? unit?
    # Is unit of C_effluent_out_M = mol/m3 to be used in density function?
    Q_effluent_m3h = Meffluent_kgh / density_mixtures(tech_config_and_params, C_effluent_out_M)
    Qeffl_calc = 0
    # What is happening in the while function?
    while abs(Q_effluent_m3h - Qeffl_calc) > 1e-4:
        Q_effluent_m3h = Qeffl_calc
        for ion in Cfeed_dict_molm3.keys():
            C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / 1000 / tech_config_and_params['MW_' + ion] * \
                                    density_mixtures(tech_config_and_params, C_effluent_out_M)
        Qeffl_calc = Meffluent_kgh / density_mixtures(tech_config_and_params, C_effluent_out_M)
    ###########################

    # Q_effluent_m3h = feed_vol_flow_per_train_m3h + feed_NaOH_sol_tot_vol_flow_m3h - Q_CaHydro_out_vol - Q_MgHydro_out_vol_m3h
    # Q_effluent_m3h = Meffluent_kgh / 1000  # supposed density = 1000 kg/m3
    # C_Na_effluent = (feed_vol_flow_per_train_m3h * Cfeed_dict_molm3['Na'] + feed_NaOH_tot_mol_flow_molh) / Q_effluent_m3h
    # C_Cl_effluent = feed_vol_flow_per_train_m3h * Cfeed_dict_molm3['Cl'] / Q_effluent_m3h
    # C_OH_effluent = (feed_NaOH_tot_mol_flow_molh - 2 * (Q_CaHydro_out_mol + Q_MgHydro_out_mol)) / Q_effluent_m3h  ## Nikhil
    # C_OH_effluent = (feed_NaOH_tot_mol_flow_molh - 2 * Q_MgHydro_out_mol) / Q_effluent_m3h  ## Nikhil

    spec_cost_NaOH_cryst_mol = proj_config_and_params['spec_cost_commodity']['NaOH'] * 1e-6 * tech_config_and_params['MW_NaOH']  # [$/mol of pure NaOH]
    spec_cost_water_l = proj_config_and_params['spec_cost_commodity']['water'] / 1000  # [$/l of pure water]
    specific_cost_NaOH_sol = (spec_cost_NaOH_cryst_mol * tech_config_and_params['NaOH_sol']['conc'] + spec_cost_water_l *
                              (1 - tech_config_and_params['NaOH_sol']['conc'] * tech_config_and_params['MW_NaOH'] /
                               (tech_config_and_params['NaOH_density_gm3'] / 1000))) * 1000  # [$/m3 of NaOH sol]
    print('specific_cost_NaOH_sol [$/m3]', specific_cost_NaOH_sol)
    print('feed_NaOH_sol_tot_vol_flow_m3h [m3/h]', feed_NaOH_sol_tot_vol_flow_m3h)
    cost_NaOH_sol_annual = specific_cost_NaOH_sol * feed_NaOH_sol_tot_vol_flow_m3_annual
    revenue_MgHydro_cryst = proj_config_and_params['spec_price_commodity']['MgHydro'] * M_MgHydro_out_kg_annual / 1000
    # Nikhil
    # revenue_CaHydro_cryst = tech_config_and_params['price_CaHydro_cryst'] / 1e6 * tech_config_and_params['CaOH2_density'] * \
    #                        Q_CaHydro_out_vol * 8760 * tech_config_and_params[    ## Nikhil
    #                            'plant_availability']   ## Nikhil

    total_flow_rate_m3h = feed_vol_flow_per_train_m3h + feed_NaOH_sol_tot_vol_flow_m3h
    velocity = tech_config_and_params['fluid_vel'] * 3600
    section_area = total_flow_rate_m3h / velocity
    cryst_volume = section_area * tech_config_and_params['reactor_length']
    Cp_0_cryst = 10**(tech_config_and_params['k1_cryst_batch'] + tech_config_and_params['k2_cryst_batch'] * np.log10(cryst_volume)
                      + tech_config_and_params['k3_cryst_batch'] * np.log10(cryst_volume)**2)
    Cp_0_cryst_current = Cp_0_cryst * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']]
    Cbm_cryst = Cp_0_cryst_current * (tech_config_and_params['b1_cryst_batch'] + tech_config_and_params['b2_cryst_batch'] *
                                      tech_config_and_params['Fm_cryst_batch'] * tech_config_and_params['Fp_cryst_batch'])
    # Nikhil: Is it because we need 1 filter for Mg and Ca each? Then in this case we need only 1
    # Cbm_tot_cryst = Cbm_cryst * 2
    Cbm_tot_cryst = Cbm_cryst
    Cbm_tot_cryst_annualized = calculate_annual_depreciation_cost(
        proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'], Cbm_tot_cryst
    )
    A_filter_Mg = tech_config_and_params['design_ratio_filter'] * Q_MgHydro_out_vol_m3h * tech_config_and_params['MgOH2_density_gm3'] / 1000
    # A_filter_Ca = tech_config_and_params['design_ratio_filter'] * Q_CaHydro_out_vol * tech_config_and_params['CaOH2_density'] \
    #    / 1000  ## Nikhil
    # A_filter = max([A_filter_Ca, A_filter_Mg])  ## Nikhil
    A_filter = A_filter_Mg  # Nikhil
    Cp_0_filter = 10**(tech_config_and_params['k1_disc-drum_filter'] + tech_config_and_params['k2_disc-drum_filter'] *
                       np.log10(A_filter) + tech_config_and_params['k3_disc-drum_filter'] * np.log10(A_filter)**2)
    Cp_0_filter_current = Cp_0_filter * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']]
    Cbm_filter = Cp_0_filter_current * tech_config_and_params['Fbm_disc-drum_filter']
    # Nikhil: Is it because we need 1 filter for Mg and Ca each? Then in this case we need only 1
    # Cbm_tot_filter = Cbm_filter * 2
    Cbm_tot_filter = Cbm_filter
    Cbm_tot_filter_annualized = calculate_annual_depreciation_cost(
        proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'], Cbm_tot_filter
    )
    total_capital_cost = Cbm_tot_cryst + Cbm_tot_filter  # [USD_current]
    total_annual_capex = calculate_annual_depreciation_cost(
        proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'], total_capital_cost
    )
    total_fixed_opex = total_capital_cost * tech_config_and_params['OM_perc_factor']  # [USD_current/yr]

    pump_efficiency = calculate_pump_efficiency(total_flow_rate_m3h * 24)
    power_pump_cryst_kw = tech_config_and_params['Pscarico_pump_cryst'] * 1e5 * total_flow_rate_m3h / 3600 / pump_efficiency * 1e-3  # [kW]
    flowrate_sedim_Mg = M_MgHydro_out_kgh / tech_config_and_params['magma_density_sedim_Mg']  # [m3/h]
    # flowrate_sedim_Ca = Q_CaHydro_out_mass / tech_config_and_params['magma_density_sedim_Ca']  # [m3/h]  ## Nikhil
    power_Mg_filtration = tech_config_and_params['ref_consumption_filter'] / tech_config_and_params['ref_flowrate_filter'] \
        * flowrate_sedim_Mg
    # power_Ca_filtration = tech_config_and_params['ref_consumption_filter'] / tech_config_and_params['ref_flowrate_filter'] \
    #    * flowrate_sedim_Ca  ## Nikhil
    # power_filtration_tot_kw = power_Mg_filtration + power_Ca_filtration ## Nikhil
    power_filtration_tot_kw = power_Mg_filtration  # Nikhil
    total_power_kw = power_pump_cryst_kw + power_filtration_tot_kw
    
    # electricity consumption
    elec_pump_cryst_kwh_annual = power_pump_cryst_kw * 8760 * tech_config_and_params['plant_availability']
    elec_filtration_tot_kwh_annual = power_filtration_tot_kw * 8760 * tech_config_and_params['plant_availability']
    tot_elec_consumption_kwh_annual = elec_pump_cryst_kwh_annual + elec_filtration_tot_kwh_annual
    cost_electric_energy_demand = tot_elec_consumption_kwh_annual * electricity_cost
    
    total_var_opex = cost_NaOH_sol_annual + cost_electric_energy_demand   # [$/y]
    total_annual_cost = total_annual_capex + total_fixed_opex + total_var_opex   # [$/y]

    ## levelized costs
    levelized_cost_fixed = (total_annual_capex + total_fixed_opex) / (M_MgHydro_out_kg_annual / 1000)   # [$/ton of Mg(OH)2]
    levelized_cost_variable = total_var_opex / (M_MgHydro_out_kg_annual / 1000)   # [$/ton of Mg(OH)2]
    levelized_cost_tot = levelized_cost_fixed + levelized_cost_variable
    print("Levelized costs (fixed, variable, total): ", levelized_cost_fixed, levelized_cost_variable, levelized_cost_tot)

    crystallizer_inputs = ResultList(title="precipitation inputs", xls_sheet="Mg precipitation", results=[
        ('No. of trains', (N_trains, '-')),
        ('Feed volume flow rate per train', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('Recocvery rate of permeate', (0, 'm3/h')),
        ('rho feed', (rho_feed, 'kg/m3')),
        ('Feed mass flow rate per train', (feed_mass_flow_per_train_kgh, 'kg/h')),
        ('conc NaOH sol', (tech_config_and_params['NaOH_sol']['conc'], 'mol/l')),
        ('rho NaOH sol', (tech_config_and_params['NaOH_sol']['density'], 'kg/m3')),
        ('ion', ('C [mol/m3', 'C [ppm]')),
    ] + [(key, (Cfeed_dict_molm3[key], Cfeed_dict_ppm[key])) for key in Cfeed_dict_molm3.keys()])

    tech_results = ResultList(title="Technical results", xls_sheet="Mg precipitation", xls_y0=4, results=[
         ('Q Mg inlet mol', (feed_Mg_mol_flow_molh, 'mol/h')),
         ('', ()),
         #('M NaOH crystals tot', (, 'kg/h')),
         ('Pure NaOH moles', (feed_NaOH_tot_mol_flow_molh, 'mol/h')),
         ('Pure NaOH mass', (feed_NaOH_tot_mol_flow_kgh, 'kg/h')),
         ('Q NaOH solution tot vol', (feed_NaOH_sol_tot_vol_flow_m3h, 'm3/h')),
         ('Q water in NaOH solution tot vol', (feed_water_in_NaOH_sol_tot_vol_flow_m3h, 'm3/h')),
         ('Pure Mg(OH)2 vol', (Q_MgHydro_out_vol_m3h, 'm3/h')),
         ('Pure Mg(OH)2 mass', (M_MgHydro_out_kgh, 'kg/h')),
         ('Q effluent', (Q_effluent_m3h, 'm3/h')),
         ('M effluent', (Meffluent_kgh, 'kg/h')),
         ('C NaCl effluent', (C_NaCl_effluent_molm3, 'mol/m3')),
         ('C NaCl ppm', (C_NaCl_effluent_ppm, 'ppm')),
         ('Power and electricity consumption', ('kW', 'kWh_el/a', 'kWh_el/kg_MgOH2')),
         ('Cryst', (power_pump_cryst_kw, elec_pump_cryst_kwh_annual, elec_pump_cryst_kwh_annual / M_MgHydro_out_kg_annual)),
         ('Filter', (power_filtration_tot_kw, elec_filtration_tot_kwh_annual, elec_filtration_tot_kwh_annual / M_MgHydro_out_kg_annual)),
         ('Total', (total_power_kw, tot_elec_consumption_kwh_annual, elec_filtration_tot_kwh_annual / M_MgHydro_out_kg_annual)),
    ])

    eco_results = ResultList(title="Economic results", xls_sheet="Mg precipitation", xls_x0=0, xls_y0=9, results=[
         ('Capex', ('$/y', '$', '$/MgOH2_kg_capacity/day')),
         ('Crystallizer', (Cbm_tot_cryst_annualized, Cbm_tot_cryst, Cbm_tot_cryst / M_MgHydro_out_kgd_capacity)),
         ('Filter', (Cbm_tot_filter_annualized, Cbm_tot_filter, Cbm_tot_filter / M_MgHydro_out_kgd_capacity)),
         ('Total', (total_annual_capex, total_capital_cost, total_capital_cost / M_MgHydro_out_kgd_capacity)),
         ('Opex', ('$/y', '$/kg_MgOH2', '$/y/MgOH2_kg_capacity/day')),
         ('Fixed', (total_fixed_opex, '', total_fixed_opex / M_MgHydro_out_kgd_capacity)),
         ('Variable', ('')),
         ('NaOH solution', (cost_NaOH_sol_annual, cost_NaOH_sol_annual / M_MgHydro_out_kg_annual)),
         ('Electricity', (cost_electric_energy_demand, cost_electric_energy_demand / M_MgHydro_out_kg_annual)),
         ('Total variable', (total_var_opex, total_var_opex / M_MgHydro_out_kg_annual)),
         ('Total annual cost (capex + opex (fixed and variable))', (total_annual_cost)),
         ('Levelized cost (fixed)', (levelized_cost_fixed, '$/ton_MgOH2')),
         ('Levelized cost (variable)', (levelized_cost_variable, '$/ton_MgOH2')),
         ('Levelized cost (total)', (levelized_cost_tot, '$/ton_MgOH2')),
         ('Potential revenue Mg(OH)2', (revenue_MgHydro_cryst, '$/y')),
    ])

    ion_list = ResultList(xls_sheet="Mg precipitation", xls_x0=22, xls_y0=4, results=[
        ('ion', ('C out [ppm]', 'C out [mol/m3]')),
    ] + [(key, (C_effluent_out_ppm[key], C_effluent_out_M[key])) for key in Cfeed_dict_molm3.keys()])

    #return [crystallizer_inputs, tech_results, eco_results, ion_list]
    return [crystallizer_inputs, tech_results, eco_results]
