import numpy as np
from scipy.optimize import minimize
from .NFmodel import membrane_profiles, single_stage, economic_function, solution_prop
from .NFmodel.logger import logger
from colorama import Fore, Style

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_mixtures
from ...common import calculate_pump_efficiency
from ...common.iotools import ResultList


# Refactored from NFmodel/solution_prop -- there these ions were ignored
# TODO: Figure out why these ions should be ignored, and note it here!
IGNORE_IONS_IN_DENSITY = ["COD", "dye"]

def get_power_requirement_memb_system(config, Qfeed): # Qfeed in m3/s
    power_req_memb_sys = config['cons_membrane_system'] * Qfeed * 3600 / config['energy_efficiency']* 1e-3  # kW

    return power_req_memb_sys

def get_power_req_pump(Qfeed_effective, Pfeed_effective): # Qfeed in m3/s, Pfeed in bar

    # pump power requirement: reduce the requirement, based on: 
    # i) Pfeed_effective: the availability of a higher-pressure Pfeed_pump (in case of NF2, where it takes in the NF1 retentate, which almost has the same pressure as NF1 feed)
    # ii) Qfeed_effective: reduced feed flow rate to NF1 HPP, based on use of ERD after NF2

    pump_efficiency = calculate_pump_efficiency(Qfeed_effective*3600*24)
    power_req_pump_HPP = (Pfeed_effective) * 1e5 * Qfeed_effective / pump_efficiency * 1e-3  # kW

    return power_req_pump_HPP

    
def nanofiltration_model(tech_config_and_params, proj_config_and_params, Cfeed_plant_unordered, Pfeed, Pfeed_pump, recovery_req, N_trains, with_pretreatment_cost, Qfeed_plant, electricity_cost, R_ion_given, with_erd, Pin_erd_hp, erd_feed_q_in_hp_train):
    Qfeed_plant = Qfeed_plant / 3600  # convert from m3/h to m3/s
    #ions = {}
    # ions position or sequence (to maintain consistency in excel, while referring to their results in RCE)
    pos = proj_config_and_params["pos"]
    # Creating an empty ions dict
    ions = {k: 0 for k in pos.keys()}
    #i = 0
    # Creating a new dictionary Cfeed_plant with keys ordered based on the provided order
    Cfeed_plant = {k: Cfeed_plant_unordered[k] for k in sorted(pos, key=pos.get)}

    #for key in Cfeed_plant:
    #    ions[key] = 0
    #    pos[key] = i
    #    i =+ i + 1
    #print("old Cfeed_plant, pos, ions", Cfeed_plant, pos, ions)
    Ncomp = len(ions)
    N_nodes = tech_config_and_params['N_nodes']
    n_discr_L = tech_config_and_params['N_discr_L']
    n_elem = tech_config_and_params['n_elements']
    Mfeed_elem = np.zeros(n_elem)
    Mconc_elem = np.zeros(n_elem)
    Mp_elem = np.zeros(n_elem)
    Pin_elem = np.zeros(n_elem)
    Cp_elem = {}
    Cconc_elem = {}
    Cfeed_elem = {}
    R_ion = {}
    for key in pos:
        Cp_elem[key] = np.zeros(n_elem)
        Cconc_elem[key] = np.zeros(n_elem)
        R_ion[key] = np.zeros(n_elem)
    Qp_out_plant = Qfeed_plant * recovery_req
    Qconc_out_plant = Qfeed_plant - Qp_out_plant
    Cp_out_plant = {}
    Cconc_out_plant = {}
    R_ion_plant = {}
    for key in pos:
        Cconc_out_plant[key] = Cfeed_plant[key]
        Cp_out_plant[key] = 0
        R_ion_plant[key] = 0
    eta_solvent = tech_config_and_params['eta_solvent'] * 1e-3  # mPa-s to Pa-s
    n_vessel = single_stage.estimation_n_vessel(tech_config_and_params, Pfeed * 0.5, eta_solvent, n_elem)
    print('very first n_vessel assumption:', n_vessel)
    logger.info('membrane parameters: rp={}, deltax={}, epsilonp={}, Xd={}'.format(tech_config_and_params['rp'],
                                                                             tech_config_and_params['thickness_membrane'],
                                                                             tech_config_and_params['epsilon_r_pore'], tech_config_and_params['charge_density']))
    solver = single_stage.NFSolverPressureVessel_Rejectiongiven(n_elem, tech_config_and_params, ions, pos, Mfeed_elem, Mconc_elem,
                                                                Mp_elem, Cp_elem, Cconc_elem, Cfeed_elem,
                                                 R_ion, Pin_elem, n_discr_L, Qconc_out_plant, Qp_out_plant, Cp_out_plant,
                                                 Cconc_out_plant, R_ion_plant, N_nodes, n_vessel, recovery_req, Qfeed_plant,
                                                 Cfeed_plant, R_ion_given)

    solver.resolution_pressurevessel_approxfirstelem_recoveryreq_Rejectiongiven(Pfeed)
    n_vessel = solver.n_vessel
    logger.info('calculation with the approximation of the 1st element completed')
    print('n vessel', n_vessel)
    solver.resolution_pressurevessel_Rejectiongiven(Pfeed)
    logger.info('calculation for every element completed')
    print('out_plant')
    print('Mconc_out', solver.Mconc_out_plant, 'Mp_out', solver.Mp_out_plant)
    for key in ions:
        print(key, 'Cconc', solver.Cconc_out_plant[key], 'Cp', solver.Cp_out_plant[key], 'R', solver.R_ion_plant[key])

    #n_vessel_economics = solver.n_vessel / tech_config_and_params['Amembr_econ'] * tech_config_and_params['A_membrane'] \
    #                     * tech_config_and_params['n_elements']
    def n_vessel_economics(n_vessel, config):
        n_vessel_eco = solver.n_vessel / config['Amembr_econ'] * config['A_membrane'] \
                         * config['n_elements']
        return n_vessel_eco
    n_vessel_eco = n_vessel_economics(solver.n_vessel, tech_config_and_params)
    # Qfeed_plant in m3/s
    '''economic function'''
    #energy_requirement = (tech_config_and_params['cons_membrane_system'] * Qfeed_plant * 3600 / tech_config_and_params['energy_efficiency'] +\
    #                     Pfeed * 1e5 * Qfeed_plant / tech_config_and_params['energy_efficiency'])* 1e-3  # kW
    
    # power requirement
    #, with_erd, Pin_erd_hp, erd_feed_q_in_hp_train
    # effect of ERD
    if with_erd:
        Qin_erd_hp_m3s = erd_feed_q_in_hp_train / 3600  # [m3/h to m3/s]
        energy_in_erd = Pin_erd_hp * Qin_erd_hp_m3s  # high-energy stream coming from NF2 retentate
        Pout_erd_hp = Pfeed # [bar] Expected pressure of diverted NF1 feed will be equal to the feed pressure requirement of it
        # amount of NF1 feed diverted to ERD (while skipping HPP):
        Qin_erd_lp_m3s = tech_config_and_params['erd_type']['PX']['efficiency'] * energy_in_erd / Pout_erd_hp  # [m3/s]
        # assuming no fluid loss / mixture within the ERD
        Qout_erd_hp_m3s = Qin_erd_lp_m3s  # pressurized volume from ERD that will be fed to NF1
    else:
        erd_feed_q_in_hp_train = 0
        Pout_erd_hp = 0
        Qin_erd_lp_m3s = 0
        Qout_erd_hp_m3s = 0

    power_req_memb_sys = get_power_requirement_memb_system(tech_config_and_params, Qfeed_plant)  # [kW]
    
    Qfeed_to_HPP = Qfeed_plant - Qout_erd_hp_m3s  # [m3/s] Gets reduced in case of ERD
    Pdelta_HPP = Pfeed-Pfeed_pump  # [bar] Gets reduced in case of NF2, where it received high-pressure retentate from NF1
    power_req_pump_HPP = get_power_req_pump(Qfeed_to_HPP, Pdelta_HPP)  # [kW]
    # specific energy requierment
    spec_el_consumption_conc = (power_req_memb_sys + power_req_pump_HPP) / (Qconc_out_plant*3600)  # [kWh/m3 of concentrate]
    spec_el_consumption_perm = (power_req_memb_sys + power_req_pump_HPP) / (Qp_out_plant*3600)  # [kWh/m3 of permeate]
    
    # Capital costs [$]
    C_membrane = economic_function.membrane_cost(n_vessel_eco, tech_config_and_params, proj_config_and_params)
    C_civil = economic_function.civil_investment_cost(n_vessel_eco, Qfeed_plant, proj_config_and_params)
    C_mechanical = economic_function.mechanical_cost(n_vessel_eco, Qfeed_plant, proj_config_and_params)
    C_electrotech_only_filter_and_piping = economic_function.electrotechnic_cost_only_filter_and_piping(solver.Mp_out_plant*3600, proj_config_and_params)
    ## HPP
    if Qfeed_to_HPP <= 450:
        C_high_pressure_pump = economic_function.estimation_HPpump_cost_single(Qfeed_to_HPP, Pdelta_HPP, tech_config_and_params, proj_config_and_params)
    
    else:
        C_high_pressure_pump = economic_function.estimation_HPpump_cost_parallpumps(Qfeed_to_HPP, Pdelta_HPP, tech_config_and_params, proj_config_and_params)
    C_erd = economic_function.estimation_cost_erd_px(erd_feed_q_in_hp_train, proj_config_and_params)
    

    ## annualization of fixed expenditures, if already not annual (USD/y)
    
    annual_civil_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_civil'], C_civil)
    annual_mechanical_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_mech_electro'], C_mechanical)
    annual_electrotech_only_filter_and_piping_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_mech_electro'], C_electrotech_only_filter_and_piping)
    annual_high_pressure_pump_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_pump'], C_high_pressure_pump)
    annual_erd_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_erd'], C_erd)
    annual_membrane_cost = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['Ndepr_membrane'], C_membrane)
    C_depr = annual_civil_cost + annual_mechanical_cost + annual_electrotech_only_filter_and_piping_cost + annual_high_pressure_pump_cost + annual_erd_cost + annual_membrane_cost
    annualized_total_capital_cost = annual_civil_cost + annual_mechanical_cost + annual_electrotech_only_filter_and_piping_cost + annual_high_pressure_pump_cost + C_membrane
    levelized_capital_cost_per_m3_perm = annualized_total_capital_cost / (Qp_out_plant*3600 * 24 * 365 * tech_config_and_params['plant_availability'])  # [USD/m3 of perm]
    levelized_capital_cost_per_m3_conc = annualized_total_capital_cost / (Qconc_out_plant*3600 * 24 * 365 * tech_config_and_params['plant_availability'])  # [USD/m3 of concentrate]

    #### operating costs (assumed to consist of labor, maintenance, energy, membrane replacement, chemicals, quality)
    ## variable OPEX (in USD/y)
    C_energy = economic_function.energy_cost(Qfeed_plant, Pfeed-Pfeed_pump, electricity_cost, tech_config_and_params)
    if with_pretreatment_cost:
        print(Fore.GREEN + "Considering chemical pretreatment cost in the economic model" + Style.RESET_ALL)
        C_chemicals = economic_function.chemical_cost(solver.Mp_out_plant, tech_config_and_params, proj_config_and_params)
    else:
        print(Fore.RED + "Excluding chemical pretreatment cost in the economic model" + Style.RESET_ALL)
        C_chemicals = 0
    ## fixed OPEX (in USD/y)
    C_capital = C_civil + C_mechanical + C_electrotech_only_filter_and_piping + C_high_pressure_pump + C_erd + C_membrane
    C_manteinance = economic_function.manteinance_cost(C_capital, tech_config_and_params)
    C_quality = economic_function.quality_control_cost(C_capital, tech_config_and_params)
    C_install = economic_function.installation_cost(C_capital, tech_config_and_params)
    # total OPEX
    C_operating = C_energy + C_chemicals + C_manteinance + C_quality + C_install
    levelized_operating_cost_per_m3_perm = C_operating / (Qp_out_plant*3600 * 24 * 365 * tech_config_and_params['plant_availability'])  # [USD/m3 of perm]
    levelized_operating_cost_per_m3_conc = C_operating / (Qconc_out_plant*3600 * 24 * 365 * tech_config_and_params['plant_availability'])  # [USD/m3 of concentrate]

    nanofiltration_inputs = ResultList(
        title="NF input and parameters", xls_sheet="nanofiltration", results=[
        ('No. of trains', (N_trains, '-')),
        ('Qfeed per train', (Qfeed_plant*3600, 'm3/h')),
        ('r pore', (tech_config_and_params['rp'], 'nm')),
        ('thickness', (tech_config_and_params['thickness_membrane'], 'micron')),
        ('epsilon pore', (tech_config_and_params['epsilon_r_pore'], '-')),
        ('charge', (tech_config_and_params['charge_density'], 'mol/m3')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ('rho feed', (density_mixtures(tech_config_and_params, Cfeed_plant, ignore_ions=IGNORE_IONS_IN_DENSITY), 'kg/m3')),
        ('M feed', (Qfeed_plant * density_mixtures(tech_config_and_params, Cfeed_plant, ignore_ions=IGNORE_IONS_IN_DENSITY)*3600, 'kg/h')),
        ] + [('Cfeed' + key, (Cfeed_plant[key], 'mol/m3')) for key in Cfeed_plant.keys()]
        )

    #N_elem = range(1, n_elem+1)
    dict_N_elem = {k: 0 for k in range(1, n_elem+1)}
    for i, key in enumerate(dict_N_elem):
        dict_N_elem[key] = i
    keys_list = list(pos)
    print("if 5 > len(keys_list):", 5 > len(keys_list))

    
    ### Results: for single elements
    # Generate the column headers dynamically
    headers = ['Mfeed [m3/h]', 'Mconc [m3/h]', 'Mpermeate [m3/h]'] + [
        f"Cconc {keys_list[i]} [mol/m3] " for i in range(min(len(keys_list), 10))
    ]

    # Generate the result tuples dynamically
    results = [
        (str(key), (
            solver.Mfeed_elem[value]*3600, 
            solver.Mconc_elem[value]*3600,
            solver.Mp_elem[value]*3600,
            *(solver.Cconc_elem[keys_list[i]][value] if i < len(keys_list) else None for i in range(10))
        )) for key, value in dict_N_elem.items()
    ]

    # Combine everything into the ResultList object
    nanofiltration_results_single_elem_conc = ResultList(
        title="NF calculation for the single elements",
        xls_sheet="nanofiltration",
        xls_y0=3,
        results=[('elem', tuple(headers))] + results
    )

    ##### permeate concentration per element
    # Generate the column headers for permeate concentration results (only Cperm)
    headers_perm = [
        f"Cperm {keys_list[i]} [mol/m³]" for i in range(min(len(keys_list), 10))
    ]

    # Generate the result tuples for permeate concentration results
    results_perm = [
    (str(key), tuple(
        solver.Cp_elem[keys_list[i]][value] if i < len(keys_list) else None for i in range(10)
    )) for key, value in dict_N_elem.items()
    ]

    # Combine everything into the ResultList object for permeate concentration results
    nanofiltration_results_single_elem_perm = ResultList(
        title="NF calculation for the single elements (perm)",
        xls_sheet="nanofiltration",
        xls_x0=10,
        xls_y0=6,
        results=[('elem', tuple(headers_perm))] + results_perm
    )

    nanofiltration_results = ResultList(
        title="NF out plant", xls_sheet="nanofiltration", xls_y0=18, results=[
        ('Qconc', (solver.Mconc_out_plant*3600, 'm3/h')),
        ('Qpermeate', (solver.Mp_out_plant*3600, 'm3/h')),
        #('Permeate flux', (solver.Mp_out_plant*density_mixtures(tech_config_and_params, solver.Cp_out_plant, ignore_ions=IGNORE_IONS_IN_DENSITY)*3600/(tech_config_and_params["Amembr_econ"]*n_vessel_eco), 'l/m2/h')),
        ('Pfeed (to NF)', (Pfeed, 'bar')),
        ('Pfeed (to inlet of pump)', (Pfeed_pump, 'bar')),
        ('Pfeed_erd_hp (from high-pressure NF2 retentate, if this is NF1+ERD)', (Pin_erd_hp, 'bar')),
        ('Qfeed per train diverted to ERD', (Qin_erd_lp_m3s*3600, 'm3/h')),
        ('recovery', (solver.recovery_calc, '-')),
        ('rho conc', (density_mixtures(tech_config_and_params, solver.Cconc_out_plant, ignore_ions=IGNORE_IONS_IN_DENSITY), 'kg/m3')),
        ('rho perm', (density_mixtures(tech_config_and_params, solver.Cp_out_plant, ignore_ions=IGNORE_IONS_IN_DENSITY), 'kg/m3')),
        ('n vessel recalc', (solver.n_vessel, '-')),
        ('n vessel economic', (n_vessel_eco, '-')),
        
        ('', ('R ion plant', 'Cconc out plant', 'Cp out plant')),] +
        [(key, (solver.R_ion_plant[key], solver.Cconc_out_plant[key], solver.Cp_out_plant[key])) for key in pos.keys()]
        )
    
    nanofiltration_cost_results = ResultList(title="NF economics", xls_sheet="nanofiltration", xls_y0=23, results=[
        ('capex', ('$', '$/y')),
        ('Civil', (C_civil, annual_civil_cost)),
        ('Mech', (C_mechanical, annual_mechanical_cost)),
        ('Electrotech (only filters and piping)', (C_electrotech_only_filter_and_piping, annual_electrotech_only_filter_and_piping_cost)),
        ('Electrotech (HPP only)', (C_high_pressure_pump, annual_high_pressure_pump_cost)),
        ('ERD', (C_erd, annual_erd_cost)),
        ('C membrane', (C_membrane, annual_membrane_cost)),
        ('Total Capital', (C_capital, C_depr)),
        ('', ('', '')),
        ('opex', ('', '$/y')),
        ('C energy', ('', C_energy)),
        ('C chem', ('', C_chemicals)),
        ('C mant', ('', C_manteinance)),
        ('C quality and inst', ('', C_quality+C_install)),
        ('C operating (total)', ('', C_operating)),
        #('', ('', '')),
        ('total annual cost', ('', C_depr+C_operating)),
        #('', ('', '')),
        ('Levelized costs', ('$/m3_perm', '$/m3_conc')),
        ('levelized capital cost', (levelized_capital_cost_per_m3_perm, levelized_capital_cost_per_m3_conc)),
        ('levelized operating cost', (levelized_operating_cost_per_m3_perm, levelized_operating_cost_per_m3_conc)),
        ('levelized cost (capital+operating)', (levelized_capital_cost_per_m3_perm+levelized_operating_cost_per_m3_perm, levelized_capital_cost_per_m3_conc+levelized_operating_cost_per_m3_conc)),
        #('', ('', '')),
        ('power req', ('kW_el')),
        ('membrane system', (power_req_memb_sys)),
        ('HPP system', (power_req_pump_HPP)),
        #('', ('', '')),
        ('Spec. energy requirement', ('kWh/m3')),
        ('Energy per conc.', (spec_el_consumption_conc)),
        ('Energy per perm.', (spec_el_consumption_perm)),
    ])

    return [nanofiltration_inputs, nanofiltration_results_single_elem_conc, nanofiltration_results_single_elem_perm, nanofiltration_results, nanofiltration_cost_results]
    