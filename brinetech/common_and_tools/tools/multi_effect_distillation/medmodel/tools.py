import yaml
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


def xcheck(i):
    assert type(i) == type(1)


def itereffects(start, max):
    for i in range(start, max):
        yield int(i)


def read_program_config(file_yaml):
    with open(file_yaml, "r") as file_config_input:
        program_configuration = yaml.load(file_config_input, Loader=Loader)
    return dict(program_configuration)


def read_data_files(file_input, file_parameter):
    with open(file_input, "r") as input_data:
        input_configuration = yaml.load(input_data, Loader=Loader)
    with open(file_parameter, "r") as parameter_data:
        parameter_configuration = yaml.load(parameter_data, Loader=Loader)
    return input_configuration, parameter_configuration
	

def read_data_files_old(program_configuration):
    file_input = program_configuration['Input file']
    file_parameter = program_configuration['Parameter file']
    with open(file_input, "r") as input_data:
        input_configuration = yaml.load(input_data)
    with open(file_parameter, "r") as parameter_data:
        parameter_configuration = yaml.load(parameter_data)
    return input_configuration, parameter_configuration
