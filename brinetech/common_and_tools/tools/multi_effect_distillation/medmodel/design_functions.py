from .logger import logger
from scipy.optimize import minimize
import numpy as np
from . import heattransfer


def design_based_on_equal_dt(config, solver):
    loop_counter = config['loop_counter']
    Mms_res = [0]
    Eff_error_Mdist = 100.
    Ms_err = 100.
    Ms_min = config['Ms_min']
    y0 = config['runtime']['y0']
    while Eff_error_Mdist > 1e-4 or Ms_err > 1e-2:
        logger.info('iteration number: {}'.format(loop_counter))
        Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        Eff_error_Mdist = solver.steamcalculation(Ms)
        y0 = Ms
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        solver.n_tubes = solver.A_hx / (np.pi * config['L_evap'] * config['d_evap'])
        loop_counter += 1
        if loop_counter > 40:
            logger.warning('More than 40 iterations are required!')
            print(Ms_err, Eff_error_Mdist)
            break
    if 4 < loop_counter < 40:
        print(Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(Mms_res[i])
    else:
        print(loop_counter, Ms_err, Ms_min)
        print(Mms_res[loop_counter - 1])
    return Mms_res, Eff_error_Mdist

def design_based_on_equal_ahx(config, solver):
    loop_counter = config['loop_counter']
    DeltaT_res = [0]
    Mms_res = [0]
    DeltaT_err = config['DeltaT_err']
    Ms_err = config['Ms_err']
    Eff_area_err = config['Eff_area_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaT_min = config['DeltaT_min']
    Ms_min = config['Ms_min']
    x0 = config['runtime']['x0']
    y0 = config['runtime']['y0']
    while Eff_area_err > 1e-3 or Eff_error_Mdist > 1e-3 or Ms_err > 1e-3 or DeltaT_err > 1e-3:
        logger.info('iteration number: {}'.format(loop_counter))
        DeltaT_min = minimize(solver.areacalculation, x0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT = np.array(DeltaT_min.x[0:solver.N])
        DeltaT_res.append(DeltaT)
        solver.DeltaT = DeltaT
        Eff_area_err = solver.areacalculation(DeltaT)
        Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        solver.Mms = Ms
        Eff_error_Mdist = solver.steamcalculation(Ms)
        x0 = DeltaT
        y0 = Ms
        DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        solver.n_tubes = solver.A_hx[solver.N - 1] / (np.pi * config['L_evap'] * config['d_evap'])
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 30 iterations are required!')
            print(DeltaT_err, Ms_err, Eff_area_err, Eff_error_Mdist)
            break
    print(Eff_area_err, Eff_error_Mdist)
    if 4 < loop_counter < 40:
        print(DeltaT_min, Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(DeltaT_res[i], Mms_res[i])
    else:
        print(loop_counter, DeltaT_err, Ms_err)
        print(DeltaT_min, Ms_min)
        print(DeltaT_res[loop_counter - 1], Mms_res[loop_counter - 1])

def design_based_on_equal_dt_aph(config, solver):
    loop_counter = config['loop_counter']
    DeltaTph_res = [0]
    Mms_res = [0]
    DeltaTph_err = config['DeltaTph_err']
    Ms_err = config['Ms_err']
    Eff_areaph_err = config['Eff_areaph_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaTph_min = config['DeltaTph_min']
    Ms_min = config['Ms_min']
    z0 = config['runtime']['z0']
    y0 = config['runtime']['y0']
    while Eff_areaph_err > 1e-2 or Eff_error_Mdist > 1e-3 or DeltaTph_err > 1e-2 or Ms_err > 1e-3:
        logger.info('iteration number: {}'.format(loop_counter))
        DeltaTph_min = minimize(solver.areaphcalculation, z0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT_ph = DeltaTph_min.x
        DeltaTph_res.append(DeltaT_ph)
        solver.DeltaT_ph = DeltaT_ph
        Eff_areaph_err = solver.areaphcalculation(DeltaT_ph)
        DeltaTph_err = float(np.abs(np.sum(DeltaTph_res[loop_counter] - DeltaTph_res[loop_counter - 1])))
        Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Mms = Ms_min.x
        Mms_res.append(Mms)
        solver.Mms = Mms
        Eff_error_Mdist = solver.steamcalculation(Mms)
        y0 = Mms
        z0 = DeltaT_ph
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        solver.n_tubes = solver.A_hx / (np.pi * config['L_evap'] * config['d_evap'])
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 30 iterations are required!')
            print('Ms_err:', Ms_err, 'Error Mdist:', Eff_error_Mdist, 'DeltaTph_err:', DeltaTph_err, 'Error area preh:', Eff_areaph_err)
            break
    if 4 < loop_counter < 30:
        print(DeltaTph_min, Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(DeltaTph_res[i], Mms_res[i])
    else:
        print(loop_counter, 'Ms_err:', Ms_err, 'Error Mdist:', Eff_error_Mdist, 'DeltaTph_err:', DeltaTph_err, 'Error area preh:', Eff_areaph_err)
        print(DeltaTph_res[loop_counter-1], Mms_res[loop_counter - 1])


def design_based_on_equal_ahx_aph(config, solver):
    loop_counter = config['loop_counter']
    DeltaT_res = [0]
    DeltaTph_res = [0]
    Mms_res = [0]
    DeltaT_err = config['DeltaT_err']
    DeltaTph_err = config['DeltaTph_err']
    Ms_err = config['Ms_err']
    Eff_area_err = config['Eff_area_err']
    Eff_areaph_err = config['Eff_areaph_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaT_min = config['DeltaT_min']
    DeltaTph_min = config['DeltaTph_min']
    Ms_min = config['Ms_min']
    x0 = config['runtime']['x0']
    z0 = config['runtime']['z0']
    y0 = config['runtime']['y0']
    logger.info('y0 in design_functions is: "' + str(y0) + '"')
    #logger.info('solver in design_functions is: "' + str(solver) + '"')
    Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    y0 = Ms_min.x
    logger.info('Ms first guess is: "' + str(y0) + '"')
    print('Ms first guess:', y0)
    while Eff_area_err > 1e-2 or Eff_error_Mdist > 1e-2 or Ms_err > 1e-2 or DeltaT_err > 1e-2 or Eff_areaph_err > 1e-2 or DeltaTph_err > 1e-2:
        logger.info('iteration number: {}'.format(loop_counter))
        DeltaT_min = minimize(solver.areahxcalculation, x0, method='Nelder-Mead', options={'maxfev': 10000}) #'trust-krylov'
        DeltaT = np.array(DeltaT_min.x[0:solver.N])
        DeltaT_res.append(DeltaT)
        solver.DeltaT = DeltaT
        Eff_area_err = solver.areahxcalculation(DeltaT)
        DeltaTph_min = minimize(solver.areaphcalculation, z0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT_ph = DeltaTph_min.x
        DeltaTph_res.append(DeltaT_ph)
        solver.DeltaT_ph = DeltaT_ph
        Eff_areaph_err = solver.areaphcalculation(DeltaT_ph)
        Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        solver.Mms = Ms
        Eff_error_Mdist = solver.steamcalculation(Ms)
        x0 = DeltaT
        y0 = Ms
        z0 = DeltaT_ph
        solver.n_tubes = solver.A_hx[solver.N-1]/(np.pi * config['L_evap'] * config['d_evap'])
        DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        DeltaTph_err = float(np.abs(np.sum(DeltaTph_res[loop_counter] - DeltaTph_res[loop_counter - 1])))
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 40 iterations are required!')
            # print(DeltaT_err, Ms_err, DeltaTph_err, Eff_area_err, Eff_error_Mdist, Eff_areaph_err)
            break
    # print(Eff_area_err, Eff_areaph_err, Eff_error_Mdist)
    # if 4 < loop_counter < 30:
    #     print(DeltaT_min, DeltaTph_min, Ms_min)
    #     for i in range(loop_counter - 4, loop_counter):
    #         print(DeltaT_res[i], DeltaTph_res[i], Mms_res[i])
    # else:
    #     print(loop_counter, DeltaT_err, Ms_err)
    #     print(DeltaT_min, DeltaTph_min, Ms_min)
    #     print(DeltaT_res[loop_counter - 1], DeltaTph_res[loop_counter - 1], Mms_res[loop_counter - 1])

def design_based_on_equal_ahx_aph_Ucalc(config, solver):
    loop_counter = config['loop_counter']
    DeltaT_res = [0]
    DeltaTph_res = [0]
    Mms_res = [0]
    DeltaT_err = config['DeltaT_err']
    DeltaTph_err = config['DeltaTph_err']
    Ms_err = config['Ms_err']
    Eff_area_err = config['Eff_area_err']
    Eff_areaph_err = config['Eff_areaph_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaT_min = config['DeltaT_min']
    DeltaTph_min = config['DeltaTph_min']
    Ms_min = config['Ms_min']
    x0 = config['runtime']['x0']
    z0 = config['runtime']['z0']
    y0 = config['runtime']['y0']
    Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
    y0 = Ms_min.x
    print('Ms first guess:', y0)
    while Eff_area_err > 1e-4 or Eff_error_Mdist > 1e-4 or Ms_err > 1e-3 or DeltaT_err > 1e-2 or Eff_areaph_err > 1e-4 or DeltaTph_err > 1e-2:
        logger.info('iteration number: {}'.format(loop_counter))
        DeltaT_min = minimize(solver.areahxcalculation, x0, method='Nelder-Mead', options={'maxfev': 10000}) #'trust-krylov'
        DeltaT = np.array(DeltaT_min.x[0:solver.N])
        DeltaT_res.append(DeltaT)
        solver.DeltaT = DeltaT
        Eff_area_err = solver.areahxcalculation(DeltaT)
        DeltaTph_min = minimize(solver.areaphcalculation, z0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT_ph = DeltaTph_min.x
        DeltaTph_res.append(DeltaT_ph)
        solver.DeltaT_ph = DeltaT_ph
        Eff_areaph_err = solver.areaphcalculation(DeltaT_ph)
        Ms_min = minimize(solver.steamcalculation, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        solver.Mms = Ms
        Eff_error_Mdist = solver.steamcalculation(Ms)
        x0 = DeltaT
        y0 = Ms
        z0 = DeltaT_ph
        solver.n_tubes = solver.A_hx[solver.N-1]/(np.pi * config['L_evap'] * config['d_evap'])
        solver.U_evap[0] = heattransfer.U_overall(solver.T[0], solver.Ts, solver.Xf,
                                                             solver.Mfeed, config, solver.n_tubes)
        for i_effect in range(1, solver.N):
            solver.U_evap[i_effect] = heattransfer.U_overall(solver.T[i_effect], solver.Tc[i_effect-1], solver.Xb[i_effect-1],
                                                             solver.Mb[i_effect-1], config, solver.n_tubes)
        print(solver.U_evap)
        DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        DeltaTph_err = float(np.abs(np.sum(DeltaTph_res[loop_counter] - DeltaTph_res[loop_counter - 1])))
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 40 iterations are required!')
            print(DeltaT_err, Ms_err, DeltaTph_err, Eff_area_err, Eff_error_Mdist, Eff_areaph_err)
            break
    print(Eff_area_err, Eff_areaph_err, Eff_error_Mdist)
    if 4 < loop_counter < 30:
        print(DeltaT_min, DeltaTph_min, Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(DeltaT_res[i], DeltaTph_res[i], Mms_res[i])
    else:
        print(loop_counter, DeltaT_err, Ms_err)
        print(DeltaT_min, DeltaTph_min, Ms_min)
        print(DeltaT_res[loop_counter - 1], DeltaTph_res[loop_counter - 1], Mms_res[loop_counter - 1])

def design_based_on_equal_dt_parallcross(config, solver):
    loop_counter = config['loop_counter']
    Mms_res = [0]
    Ms_err = config['Ms_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    Ms_min = config['Ms_min']
    y0 = config['runtime']['y0']
    while Eff_error_Mdist > 1e-2 or Ms_err > 1e-5:
        logger.info('iteration number: {}'.format(loop_counter))
        Ms_min = minimize(solver.steamcalculation_parallcross, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        Eff_error_Mdist = solver.steamcalculation_parallcross(Ms)
        print(Ms, Eff_error_Mdist)
        y0 = Ms
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        solver.n_tubes = solver.A_hx / (np.pi * config['L_evap'] * config['d_evap'])
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 30 iterations are required!')
            print(Ms_err, Eff_error_Mdist)
            break
    if 4 < loop_counter < 40:
        print(Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(Mms_res[i])
    else:
        print(loop_counter, Ms_err, Ms_min)
        print(Mms_res[loop_counter - 1])


def design_based_on_equal_ahx_parallcross(config, solver):
    loop_counter = config['loop_counter']
    DeltaT_res = [0]
    Mms_res = [0]
    DeltaT_err = config['DeltaT_err']
    Ms_err = config['Ms_err']
    Eff_area_err = config['Eff_area_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaT_min = config['DeltaT_min']
    Ms_min = config['Ms_min']
    Tot_error = config['Tot_error']
    firstguess = config['firstguess']
    x0 = config['runtime']['x0']
    y0 = config['runtime']['y0']
    x0 = np.append(x0, y0)
    firstguess = minimize(solver.firstguess_parallcross, x0, method='Nelder-Mead', options={'maxfev': 10000})
    x0 = firstguess.x[0:solver.N]
    y0 = firstguess.x[solver.N]
    Ms_min = minimize(solver.steamcalculation_parallcross, y0, method='Nelder-Mead', options={'maxfev': 10000})
    y0 = Ms_min.x
    print('DeltaT first guess:',  x0, 'Ms first guess:', y0)
    while Eff_area_err > 1e-4 or Eff_error_Mdist > 1e-4 or Ms_err > 1e-3 or DeltaT_err > 1e-3:
        logger.info('iteration number: {}'.format(loop_counter))
        Ms_min = minimize(solver.steamcalculation_parallcross, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        solver.Ms = Ms
        Eff_error_Mdist = solver.steamcalculation_parallcross(Ms)
        DeltaT_min = minimize(solver.areacalculation_parallcross, x0, method='Nelder-Mead', options={'maxfev': 10000})
        # DeltaT_min = minimize(solver.area_Mscalculation_parallcross, x0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT = np.array(DeltaT_min.x[0:solver.N])
        DeltaT_res.append(DeltaT)
        solver.DeltaT = DeltaT
        Eff_area_err = solver.areacalculation_parallcross(DeltaT)
        x0 = DeltaT
        y0 = Ms
        DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        solver.n_tubes = solver.A_hx/ (np.pi * config['L_evap'] * config['d_evap'])
        loop_counter += 1
        if loop_counter > 30:
            logger.warning('More than 30 iterations are required!')
            print(DeltaT_err, Ms_err, Eff_area_err, Eff_error_Mdist)
            break
    print(Eff_area_err, Eff_error_Mdist)
    if 4 < loop_counter < 40:
        print(DeltaT_min, Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(DeltaT_res[i], Mms_res[i])
    else:
        print(loop_counter, DeltaT_err, Ms_err)
        print(DeltaT_min, Ms_min)
        print(DeltaT_res[loop_counter - 1], Mms_res[loop_counter - 1])

def design_based_on_equal_ahx_aph_parallcross(config, solver):
    loop_counter = config['loop_counter']
    DeltaT_res = [0]
    DeltaTph_res = [0]
    Mms_res = [0]
    DeltaT_err = config['DeltaT_err']
    DeltaTph_err = config['DeltaTph_err']
    Ms_err = config['Ms_err']
    Eff_area_err = config['Eff_area_err']
    Eff_areaph_err = config['Eff_areaph_err']
    Eff_error_Mdist = config['Eff_error_Mdist']
    DeltaT_min = config['DeltaT_min']
    DeltaTph_min = config['DeltaTph_min']
    Ms_min = config['Ms_min']
    # DeltaT0 = config['runtime']['x0']
    # DeltaT_ph0 = config['runtime']['z0']
    # Ms0 = config['runtime']['y0']
    # first_values = np.append(DeltaT0, DeltaT_ph0)
    # first_values = np.append(first_values, Ms0)
    # firstguess = minimize(solver.firstguess_parallcross, first_values, method='Nelder-Mead', options={'maxfev': 10000})
    # x0 = firstguess.x[0:solver.N]
    # z0 = firstguess.x[solver.N: solver.N+(solver.N-1)]
    # y0 = firstguess.x[2 * solver.N - 1]
    x0 = config['runtime']['x0']
    z0 = config['runtime']['z0']
    y0 = config['runtime']['y0']
    Ms_min = minimize(solver.steamcalculation_parallcross, y0, method='Nelder-Mead', options={'maxfev': 10000})
    y0 = Ms_min.x
    print('Delta T first guess:', x0, 'DeltaT ph first guess:', z0, 'Ms first guess:', y0)
    while Eff_area_err > 1e-4 or Eff_error_Mdist > 1e-4 or Ms_err > 1e-4 or DeltaT_err > 1e-4 or Eff_areaph_err > 1e-4 or DeltaTph_err > 1e-4:
        logger.info('iteration number: {}'.format(loop_counter))
        Ms_min = minimize(solver.steamcalculation_parallcross, y0, method='Nelder-Mead', options={'maxfev': 10000})
        Ms = Ms_min.x
        Mms_res.append(Ms)
        solver.Ms = Ms
        Eff_error_Mdist = solver.steamcalculation_parallcross(Ms)
        DeltaT_min = minimize(solver.areahxcalculation_parallcross, x0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT = np.array(DeltaT_min.x[0:solver.N])
        DeltaT_res.append(DeltaT)
        solver.DeltaT = DeltaT
        Eff_area_err = solver.areahxcalculation_parallcross(DeltaT)
        DeltaTph_min = minimize(solver.areaphcalculation_parallcross, z0, method='Nelder-Mead', options={'maxfev': 10000})
        DeltaT_ph = DeltaTph_min.x
        DeltaTph_res.append(DeltaT_ph)
        solver.DeltaT_ph = DeltaT_ph
        Eff_areaph_err = solver.areaphcalculation_parallcross(DeltaT_ph)
        x0 = DeltaT
        y0 = Ms
        z0 = DeltaT_ph
        DeltaT_err = np.abs(np.sum(DeltaT_res[loop_counter] - DeltaT_res[loop_counter - 1]))
        Ms_err = float(np.abs(Mms_res[loop_counter] - Mms_res[loop_counter - 1]))
        DeltaTph_err = float(np.abs(np.sum(DeltaTph_res[loop_counter] - DeltaTph_res[loop_counter - 1])))
        solver.n_tubes = solver.A_hx / (np.pi * config['L_evap'] * config['d_evap'])
        # print(DeltaT_err, DeltaTph_err, Ms_err)
        loop_counter += 1
        if loop_counter > 40:
            logger.warning('More than 30 iterations are required!')
            print(DeltaT_err, Ms_err, DeltaTph_err, Eff_area_err, Eff_error_Mdist, Eff_areaph_err)
            break
    print(Eff_area_err, Eff_areaph_err, Eff_error_Mdist)
    if 4 < loop_counter < 50:
        print(DeltaT_min, DeltaTph_min, Ms_min)
        for i in range(loop_counter - 4, loop_counter):
            print(DeltaT_res[i], DeltaTph_res[i], Mms_res[i])
    else:
        print(loop_counter, DeltaT_err, Ms_err)
        print(DeltaT_min, DeltaTph_min, Ms_min)
        print(DeltaT_res[loop_counter - 1], DeltaTph_res[loop_counter - 1], Mms_res[loop_counter - 1])


# define all design functions above this point!
import inspect
import sys
designReg = {name.split('design_based_on_')[-1]: obj for name, obj in inspect.getmembers(sys.modules[__name__])
                     if (inspect.isfunction(obj) and
                         name.startswith('design_based_on'))}
