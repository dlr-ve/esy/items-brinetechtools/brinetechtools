from argparse import ArgumentParser
from sys import argv
import os
import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from common_and_tools import save_resultlists
from common_and_tools import model_nanofiltration_rejection_given

def driver(model, parameterconfigfilepath, Cfeed_filepath, Pfeed, recovery_req, Qfeed, electricity_cost, Rionfilepath,
           nNF, output_directory, output_namefile, output_filename, output_yamlname):
    with open(parameterconfigfilepath, "r") as file_params:
        parameter_config = yaml.load(file_params, Loader=Loader)
    with open(Cfeed_filepath, "r") as file_conc:
        Cfeed = yaml.load(file_conc, Loader=Loader)
    with open(Rionfilepath, "r") as file_rej:
        R_ion_given = yaml.load(file_rej, Loader=Loader)
        print(model)

    result_lists = model(parameter_config, Cfeed, Pfeed, recovery_req, Qfeed, electricity_cost, R_ion_given)
     
    with open(output_directory + '/' + output_yamlname, 'w') as file:
        documents = yaml.dump(result_lists, file)
    # TODO: code for saving output (excel and yaml) should be part of save_resultlists module

    for result_list in result_lists:
        print(result_list)

    return save_resultlists(
        output_directory,
        output_filename,
        output_namefile,
        result_lists
    )


def run(argv):
    models = {
    'nanofiltration': model_nanofiltration_rejection_given
    }
    parser = ArgumentParser(prog='brinetech_nanofiltration', description='Nanofiltration simulation tool')
    parser.add_argument('-m', '--model')  # should be 'nanofiltration'
    parser.add_argument('-q', '--feed-q', type=float, required=True, help="Feed flux Q in m³/h")
    parser.add_argument('-e', '--electricity-cost', type=float, default=0.1035, help="Cost of electricity in $/kWh")
    parser.add_argument('-rr', '--req-rec', type=float, required=True, help="required recovery in [-]")
    parser.add_argument('-p', '--feed-p', type=float, required=True, help="Feed pressure P in bar")
    parser.add_argument('-nnf', '--nNF', type=int, required=True, help="Number of nanofiltration in treatment chain [-]")
    #parser.add_argument('-rej', '--rej-file', required=True, help="Ion rejection in [-]")
    
    args = parser.parse_args(argv)

    ## getting paths for parameters.yml and concentration_ion.yml for each tool:
    main_tool_name = ""
    sub_tool_name = ""
    model_name = args.model
    if model_name == 'nanofiltration':
        main_tool_name = model_name
    else:
        main_tool_name = model_name.split("_", 1)[0]  # applies for crystallizer only (upto now)
    # parameters file path
    param_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'parameters.yml')
    
    # concentration file path, output directory & output file name
    if main_tool_name == "crystallizer":
        sub_tool_name = model_name
        c_feed_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                sub_tool_name, 'concentration_ion.yml')
        output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name,
                sub_tool_name)
        output_filename = f"results_qfeed_{round(args.feed_q, 2)}.xls"
    elif main_tool_name == "nanofiltration":
        c_feed_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'concentration_ion.yml')
        rej_feed_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "inputs", main_tool_name,
                'rejection_ion.yml')
        output_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "results", main_tool_name)
        
        common_string = f"results_Rgiven_noCorrFactor_qfeed={int(round(args.feed_q, 0))}_NF_{args.nNF}"
        output_filename = common_string + ".xls"
        output_yamlname = common_string + ".yaml"
    # name of the dat. file to store output file name:
    output_namefile = "fila_name.dat"


    return driver(
        model=models[args.model],
        parameterconfigfilepath=param_path,
        Cfeed_filepath=c_feed_path,
        Pfeed=args.feed_p,
        recovery_req=args.req_rec,
        Qfeed=args.feed_q,
        electricity_cost=args.electricity_cost,
        Rionfilepath=rej_feed_path,
        nNF=args.nNF,
        output_directory=output_directory,
        output_namefile=output_namefile,
        output_filename=output_filename,
        output_yamlname = output_yamlname
    )

def entry_point():
    return run(argv[1:])

if __name__ == "__main__":
    entry_point()

"""
## model_nanofiltration_rejection_given
python main_nanofiltration.py -m "nanofiltration" -q 10 -e 0.06 -rr 0.65 -p 20 -nnf 1  ## feed in m3/h (EW nexus)

python main_nanofiltration.py -m "nanofiltration" -q 0.0165 -e 0.06 -rr 0.8 -p 50 -nnf 2
"""