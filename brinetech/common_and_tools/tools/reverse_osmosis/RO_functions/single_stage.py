from . import tools, membrane_properties, economic_function
from scipy.optimize import minimize
import numpy as np

'''function for the design of the RO single stage'''

def global_balance_mpermeate(mp, r1):
    mf = mp/r1
    mc = mf - mp
    return mf, mc

def global_balance_mfeed(mf, r1):
    mp = mf * r1
    mc = mf - mp
    return mp, mc

def global_balance_xc(xf, r1, sr1):
    xc = xf * (1 + (r1 * sr1) / (1 - r1))
    return xc

def global_balance_Rtot(xf, xc, sr1):
    r = (xc - xf) / (xc - xf * (1 - sr1))
    return r

def average_feed_salinity(xf, r, cpf, tcf):
    afs = xf/r * np.log(1/(1-r)) * cpf * tcf
    return afs

def osmotic_pressure_out(config, xc, xp):
    Posm_out = config['osmotic_coeff'] * (xc - xp) * 1e-4
    return Posm_out

def feed_pressure(config, posm_out, p_losses):
    Pin = posm_out * config['security_factor'] + p_losses
    return Pin

def element_fluxes(config, afs_el, xp_cum_el, p_el, A_membrane, B_membrane):
    posm_el = config['osmotic_coeff'] * (afs_el - xp_cum_el) * 1e-4
    water_flux_el = A_membrane * (p_el - posm_el)
    salt_flux_el = B_membrane * (afs_el - xp_cum_el) * 1e-6
    return water_flux_el, salt_flux_el

def net_driving_pressure(p_in, posm_out, p_losses, nelem):
    ndp = p_in - posm_out - p_losses /(nelem * 2)
    return ndp

def minimum_Pfeed(Xfeed):
    Pfeed_min = 0.0008*Xfeed + 6.9
    return Pfeed_min

class ROsolver_singlestage_discrete:
    def __init__(self, Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed, Mf, Mp, Mc, Mp_cum, MXp_cum, Xp, Xc, Mfeed,
                 Fw, Fs, TCF, AFS, CPF, Amembr_real, Bmembr_real, Amembr, NPV, parameter_config, nelem, Plosses, Posm_out, Mpermeate_calc,
                 Xp_cum, ndiscr, Mp_cum_elem, MXp_cum_elem, Xp_cum_elem, Xfs, Xp_out_elem, Amembr_tot):
        self.Rtot = Rtot  # the condition to be fulfilled may regard the total recovery or the concentration of the outlet retentate
        self.Xret_out = Xret_out
        self.R1_calc = R1_calc
        self.toll_R1 = toll_R1
        self.Rel_calc = Rel_calc
        self.Rel = Rel
        self.toll_Rel = toll_Rel
        self.P = P
        self.Pfeed = Pfeed
        self.Xf = Xf
        self.Xfeed = Xfeed
        self.Mf = Mf
        self.Mp = Mp
        self.Mc = Mc
        self.Mp_cum = Mp_cum
        self.MXp_cum = MXp_cum
        self.Xp = Xp
        self.Xc = Xc
        self.Mfeed = Mfeed
        self.Fw = Fw
        self.Fs = Fs
        self.TCF = TCF
        self.AFS = AFS
        self.CPF = CPF
        self.Amembr_real = Amembr_real
        self.Bmembr_real = Bmembr_real
        self.Amembr = Amembr
        self.NPV = NPV
        self.parameter_config = parameter_config
        self.nelem = nelem
        self.Plosses = Plosses
        self.Posm_out = Posm_out
        self.Mpermeate_calc = Mpermeate_calc
        self.Xp_cum = Xp_cum
        self.ndiscr = ndiscr
        self.Mp_cum_elem = Mp_cum_elem
        self.MXp_cum_elem = MXp_cum_elem
        self.Xp_cum_elem = Xp_cum_elem
        self.Xfs = Xfs
        self.Xp_out_elem = Xp_out_elem
        self.Amembr_tot = Amembr_tot
        self._log = []
        self.Amembr_tot_calc = 0
        self.average_Fw = 0
        self.average_Fs = 0
        self.average_NDP = 0
        self.aver_recovery_elem = np.zeros(self.nelem)
        self.feed_flow_elem = np.zeros(self.nelem)
        self.feed_pressure_elem = np.zeros(self.nelem)
        self.feed_conc_elem = np.zeros(self.nelem)
        self.conc_flow_elem = np.zeros(self.nelem)
        self.permeate_flow_elem = np.zeros(self.nelem)
        self.permeate_conc_elem = np.zeros(self.nelem)
        self.av_elem = int(self.nelem / 2) - 1

    def inizialization_singlestage(self):
        self.Mp[0, 0] = self.Mpermeate_calc / self.nelem / self.ndiscr
        self.Mp_cum[0, 0] = self.Mp[0, 0]
        self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
        for i_elem in range(self.nelem):
            for i_subelem in range(self.ndiscr):
                self.Mp[i_subelem, i_elem] = self.Mpermeate_calc / self.nelem / self.ndiscr
        self.Mf[0, 0] = self.Mfeed
        self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
        self.Xf[0, 0] = self.Xfeed
        Xc_out_elem = np.zeros(self.nelem)
        DeltaX = np.zeros(self.nelem)
        Xc_out_elem[0] = global_balance_xc(self.Xf[0, 0], self.Rel[0], self.parameter_config['salt_rejection_sw'])
        DeltaX[0] = (Xc_out_elem[0] - self.Xf[0, 0]) / self.ndiscr
        self.Xc[0, 0] = self.Xf[0, 0] + DeltaX[0]
        self.Xp[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mc[0, 0] * self.Xc[0, 0]) / self.Mp[0, 0]
        self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
        self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
        self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
        for i_subelem in range(1, self.ndiscr):
            self.Mf[i_subelem, 0] = self.Mc[i_subelem - 1, 0]
            self.Mc[i_subelem, 0] = self.Mf[i_subelem, 0] - self.Mp[i_subelem, 0]
            self.Xf[i_subelem, 0] = self.Xc[i_subelem - 1, 0]
            self.Xc[i_subelem, 0] = self.Xf[i_subelem, 0] + DeltaX[0]
            self.Xp[i_subelem, 0] = (self.Mf[i_subelem, 0] * self.Xf[i_subelem, 0] - self.Mc[i_subelem, 0] *
                                     self.Xc[i_subelem, 0]) / self.Mp[i_subelem, 0]
            self.Mp_cum[i_subelem, 0] = self.Mp_cum[i_subelem - 1, 0] + self.Mp[i_subelem, 0]
            self.MXp_cum[i_subelem, 0] = self.MXp_cum[i_subelem - 1, 0] + self.Mp[i_subelem, 0] * self.Xp[i_subelem, 0]
            self.Mp_cum_elem[i_subelem, 0] = self.Mp_cum_elem[i_subelem - 1, 0] + self.Mp[i_subelem, 0]
            self.MXp_cum_elem[i_subelem, 0] = self.MXp_cum_elem[i_subelem - 1, 0] + self.Mp[i_subelem, 0] * self.Xp[i_subelem, 0]
            self.Xp_cum_elem[i_subelem, 0] = self.MXp_cum_elem[i_subelem, 0] / self.Mp_cum_elem[i_subelem, 0]
            self.Xp_out_elem[0] = self.MXp_cum_elem[self.ndiscr - 1, 0] / self.Mp_cum_elem[self.ndiscr - 1, 0]
        for i_elem in range(1, self.nelem):
            self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
            self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
            self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
            Xc_out_elem[i_elem] = global_balance_xc(self.Xf[0, i_elem], self.Rel[i_elem],
                                                    self.parameter_config['salt_rejection_sw'])
            DeltaX[i_elem] = (Xc_out_elem[i_elem] - self.Xf[0, i_elem]) / self.ndiscr
            self.Xc[0, i_elem] = self.Xf[0, i_elem] + DeltaX[i_elem]
            self.Xp[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mc[0, i_elem] * self.Xc[0, i_elem]) / self.Mp[0, i_elem]
            self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
            self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
            self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
            self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
            self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_subelem in range(1, self.ndiscr):
                self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                self.Xc[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] + DeltaX[i_elem]
                self.Xp[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] - self.Mc[i_subelem, i_elem] *
                                              self.Xc[i_subelem, i_elem]) / self.Mp[i_subelem, i_elem]
                self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] *\
                                                                                        self.Xp[i_subelem, i_elem]
                self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                                  * self.Xp[i_subelem, i_elem]
                self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
            self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]

        self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
        AFS_global = (self.Xc[self.ndiscr - 1, self.nelem - 1] - self.Xf[0, 0]) / np.log(self.Xc[self.ndiscr - 1, self.nelem - 1] / self.Xf[0, 0])
        self.Posm_out = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.nelem - 1], self.Xp_cum) # self.Xp_out_elem[self.nelem - 1]
        Posm_av = self.parameter_config['osmotic_coeff'] * (AFS_global) * 1e-4
        av_elem = int(self.nelem / 2)
        # Posm_av = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, av_elem],
        #                                self.Xp_out_elem[av_elem])
        self.Amembr_tot = self.Mp_cum[self.ndiscr - 1, self.nelem - 1] / (0.9 * self.Amembr_real * ((self.Pfeed +
                                                                                                     self.Posm_out * self.parameter_config['security_factor']) / 2 - Posm_av))
        A = 1

    def resolution_singlestage_Xpobj(self, x):
        self.Pfeed = x
        if self.Pfeed < minimum_Pfeed(self.Xfeed):
            return np.exp(self.Xfeed)
        loop_count = 0
        self.Amembr_tot_calc = self.Amembr_tot * 1
        self.Amembr_tot = 0
        while abs(self.Amembr_tot - self.Amembr_tot_calc) > self.toll_Rel:
            self.Amembr_tot = self.Amembr_tot_calc
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], self.Xp_cum_elem[0, 0],
                                                          self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot / self.nelem / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                self.P[i_elem] = self.P[i_elem - 1] - self.Plosses / self.nelem
                self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem],
                                                                        self.Xp_cum_elem[0, i_elem], self.P[i_elem],
                                                                        self.Amembr_real, self.Bmembr_real)
                self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                     / self.Mc[0, i_elem]
                self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                for i_subelem in range(1, self.ndiscr):
                    self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                    self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                    self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                            self.Xfs[i_subelem, i_elem],
                                                                                            self.Xp_cum_elem[i_subelem, i_elem],
                                                                                            self.P[i_elem],
                                                                                            self.Amembr_real, self.Bmembr_real)
                    self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                    self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                    self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                            * self.Xp[i_subelem, i_elem]
                    self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                    self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                  self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                i_subelem, i_elem]
                    self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                        i_subelem, i_elem]
                    self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                A = 1
                if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                    self.Rel[i_elem] = self.Rtot / self.nelem
                # loop_count = loop_count + 1
                # if loop_count > 1000:
                #     break
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc / self.Mfeed
            # self.Rel = np.array([self.R1_calc / self.nelem for i in range(0, self.nelem)])
            # self.Posm_out = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.nelem - 1], self.Xp_cum)
            self.nelem_av = int(self.nelem / 2 - 1)
            AFS_global = (self.Xc[self.ndiscr - 1, self.nelem - 1] - self.Xfeed) / np.log(
                self.Xc[self.ndiscr - 1, self.nelem - 1] / self.Xfeed)
            Posm_av = self.parameter_config['osmotic_coeff'] * (
            AFS_global - self.Xp_cum_elem[self.ndiscr - 1, self.nelem_av]) * 1e-4
            self.P_av = self.P[self.nelem_av]
            self.Amembr_tot_calc = self.Mpermeate_calc / (0.9 * self.Amembr_real * (self.P_av - Posm_av))
            self.NPV = self.Amembr_tot_calc / (self.nelem * self.parameter_config['membrane_area_sw'])
        diff_perm_salinity = ((self.Xp_cum - self.parameter_config['Permeate_salinity'])**2)**(1/4)
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr - 1, self.nelem - 1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr - 1, self.nelem - 1]
        return diff_perm_salinity
        # return abs(self.Rtot - self.R1_calc)

    def resolution_singlestage_Mpobj(self, x):
        self.Pfeed = x
        if self.Pfeed < minimum_Pfeed(self.Xfeed):
            return np.exp(self.Xfeed)
        loop_count = 0
        self.Amembr_tot_calc = self.Amembr_tot * 1
        self.Amembr_tot = 0
        while abs(self.Amembr_tot - self.Amembr_tot_calc) > self.toll_Rel:
            self.Amembr_tot = self.Amembr_tot_calc
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], self.Xp_cum_elem[0, 0],
                                                          self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot / self.nelem / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                self.P[i_elem] = self.P[i_elem - 1] - self.Plosses / self.nelem
                self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem],
                                                                        self.Xp_cum_elem[0, i_elem], self.P[i_elem],
                                                                        self.Amembr_real, self.Bmembr_real)
                self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                     / self.Mc[0, i_elem]
                self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                for i_subelem in range(1, self.ndiscr):
                    self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                    self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                    self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                            self.Xfs[i_subelem, i_elem],
                                                                                            self.Xp_cum_elem[i_subelem, i_elem],
                                                                                            self.P[i_elem],
                                                                                            self.Amembr_real, self.Bmembr_real)
                    self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                    self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                    self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                            * self.Xp[i_subelem, i_elem]
                    self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                    self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                  self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                i_subelem, i_elem]
                    self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                        i_subelem, i_elem]
                    self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                    self.Rel[i_elem] = self.Rtot / self.nelem
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc / self.Mfeed
            # self.Posm_out = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.nelem - 1], self.Xp_cum)
            self.nelem_av = int(self.nelem / 2 - 1)
            AFS_global = (self.Xc[self.ndiscr - 1, self.nelem - 1] - self.Xfeed) / np.log(
                self.Xc[self.ndiscr - 1, self.nelem - 1] / self.Xfeed)
            Posm_av = self.parameter_config['osmotic_coeff'] * (
            AFS_global - self.Xp_cum_elem[self.ndiscr - 1, self.nelem_av]) * 1e-4
            self.P_av = self.P[self.nelem_av]
            self.Amembr_tot_calc = self.Mpermeate_calc / (0.9 * self.Amembr_real * (self.P_av - Posm_av))
            self.NPV = self.Amembr_tot_calc / (self.nelem * self.parameter_config['membrane_area_sw'])
            # average_deltaP = (self.Pfeed + self.Posm_out *self.parameter_config['security_factor']) / 2 - Posm_av
            # self._log.append(average_deltaP)
            # self.Amembr_tot_calc = self.Mpermeate_calc / (0.9 * self.Amembr_real * ((self.Pfeed + self.Posm_out *
            #                                                                          self.parameter_config['security_factor']) / 2 - Posm_av))
        self.NPV = self.Amembr_tot_calc / (self.parameter_config['membrane_area_sw'] * self.nelem)
        diff_perm_flowrate = ((self.Rtot * self.Mfeed - self.Mpermeate_calc)**2)**(1/4)
        for i_elem in range(0, self.nelem):
            self.average_Fw += np.average(self.Fw[:, i_elem])
            self.average_Fs += np.average(self.Fs[:, i_elem])
        self.average_Fw = self.average_Fw / self.nelem
        self.average_Fs = self.average_Fs / self.nelem
        Posm_av_2 = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.av_elem],
                                         self.Xp_out_elem[self.av_elem])
        self.average_NDP = net_driving_pressure(self.P[self.av_elem], Posm_av_2, self.parameter_config['Plosses'], self.parameter_config)
        for i_elem in range(0, self.nelem):
            self.aver_recovery_elem[i_elem] = np.average(self.Rel[i_elem])
            self.feed_flow_elem[i_elem] = np.average(self.Mf[:, i_elem])
            self.feed_pressure_elem[i_elem] = self.P[i_elem]
            self.feed_conc_elem[i_elem] = np.average(self.Xf[:, i_elem])
            self.conc_flow_elem[i_elem] = np.average(self.Mc[:, i_elem])
            self.permeate_flow_elem[i_elem] = np.average(self.Mp[:, i_elem])
            self.permeate_conc_elem[i_elem] = np.average(self.Xp[:, i_elem])
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr - 1, self.nelem - 1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr - 1, self.nelem - 1]
        return diff_perm_flowrate

    def resolution_singlestage_Xretobj(self, x):
        self.Pfeed = x
        if self.Pfeed < minimum_Pfeed(self.Xfeed):
            return np.exp(self.Xfeed)
        loop_count = 0
        self.Amembr_tot_calc = self.Amembr_tot * 1
        self.Amembr_tot = 0
        while abs(self.Amembr_tot - self.Amembr_tot_calc) > self.toll_Rel:
            self.Amembr_tot = self.Amembr_tot_calc
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], self.Xp_cum_elem[0, 0],
                                                          self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot / self.nelem / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem
                self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem],
                                                                        self.Xp_cum_elem[0, i_elem], self.P[i_elem],
                                                                        self.Amembr_real, self.Bmembr_real)
                self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                     / self.Mc[0, i_elem]
                self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                for i_subelem in range(1, self.ndiscr):
                    self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                    self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                    self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                            self.Xfs[i_subelem, i_elem],
                                                                                            self.Xp_cum_elem[i_subelem, i_elem],
                                                                                            self.P[i_elem],
                                                                                            self.Amembr_real, self.Bmembr_real)
                    self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                    self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                    self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                            * self.Xp[i_subelem, i_elem]
                    self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                    self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                  self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                i_subelem, i_elem]
                    self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                        i_subelem, i_elem]
                    self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                    self.Rel[i_elem] = self.Rtot / self.nelem
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc / self.Mfeed
            # self.Posm_out = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.nelem - 1], self.Xp_cum)
            self.nelem_av = int(self.nelem/2-1)
            AFS_global = (self.Xc[self.ndiscr - 1, self.nelem - 1] - self.Xfeed) / np.log(self.Xc[self.ndiscr - 1, self.nelem - 1] / self.Xfeed)
            Posm_av = self.parameter_config['osmotic_coeff'] * (AFS_global - self.Xp_cum_elem[self.ndiscr-1, self.nelem_av]) * 1e-4
            self.P_av = self.P[self.nelem_av]
            self.Amembr_tot_calc = self.Mpermeate_calc / (0.9 * self.Amembr_real * (self.P_av - Posm_av))
            self.NPV = self.Amembr_tot_calc / (self.nelem * self.parameter_config['membrane_area_sw'])
            # average_deltaP = (self.Pfeed + self.Posm_out *self.parameter_config['security_factor']) / 2 - Posm_av
            # self._log.append(average_deltaP)
            # self.Amembr_tot_calc = self.Mpermeate_calc / (0.9 * self.Amembr_real * ((self.Pfeed + self.Posm_out *
            #                                                                          self.parameter_config['security_factor']) / 2 - Posm_av))
        diff_retentate_conc = abs((self.Xret_out - self.Xc[self.ndiscr-1, self.nelem-1]))**(1/4)
        # diff_perm_flowrate = ((self.Rtot * self.parameter_config['Feed_flowrate'] /3600 *1000 - self.Mpermeate_calc)**2)**(1/4)
        for i_elem in range(0, self.nelem):
            self.average_Fw += np.average(self.Fw[:, i_elem])
            self.average_Fs += np.average(self.Fs[:, i_elem])
        self.average_Fw = self.average_Fw / self.nelem
        self.average_Fs = self.average_Fs / self.nelem
        Posm_av_2 = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.av_elem],
                                         self.Xp_out_elem[self.av_elem])
        self.average_NDP = net_driving_pressure(self.P[self.av_elem], Posm_av_2, self.parameter_config['Plosses'], self.nelem)
        for i_elem in range(0, self.nelem):
            self.aver_recovery_elem[i_elem] = np.average(self.Rel[i_elem])
            self.feed_flow_elem[i_elem] = np.average(self.Mf[:, i_elem])
            self.feed_pressure_elem[i_elem] = self.P[i_elem]
            self.feed_conc_elem[i_elem] = np.average(self.Xf[:, i_elem])
            self.conc_flow_elem[i_elem] = np.average(self.Mc[:, i_elem])
            self.permeate_flow_elem[i_elem] = np.average(self.Mp[:, i_elem])
            self.permeate_conc_elem[i_elem] = np.average(self.Xp[:, i_elem])
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr-1, self.nelem-1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr-1,  self.nelem-1]
        return diff_retentate_conc

    def resolution_singlestage_Xretobj_varAmembr(self, x):
        # self.Pfeed = Pfeed
        self.Amembr_tot = x
        # print(x)
        # if self.Pfeed < minimum_Pfeed(self.Xfeed):
        #     return np.exp(self.Xfeed)
        loop_count = 0
        diff_retentate_conc = 1000
        Rtot = global_balance_Rtot(self.Xfeed, self.Xret_out, self.parameter_config['salt_rejection_sw'])
        Mperm_req, Mconc_req = global_balance_mfeed(self.Mfeed, Rtot)
        print(Mperm_req)
        for i in range(10):
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], self.Xp_cum_elem[0, 0],
                                                          self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot / self.nelem / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem
                self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem],
                                                                        self.Xp_cum_elem[0, i_elem], self.P[i_elem],
                                                                        self.Amembr_real, self.Bmembr_real)
                self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                     / self.Mc[0, i_elem]
                self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                for i_subelem in range(1, self.ndiscr):
                    self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                    self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                    self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                            self.Xfs[i_subelem, i_elem],
                                                                                            self.Xp_cum_elem[i_subelem, i_elem],
                                                                                            self.P[i_elem],
                                                                                            self.Amembr_real, self.Bmembr_real)
                    self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot / self.nelem / self.ndiscr
                    self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                    self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                            * self.Xp[i_subelem, i_elem]
                    self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                    self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                  self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                i_subelem, i_elem]
                    self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                    self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                        i_subelem, i_elem]
                    self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                    self.Rel[i_elem] = self.Rtot / self.nelem
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            if self.Mpermeate_calc < 0:
                self.Mpermeate_calc = Mperm_req * 2
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc / self.Mfeed
            # self.Posm_out = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.nelem - 1], self.Xp_cum)
            self.nelem_av = int(self.nelem/2-1)
            # AFS_global = (self.Xc[self.ndiscr - 1, self.nelem - 1] - self.Xfeed) / np.log(self.Xc[self.ndiscr - 1, self.nelem - 1] / self.Xfeed)
            # Posm_av = self.parameter_config['osmotic_coeff'] * (AFS_global - self.Xp_cum_elem[self.ndiscr-1, self.nelem_av]) * 1e-4
            self.P_av = self.P[self.nelem_av]
            self.NPV = self.Amembr_tot / (self.nelem * self.parameter_config['membrane_area_sw'])
        diff_retentate_conc = abs((self.Xret_out**2 - self.Xc[self.ndiscr-1, self.nelem-1]**2))**(1/2)
        diff_perm_flowrate = (abs(Mperm_req**2 - self.Mpermeate_calc**2))
        # print(self.Amembr_tot, diff_perm_flowrate, diff_retentate_conc, self.Xc[self.ndiscr-1, self.nelem-1], self.Mpermeate_calc)
            # self.Amembr_tot = self.Amembr_tot * (Mperm_req / self.Mpermeate_calc)
            # if loop_count > 1000:
            #     print('N iterations > 1000')
            #     break
        # diff_perm_flowrate = ((self.Rtot * self.parameter_config['Feed_flowrate'] /3600 *1000 - self.Mpermeate_calc)**2)**(1/4)
        for i_elem in range(0, self.nelem):
            self.average_Fw += np.average(self.Fw[:, i_elem])
            self.average_Fs += np.average(self.Fs[:, i_elem])
        self.average_Fw = self.average_Fw / self.nelem
        self.average_Fs = self.average_Fs / self.nelem
        Posm_av_2 = osmotic_pressure_out(self.parameter_config, self.Xc[self.ndiscr - 1, self.av_elem],
                                         self.Xp_out_elem[self.av_elem])
        self.average_NDP = net_driving_pressure(self.P[self.av_elem], Posm_av_2, self.parameter_config['Plosses'], self.parameter_config)
        for i_elem in range(0, self.nelem):
            self.aver_recovery_elem[i_elem] = np.average(self.Rel[i_elem])
            self.feed_flow_elem[i_elem] = np.average(self.Mf[:, i_elem])
            self.feed_pressure_elem[i_elem] = self.P[i_elem]
            self.feed_conc_elem[i_elem] = np.average(self.Xf[:, i_elem])
            self.conc_flow_elem[i_elem] = np.average(self.Mc[:, i_elem])
            self.permeate_flow_elem[i_elem] = np.average(self.Mp[:, i_elem])
            self.permeate_conc_elem[i_elem] = np.average(self.Xp[:, i_elem])
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr-1, self.nelem-1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr-1,  self.nelem-1]
        return diff_perm_flowrate + diff_retentate_conc

class ROsolver_doublestage_discrete:
    def __init__(self, Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed, Mf, Mp, Mc, Mp_cum,
                 MXp_cum, Xp, Xc, Mfeed, Fw, Fs, TCF, AFS, CPF, Amembr_real, Bmembr_real, Amembr, NPV_1, NPV_2, parameter_config,
                 nelem_1, nelem_2, Plosses, Posm_out, Mpermeate_calc, Xp_cum, ndiscr, Mp_cum_elem, MXp_cum_elem, Xp_cum_elem,
                 Xfs, Xp_out_elem, Amembr_tot_1, Amembr_tot_2):
        self.Rtot = Rtot  # the condition to be fulfilled may regard the total recovery or the concentration of the outlet retentate
        self.Xret_out = Xret_out
        self.R1_calc = R1_calc
        self.toll_R1 = toll_R1
        self.Rel_calc = Rel_calc
        self.Rel = Rel
        self.toll_Rel = toll_Rel
        self.P = P
        self.Pfeed = Pfeed
        self.Xf = Xf
        self.Xfeed = Xfeed
        self.Mf = Mf
        self.Mp = Mp
        self.Mc = Mc
        self.Mp_cum = Mp_cum
        self.MXp_cum = MXp_cum
        self.Xp = Xp
        self.Xc = Xc
        self.Mfeed = Mfeed
        self.Fw = Fw
        self.Fs = Fs
        self.TCF = TCF
        self.AFS = AFS
        self.CPF = CPF
        self.Amembr_real = Amembr_real
        self.Bmembr_real = Bmembr_real
        self.Amembr = Amembr
        self.NPV_1 = NPV_1
        self.NPV_2 = NPV_2
        self.parameter_config = parameter_config
        self.nelem_1 = nelem_1
        self.nelem_2 = nelem_2
        self.Plosses = Plosses
        self.Posm_out = Posm_out
        self.Mpermeate_calc = Mpermeate_calc
        self.Xp_cum = Xp_cum
        self.ndiscr = ndiscr
        self.Mp_cum_elem = Mp_cum_elem
        self.MXp_cum_elem = MXp_cum_elem
        self.Xp_cum_elem = Xp_cum_elem
        self.Xfs = Xfs
        self.Xp_out_elem = Xp_out_elem
        self.Amembr_tot_1 = Amembr_tot_1
        self.Amembr_tot_2 = Amembr_tot_2
        self._log = []
        self.Amembr_tot_calc_1 = NPV_1 * self.parameter_config['membrane_area_sw'] * nelem_1
        self.Amembr_tot_calc_2 = NPV_2 * self.parameter_config['membrane_area_sw'] * nelem_2
        self.Amembr_tot_calc = self.Amembr_tot_calc_1 + self.Amembr_tot_calc_2
        self.Amembr_tot = self.Amembr_tot_1 + self.Amembr_tot_2
        self.average_Fw_1 = 0
        self.average_Fs_1 = 0
        self.average_NDP = 0
        self.nelem = self.nelem_1 + self.nelem_2


    def resolution_doublestage_Xretobj(self, x):
        self.Pfeed = x
        if self.Pfeed < minimum_Pfeed(self.Xfeed):
            return np.exp(self.Xfeed)
        self.Amembr_tot_1 = 0
        self.Amembr_tot_calc_1 = 10 * self.nelem_1 * self.parameter_config['membrane_area_sw']
        self.Amembr_tot_calc_2 = 5 * self.nelem_1 * self.parameter_config['membrane_area_sw']
        self.Amembr_tot_calc = self.Amembr_tot_calc_1 + self.Amembr_tot_calc_2
        while abs(self.Amembr_tot - self.Amembr_tot_calc) > self.toll_Rel:
            self.Amembr_tot = self.Amembr_tot_calc
            self.Amembr_tot_1 = self.Amembr_tot_calc_1
            self.Amembr_tot_2 = self.Amembr_tot_calc_2
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], # self.Xp_cum_elem[0, 0],   # 0,
                                                          0, self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                if i_elem < self.nelem_1:
                    self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem_1
                    self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                    self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                    self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem], # self.Xp_cum_elem[0, i_elem],
                                                                            0, self.P[i_elem], self.Amembr_real, self.Bmembr_real)
                    self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
                    self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                    self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                    self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                         / self.Mc[0, i_elem]
                    self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                    self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                    self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
                elif self.nelem_1 <= i_elem < self.nelem:
                    self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem_2
                    self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                    self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                    self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem], # self.Xp_cum_elem[0, i_elem],  #
                                                                            0, self.P[i_elem], self.Amembr_real, self.Bmembr_real)
                    self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot_2 / self.nelem_2 / self.ndiscr
                    self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                    self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                    self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[
                        0, i_elem]) / self.Mc[0, i_elem]
                    self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                    self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[
                        0, i_elem]
                    self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                    self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                if i_elem < self.nelem_1:
                    for i_subelem in range(1, self.ndiscr):
                        self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                        self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                        self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                        # if self.Xp_cum_elem[i_subelem, i_elem] < 0:
                        #     self.Xp_cum_elem[i_subelem, i_elem] = 0
                        self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                                self.Xfs[i_subelem, i_elem], 0,
                                                                                                # self.Xp_cum_elem[i_subelem, i_elem],
                                                                                                self.P[i_elem],
                                                                                                self.Amembr_real, self.Bmembr_real)
                        # if self.Fw[i_subelem, i_elem] < 0:
                        #     self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                        #                                                        self.Xfs[i_subelem, i_elem], 0, self.P[i_elem],
                        #                                                        self.Amembr_real, self.Bmembr_real)
                        self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * \
                                                     self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
                        self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                        self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                                * self.Xp[i_subelem, i_elem]
                        self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                        self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                      self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                    i_subelem, i_elem]
                        self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                            i_subelem, i_elem]
                        self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                    self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                    self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                    if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                        self.Rel[i_elem] = self.Rtot / self.nelem
                elif self.nelem_1 <= i_elem < self.nelem:
                    for i_subelem in range(1, self.ndiscr):
                        self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                        self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                        self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                        # if self.Xp_cum_elem[i_subelem, i_elem] < 0:
                        #     self.Xp_cum_elem[i_subelem, i_elem] = 0
                        self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                                self.Xfs[i_subelem, i_elem], 0,
                                                                                                # self.Xp_cum_elem[i_subelem, i_elem],
                                                                                                self.P[i_elem],
                                                                                                self.Amembr_real, self.Bmembr_real)
                        # if self.Fw[i_subelem, i_elem] < 0:
                        #     self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                        #                                                        self.Xfs[i_subelem, i_elem], 0, self.P[i_elem],
                        #                                                        self.Amembr_real, self.Bmembr_real)
                        self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot_2 / self.nelem_2 / self.ndiscr
                        self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                        self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                                * self.Xp[i_subelem, i_elem]
                        self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                        self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                      self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                    i_subelem, i_elem]
                        self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                            i_subelem, i_elem]
                        self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[
                            i_subelem, i_elem]
                    self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                    self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                    if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                        self.Rel[i_elem] = self.Rtot / self.nelem
            self.Mpermeate_calc_1 = self.Mp_cum[self.ndiscr - 1, self.nelem_1 - 1]
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            self.Mpermeate_calc_2 = self.Mpermeate_calc - self.Mpermeate_calc_1
            self.Xp_cum_1 = self.MXp_cum[self.ndiscr - 1, self.nelem_1 - 1] / self.Mpermeate_calc_1
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc_1 / self.Mfeed
            self.R2_calc = self.Mpermeate_calc_2 / self.Mc[self.ndiscr - 1, self.nelem_1 - 1]
            self.Rtot_calc = self.Mpermeate_calc / self.Mfeed
            '''variables of stage 1 used to calculate Amembr_tot_1 and NPV_1, NPV_2 is calculated as half of NPV_1'''
            self.n_elem_1_av = int(self.nelem_1/2 - 1)
            for i_elem in range(0, self.nelem_1):
                self.average_Fw_1 += np.average(self.Fw[:, i_elem])
                self.average_Fs_1 += np.average(self.Fs[:, i_elem])
            self.average_Fw_1 = self.average_Fw_1 / self.nelem_1
            self.average_Fs_1 = self.average_Fs_1 / self.nelem_1
            AFS_global_1 = (self.Xc[self.ndiscr - 1, self.nelem_1 - 1] - self.Xfeed) / np.log(self.Xc[self.ndiscr - 1, self.nelem_1 - 1] / self.Xfeed)
            Posm_av_1 = self.parameter_config['osmotic_coeff'] * (AFS_global_1 - self.Xp_cum_elem[self.ndiscr-1, self.n_elem_1_av]) * 1e-4
            self.P_av_1 = self.P[self.n_elem_1_av]
            self.Amembr_tot_calc_1 = self.Mpermeate_calc_1 / (0.9 * self.Amembr_real * (self.P_av_1 - Posm_av_1))
            self.NPV_1 = self.Amembr_tot_calc_1 / (self.nelem_1 * self.parameter_config['membrane_area_sw'])
            self.NPV_2 = self.NPV_1 / 2
            self.Amembr_tot_calc_2 = self.NPV_2 * self.nelem_2 * self.parameter_config['membrane_area_sw']
            self.Amembr_tot_calc = self.Amembr_tot_calc_1 + self.Amembr_tot_calc_2
        diff_retentate_conc = abs((self.Xret_out - self.Xc[self.ndiscr-1, self.nelem-1]))**(1/4)
        # print(self.Pfeed, self.Fw[self.ndiscr-1, 0], self.Fw[self.ndiscr-1, self.nelem_1-1], self.Fw[self.ndiscr-1, self.nelem-1], self.Amembr_tot_calc_1,
        #       diff_retentate_conc, self.Xp_cum_elem[self.ndiscr-1, 0], self.Xp_cum_elem[self.ndiscr-1, self.nelem_1-1], self.Xp_cum_elem[self.ndiscr-1, self.nelem - 1],
        #       self.Xc[self.ndiscr - 1, self.nelem - 1])
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr - 1, self.nelem - 1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr - 1, self.nelem - 1]
        return diff_retentate_conc

    def resolution_doublestage_Xretobj_varAmembr(self, x):
        self.Amembr_tot_1 = x[0]
        self.Amembr_tot_2 = x[1]
        Rtot = global_balance_Rtot(self.Xfeed, self.Xret_out, self.parameter_config['salt_rejection_sw'])
        Mperm_req, Mconc_req = global_balance_mfeed(self.Mfeed, Rtot)
        print(Mperm_req)
        # self.Pfeed = x
        # if self.Pfeed < minimum_Pfeed(self.Xfeed):
        #     return np.exp(self.Xfeed)
        for i in range(10):
            self.CPF = membrane_properties.concentration_polarization_factor(self.Rel)
            self.P[0] = self.Pfeed
            self.Xf[0, 0] = self.Xfeed
            self.Mf[0, 0] = self.Mfeed
            self.Xfs[0, 0] = self.Xf[0, 0] * self.CPF[0] * self.TCF
            self.Fw[0, 0], self.Fs[0, 0] = element_fluxes(self.parameter_config, self.Xfs[0, 0], # self.Xp_cum_elem[0, 0],   # 0,
                                                          0, self.P[0], self.Amembr_real, self.Bmembr_real)
            self.Mp[0, 0] = (self.Fw[0, 0] + self.Fs[0, 0]) * self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
            self.Xp[0, 0] = self.Fs[0, 0] / self.Fw[0, 0] * 1e6
            self.Mc[0, 0] = self.Mf[0, 0] - self.Mp[0, 0]
            self.Xc[0, 0] = (self.Mf[0, 0] * self.Xf[0, 0] - self.Mp[0, 0] * self.Xp[0, 0]) / self.Mc[0, 0]
            self.Mp_cum[0, 0] = self.Mp[0, 0]
            self.MXp_cum[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Mp_cum_elem[0, 0] = self.Mp[0, 0]
            self.MXp_cum_elem[0, 0] = self.Mp[0, 0] * self.Xp[0, 0]
            self.Xp_cum_elem[0, 0] = self.Xp[0, 0]
            for i_elem in range(1, self.nelem):
                if i_elem < self.nelem_1:
                    self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem_1
                    self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                    self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                    self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem], # self.Xp_cum_elem[0, i_elem],
                                                                            0, self.P[i_elem], self.Amembr_real, self.Bmembr_real)
                    self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
                    self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                    self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                    self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[0, i_elem])\
                                         / self.Mc[0, i_elem]
                    self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                    self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                    self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
                elif self.nelem_1 <= i_elem < self.nelem:
                    self.P[i_elem] = self.P[i_elem - 1] - self.Plosses  # / self.nelem_2
                    self.Xf[0, i_elem] = self.Xc[self.ndiscr - 1, i_elem - 1]
                    self.Mf[0, i_elem] = self.Mc[self.ndiscr - 1, i_elem - 1]
                    self.Xfs[0, i_elem] = self.Xf[0, i_elem] * self.CPF[i_elem] * self.TCF
                    self.Fw[0, i_elem], self.Fs[0, i_elem] = element_fluxes(self.parameter_config, self.Xfs[0, i_elem], # self.Xp_cum_elem[0, i_elem],  #
                                                                            0, self.P[i_elem], self.Amembr_real, self.Bmembr_real)
                    self.Mp[0, i_elem] = (self.Fw[0, i_elem] + self.Fs[0, i_elem]) * self.Amembr_tot_2 / self.nelem_2 / self.ndiscr
                    self.Xp[0, i_elem] = self.Fs[0, i_elem] / self.Fw[0, i_elem] * 1e6
                    self.Mc[0, i_elem] = self.Mf[0, i_elem] - self.Mp[0, i_elem]
                    self.Xc[0, i_elem] = (self.Mf[0, i_elem] * self.Xf[0, i_elem] - self.Mp[0, i_elem] * self.Xp[
                        0, i_elem]) / self.Mc[0, i_elem]
                    self.Mp_cum[0, i_elem] = self.Mp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem]
                    self.MXp_cum[0, i_elem] = self.MXp_cum[self.ndiscr - 1, i_elem - 1] + self.Mp[0, i_elem] * self.Xp[
                        0, i_elem]
                    self.Mp_cum_elem[0, i_elem] = self.Mp[0, i_elem]
                    self.MXp_cum_elem[0, i_elem] = self.Mp[0, i_elem] * self.Xp[0, i_elem]
                    self.Xp_cum_elem[0, i_elem] = self.Xp[0, i_elem]
            for i_elem in range(0, self.nelem):
                if i_elem < self.nelem_1:
                    for i_subelem in range(1, self.ndiscr):
                        self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                        self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                        self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                        # if self.Xp_cum_elem[i_subelem, i_elem] < 0:
                        #     self.Xp_cum_elem[i_subelem, i_elem] = 0
                        self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                                self.Xfs[i_subelem, i_elem], 0,
                                                                                                # self.Xp_cum_elem[i_subelem, i_elem],
                                                                                                self.P[i_elem],
                                                                                                self.Amembr_real, self.Bmembr_real)
                        # if self.Fw[i_subelem, i_elem] < 0:
                        #     self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                        #                                                        self.Xfs[i_subelem, i_elem], 0, self.P[i_elem],
                        #                                                        self.Amembr_real, self.Bmembr_real)
                        self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * \
                                                     self.Amembr_tot_1 / self.nelem_1 / self.ndiscr
                        self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                        self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                                * self.Xp[i_subelem, i_elem]
                        self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                        self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                      self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                    i_subelem, i_elem]
                        self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                            i_subelem, i_elem]
                        self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[i_subelem, i_elem]
                    self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                    self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                    if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                        self.Rel[i_elem] = self.Rtot / self.nelem
                elif self.nelem_1 <= i_elem < self.nelem:
                    for i_subelem in range(1, self.ndiscr):
                        self.Mf[i_subelem, i_elem] = self.Mc[i_subelem - 1, i_elem]
                        self.Xf[i_subelem, i_elem] = self.Xc[i_subelem - 1, i_elem]
                        self.Xfs[i_subelem, i_elem] = self.Xf[i_subelem, i_elem] * self.CPF[i_elem] * self.TCF
                        # if self.Xp_cum_elem[i_subelem, i_elem] < 0:
                        #     self.Xp_cum_elem[i_subelem, i_elem] = 0
                        self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                                                                                                self.Xfs[i_subelem, i_elem], 0,
                                                                                                # self.Xp_cum_elem[i_subelem, i_elem],
                                                                                                self.P[i_elem],
                                                                                                self.Amembr_real, self.Bmembr_real)
                        # if self.Fw[i_subelem, i_elem] < 0:
                        #     self.Fw[i_subelem, i_elem], self.Fs[i_subelem, i_elem] = element_fluxes(self.parameter_config,
                        #                                                        self.Xfs[i_subelem, i_elem], 0, self.P[i_elem],
                        #                                                        self.Amembr_real, self.Bmembr_real)
                        self.Mp[i_subelem, i_elem] = (self.Fw[i_subelem, i_elem] + self.Fs[i_subelem, i_elem]) * self.Amembr_tot_2 / self.nelem_2 / self.ndiscr
                        self.Xp[i_subelem, i_elem] = self.Fs[i_subelem, i_elem] / self.Fw[i_subelem, i_elem] * 1e6
                        self.Mp_cum[i_subelem, i_elem] = self.Mp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum[i_subelem, i_elem] = self.MXp_cum[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] \
                                                                                                * self.Xp[i_subelem, i_elem]
                        self.Mc[i_subelem, i_elem] = self.Mf[i_subelem, i_elem] - self.Mp[i_subelem, i_elem]
                        self.Xc[i_subelem, i_elem] = (self.Mf[i_subelem, i_elem] * self.Xf[i_subelem, i_elem] -
                                                      self.Mp[i_subelem, i_elem] * self.Xp[i_subelem, i_elem]) / self.Mc[
                                                    i_subelem, i_elem]
                        self.Mp_cum_elem[i_subelem, i_elem] = self.Mp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem]
                        self.MXp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem - 1, i_elem] + self.Mp[i_subelem, i_elem] * self.Xp[
                            i_subelem, i_elem]
                        self.Xp_cum_elem[i_subelem, i_elem] = self.MXp_cum_elem[i_subelem, i_elem] / self.Mp_cum_elem[
                            i_subelem, i_elem]
                    self.Xp_out_elem[i_elem] = self.MXp_cum_elem[self.ndiscr - 1, i_elem] / self.Mp_cum_elem[self.ndiscr - 1, i_elem]
                    self.Rel[i_elem] = self.Mp_cum_elem[self.ndiscr - 1, i_elem] / self.Mf[0, i_elem]
                    if self.Rel[i_elem] > 1 or self.Rel[i_elem] < 0:
                        self.Rel[i_elem] = self.Rtot / self.nelem
            self.Mpermeate_calc_1 = self.Mp_cum[self.ndiscr - 1, self.nelem_1 - 1]
            self.Mpermeate_calc = self.Mp_cum[self.ndiscr - 1, self.nelem - 1]
            self.Mpermeate_calc_2 = self.Mpermeate_calc - self.Mpermeate_calc_1
            self.Xp_cum_1 = self.MXp_cum[self.ndiscr - 1, self.nelem_1 - 1] / self.Mpermeate_calc_1
            self.Xp_cum = self.MXp_cum[self.ndiscr - 1, self.nelem - 1] / self.Mpermeate_calc
            self.R1_calc = self.Mpermeate_calc_1 / self.Mfeed
            self.R2_calc = self.Mpermeate_calc_2 / self.Mc[self.ndiscr - 1, self.nelem_1 - 1]
            self.Rtot_calc = self.Mpermeate_calc / self.Mfeed
            '''variables of stage 1 used to calculate Amembr_tot_1 and NPV_1, NPV_2 is calculated as half of NPV_1'''
            self.n_elem_1_av = int(self.nelem_1/2 - 1)
            for i_elem in range(0, self.nelem_1):
                self.average_Fw_1 += np.average(self.Fw[:, i_elem])
                self.average_Fs_1 += np.average(self.Fs[:, i_elem])
            self.average_Fw_1 = self.average_Fw_1 / self.nelem_1
            self.average_Fs_1 = self.average_Fs_1 / self.nelem_1
            AFS_global_1 = (self.Xc[self.ndiscr - 1, self.nelem_1 - 1] - self.Xfeed) / np.log(self.Xc[self.ndiscr - 1, self.nelem_1 - 1] / self.Xfeed)
            Posm_av_1 = self.parameter_config['osmotic_coeff'] * (AFS_global_1 - self.Xp_cum_elem[self.ndiscr-1, self.n_elem_1_av]) * 1e-4
            self.P_av_1 = self.P[self.n_elem_1_av]
            # self.Amembr_tot_calc_1 = self.Mpermeate_calc_1 / (0.9 * self.Amembr_real * (self.P_av_1 - Posm_av_1))
            self.NPV_1 = self.Amembr_tot_1 / (self.nelem_1 * self.parameter_config['membrane_area_sw'])
            self.NPV_2 = self.Amembr_tot_2 / (self.nelem_2 * self.parameter_config['membrane_area_sw'])
            # self.Amembr_tot_calc_2 = self.NPV_2 * self.nelem_2 * self.parameter_config['membrane_area_sw']
            # self.Amembr_tot_calc = self.Amembr_tot_calc_1 + self.Amembr_tot_calc_2
        diff_retentate_conc = abs((self.Xret_out ** 2 - self.Xc[self.ndiscr - 1, self.nelem - 1] ** 2)) ** (1 / 2)
        diff_perm_flowrate = (abs(Mperm_req ** 2 - self.Mpermeate_calc ** 2))
        # print(self.Amembr_tot_1, self.Amembr_tot_2, diff_perm_flowrate, diff_retentate_conc, self.Xc[self.ndiscr - 1, self.nelem - 1],
        #       self.Mpermeate_calc)
        '''definition of the outlet flow rates and concentration'''
        self.Mpermeate_out_plant = self.Mpermeate_calc
        self.Xpermeate_out_plant = self.Xp_cum
        self.Mretentate_out_plant = self.Mc[self.ndiscr - 1, self.nelem - 1]
        self.Xretentate_out_plant = self.Xc[self.ndiscr - 1, self.nelem - 1]
        return diff_retentate_conc + diff_perm_flowrate









