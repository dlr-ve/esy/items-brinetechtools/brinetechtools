import numpy as np
import logging
import math
from . import h2oprop, naclprops
logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)

'''functions to estimate the pressure drops between two effects'''

def demister_deltap(density, vapvel, thickness):  # DeltaP_dem in [Pa/m]
    deltap_dem = 3.88178 * np.power(density, 0.375798) * np.power(vapvel, 0.81317) * np.power(thickness, (-1.56114147))
    return deltap_dem


def connlines_deltap(m_vap, l_tube, rho_vap, diam_tube):  # DeltaP in [Pa/m], Mvap is the vapor mass flow rate [kg/s],
    # l_tube [m] and diam_tube is the tube inner diameter [m]
    deltap_lines = 0.0001306 * m_vap ** 2 * l_tube * (1 + 3.6 / diam_tube) / (rho_vap * diam_tube ** 5)
    return deltap_lines


def alfa(rho_v, rho_l, chi):  # alfa [-], chi is the vapor mass fraction (0<chi<1), rho_v and rho_l are the
    # density of vapor and liquid at saturation condition in [kg/m3].
    a_fraction = 1 / (1 + (1 - chi) / chi * (rho_v / rho_l) ** 0.5)
    return a_fraction


def gravitational_deltap(rho_v, rho_l, chi, angle, l_evap):  # DeltaP_grav in [Pa], L_evap in [m], theta is the inclination angle.
    deltap_grav = (rho_v * alfa(rho_v, rho_l, chi) + (1 - alfa(rho_v, rho_l, chi)) * rho_l) * 9.81 * l_evap * math.sin(math.radians(angle))
    return deltap_grav


def acceleration_deltap(m_vap, d_evap, n_tubes, rho_v1, rho_l1, rho_v2, rho_l2, chi1=0.99, chi2=0.01):  # DeltaP_acc in [Pa],
    # index = 1 for inlet, =2 for outlet conditions. rho in [kg/m3], Mvap in [kg/s], A_cross in [m2]
    area = n_tubes * np.pi * d_evap ** 2 / 4
    deltap_acc = m_vap ** 2 / area ** 2 * (chi1 ** 2 / (alfa(rho_v1, rho_l1, chi1) * rho_v1) + (1 - chi1) ** 2 /
                        ((1 - alfa(rho_v1, rho_l1, chi1)) * rho_l1) + chi2 ** 2 / (alfa(rho_v2, rho_l2, chi2) * rho_v2)
                                           + (1 - chi2) ** 2 / ((1 - alfa(rho_v2, rho_l2, chi2)) * rho_l2))
    return deltap_acc


def friction_deltap(m_vap, d_evap, n_tubes, l_tube, t, epsilon):
    m_flux = 4 * m_vap / (np.pi * np.power(d_evap, 2) * n_tubes)
    q_flux = (m_vap * h2oprop.latent_heat(t) * 1e3) / (np.pi * d_evap * l_tube * n_tubes)
    Re_liq = m_flux * d_evap / h2oprop.dynviscosity_satwat_liquid(t)
    Re_vap = m_flux * d_evap / h2oprop.dynviscosity_satwat_vapor(t)
    Co = q_flux / (m_flux * h2oprop.latent_heat(t) * 1e3)
    rho_ratio = h2oprop.density_satwat_liquid(t) / h2oprop.density_satwat_vapor(t)
    visc_ratio = h2oprop.dynviscosity_satwat_liquid(t) / h2oprop.dynviscosity_satwat_vapor(t)
    Fr_number = (m_flux / h2oprop.density_satwat_liquid(t))**2 / (9.81 * d_evap)
    We_number = (m_flux ** 2 * d_evap) / (h2oprop.density_satwat_liquid(t) * h2oprop.surface_tension_satwat(t))
    x_vap_inlet = 1
    x_vap_outlet = 0
    x_vap_midpoint = (x_vap_outlet + x_vap_inlet) / 2
    Aprime_liq = np.power((-2.457 * np.log((7/Re_liq) ** 0.9 + 0.27 * epsilon / d_evap)), 16)
    B_liq = np.power((37530 / Re_liq), 16)
    f_liq = 2 * np.power((np.power(8/Re_liq, 12) + np.power(Aprime_liq + B_liq, (-1.5))), (1/12))
    Aprime_vap = np.power((-2.457 * np.log((7 / Re_vap) ** 0.9 + 0.27 * epsilon / d_evap)), 16)
    #logger.info('m_vap, m_flux, Re_liq, Aprime_liq, epsilon, d_evap in pressure.py is: "' + str(m_vap) + str(', ') + str(m_flux) + str(', ') + str(Re_liq) + str(', ') + str(
    #    Aprime_liq) + str(', ') + str(epsilon) + str(', ') + str(d_evap) + '"')
    B_vap = np.power((37530 / Re_vap), 16)
    f_vap = 2 * np.power((np.power(8 / Re_vap, 12) + np.power(Aprime_vap + B_vap, (-1.5))), (1 / 12))
    dp_friction_1 = 2 * f_liq * m_flux**2 / (d_evap * h2oprop.density_satwat_liquid(t))
    x_vap = [x_vap_inlet, x_vap_midpoint, x_vap_outlet]
    A = [1, 1, 1]
    R = [2, 2, 2]
    for i_x, x in enumerate(x_vap):
        A[i_x] = (1 - x)**2 + x**2 * ((h2oprop.density_satwat_liquid(t) * f_vap) / (h2oprop.density_satwat_vapor(t) * f_liq))
        R[i_x] = A[i_x] + 3.43 * (np.power(x, 0.685) * np.power(1-x, 0.24)) / (np.power(Fr_number, 0.047) * np.power(We_number, 0.0334)) \
                          * np.power((h2oprop.density_satwat_liquid(t)/ h2oprop.density_satwat_vapor(t)), 0.8) \
                          * np.power((h2oprop.dynviscosity_satwat_liquid(t)/h2oprop.dynviscosity_satwat_vapor(t)) - 1, 0.89) \
                          / np.power(h2oprop.dynviscosity_satwat_liquid(t)/h2oprop.dynviscosity_satwat_vapor(t), 1.11) \
                          * ((1 + np.power(10, (-220 * epsilon / d_evap))) / 2)
    int_R = (R[0] + 4 * R[1] + R[2]) / 3 * (l_tube/2)
    DeltaP_friction = dp_friction_1 * int_R
    E = 1
    if 0 < Co < 0.0003:
        if 10 < rho_ratio < 40:
            E = 1.4
        elif 40 < rho_ratio < 600:
            E = 1
        elif 600 < rho_ratio < 1200:
            E = 0.73
        elif 1200 < rho_ratio < 2000:
            E = 0.76
    elif 0.0003 < Co < 0.0005:
        if 10 < rho_ratio < 40:
            E = 1.45
        elif 40 < rho_ratio < 150:
            E = 1
        elif 150 < rho_ratio < 600:
            E = 0.71
        elif 600 < rho_ratio < 1200:
            E = 0.77
        elif 1200 < rho_ratio < 2000:
            E = 0.6
    elif 0.0005 < Co < 0.0007:
        if 10 < rho_ratio < 40:
            E = 1.70
        elif 40 < rho_ratio < 150:
            E = 2.06
        elif 150 < rho_ratio < 600:
            E = 1.47
        elif 600 < rho_ratio < 1200:
            E = 1.74
        elif 1200 < rho_ratio < 2000:
            E = 1
    elif 0.0007 < Co < 0.0009:
        if 10 < rho_ratio < 40:
            E = 2.75
        elif 40 < rho_ratio < 150:
            E = 1.72
        elif 150 < rho_ratio < 600:
            E = 1.53
        elif 600 < rho_ratio < 1200:
            E = 1.29
        elif 1200 < rho_ratio < 2000:
            E = 1.13
    elif 0.0009 < Co < 0.0015:
        if 10 < rho_ratio < 40:
            E = 3.70
        elif 40 < rho_ratio < 150:
            E = 1.91
        elif 150 < rho_ratio < 600:
            E = 1.65
        elif 600 < rho_ratio < 1200:
            E = 1.23
        elif 1200 < rho_ratio < 2000:
            E = 1.43
    elif Co > 0.0015:
        if 10 < rho_ratio < 40:
            E = 4
        elif 40 < rho_ratio < 150:
            E = 3
        elif 150 < rho_ratio < 600:
            E = 2.34
        elif 600 < rho_ratio < 1200:
            E = 2.19
        elif 1200 < rho_ratio < 2000:
            E = 3.58
    DeltaP_acc = m_flux**2 * (x_vap_outlet / h2oprop.density_satwat_vapor(t) - x_vap_inlet / h2oprop.density_satwat_vapor(t) -
                              (x_vap_outlet - x_vap_inlet) / h2oprop.density_satwat_liquid(t))
    DeltaPtot = (DeltaP_friction + DeltaP_acc) * E
    return DeltaPtot