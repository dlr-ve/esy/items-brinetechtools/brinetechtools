from ...common.properties import density_mixtures


def crystallizer_start(tech_config_and_params, Cfeed_dict_molm3, feed_vol_flow_m3h, use_NaOH_Mg_mol_as_tot_mol=False):
    """
    Calculates the required parameters for the crystallizer process, including feed mols, density,
    feed concentrations, and total NaOH required based on the feed composition and configuration.
    
    Parameters:
    - parameter_config: dict, configuration parameters for the crystallizer
    - Cfeed_dict_molm3: dict, feed composition in molar concentrations for each ion (e.g., 'Mg', 'Ca') in mol/m³
    - feed_vol_flow_m3h: float, total feed flow rate in m³/h
    - use_NaOH_Mg_mol_as_tot_mol: bool, if True, total NaOH moles are set to Mg-related NaOH mols

    Returns:
    - feed_Mg_mol_flow_molh: float, moles of Mg in the feed (mol/h)
    - feed_Ca_mol_flow_molh: float, moles of Ca in the feed (mol/h)
    - rho_feed: float, density of the feed mixture (kg/m³)
    - Cfeed_dict_ppm: dict, feed composition in ppm for each ion
    - C_Na_NaOHsol_ppm: float, concentration of Na in NaOH solution in ppm
    - feed_NaOH_tot_mol_flow_molh: float, total moles of NaOH required (mol/h)
    """

    feed_Mg_mol_flow_molh = feed_vol_flow_m3h * Cfeed_dict_molm3['Mg']
    feed_Ca_mol_flow_molh = feed_vol_flow_m3h * Cfeed_dict_molm3['Ca']

    rho_feed = density_mixtures(tech_config_and_params, Cfeed_dict_molm3)

    Cfeed_dict_ppm = {}
    for ion in Cfeed_dict_molm3.keys():
        Cfeed_dict_ppm[ion] = Cfeed_dict_molm3[ion] / rho_feed * tech_config_and_params['MW_'+ion] * 1000
    # TODO: Check if this variable should be used?
    # C_NaOH_ppm = tech_config_and_params['NaOH_sol']['conc'] * 1000 / tech_config_and_params['NaOH_sol']['density'] \
    #             * tech_config_and_params['MW_NaOH'] * 1000
    C_Na_NaOHsol_ppm = tech_config_and_params['NaOH_sol']['conc'] * 1000 / tech_config_and_params['NaOH_sol']['density'] \
        * tech_config_and_params['MW_Na'] * 1000
    # every atom of Mg requires 2 moles of NaOH (right side of eqn)
    feed_NaOH_for_Mg_mol_flow_molh = feed_Mg_mol_flow_molh * tech_config_and_params['stoichiometric_coeff']['Mg'] * tech_config_and_params['excess_NaOH']
    # every atom of Ca requires 2 moles of NaOH (right side of eqn)
    feed_NaOH_for_Ca_mol_flow_molh = feed_Ca_mol_flow_molh * tech_config_and_params['stoichiometric_coeff']['Ca'] * tech_config_and_params['excess_NaOH']
    feed_NaOH_tot_mol_flow_molh = feed_NaOH_for_Ca_mol_flow_molh + feed_NaOH_for_Mg_mol_flow_molh
    if use_NaOH_Mg_mol_as_tot_mol:
        # Here, the total NaOH feed is assumed to be only equal to that required for Mg. Why not the combination og Mg+Ca as calculated above? Is this done on purpose?  # Nikhil
        feed_NaOH_tot_mol_flow_molh = feed_NaOH_for_Mg_mol_flow_molh

    return feed_Mg_mol_flow_molh, feed_Ca_mol_flow_molh, rho_feed, Cfeed_dict_ppm, C_Na_NaOHsol_ppm, feed_NaOH_tot_mol_flow_molh
