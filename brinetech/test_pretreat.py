
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

df_output = pd.read_excel('./results/pretreatment/arabian_gulf_kuwait/average_tds/results_qfeedm3h_train=200.0.xls')

df_output

#%%

# mass and energy flow
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np'
no_of_trains = df_output.iloc[0,1]
Qfeed_out_m3h_tot = float(df_output.iloc[0,5]) * no_of_trains
recovery_pretreat = float(df_output.iloc[2,1])
specific_energy_consumption = float(df_output.iloc[3,1])   # kWh_el/m3_feed_out

# costs
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np'
capital_cost_tot = float(df_output.iloc[1,10]) * no_of_trains  # [USD]
capex_tot = float(df_output.iloc[1,9]) * no_of_trains  # [USD/y]
fixed_opex_tot = float(df_output.iloc[3,9]) * no_of_trains  # [USD/y]
variable_opex_chemicals_tot = float(df_output.iloc[6,9]) * no_of_trains  # [USD/y]
variable_opex_elec_tot = float(df_output.iloc[5,9]) * no_of_trains  # [USD/y]
variable_opex_total_tot = variable_opex_chemicals_tot + variable_opex_elec_tot
total_opex_tot = fixed_opex_tot + variable_opex_total_tot  # [USD/y]
total_annual_cost_tot = capex_tot + total_opex_tot

print(f"no_of_trains: {no_of_trains}, Qfeed_out_m3h: {Qfeed_out_m3h_tot}, recovery_pretreat: {recovery_pretreat}, specific_energy_consumption: {specific_energy_consumption}")
print(f"capital_cost_tot: {capital_cost_tot}, capex_tot: {capex_tot}, fixed_opex_tot: {fixed_opex_tot}, variable_opex_elec_tot: {variable_opex_elec_tot}, variable_opex_chemicals_tot: {variable_opex_chemicals_tot}, total_annual_cost_tot: {total_annual_cost_tot}")

# %%
