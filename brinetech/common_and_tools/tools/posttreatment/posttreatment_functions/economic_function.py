import numpy as np
from ....common import calculate_annual_chemical_cost

def estimation_cost_posttreatment(Qfeed_m3h, tech_config_and_params, proj_config_and_params):
    """
    Estimates both the capital and annual chemical costs for post-treatment processes based on feed volume, project configuration, and economic parameters.

    Parameters:
        Qfeed_m3h (float, list, or np.ndarray): The feed flow rate in cubic meters per hour (m³/h). Can be a single value or a list/array representing multiple values.
        project_config (dict): Configuration dictionary containing details on post-treatment processes (e.g., stabilization and disinfection methods).
        tech_config_and_params (dict): Dictionary of technical parameters such as required chemical concentrations, commercial concentrations, operation times, and specific costs.
        proj_config_and_params (dict): Contains tech_config_and_params settings like post-treatment processes (e.g., stabilization and disinfection methods). Among params, contains economic parameters including CEPCI (Chemical Engineering Plant Cost Index) for current and base years.

    Returns:
        tuple: Contains the following two elements:
            - total_capital_cost_post_treatment (float): Total capital cost for feed stabilization and disinfection [in USD].
            - total_annual_cost_post_treatment (float): Total annual operating cost for feed stabilization and disinfection [in USD].

    Raises:
        SystemExit: If the formulation for certain treatments (e.g., annual chemical cost for calcite with CO2 or chlorine dioxide) is missing.

    Notes:
        - The capital cost estimation is adjusted based on the current and reference CEPCI values.
        - The stabilization methods supported are "lime_with_co2" and "calcite_with_co2".
        - The disinfection methods supported are "NaHypochlorite" and "chlorine_dioxide".
        - The calculations are based on formulas derived from cost estimation models, adjusted for the relevant scale and CEPCI index.

    Example Usage:
        >>> estimation_cost_posttreatment_updated(100, project_config, tech_config_and_params, eco_param)
        (total_capital_cost_post_treatment, total_annual_cost_post_treatment)
    """
    
    total_Qfeed_m3h = sum(Qfeed_m3h) if isinstance(Qfeed_m3h, (list, np.ndarray)) else Qfeed_m3h
    total_Qfeed_m3day = total_Qfeed_m3h * 24
    
    ## Capital cost
    # Construction cost varies between 150-230 USD_2018/m3_perm/day (avg. 190) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    if total_Qfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_posttreatment = tech_config_and_params["spec_capital_cost"]["upper_limit"] * total_Qfeed_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= total_Qfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_posttreatment = tech_config_and_params["spec_capital_cost"]["average"] * total_Qfeed_m3day
    elif total_Qfeed_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_posttreatment = tech_config_and_params["spec_capital_cost"]["lower_limit"] * total_Qfeed_m3day

    
        ### Operational cost of chemicals
    # modify Vperm based on the share of it being treated (based on treatment method)
    stabilization_method = proj_config_and_params["posttreatment"]["permeate_stabilization"]
    total_Qfeed_treated_stabil_m3h = total_Qfeed_m3h * tech_config_and_params["perm_flow_share_stabilization"][stabilization_method]
    total_Qfeed_treated_stabil_m3day = total_Qfeed_m3day * tech_config_and_params["perm_flow_share_stabilization"][stabilization_method]  # needed only if capital cost calculated using Nikolay 2018 graph functions

    # Determine feed stabilization method and calculate costs
    if stabilization_method == "lime_with_co2":
        annual_cost_permeate_stabilization = (
            calculate_annual_chemical_cost(total_Qfeed_treated_stabil_m3h, 'Ca(OH)2', tech_config_and_params, proj_config_and_params) +
            calculate_annual_chemical_cost(total_Qfeed_treated_stabil_m3h, 'CO2', tech_config_and_params, proj_config_and_params)
        )
        #capital_cost_permeate_stabilization = ((-1e-17*total_Qfeed_treated_stabil_m3day**4) + (5e-12*total_Qfeed_treated_stabil_m3day**3) - (1e-6*total_Qfeed_treated_stabil_m3day**2) + (0.1136*total_Qfeed_treated_stabil_m3day) + 390.28) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif stabilization_method == "calcite_with_co2":
        
        #capital_cost_permeate_stabilization = ((-6e-18*total_Qfeed_treated_stabil_m3day**4) + (3e-12*total_Qfeed_treated_stabil_m3day**3) - (5e-7*total_Qfeed_treated_stabil_m3day**2) + (0.0591*total_Qfeed_treated_stabil_m3day) + 219.99) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        raise SystemExit("Error: Formulation for annual chemical cost using calcite with CO2 for feed stabilization is missing. Script terminated.")

    # Determine disinfection method and calculate costs
    disinfectant_method = proj_config_and_params["posttreatment"]["disinfectant"]
    if disinfectant_method == "NaHypochlorite":
        annual_cost_permeate_disinfectant = calculate_annual_chemical_cost(total_Qfeed_m3h, 'NaHypochlorite', tech_config_and_params, proj_config_and_params)
        #capital_cost_permeate_disinfectant = ((-1e-18*total_Qfeed_m3day**4) + (5e-13*total_Qfeed_m3day**3) - (8e-8*total_Qfeed_m3day**2) + (0.009*total_Qfeed_m3day) + 34.285) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
    elif disinfectant_method == "chlorine_dioxide":
        #capital_cost_permeate_disinfectant = ((-6e-18*total_Qfeed_m3day**4) + (2e-12*total_Qfeed_m3day**3) - (2e-7*total_Qfeed_m3day**2) + (0.0165*total_Qfeed_m3day) + 39.919) * 1000 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018']
        raise SystemExit("Error: Formulation for annual chemical cost using chlorine_dioxide for feed disinfection is missing. Script terminated.")

    # Calculate total capital and annual costs
    #capital_cost_posttreatment = capital_cost_permeate_stabilization + capital_cost_permeate_disinfectant
    annual_posttreatment_chem_cost = annual_cost_permeate_stabilization + annual_cost_permeate_disinfectant

    return capital_cost_posttreatment * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'], annual_posttreatment_chem_cost


def estimation_electric_cost_for_vol(spec_el_consumption, Qfeed_m3h, electricity_cost, tech_config_and_params):  #Qfeed_m3h in m3/h, spec_el_consumption in kWh/m3
    operating_hours = 8760 * tech_config_and_params['plant_availability']
    electric_consumption = spec_el_consumption * Qfeed_m3h * operating_hours # [kWh/y]
    annual_el_cost = electric_consumption * electricity_cost # [US$/y]
    return annual_el_cost



