
def calculate_annual_chemical_cost(Volume_m3h, base_key, tech_params_dict, proj_params_dict):
    """
    Calculates the annual chemical cost for a given post-treatment process, adjusting for the Chemical Engineering Plant Cost Index (CEPCI).

    Parameters:
        Volume_m3h (float): The flow rate of permeate in cubic meters per hour (m³/h).
        base_key (str): The base key for identifying specific cost and related parameters (e.g., 'chlorination', 'coagulant').
        tech_params_dict (dict): A dictionary containing technical parameters with keys for specific cost, needed concentration, commercial concentration, and operation time. Keys should follow the pattern:
            - 'specific_cost_<base_key>_<year>': Specific cost of the chemical.
            - 'needed_perc_<base_key>': The required concentration of the chemical in milligrams per liter (mg/L).
            - 'comm_conc_<base_key>': The commercial concentration of the chemical as a percentage-factor.
            - 'operation_time_<base_key>': The operational time factor as a fraction of total operation time.
        proj_params_dict (dict): A dictionary containing CEPCI values under the 'CEPCI' key. It should have:
            - 'current': The current CEPCI value.
            - '<year>': CEPCI values for specific years.

    Returns:
        float: The estimated annual chemical cost, adjusted for the current CEPCI value.

    Formula:
        The formula used is:
        cost = Volume_m3h * 8760 * 1e-6 * (needed_conc_mg_per_l / commercial_conc_perc_factor * operation_time * specific_cost)
        adjusted_cost = cost * (proj_params_dict['CEPCI']['current'] / proj_params_dict['CEPCI'][year_of_cost])

    Explanation:
        - `8760` represents the number of hours in a year (365 days * 24 hours).
        - `1e-6` converts milligrams per liter (mg/L) to tons per cubic meter (t/m³).
        - The cost calculation considers the ratio of needed concentration to commercial concentration and adjusts for the operational time.
        - The cost is adjusted based on CEPCI values to reflect current costs relative to the year of the specific cost.

    Example Usage:
        >>> tech_params_dict = {
        ...     'specific_cost_chlorination_2018': 150.0,
        ...     'needed_perc_chlorination': 0.5,
        ...     'comm_conc_chlorination': 10.0,
        ...     'operation_time_chlorination': 0.8
        ... }
        >>> proj_params_dict = {'CEPCI': {'current': 600, '2018': 567}}
        >>> calculate_annual_chemical_cost_updated(1000, 'chlorination', tech_params_dict, proj_params_dict)
        849.22
    """
    
    # Find specific cost and year from base_key
    adjusted_specific_cost = find_adjusted_cost(base_key, tech_params_dict, proj_params_dict)

    # Calculate the annual chemical cost [USD_current/year]
    annual_chemical_cost = Volume_m3h * 8760 * 1e-6 * (
        tech_params_dict[f"needed_perc_{base_key}"] /
        tech_params_dict[f"comm_conc_{base_key}"] *
        tech_params_dict[f"operation_time_{base_key}"] *
        adjusted_specific_cost
    )
    
    return annual_chemical_cost


def find_adjusted_cost(base_key, tech_params_dict, proj_params_dict):
    """
    Finds and adjusts the cost associated with a specific chemical process based on the CEPCI index.

    The function searches for keys in the `tech_params_dict` following the format 'specific_cost_<base_key>_<year>'.
    It extracts the cost value (in USD/ton) and the year, then adjusts the cost using the current CEPCI index relative to the index
    of the extracted year.

    Args:
        base_key (str): The base part of the key to search for in the dictionary, e.g., 'coagulant'. 
                        The function expects keys in the format 'specific_cost_<base_key>_<year>'.
        tech_params_dict (dict): A dictionary containing the specific costs with their corresponding years. 
                                 The keys should be in the format 'specific_cost_<base_key>_<year>'.
        proj_params_dict (dict): A dictionary containing the CEPCI indices, where 'current' refers to the current 
                                 CEPCI index, and other keys correspond to the year of the specific cost.

    Raises:
        KeyError: If no key matching the pattern 'specific_cost_<base_key>_<year>' is found in the `tech_params_dict`.

    Returns:
        float: The adjusted cost value based on the current CEPCI index.

    Example:
        Given a `tech_params_dict` like:

        tech_params_dict = {
            'specific_cost_coagulant_2018': 150.0,  # in USD/ton
            'specific_cost_floacculant_2019': 160.0,  # in USD/ton
            'specific_cost_sulphacid_2020': 140.0  # in USD/ton
        }

        And a `proj_params_dict` like:

        proj_params_dict = {
            'CEPCI': {
                'current': 600.0,
                '2018': 550.0,
                '2019': 560.0,
                '2020': 570.0
            }
        }

        Calling `find_adjusted_cost('coagulant', tech_params_dict, proj_params_dict)` will return:

        163.64  # Adjusted cost for 'coagulant' to current year (in USD/ton).

        If the key 'specific_cost_coagulant_2018' does not exist, a KeyError will be raised.
    """

    for key in tech_params_dict:
        if key.startswith(f'specific_cost_{base_key}'):
            # Extract the year from the key
            year_of_cost = key.replace(f'specific_cost_{base_key}_', '')
            # Extract the specific cost
            specific_cost = tech_params_dict[key]
            # Adjust the cost based on the CEPCI index
            ce_index = proj_params_dict['CEPCI']
            
            adjusted_specific_cost = specific_cost * (ce_index['current'] / ce_index[year_of_cost])
            
            return adjusted_specific_cost
        
    raise KeyError(f"No key found matching 'specific_cost_{base_key}_<year>' in the configuration dictionary.")
