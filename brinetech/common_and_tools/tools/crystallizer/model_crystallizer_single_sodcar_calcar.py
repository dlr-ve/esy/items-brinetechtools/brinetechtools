import numpy as np

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_mixtures
from ...common import calculate_pump_efficiency
from ...common.iotools import ResultList

#def crystallizer_model(tech_config_and_params, proj_config_and_params, Cfeed, N_trains, Qfeed, electricity_cost):  # Qfeed in m3/h
def crystallizer_model(tech_config_and_params, proj_config_and_params, Cfeed, N_trains, Qfeed, electricity_cost):  # Qfeed in m3/h
    Qfeed_Mg_mol = Qfeed * Cfeed['Mg']  # mol/h
    rho_feed = density_mixtures(tech_config_and_params, Cfeed)
    Cfeed_ppm = {}
    for ion in Cfeed.keys():
        Cfeed_ppm[ion] = Cfeed[ion] / rho_feed * tech_config_and_params['MW_'+ion] * 1000
    Mfeed_kg_h = Qfeed * rho_feed

    '''CaCO3 precipitation with NaCO3 solution'''
    Q_Ca_effluent_out_1st_mol = Qfeed * Cfeed['Ca']  # mol/h
    Q_CO3_req_mol = Q_Ca_effluent_out_1st_mol * 1  # [mol/h]
    print('Q_CO3_req_mol',Q_CO3_req_mol)
    Q_CO3_avail_mol = Qfeed * Cfeed['CO3']  # mol/h  ## Nikhil: As per Caroline (Eurecat), brine will also contain some CO3 ions
    print('Q_CO3_avail_mol', Q_CO3_avail_mol)
    #Q_Na2CO3_sol_req_mol = Q_CO3_req_mol * 1  # [mol/h]
    Q_Na2CO3_sol_req_mol = (Q_CO3_req_mol - Q_CO3_avail_mol) * 1  # [mol/h]  ## Nikhil
    Q_Na2CO3_sol_req_mass = Q_Na2CO3_sol_req_mol / tech_config_and_params['conc_Na2CO3_sol'] / 1000 * tech_config_and_params['Na2CO3_sol_density']  # [kg/h]
    Q_Na2CO3_sol_req_vol = Q_Na2CO3_sol_req_mass / tech_config_and_params['Na2CO3_sol_density']  # m3/h
    Q_CaCO3_prod_mol = Q_Ca_effluent_out_1st_mol * 1  # [mol/h] Nikhil: Ca ions are the limiting reactant and not CO3, which we have in brine and also get from Na2CO3
    Q_CaCO3_out_vol = Q_CaCO3_prod_mol * tech_config_and_params['MW_CaCO3'] / tech_config_and_params['CaCO3_density']
    Q_CaCO3_prod_mass = Q_CaCO3_prod_mol * tech_config_and_params['MW_CaCO3'] / 1000 # [kg/h]
    M_effluent_out = Mfeed_kg_h + Q_Na2CO3_sol_req_mass - Q_CaCO3_prod_mass  ## Nikhil
    C_effluent_out_ppm = {}
    C_effluent_out_M = {}
    ## In below for loop, Nikhil replaced Meffluent_Mg_rec_kg_h with Mfeed_kg_h and C_effluent_Mg_rec_ppm with Cfeed_ppm
    for ion in Cfeed.keys():
        if ion == 'Ca':
            C_effluent_out_ppm[ion] = (Mfeed_kg_h * Cfeed_ppm[ion] - Q_CaCO3_prod_mol
                                       * tech_config_and_params['MW_' + ion] * 1000) / M_effluent_out
            if C_effluent_out_ppm[ion] < 0:
                C_effluent_out_ppm[ion] = 0
        elif ion == 'Na':
            C_effluent_out_ppm[ion] = (Mfeed_kg_h * Cfeed_ppm[ion]
                                       + Q_Na2CO3_sol_req_mol * 2 * tech_config_and_params[
                                           'MW_' + ion] * 1000) / M_effluent_out
        ## Nikhil: CO3 in effluent_out: already in brine + what we get from Na2CO3 sol - what goes out in mineral form
        ## with CaCO3
        elif ion == 'CO3':
            C_effluent_out_ppm[ion] = (Mfeed_kg_h * Cfeed_ppm[ion]
                                       + (Q_Na2CO3_sol_req_mol - Q_CaCO3_prod_mol) * tech_config_and_params[
                                           'MW_' + ion] * 1000) / M_effluent_out
            # Nikhil: for some reason CO3 ppm in output is negative. So this workaround (not sure if it is scientific)
            ## Possible reasoning to make it zero: we use up all the CO3 from the feed and add only enough NaCO3
            ## to make equivalent moles of CaCO3 using the CO3 from these two sources. Thus, no CO3 left in effluent out
            if C_effluent_out_ppm[ion] < 0:
                C_effluent_out_ppm[ion] = 0
        else:
            C_effluent_out_ppm[ion] = (Mfeed_kg_h * Cfeed_ppm[ion]) / M_effluent_out
    for ion in Cfeed.keys():
        C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / tech_config_and_params['MW_' + ion]  ## what is this? unit?
    #print('type of c_effluent_put_m', type(C_effluent_out_M))
    Q_effluent_out = M_effluent_out / density_mixtures(tech_config_and_params, C_effluent_out_M) ## Is unit of C_effluent_out_M = mol/m3 to be used in density function?
    Qeffl_out_calc = 0
    ## What is happening in the while function?
    while abs(Q_effluent_out - Qeffl_out_calc) > 1e-4:
        Q_effluent_out = Qeffl_out_calc
        for ion in Cfeed.keys():
            C_effluent_out_M[ion] = C_effluent_out_ppm[ion] / 1000 / tech_config_and_params['MW_' + ion] * \
                                             density_mixtures(tech_config_and_params, C_effluent_out_M)
        Qeffl_out_calc = M_effluent_out / density_mixtures(tech_config_and_params, C_effluent_out_M)

    '''CaCO3 step costs'''
    #total_flow_rate_2 = Q_effluent_Mg_rec  ## Nikhil
    total_flow_rate_2 = Qfeed
    velocity_2 = tech_config_and_params['fluid_vel'] * 3600
    section_area_2 = total_flow_rate_2 / velocity_2
    cryst_volume_2 = section_area_2 * tech_config_and_params['reactor_length']
    Cp_0_cryst_CaCO3 = 10 ** (tech_config_and_params['k1_cryst_batch'] + tech_config_and_params['k2_cryst_batch'] * np.log10(cryst_volume_2)
                    + tech_config_and_params['k3_cryst_batch'] * np.log10(cryst_volume_2) ** 2)
    Cp_0_cryst_current_CaCO3 = Cp_0_cryst_CaCO3 * proj_config_and_params['CEPCI']['current'] / tech_config_and_params[
                    'CEPCI_reference']
    Cbm_cryst_CaCO3 = Cp_0_cryst_current_CaCO3 * (tech_config_and_params['b1_cryst_batch'] + tech_config_and_params['b2_cryst_batch'] *
                    tech_config_and_params['Fm_cryst_batch'] * tech_config_and_params['Fp_cryst_batch'])
    ## Add filter element (Marina had considered filter in the dolime part and miltiplied directly by 2)
    A_filter_Ca = tech_config_and_params['design_ratio_filter'] * Q_CaCO3_out_vol * tech_config_and_params['CaCO3_density'] / 1000
    A_filter = A_filter_Ca
    Cp_0_filter_Ca = 10**(tech_config_and_params['k1_disc-drum_filter'] + tech_config_and_params['k2_disc-drum_filter'] *
                      np.log10(A_filter) + tech_config_and_params['k3_disc-drum_filter'] * np.log10(A_filter)**2)
    Cp_0_filter_current_Ca = Cp_0_filter_Ca * proj_config_and_params['CEPCI']['current'] / tech_config_and_params['CEPCI_reference']
    Cbm_filter_Ca = Cp_0_filter_current_Ca * tech_config_and_params['Fbm_disc-drum_filter']

    spec_cost_Na2CO3_cryst_mol = tech_config_and_params['specific_cost_Na2CO3_cryst'] * 1e-6 * tech_config_and_params[
                    'MW_Na2CO3']  # [$/mol]
    spec_cost_water_l = tech_config_and_params['specific_cost_water'] / 1000  # [$/l]
    specific_cost_Na2CO3_sol = (spec_cost_Na2CO3_cryst_mol * tech_config_and_params['conc_Na2CO3_sol'] + spec_cost_water_l *
                              (1 - tech_config_and_params['conc_Na2CO3_sol'] * tech_config_and_params['MW_Na2CO3'] /
                               (tech_config_and_params['Na2CO3_density'] / 1000))) * 1000  # [$/m3]
    print('Na2CO3 sol', specific_cost_Na2CO3_sol)
    cost_Na2CO3_sol = specific_cost_Na2CO3_sol * Q_Na2CO3_sol_req_vol \
                      * 8760 * tech_config_and_params['plant_availability']
    revenue_CaCO3_cryst = tech_config_and_params['price_CaCO3_cryst'] / 1e3 * Q_CaCO3_prod_mass *\
                          8760 * tech_config_and_params['plant_availability']

    '''total costs'''
    #Cbm_tot_cryst = Cbm_cryst_Mg + Cbm_cryst_CaCO3  ## Nikhil
    Cbm_tot_cryst = Cbm_cryst_CaCO3
    Cbm_tot_filter = Cbm_filter_Ca  # Previously multiplied by 2, since there was Mg and Ca

    total_annual_capex = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'],
                                           (Cbm_tot_cryst + Cbm_tot_filter))

    pump_efficiency = calculate_pump_efficiency(total_flow_rate_2*24)
    pump_power_cryst = tech_config_and_params['Pscarico_pump_cryst'] * 1e5 * total_flow_rate_2\
                       / 3600 / pump_efficiency *1e-3  # [kW]
    flowrate_sedim_Ca = Q_CaCO3_prod_mass / tech_config_and_params['magma_density_sedim_Ca']  # [m3/h]
    power_Ca_filtration = tech_config_and_params['ref_consumption_filter'] / tech_config_and_params['ref_flowrate_filter'] * flowrate_sedim_Ca
    power_filtration_tot = power_Ca_filtration
    total_power = pump_power_cryst + power_filtration_tot
    cost_electric_energy_demand = total_power * electricity_cost * 8760 * tech_config_and_params['plant_availability']
    total_opex = cost_Na2CO3_sol + cost_electric_energy_demand   # [$/y]

    crystallizer_inputs = ResultList(title="crystallizer inputs", xls_sheet="crystallizer", results=[
        ('No. of trains', (N_trains, '-')),
        ('Qfeed per train', (Qfeed, 'm3/h')),
        ('rho feed', (rho_feed, 'kg/m3')),
        ('Mfeed', (Mfeed_kg_h, 'kg/h')),
        ('conc Na2CO3 sol', (tech_config_and_params['conc_Na2CO3_sol'], 'mol/l')),
        ('rho Na2CO3 sol', (tech_config_and_params['Na2CO3_sol_density'], 'kg/m3')),
        ('ion', ('C [mol/m3', 'C [ppm]')),
    ] + [(key, (Cfeed[key], Cfeed_ppm[key])) for key in Cfeed.keys()])

    crystallizer_results = ResultList(title="crystallizer results", xls_sheet="crystallizer", xls_y0=4, results=[
         ('Q Ca effluent', (Q_Ca_effluent_out_1st_mol, 'mol/h')),
         ('Q Na2CO3 mol', (Q_Na2CO3_sol_req_mol, 'mol/h')),
         ('Q Na2CO3 mass', (Q_Na2CO3_sol_req_mass, 'kg/h')),
         ('Q CaCO3 tot mass', (Q_CaCO3_prod_mass, 'kg/h')),
         ('M effluent out', (M_effluent_out, 'kg/h')),
         ('rho effluent out', (density_mixtures(tech_config_and_params, C_effluent_out_M), 'kg/m3')),
         ('Q effluent out', (Q_effluent_out, 'm3/h')),
         ('ion', (
             'C ppm out',
             'C mol/m3 out')),
    ] + [
         (key, (
             C_effluent_out_ppm[key],
             C_effluent_out_M[key]
         )) for key in Cfeed.keys()
    ])

    cost_results = ResultList(title="cost results", xls_sheet="crystallizer", xls_y0=12, results=[
        ('Na2CO3 sol cost',      (cost_Na2CO3_sol, '$/y')),
        ('electric cons cryst',  (pump_power_cryst, 'kW')),
        ('electric cons filter', (power_filtration_tot, 'kW')),
        ('cost electricity',     (cost_electric_energy_demand, '$/y')),
        ('cost crystallizer',    (Cbm_tot_cryst, '$')),
        ('cost filter',          (Cbm_tot_filter, '$')),
        ('annual capex',         (total_annual_capex, '$/y')),
        ('annual opex',          (total_opex, '$/y')),
        ('revenue CaCO3',        (revenue_CaCO3_cryst,  '$/y')),
    ])

    return [crystallizer_inputs, crystallizer_results, cost_results]
