
#%%

# You can read inputs with
# 
#       RCE.read_input(String input_name)
# 
# You can write outputs with
# 
#       RCE.write_output(String output_name, OutputDataType value)
#
# Thereby, the type (OutputDataType) of the value must fit the data type of the output (as defined in the tab Inputs/Outputs). File and Directory are represented by the absolute file paths.
#
# (Note: The module RCE used is already imported in the script during execution.)
#
# Examples:
# - If you like to double an incoming value (x is an input of type Integer and y an output of type Integer):
#       RCE.write_output("y", 2 * RCE.read_input("x"))
# - If you like to access an incoming file (f_in is an input of type File):
#       file = open(RCE.read_input("f_in"),"r")
# - If you like to send a file to an output (f_out is an output of type File):
#       absolute_file_path = /home/user_1/my_file.txt
#       RCE.write_output("f_out", absolute_file_path)
#


#import sys
import yaml
#import xlrd
#import xlwt
from yaml import Loader, Dumper
import pandas as pd

def density_mixtures(param_config, conc_dict):  # conc in mol/m3, v_molar and alpa in [cm3/mol], Mw in g/mol, rho_mix in kg/m3
    conc_molarity = {}
    mole_fraction = {}
    for ion in conc_dict.keys():
        conc_molarity[ion] = conc_dict[ion] / 1000
    l_ions_lsol = 0
    for ion in conc_dict.keys():
        l_ions_lsol += conc_molarity[ion] * param_config['v_molar_'+ion] * 1e-3
    l_H2O_lsol = 1 - l_ions_lsol
    molarity_H2o = l_H2O_lsol * 1000 / param_config['MW_water']
    mol_tot_lsol = molarity_H2o
    for ion in conc_dict.keys():
        mol_tot_lsol += conc_molarity[ion]
    for ion in conc_dict.keys():
        mole_fraction[ion] = conc_molarity[ion] / mol_tot_lsol
    sum_xi_Mi = 0
    sum_xi = 0
    sum_vi_xi = 0
    sum_ai_xi = 0
    for ion in conc_dict.keys():
        sum_xi_Mi += mole_fraction[ion] * param_config['MW_'+ion]
        sum_xi += mole_fraction[ion]
        sum_vi_xi += mole_fraction[ion] * param_config['v_molar_'+ion]
        sum_ai_xi += mole_fraction[ion] * param_config['alpha_'+ion]
    rho_mix = (sum_xi_Mi + param_config['MW_water'] * (1 - sum_xi)) / (sum_vi_xi + param_config['v_molar_water'] *
                                                                       (1-sum_xi) + sum_ai_xi * (1-sum_xi)) * 1000
    return rho_mix

parameter_file = r"O:/ESY/06_Projekte-ST/ZERO BRINE/22_ToolRelease/NFmodelTool_Rejgiven/parameters.yml"

excel_NF_path = "./results_Rgiven_noCorrFactor_qfeed=600.0_NF_1.xls"
df_nf_output = pd.read_excel(excel_NF_path)

with open(parameter_file, "r") as file_params:
    parameter_dict = yaml.load(file_params, Loader=Loader)

## Mass and volume flow rates
density_perm_kgm3 = df_nf_output.iloc[6, 19]
Qperm_m3s_NF = df_nf_output.iloc[2, 19]
Qperm_m3h_NF = Qperm_m3s_NF*3600
Mperm_kgs_NF = Qperm_m3s_NF * density_perm_kgm3
Qretent_m3s_NF = df_nf_output.iloc[1, 19]
Qretent_m3h_NF = Qretent_m3s_NF*3600

print(Qperm_m3s_NF, Mperm_kgs_NF, Qretent_m3s_NF)

#%%

## Chain inputs (###PS: Only when NF is the first component in the chain)
Mfeed_chain_kgs = df_nf_output.iloc[6, 1]
rhofeed_chain = df_nf_output.iloc[5, 1]

print(Mfeed_chain_kgs, rhofeed_chain)
#%%


## Flow concentrations
Cperm_NF = {}
Cperm_NF['Na']=df_nf_output.iloc[9, 21]
Cperm_NF['Cl']=df_nf_output.iloc[10, 21]
Cperm_NF['Mg']=df_nf_output.iloc[11, 21]
Cperm_NF['Ca']=df_nf_output.iloc[12, 21]
Cperm_NF['SO4']=df_nf_output.iloc[13, 21]
Cperm_NF['CO3']=df_nf_output.iloc[14, 21]
with open('./C_perm_NF_dict.yml', 'w') as perm_NF_conc_file:
	yaml.dump(Cperm_NF, perm_NF_conc_file, default_flow_style=False, Dumper=Dumper)

Cretent_NF = {}
Cretent_NF['Na']=df_nf_output.iloc[9, 20]
Cretent_NF['Cl']=df_nf_output.iloc[10, 20]
Cretent_NF['Mg']=df_nf_output.iloc[11, 20]
Cretent_NF['Ca']=df_nf_output.iloc[12, 20]
Cretent_NF['SO4']=df_nf_output.iloc[13, 20]
Cretent_NF['CO3']=df_nf_output.iloc[14, 20]

with open('./C_retent_NF_dict.yml', 'w') as retent_NF_conc_file:
	yaml.dump(Cretent_NF, retent_NF_conc_file, default_flow_style=False, Dumper=Dumper)

## For RO: calculate NaCl (ppm) in NF permeate
# 1. calculate density of NF permeate
rho_perm_NF = density_mixtures(parameter_dict, Cperm_NF)

# 2. convert molecular concentration of NaCl to ppm
Cperm_NF_NaCl = min(Cperm_NF['Na'], Cperm_NF['Cl'])
Cperm_NF_ppm_NaCl = Cperm_NF_NaCl * parameter_dict['MW_NaCl'] * 1000 / rho_perm_NF 

print(Cperm_NF_NaCl, Cperm_NF_ppm_NaCl, rho_perm_NF, Cretent_NF, Cperm_NF)

#%%


## Economic and energy requirements
capital_NF=round(float(df_nf_output.iloc[4, 23]),2)
capex_NF=round(float(df_nf_output.iloc[5, 23]),2)
opex_NF=round(float(df_nf_output.iloc[10, 23]),2)
electric_req_NF=round(float(df_nf_output.iloc[11, 23]),2)

print(capital_NF, capex_NF, opex_NF, electric_req_NF)
print(type(capital_NF), type(capex_NF), type(opex_NF), type(electric_req_NF))

#%%

## Writing RCE output
## Change the below numbering based on nNF 
RCE.write_output('Mperm_kgs_NF1', Mperm_kgs_NF)
RCE.write_output('Qperm_m3s_NF1', Qperm_m3s_NF)
RCE.write_output('Qperm_m3h_NF1', Qperm_m3h_NF)
#RCE.write_output('C_perm_NF1_file', './C_perm_NF_dict.yml')
RCE.write_output('Qretent_m3s_NF1', Qretent_m3s_NF)
RCE.write_output('Qretent_m3h_NF1', Qretent_m3h_NF)
#RCE.write_output('C_retent_NF1_file', './C_retent_NF_dict.yml')
#RCE.write_output('Cperm_NF_ppm_NaCl', Cperm_NF_ppm_NaCl)
#RCE.write_output('capital_NF1', capital_NF)
#RCE.write_output('capex_NF1', capex_NF)
#RCE.write_output('opex_NF1', opex_NF)
#RCE.write_output('electric_req_NF1', electric_req_NF)

#RCE.write_output('Mfeed_chain_kgs', Mfeed_chain_kgs)
#RCE.write_output('rhofeed_chain', rhofeed_chain)