
#%%

import pandas as pd
from common_and_tools.common import density_mixtures
from common_and_tools.common import density_seawater_liquid

#%%

df_output = pd.read_excel('./results/crystallizer/crystallizer_single_sodhyd_maghyd/arabian_gulf_kuwait/average_tds/results_qfeedm3h_train_171.09.xls')

df_output

#%%
df_output.iloc[7,5]

#%%


## Flow rates
no_of_trains = float(df_output.iloc[0,1])
recovery_mgcryst = float(df_output.iloc[2,1])
Mnaoh_kgh_tot = float(df_output.iloc[3,5]) * no_of_trains
Qwater_req_m3h_tot = float(df_output.iloc[5,5]) * no_of_trains
Qeffluent_m3h_tot = float(df_output.iloc[8,5]) * no_of_trains
Mmgoh2_kgh_tot = float(df_output.iloc[7,5]) * no_of_trains

print(f"no_of_trains: {no_of_trains}, recovery_mgcryst: {recovery_mgcryst}, Mnaoh_kgh_tot: {Mnaoh_kgh_tot}, Qwater_req_m3h_tot: {Qwater_req_m3h_tot}, Mmgoh2_kgh_tot: {Mmgoh2_kgh_tot}, Qeffluent_m3h_tot: {Qeffluent_m3h_tot}")


#%%
df_output.iloc[15,6]

#%%
## Economic and energy requirements (same cells for single and double stage)
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
total_capex_tot = float(df_output.iloc[1,10]) * no_of_trains
fixed_opex_tot = float(df_output.iloc[5,10]) * no_of_trains
variable_opex_tot = float(df_output.iloc[9,10]) * no_of_trains
total_opex_tot = fixed_opex_tot + variable_opex_tot
total_annual_cost_tot = total_capex_tot + total_opex_tot # [USD/y] annualized capital + opex (fixed+variable)
annual_electric_energy_req_tot = float(df_output.iloc[15,6]) * no_of_trains   # kWh_el/a

print(f"total_annual_cost_tot: {total_annual_cost_tot}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}")


# %%
