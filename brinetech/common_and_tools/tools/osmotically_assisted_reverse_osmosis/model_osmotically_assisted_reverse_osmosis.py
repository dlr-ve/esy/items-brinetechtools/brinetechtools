import os
import yaml

import numpy as np
from colorama import Fore, Style

from .OARO_functions import economic_function

from ...common import calculate_annual_depreciation_cost
from ...common import ResultList

def validate_feed_concentration(feed_ion_conc_ppm_tot, tech_config_and_params):
    """
    Validates that the total feed ion concentration for oaro is within a 5% (or given) error margin
    of the required concentration.

    Parameters:
        feed_ion_conc_ppm_tot (float): Total ion concentration in ppm.
        tech_config_and_params (dict): A dictionary containing the reference feed concentration under the key 'ref_feed_conc_ppm',
                                      as well as %-factor of allowable variation under the key 'allowable_feed_ppm_variation'.

    Raises:
        ValueError: If the concentrations are not within a 5% (or given) error margin.
    """
    feed_ion_required_conc_ppm_tot = tech_config_and_params['ref_feed_conc_before_mixing_ppm']
    #print("feed_ion_conc_ppm_tot", feed_ion_conc_ppm_tot, type(feed_ion_conc_ppm_tot))
    #print("feed_ion_required_conc_ppm_tot", feed_ion_required_conc_ppm_tot, type(feed_ion_required_conc_ppm_tot))

    # Calculate the acceptable margin of error
    allowed_margin = tech_config_and_params['allowable_feed_ppm_variation'] * feed_ion_required_conc_ppm_tot

    # Check if the concentration difference is within the allowed margin
    if abs(feed_ion_conc_ppm_tot - feed_ion_required_conc_ppm_tot) > allowed_margin:
        raise ValueError(f"Error: The feed ion concentration {feed_ion_conc_ppm_tot} ppm is not within the "
                         f"allowed 5% margin of the required concentration {feed_ion_required_conc_ppm_tot} ppm.")
    else:
        print("Validation successful: Feed ion concentration is within the allowed 5% (or given) error margin.")


def two_stage_oaro_model(tech_config_and_params, proj_config_and_params, N_trains, feed_vol_flow_per_train_m3h, feed_ion_conc_ppm_tot, electricity_cost):

    # feed ppm validation (since OARO costs and energy consumption are based on a specific OARO from literature)
    retent_ion_conc_ppm_tot = tech_config_and_params['ref_final_concentrate_conc_ppm']
    validate_feed_concentration(feed_ion_conc_ppm_tot, tech_config_and_params)

    # read techno-economic params from dict
    Pfeed = tech_config_and_params['feed_pressure']  # [bar]
    recovery_rate_retent = tech_config_and_params['recovery_rate_final_concentrate']  # [%-factor]
    spec_elec_consumption_retent = tech_config_and_params['spec_elec_consumption_final_concentrate']  # [kWh_el/m3 of retentate] It is assumed that it considers the savings through the use of ERD in it
    spec_capital_cost_retent = tech_config_and_params['spec_capital_cost'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']] # [USD_current/(m3_capacity/day)]
    spec_fixed_opex_retent = tech_config_and_params['spec_fixed_opex'] * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][tech_config_and_params['ref_cost_year']]  # [USD_current/year/(m3_capacity/day)]
    
    # output flow rates
    Qretent_m3h = feed_vol_flow_per_train_m3h * recovery_rate_retent
    Qretent_m3_per_annum = Qretent_m3h * 8760 * tech_config_and_params['plant_availability']
    Qretent_m3day_capacity = Qretent_m3h * 24  # [capacity of m3_retent/day]
    #Pretent = Pfeed - tech_config_and_params['retentate_head_loss']  # [bar]

    # power
    power_total = spec_elec_consumption_retent * Qretent_m3h  # [kW_el] Total plant power
    # elec consumption
    annual_elec_consumption = spec_elec_consumption_retent * Qretent_m3_per_annum  # [kWh_el/a]

    # capital expenditures (USD)
    total_capital_cost = spec_capital_cost_retent * Qretent_m3day_capacity  # [USD_current]
    
    ## annualization of capital cost (USD_current/y)
    annualized_capital_cost = calculate_annual_depreciation_cost(proj_config_and_params["interest_rate"], tech_config_and_params["depr_period"], total_capital_cost)
    
    # annual fixed operating cost (USD_current/y)
    annual_fixed_opex = spec_fixed_opex_retent * Qretent_m3day_capacity  # [USD_current/y]

    # total annual cost
    total_annual_fixed_cost = annualized_capital_cost + annual_fixed_opex

    #levelized_fixed_cost_per_m3_dist = total_annual_fixed_cost / Qperm_m3_per_annum
    levelized_fixed_cost_per_m3_brine = total_annual_fixed_cost / Qretent_m3_per_annum

    #### variable operating costs (assumed to consist only of energy)
    # electricity
    annual_electric_cost = economic_function.estimation_electric_cost_from_m3h(spec_elec_consumption_retent, Qretent_m3h, electricity_cost, tech_config_and_params)
    annual_operating_cost = annual_electric_cost

    #levelized_operating_cost_per_m3_dist = annual_electric_cost / Qperm_m3_per_annum
    levelized_operating_cost_per_m3_brine = annual_electric_cost / Qretent_m3_per_annum
    
    ## total (fixed + variable operating)
    annual_total_cost = total_annual_fixed_cost + annual_operating_cost
    #levelized_cost_per_m3_dist = levelized_fixed_cost_per_m3_dist + levelized_operating_cost_per_m3_dist
    levelized_cost_per_m3_brine = levelized_fixed_cost_per_m3_brine + levelized_operating_cost_per_m3_brine
    
    #annual_revenue_purewater = price_purewat_usd_m3 * Qperm_m3_per_annum
    annual_Mbrine = Qretent_m3_per_annum

    oaro_inputs = ResultList(title="OARO inputs", xls_sheet="oaro", results=[
        ('Number of trains', (N_trains, '-')),
        ('Feed vol. flow rate (per train)', (feed_vol_flow_per_train_m3h, 'm3/h')),
        ('Feed pressure to OARO', (Pfeed, 'bar')),
        ('Feed conc. (all ions)', (feed_ion_conc_ppm_tot, 'ppm')),
        ('Retent expected conc. (all ions)', (retent_ion_conc_ppm_tot, 'ppm')),
        ('Recovery rate of retent (final concentrate)', (recovery_rate_retent, '-')),
        ('Spec. capital cost', (spec_capital_cost_retent, 'USD_current/(m3_retent_capacity/day)')),
        ('Spec. fixed opex', (spec_fixed_opex_retent, 'USD_current/year/(m3_retent_capacity/day)')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ('Plant availability', (tech_config_and_params['plant_availability'], '-')),
        ])

    oaro_tech_results = ResultList(title="Technical results per train", xls_sheet="oaro", xls_y0=5, results=[
        ('Flow rates', ('m3/h', 'm3/a')),
        ('Retentate', (Qretent_m3h, Qretent_m3_per_annum)),
        ('Power and electricity consumption', ('kWh_el','kWh/m3_retent', 'kWh_el/y')),
        ('Total', (power_total, spec_elec_consumption_retent, annual_elec_consumption))
        ])
    
    oaro_eco_results = ResultList(title="Economic results per train", xls_sheet="oaro", xls_y0=10, results=[
        ('capex', ('$/y', '$', '$/m3_retent_capacity/day')),
        ('Total capital', (annualized_capital_cost, total_capital_cost, total_capital_cost/Qretent_m3day_capacity)),
        ('opex', ('$/y', '$/m3_retent', '$/a/m3_retent_capacity/day')),
        ('fixed', (annual_fixed_opex, annual_fixed_opex/Qretent_m3_per_annum, annual_fixed_opex/Qretent_m3day_capacity)),
        ('variable', ('$/y', '$/m3_retent')),
        ('electric energy', (annual_electric_cost, annual_electric_cost/Qretent_m3_per_annum)),
        ])

    oaro_tot_cost_results = ResultList(title="total costs per train", xls_sheet="oaro", xls_y0=16, results=[
        ('total annual fixed cost', (total_annual_fixed_cost, '$/y')),
        ('levelized fixed cost (per m3 brine)', (levelized_fixed_cost_per_m3_brine, '$/m3_brine')),
        ('annual variable operating cost', (annual_operating_cost, '$/y')),
        ('levelized operating cost (per m3 brine)', (levelized_operating_cost_per_m3_brine, '$/m3_brine')),
        ('total annual cost', (annual_total_cost, '$/y')),
        ('levelized cost (per m3 brine)', (levelized_cost_per_m3_brine, '$/m3_brine')),
        ('Mbrine annual', (annual_Mbrine, 'm3/y'))
        ])

    return [oaro_inputs, oaro_tech_results, oaro_eco_results, oaro_tot_cost_results]