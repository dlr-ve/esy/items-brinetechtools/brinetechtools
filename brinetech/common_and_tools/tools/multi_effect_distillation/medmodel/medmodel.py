import logging
import numpy as np
from . import heattransfer
from . import h2oprop, naclprops
import pandas as pd

logger = logging.getLogger("medmodel")
logger.setLevel(logging.INFO)

'''functions to evaluate first guess values to start iteration'''

def u_firstguess(ts, N):
    u = []
    invsum_u = 0
    u.append(heattransfer.u_evaporator(ts))
    for i in range(1, N):
        u.append(u[i-1] * 0.99)
        invsum_u = invsum_u + 1/u[i]
    return u, invsum_u


def t_firstguess(ts, tn, u, invsum_u, xf, N, deltat = [], T = []):
    deltat_tot = ts - tn
    deltat.append(deltat_tot/(u[0] * invsum_u))
    T.append(ts - deltat[0])
    for i in range(1, N):
        deltat.append(deltat[i-1] * u[i-1] / u[i])
        T.append(T[i-1] - deltat[i])
    deltat_tot = np.sum(deltat)
    if abs(T[N-1]-tn) > 0.05:
        for i in range(1, N):
            deltat[i] = deltat[i] * (ts - tn) / deltat_tot
            T[i] = T[i - 1] - deltat[i]
    T = np.array(T)
    bpe = naclprops.bpe_brine_act(T, xf)
    tv = T - bpe - 0.1
    tc_prime = tv - 0.1
    tc = tc_prime - 0.3
    # tv = T
    # tc_prime = tv
    # tc = tc_prime
    return deltat, T, tv, tc_prime, tc


def feed_t_firstguess(ph_on, t, DTT_ph, tcw):  # not used as the T of the seawater entering in the 1st effect is a parameter
    if ph_on:
        return t - DTT_ph
    return tcw


def mvap_firstguess(m_dist, n_stages):  # Mdist in [kg/s]
    m_vap = float(m_dist)/n_stages
    return m_vap


def Mms_forwardfeed_firstguess(config, conc_factor, N, Mf):
    Mms_mapsheet = config['Mms_mapsheet']
    Mms_forwardfeed = pd.read_excel(Mms_mapsheet, sheet_name='Mms first guess', usecols='V:AG')
    Mms_ff_lowCF = Mms_forwardfeed.iloc[4:12, 0:12]  # CF = 1.278
    Mms_ff_medCF = Mms_forwardfeed.iloc[18:26, 0:12]  # CF = 1.857
    Mms_ff_highCF = Mms_forwardfeed.iloc[32:40, 0:12]  # CF = 2.222
    Mms_ff_veryhighCF = Mms_forwardfeed.iloc[45:53, 0:12]  # CF = 2.555
    Mms_ff_veryveryhighCF = Mms_forwardfeed.iloc[59:67, 0:12]  # CF = 4.6
    if 1.278 <= conc_factor < 1.857:
        if 1 <= Mf < 5:
            #Mms = Mms_ff_lowCF.iloc[1, (N-4)] * Mf / 5  # Original of Marina
            Mms = Mms_ff_lowCF.iloc[2, (N - 4)] * Mf / 5
        elif 5 <= Mf < 10:
            Mms = Mms_ff_lowCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_ff_lowCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_ff_lowCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_ff_lowCF.iloc[4, (N-4)]
        elif 35 <= Mf < 45:
            Mms = Mms_ff_lowCF.iloc[5, (N-4)]
        elif 45 <= Mf < 75:
            Mms = Mms_ff_lowCF.iloc[6, N-4]
        elif Mf >= 75:
            Mms = Mms_ff_lowCF.iloc[7, N - 4] * Mf / 100
        else:
            Mms = 0.05
    elif 1.857 <= conc_factor < 2.222 :
        if 1 <= Mf < 5:
            Mms = Mms_ff_medCF.iloc[1, (N-4)] * Mf / 5
        elif 5 <= Mf < 10:
            Mms = Mms_ff_medCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_ff_medCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_ff_medCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_ff_medCF.iloc[4, (N-4)]
        elif 35 <= Mf < 45:
            Mms = Mms_ff_medCF.iloc[5, (N-4)]
        elif 45 <= Mf < 75:
            Mms = Mms_ff_medCF.iloc[6, N-4]
        elif Mf >= 75:
            Mms = Mms_ff_medCF.iloc[7, N - 4] * Mf / 100
        else:
            Mms = 0.05
    elif 2.222 <= conc_factor < 2.555:
        if 1 <= Mf < 5:
            Mms = Mms_ff_highCF.iloc[1, (N-4)] * Mf / 5
        elif 5 <= Mf < 10:
            Mms = Mms_ff_highCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_ff_highCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_ff_highCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_ff_highCF.iloc[4, (N-4)]
        elif 35 <= Mf < 45:
            Mms = Mms_ff_highCF.iloc[5, (N-4)]
        elif 45 <= Mf < 75:
            Mms = Mms_ff_highCF.iloc[6, N - 4]
        elif Mf >= 75:
            Mms = Mms_ff_highCF.iloc[7, N - 4] * Mf / 100
        else:
            Mms = 0.05
    elif 2.555 <= conc_factor < 4.6:
        if 1 <= Mf < 5:
            #Mms = Mms_ff_veryhighCF.iloc[1, (N-4)] * Mf / 5  # Original of Marina
            Mms = Mms_ff_veryhighCF.iloc[2, (N - 4)] * Mf / 5
        elif 5 <= Mf < 10:
            Mms = Mms_ff_veryhighCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_ff_veryhighCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_ff_veryhighCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_ff_veryhighCF.iloc[4, (N-4)]
        elif 35 <= Mf < 45:
            Mms = Mms_ff_veryhighCF.iloc[5, (N-4)]
        elif 45 <= Mf < 75:
            Mms = Mms_ff_veryhighCF.iloc[6, N - 4]
        elif Mf >= 75:
            Mms = Mms_ff_veryhighCF.iloc[7, N - 4] * Mf / 100
        else:
            Mms = 0.05
    elif conc_factor >=4.6:
        if 1 <= Mf < 5:
            Mms = Mms_ff_veryveryhighCF.iloc[1, (N-4)] * Mf / 5
        elif 5 <= Mf < 10:
            Mms = Mms_ff_veryveryhighCF.iloc[1, (N-4)] * Mf / 5
        elif 10 <= Mf < 15:
            Mms = Mms_ff_veryveryhighCF.iloc[1, (N-4)] * Mf / 5
        elif 15 <= Mf < 25:
            Mms = Mms_ff_veryveryhighCF.iloc[1, (N-4)]* Mf / 5
        elif 25 <= Mf < 35:
            Mms = Mms_ff_veryveryhighCF.iloc[4, (N-4)]
        elif 35 <= Mf < 45:
            Mms = Mms_ff_veryveryhighCF.iloc[5, (N-4)]
        elif 45 <= Mf < 75:
            Mms = Mms_ff_veryveryhighCF.iloc[6, (N-4)]
        elif Mf >= 75:
            Mms = Mms_ff_veryveryhighCF.iloc[7, (N-4)] * Mf / 200
        else:
            Mms = 0.05
    else:
        Mms = 0.5
    return Mms


def Mms_parallcross_firstguess(config, conc_factor, N, Mf):
    Mms_mapsheet = config['Mms_mapsheet']
    Mms_parallcross = pd.read_excel(Mms_mapsheet, sheet_name='Mms first guess', usecols='G:R')
    Mms_pc_lowCF = pd.DataFrame(Mms_parallcross.iloc[4:12, 0:12])  # CF = 1.278
    Mms_pc_medCF = pd.DataFrame(Mms_parallcross.iloc[18:26, 0:12])  # CF = 1.857
    Mms_pc_highCF = pd.DataFrame(Mms_parallcross.iloc[32:40, 0:12])  # CF = 2.222
    Mms_pc_veryhighCF = pd.DataFrame(Mms_parallcross.iloc[45:53, 0:12])  # CF = 2.555
    Mms_pc_veryveryhighCF = pd.DataFrame(Mms_parallcross.iloc[59:67, 0:12])  # CF = 4.6
    # change of the range for the concentration factor: range centered on the reference CF value
    # if 1.278 <= conc_factor < 1.857:
    if 0.9885 < conc_factor <= 1.5675:
        if 1 <= Mf < 5:
            Mms = Mms_pc_lowCF.iloc[0, (N-4)]
        elif 5 <= Mf < 10:
            Mms = Mms_pc_lowCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_pc_lowCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_pc_lowCF.loc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_pc_lowCF.iloc[4, (N-4)]
        elif 35<= Mf < 45:
            Mms = Mms_pc_lowCF.iloc[5, (N-4)]
        elif 45 <= Mf < 100:
            Mms = Mms_pc_lowCF.iloc[6, N - 4]
        elif Mf >= 100:
            Mms = Mms_pc_lowCF.iloc[7, N - 4]
        else:
            Mms = 0.05
    # elif 1.857 <= conc_factor < 2.222:
    elif 1.5675 < conc_factor <= 2.0395:
        if 1 <= Mf < 5:
            Mms = Mms_pc_medCF.iloc[0, (N-4)]
        elif 5 <= Mf < 10:
            Mms = Mms_pc_medCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_pc_medCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_pc_medCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_pc_medCF.iloc[4, (N-4)]
        elif 35<= Mf < 45:
            Mms = Mms_pc_medCF.iloc[5, (N-4)]
        elif 45 <= Mf < 100:
            Mms = Mms_pc_medCF.iloc[6, N - 4]
        elif Mf >= 100:
            Mms = Mms_pc_medCF.iloc[7, N - 4]
        else:
            Mms = 0.05
    # elif 2.222 <= conc_factor < 2.555:
    elif 2.0395 < conc_factor <= 2.38875:
        if 1 <= Mf < 5:
            Mms = Mms_pc_highCF.iloc[0, (N-4)]
        elif 5 <= Mf < 10:
            Mms = Mms_pc_highCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_pc_highCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_pc_highCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_pc_highCF.iloc[4, (N-4)]
        elif 35<= Mf < 45:
            Mms = Mms_pc_highCF.iloc[5, (N-4)]
        elif 45 <= Mf < 100:
            Mms = Mms_pc_highCF.iloc[6, N - 4]
        elif Mf >= 100:
            Mms = Mms_pc_highCF.iloc[7, N - 4]
        else:
            Mms = 0.05
    # elif 2.555 <= conc_factor < 4.6:
    elif 2.38875 < conc_factor <= 3.57775:
        if 1 <= Mf < 5:
            Mms = Mms_pc_veryhighCF.iloc[0, (N-4)]
        elif 5 <= Mf < 10:
            Mms = Mms_pc_veryhighCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_pc_veryhighCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_pc_veryhighCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_pc_veryhighCF.iloc[4, (N-4)]
        elif 35<= Mf < 45:
            Mms = Mms_pc_veryhighCF.iloc[5, (N-4)]
        elif 45 <= Mf < 100:
            Mms = Mms_pc_veryhighCF.iloc[6, N - 4]
        elif Mf >= 100:
            Mms = Mms_pc_veryhighCF.iloc[7, N - 4]
        else:
            Mms = 0.05
    # elif conc_factor >= 4.6:
    elif conc_factor > 3.57775:
        if 1 <= Mf < 5:
            Mms = Mms_pc_veryveryhighCF.iloc[0, (N-4)]
        elif 5 <= Mf < 10:
            Mms = Mms_pc_veryveryhighCF.iloc[1, (N-4)]
        elif 10 <= Mf < 15:
            Mms = Mms_pc_veryveryhighCF.iloc[2, (N-4)]
        elif 15 <= Mf < 25:
            Mms = Mms_pc_veryveryhighCF.iloc[3, (N-4)]
        elif 25 <= Mf < 35:
            Mms = Mms_pc_veryveryhighCF.iloc[4, (N-4)]
        elif 35<= Mf < 45:
            Mms = Mms_pc_veryveryhighCF.iloc[5, (N-4)]
        elif 45 <= Mf < 100:
            Mms = Mms_pc_veryveryhighCF.iloc[6, N - 4]
        elif Mf >= 100:
            Mms = Mms_pc_veryveryhighCF.iloc[7, N - 4]
        else:
            Mms = 0.05
    else:
        Mms = 0.5
    return Mms
