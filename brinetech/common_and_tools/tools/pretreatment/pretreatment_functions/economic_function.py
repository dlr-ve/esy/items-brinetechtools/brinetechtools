import numpy as np
from ....common import calculate_annual_chemical_cost

def estimation_cost_pretreatment(Vfeed_m3h, tech_config_and_params, proj_config_and_params):
    
    ## consists of physical and chemical pretreatment
    
    ## Capital cost
    # Construction cost varies between 150-230 USD_2018/m3_perm/day (avg. 190) for a SWRO plant (ref.: Nikoilay Project Estimation book (2018))
    # Assuming 50% recovery rate for the reference SWRO cost, their spec. cost should be divided by 2 to get USD_2018/m3_feed/day
    Vfeed_m3day = Vfeed_m3h * 24
    if Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["spec_capital_cost"]["upper_limit"] / 2 * Vfeed_m3day
    elif proj_config_and_params["desal_plant_size_upper_limit"]["small"] <= Vfeed_m3day < proj_config_and_params["desal_plant_size_upper_limit"]["medium"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["spec_capital_cost"]["average"] / 2 * Vfeed_m3day
    elif Vfeed_m3day >= proj_config_and_params["desal_plant_size_upper_limit"]["small"]:
        capital_cost_physical_and_chemical_pretreatment = tech_config_and_params["spec_capital_cost"]["lower_limit"] / 2 * Vfeed_m3day
    
    # Annual costs for various treatments [USD_current/a]
    cost_chlorination = calculate_annual_chemical_cost(Vfeed_m3h, 'chlorination', tech_config_and_params, proj_config_and_params)
    cost_flocculation = calculate_annual_chemical_cost(Vfeed_m3h, 'floacculant', tech_config_and_params, proj_config_and_params)
    cost_sulphacid = calculate_annual_chemical_cost(Vfeed_m3h, 'sulphacid', tech_config_and_params, proj_config_and_params)

    cost_filtration = calculate_annual_chemical_cost(Vfeed_m3h, 'filtration', tech_config_and_params, proj_config_and_params)
    
    # Costs for antiscalants [USD_current/a]
    cost_antiscalant_1stpass = calculate_annual_chemical_cost(Vfeed_m3h, 'antiscalant_1stpass', tech_config_and_params, proj_config_and_params)
    cost_bisulphite_1stpass = calculate_annual_chemical_cost(Vfeed_m3h, 'bisulphite_1stpass', tech_config_and_params, proj_config_and_params)
    
    first_pass_cost = cost_antiscalant_1stpass + cost_bisulphite_1stpass
    cost_antiscalant = first_pass_cost

    # Total annual pretreatment chemical cost [USD_current/a]
    annual_pretreatment_chem_cost = cost_chlorination + cost_flocculation + cost_sulphacid + cost_filtration + cost_antiscalant

    return capital_cost_physical_and_chemical_pretreatment * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI']['2018'], annual_pretreatment_chem_cost


def estimation_electric_cost_for_vol(spec_el_consumption, Vfeed_m3h, electricity_cost, tech_config_and_params):  #Vfeed_m3h in m3/h, spec_el_consumption in kWh/m3
    operating_hours = 8760 * tech_config_and_params['plant_availability']
    electric_consumption = spec_el_consumption * Vfeed_m3h * operating_hours # [kWh/y]
    annual_el_cost = electric_consumption * electricity_cost # [US$/y]
    return annual_el_cost