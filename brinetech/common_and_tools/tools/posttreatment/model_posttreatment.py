
from .posttreatment_functions import estimation_cost_posttreatment, estimation_electric_cost_for_vol

from ...common import calculate_annual_depreciation_cost
from ...common import ResultList


#%%

def posttreatment_model(tech_config_and_params, proj_config_and_params, N_trains, feed_vol_flow_per_train_m3h, Xfeed, electricity_cost):
    
    # flow rate of Qperm
    perm_vol_flow_per_train_m3h = feed_vol_flow_per_train_m3h * tech_config_and_params['recovery_rate']
    perm_vol_flow_per_train_m3a = perm_vol_flow_per_train_m3h * 8760 * tech_config_and_params['plant_availability']  # [m3 of perm/year]
    perm_vol_capacity_per_train_m3d = perm_vol_flow_per_train_m3h * 24  # [capacity of m3_perm/day]

    # Capital cost and annual chemical cost
    capital_cost_posttreatment, annual_posttreatment_chem_cost = estimation_cost_posttreatment(feed_vol_flow_per_train_m3h, tech_config_and_params, proj_config_and_params)
    # Annualization of costs (if not already annual)
    annualized_posttreament_cost = calculate_annual_depreciation_cost(proj_config_and_params["interest_rate"], tech_config_and_params['depr_period'], capital_cost_posttreatment)
    
    # Energy consumption
    spec_elec_consumption = tech_config_and_params["spec_elec_consumption"]  # [kWh/m3_perm]
    annual_electric_consumption = spec_elec_consumption * perm_vol_flow_per_train_m3a  # [kWh/a]
    
    ## OPEX
    # fixed (% of capital cost)
    fixed_opex_cost = capital_cost_posttreatment * tech_config_and_params['perc_maint_CAPEX']  # [$/a]
    # variable (apart from annual chemical cost)
    annual_electric_cost = estimation_electric_cost_for_vol(spec_elec_consumption, feed_vol_flow_per_train_m3h, electricity_cost, tech_config_and_params)  # [$/a]
    # total
    annual_operating_cost = fixed_opex_cost + annual_electric_cost + annual_posttreatment_chem_cost

    ## total (fixed + variable operating)
    annual_total_cost = annualized_posttreament_cost + annual_operating_cost  # [$/a]

    posttreat_inputs = ResultList(title="Inputs", xls_sheet="posttreatment", results=[
        #('input and parameters', ('', '')),
        ('No. of trains', (N_trains, '-')),
        ('Qfeed', (feed_vol_flow_per_train_m3h, 'm3h')),
        ('Feed conc. (total)', (Xfeed, 'ppm')),
        ('Recovery rate', (tech_config_and_params['recovery_rate'], '-')),
        ('specific electricity consumption', (spec_elec_consumption, 'kWh/m3_perm')),
        ('Elec cost', (electricity_cost, 'USD/kWh')),
        ])

    posttreat_results = ResultList(title="Technical results per train", xls_sheet="posttreatment", xls_y0=4, results=[
        ('Qperm', (perm_vol_flow_per_train_m3h, 'm3/h')),
        ('annual electricity consumption', (annual_electric_consumption, 'kWh/a')),
        ])
    
    posttreat_results_economic = ResultList(title="Economic results per train", xls_sheet="posttreatment", xls_y0=8, results=[
        ('capex', ('$/y', '$', '$/m3_perm_out_capacity/day')),
        ('posttreatment system', (annualized_posttreament_cost, capital_cost_posttreatment, capital_cost_posttreatment/perm_vol_capacity_per_train_m3d)),
        ('fixed opex', ('$/y', '' , '$/m3_perm_out_capacity/day')),
        ('fixed', (fixed_opex_cost, '', fixed_opex_cost/perm_vol_capacity_per_train_m3d)),
        ('variable opex', ('$/y', '', '$/m3_perm')),
        ('electric energy', (annual_electric_cost, '', annual_electric_cost/perm_vol_flow_per_train_m3a)),
        ('chemical cost', (annual_posttreatment_chem_cost, '', annual_posttreatment_chem_cost/perm_vol_flow_per_train_m3a)),
        ('annual opex (fixed+variable) cost', (annual_operating_cost, '', annual_operating_cost/perm_vol_flow_per_train_m3a)),
        ('levelized or total annual cost (capex+opex)', (annual_total_cost, '',  annual_total_cost/perm_vol_flow_per_train_m3a)),
        ])

    return [posttreat_inputs, posttreat_results, posttreat_results_economic]
