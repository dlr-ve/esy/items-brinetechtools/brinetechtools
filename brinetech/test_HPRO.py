
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

df_output = pd.read_excel('./results/hpro/arabian_gulf_kuwait/average_tds/oaro/results_qfeedm3h_train=1092.67_with_erd.xls')

df_output

#%%
df_output.iloc[5,1]

#%%


## Mass and volume flow rates
no_of_trains = float(df_output.iloc[0,1])
recovery_hpro = float(df_output.iloc[7,1])
## Flow rates
## train
# volume
Qretent_m3h_per_train = float(df_output.iloc[2,6])
Qperm_m3h_per_train = float(df_output.iloc[1,6])
# mass
Mretent_kgh_per_train = float(df_output.iloc[2,8])
Mperm_kgh_per_train = float(df_output.iloc[1,8])
Mretent_kgs_per_train = Mretent_kgh_per_train / 3600
Mperm_kgs_per_train = Mperm_kgh_per_train / 3600

## total
# volume
Qretent_m3h_tot = Qretent_m3h_per_train * no_of_trains
Qperm_m3h_tot = Qperm_m3h_per_train * no_of_trains
# mass
Mretent_kgh_tot = Mretent_kgh_per_train * no_of_trains
Mperm_kgh_tot = Mperm_kgh_per_train * no_of_trains
Mretent_kgs_tot = Mretent_kgh_tot / 3600
Mperm_kgs_tot = Mperm_kgh_tot / 3600


# Feed pressure of last stage
Pfeed_last_stage = df_output.iloc[2,1]

## Flow concentrations
Xperm_NaCl_ppm = float(df_output.iloc[6,1])
Xretent_NaCl_ppm = float(df_output.iloc[5,1])

print(f"no_of_trains: {no_of_trains}, recovery_hpro: {recovery_hpro}, Qretent_m3h_tot: {Qretent_m3h_tot}, Qperm_m3h_tot: {Qperm_m3h_tot}, Pfeed_last_stage: {Pfeed_last_stage}, Xperm_NaCl_ppm: {Xperm_NaCl_ppm}, Xretent_NaCl_ppm: {Xretent_NaCl_ppm}")
print(f"Mretent_kgh_tot: {Mretent_kgh_tot}, Mperm_kgh_tot: {Mperm_kgh_tot}")

#%%
float(df_output.iloc[9,17])

#%%
## Economic and energy requirements
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
annual_fixed_cost_tot = float(df_output.iloc[0,17]) * no_of_trains  # [USD/y] annualized capital + fixed opex
var_opex_tot = float(df_output.iloc[3,17]) * no_of_trains  # [USD/y]
annual_costs_tot_hpro = annual_fixed_cost_tot + var_opex_tot
electric_power_req_tot = float(df_output.iloc[8,6]) * no_of_trains   # kW_el
annual_electric_energy_req_tot = float(df_output.iloc[8,8]) * no_of_trains   # kWh_el/a
water_revenue_tot = float(df_output.iloc[9,17]) * no_of_trains  # [USD/y]
Mdist_annual_tot = float(df_output.iloc[10,17]) * no_of_trains  # [m3/y]

print(f"annual_fixed_cost_tot: {annual_fixed_cost_tot}, var_opex_tot: {var_opex_tot}, annual_costs_tot_hpro: {annual_costs_tot_hpro}, electric_power_req_tot: {electric_power_req_tot}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}, water_revenue_tot: {water_revenue_tot}, Mdist_annual_tot: {Mdist_annual_tot}")


# %%
