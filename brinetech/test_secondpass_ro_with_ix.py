
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

df_output = pd.read_excel('./results/secondpass_ro_with_ix/arabian_gulf_kuwait/average_tds/results_qfeedm3h_train=250.0.xls')

df_output

#%%
df_output.iloc[2,1]

#%%

# mass and energy flow
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np'
no_of_trains = float(df_output.iloc[0,1])
recovery_secondpass_ro_ix = float(df_output.iloc[5,1])
Qfeed_m3h_tot = float(df_output.iloc[0,1]) * no_of_trains
Qperm_m3h_tot = float(df_output.iloc[1,6]) * no_of_trains
Qretent_m3h_tot = float(df_output.iloc[2,6]) * no_of_trains
Xfeed_ppm = float(df_output.iloc[2,1])

print(f"no_of_trains: {no_of_trains}, recovery_secondpass_ro_ix: {recovery_secondpass_ro_ix}, Qperm_m3h_tot: {Qperm_m3h_tot}, Qretent_m3h_tot: {Qretent_m3h_tot}")

# ppm of retentate: assume all ions from feed going into retentate (since perm ppm is negligibly small at around 0.5 ppm)
Xretent_ppm = float(df_output.iloc[3,6])   # kW_el
print("Xretent_ppm", Xretent_ppm)

#%%

electric_power_req_tot = float(df_output.iloc[5,6]) * no_of_trains   # kW_el
annual_electric_energy_req_tot = float(df_output.iloc[5,8]) * no_of_trains   # kWh_el/a
specific_energy_consumption = float(df_output.iloc[5,7])   # kWh_el/m3_perm
print(f"electric_power_req_tot: {electric_power_req_tot}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}, specific_energy_consumption: {specific_energy_consumption}")

# costs
capex_tot = float(df_output.iloc[1,11]) * no_of_trains  # [USD/y]
fixed_opex_tot = float(df_output.iloc[3,11]) * no_of_trains  # [USD/y]
variable_opex_tot = float(df_output.iloc[5,11]) * no_of_trains  # [USD/y]
total_opex_tot = fixed_opex_tot + variable_opex_tot  # [USD/y]
annual_costs_tot_secondpass_ro_ix = capex_tot + total_opex_tot

print(f"capex_tot: {capex_tot}, fixed_opex_tot: {fixed_opex_tot}, variable_opex_tot: {variable_opex_tot}, total_opex_tot: {total_opex_tot}, annual_costs_tot_secondpass_ro_ix: {annual_costs_tot_secondpass_ro_ix}")

# %%
