from .model_secondpass_ro_with_ix import secondpass_ro_with_ix_model as model_secondpass_ro_with_ix

__all__ = [
    "model_secondpass_ro_with_ix",
]
