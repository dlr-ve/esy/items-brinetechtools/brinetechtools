from . import tools, membrane_properties, single_stage
import numpy as np



def definition_solver_singlestage_discrete(parameter_config, nelem, Mfeed, Xfeed, Xret_out):
    # Vfeed = parameter_config['Feed_flowrate'] # [m3/h]
    # Mfeed = Vfeed/3600*membrane_properties.density_seawater_liquid(parameter_config['Feed_temperature'], parameter_config['Feed_salinity'])
    Tfeed = parameter_config['Feed_temperature']
    # Rtot = parameter_config['Total_recovery_rate']
    toll_R1 = 1e-3
    toll_Rel = 1e-3
    ndiscr = parameter_config['ndiscr']
    Mf = np.zeros((ndiscr, nelem))
    Mc = np.zeros((ndiscr, nelem))
    Mp = np.zeros((ndiscr, nelem))
    Mp_cum = np.zeros((ndiscr, nelem))
    P = np.zeros(nelem)
    Xfs = np.zeros((ndiscr, nelem))
    Xf = np.zeros((ndiscr, nelem))
    Xp = np.zeros((ndiscr, nelem))
    MXp_cum_elem = np.zeros((ndiscr, nelem))
    Mp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_out_elem = np.zeros(nelem)
    Xc = np.zeros((ndiscr, nelem))
    Xc_out_elem = np.zeros(nelem)
    AFS = np.zeros(nelem)
    DeltaX = np.zeros(nelem)
    MXp_cum = np.zeros((ndiscr, nelem))
    Fw = np.zeros((ndiscr, nelem))
    Fs = np.zeros((ndiscr, nelem))
    # membrane properties
    Amembr = parameter_config['membrane_area_sw']
    Amembr_nom = membrane_properties.calculation_Amembr_nom_sw(parameter_config)
    Bmembr_nom = membrane_properties.calculation_Bmembr_nom_sw(parameter_config, Amembr_nom)
    TCF = membrane_properties.temperature_factor(parameter_config)
    DFw, DFs = membrane_properties.ageing_factor(parameter_config)
    Amembr_real = membrane_properties.calculation_Amembr_real(Amembr_nom, TCF, DFw)
    Bmembr_real = membrane_properties.calculation_Bmembr_real(Bmembr_nom, TCF, DFs)
    # global balances
    Rtot = single_stage.global_balance_Rtot(Xfeed, Xret_out, parameter_config['salt_rejection_sw'])
    Mpermeate, Mconc = single_stage.global_balance_mfeed(Mfeed, Rtot)
    # Xc_out = single_stage.global_balance_xc(Xfeed, Rtot, parameter_config['salt_rejection_sw'])
    Vpermeate = Mpermeate * 3600 / 1000    # in m3/h
    Plosses = parameter_config['Plosses']
    # first guess
    Rel_guess = Rtot / nelem
    CPF_guess = membrane_properties.concentration_polarization_factor(Rel_guess)
    AFS_global = single_stage.average_feed_salinity(Xfeed, Rtot, CPF_guess, TCF)
    Xp_out = (1 - parameter_config['salt_rejection_sw']) * AFS_global * DFs
    Posm_out = single_stage.osmotic_pressure_out(parameter_config, Xret_out, Xp_out)
    Pfeed = single_stage.feed_pressure(parameter_config, Posm_out, Plosses)
    Posm_av = parameter_config['osmotic_coeff'] * AFS_global * 1e-4
    Amembr_tot = Mpermeate / (0.9 * Amembr_real * ((Pfeed + Posm_out * parameter_config['security_factor']) / 2 - Posm_av))
    NPV = Amembr_tot / (parameter_config['membrane_area_sw'] * nelem)
    Rel_guess = np.array([Rtot/nelem for i in range(0, nelem)])
    Mp[0, 0] = Mpermeate/nelem/ndiscr
    Mp_cum[0, 0] = Mp[0, 0]
    Mp_cum_elem[0, 0] = Mp[0, 0]
    for i_elem in range(nelem):
        for i_subelem in range(ndiscr):
            Mp[i_subelem, i_elem] = Mpermeate/nelem/ndiscr
    Mf[0, 0] = Mfeed
    Mc[0, 0] = Mf[0, 0] - Mp[0, 0]
    Xf[0, 0] = Xfeed
    Xc_out_elem[0] = single_stage.global_balance_xc(Xf[0, 0], Rel_guess[0], parameter_config['salt_rejection_sw'])
    DeltaX[0] = (Xc_out_elem[0] - Xf[0, 0])/ndiscr
    Xc[0, 0] = Xf[0, 0] + DeltaX[0]
    Xp[0, 0] = (Mf[0, 0] * Xf[0, 0] - Mc[0, 0] * Xc[0, 0]) / Mp[0, 0]
    MXp_cum[0, 0] = Mp[0, 0] * Xp[0, 0]
    MXp_cum_elem[0, 0] = Mp[0, 0] * Xp[0, 0]
    Xp_cum_elem[0, 0] = Xp[0, 0]
    for i_subelem in range(1, ndiscr):
        Mf[i_subelem, 0] = Mc[i_subelem - 1, 0]
        Mc[i_subelem, 0] = Mf[i_subelem, 0] - Mp[i_subelem, 0]
        Xf[i_subelem, 0] = Xc[i_subelem - 1, 0]
        Xc[i_subelem, 0] = Xf[i_subelem, 0] + DeltaX[0]
        Xp[i_subelem, 0] = (Mf[i_subelem, 0] * Xf[i_subelem, 0] - Mc[i_subelem, 0] * Xc[i_subelem, 0]) / Mp[
            i_subelem, 0]
        Mp_cum[i_subelem, 0] = Mp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum[i_subelem, 0] = MXp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Mp_cum_elem[i_subelem, 0] = Mp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Xp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem, 0] / Mp_cum_elem[i_subelem, 0]
    Xp_out_elem[0] = MXp_cum_elem[ndiscr - 1, 0] / Mp_cum_elem[ndiscr - 1, 0]
    for i_elem in range(1, nelem):
        Mf[0, i_elem] = Mc[ndiscr-1, i_elem - 1]
        Mc[0, i_elem] = Mf[0, i_elem] - Mp[0, i_elem]
        Xf[0, i_elem] = Xc[ndiscr-1, i_elem - 1]
        Xc_out_elem[i_elem] = single_stage.global_balance_xc(Xf[0, i_elem], Rel_guess[i_elem], parameter_config['salt_rejection_sw'])
        DeltaX[i_elem] = (Xc_out_elem[i_elem] - Xf[0, i_elem])/ndiscr
        Xc[0, i_elem] = Xf[0, i_elem] + DeltaX[i_elem]
        Xp[0, i_elem] = (Mf[0, i_elem] * Xf[0, i_elem] - Mc[0, i_elem] * Xc[0, i_elem]) / Mp[0, i_elem]
        Mp_cum[0, i_elem] = Mp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem]
        MXp_cum[0, i_elem] = MXp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem] * Xp[0, i_elem]
        Mp_cum_elem[0, i_elem] = Mp[0, i_elem]
        MXp_cum_elem[0, i_elem] = Mp[0, i_elem] * Xp[0, i_elem]
        Xp_cum_elem[0, i_elem] = Xp[0, i_elem]
        for i_subelem in range(1, ndiscr):
            Mf[i_subelem, i_elem] = Mc[i_subelem - 1, i_elem]
            Mc[i_subelem, i_elem] = Mf[i_subelem, i_elem] - Mp[i_subelem, i_elem]
            Xf[i_subelem, i_elem] = Xc[i_subelem - 1, i_elem]
            Xc[i_subelem, i_elem] = Xf[i_subelem, i_elem] + DeltaX[i_elem]
            Xp[i_subelem, i_elem] = (Mf[i_subelem, i_elem] * Xf[i_subelem, i_elem] - Mc[i_subelem, i_elem]*
                                     Xc[i_subelem, i_elem])/Mp[i_subelem, i_elem]
            Mp_cum[i_subelem, i_elem] = Mp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum[i_subelem, i_elem] = MXp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Mp_cum_elem[i_subelem, i_elem] = Mp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]#
            Xp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem, i_elem] / Mp_cum_elem[i_subelem, i_elem]
        Xp_out_elem[i_elem] = MXp_cum_elem[ndiscr - 1, i_elem] / Mp_cum_elem[ndiscr - 1, i_elem]
    Xp_cum = MXp_cum[ndiscr-1, nelem-1]/Mpermeate
    Rel = np.array([0.1 for i in range(0, nelem)])
    Rel_calc = Rel_guess * 1
    CPF = CPF_guess
    Mpermeate_calc = Mpermeate
    R1_calc = 0.1
    solver = single_stage.ROsolver_singlestage_discrete(Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed,
                                                        Mf, Mp, Mc, Mp_cum, MXp_cum, Xp, Xc, Mfeed, Fw, Fs, TCF, AFS, CPF,
                                                        Amembr_real, Bmembr_real, Amembr, NPV, parameter_config, nelem, Plosses,
                                                        Posm_out, Mpermeate_calc, Xp_cum, ndiscr, Mp_cum_elem, MXp_cum_elem,
                                                        Xp_cum_elem, Xfs, Xp_out_elem, Amembr_tot)
    return solver

def definition_solver_singlestage_discrete_givenPfeed(parameter_config, nelem, Mfeed, Xfeed, Xret_out, Pfeed):
    # Vfeed = parameter_config['Feed_flowrate'] # [m3/h]
    # Mfeed = Vfeed/3600*membrane_properties.density_seawater_liquid(parameter_config['Feed_temperature'], parameter_config['Feed_salinity'])
    Tfeed = parameter_config['Feed_temperature']
    # Rtot = parameter_config['Total_recovery_rate']
    toll_R1 = 1e-3
    toll_Rel = 1e-3
    ndiscr = parameter_config['ndiscr']
    Mf = np.zeros((ndiscr, nelem))
    Mc = np.zeros((ndiscr, nelem))
    Mp = np.zeros((ndiscr, nelem))
    Mp_cum = np.zeros((ndiscr, nelem))
    P = np.zeros(nelem)
    Xfs = np.zeros((ndiscr, nelem))
    Xf = np.zeros((ndiscr, nelem))
    Xp = np.zeros((ndiscr, nelem))
    MXp_cum_elem = np.zeros((ndiscr, nelem))
    Mp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_out_elem = np.zeros(nelem)
    Xc = np.zeros((ndiscr, nelem))
    Xc_out_elem = np.zeros(nelem)
    AFS = np.zeros(nelem)
    DeltaX = np.zeros(nelem)
    MXp_cum = np.zeros((ndiscr, nelem))
    Fw = np.zeros((ndiscr, nelem))
    Fs = np.zeros((ndiscr, nelem))
    # membrane properties
    Amembr = parameter_config['membrane_area_sw']
    Amembr_nom = membrane_properties.calculation_Amembr_nom_sw(parameter_config)
    Bmembr_nom = membrane_properties.calculation_Bmembr_nom_sw(parameter_config, Amembr_nom)
    TCF = membrane_properties.temperature_factor(parameter_config)
    DFw, DFs = membrane_properties.ageing_factor(parameter_config)
    Amembr_real = membrane_properties.calculation_Amembr_real(Amembr_nom, TCF, DFw)
    Bmembr_real = membrane_properties.calculation_Bmembr_real(Bmembr_nom, TCF, DFs)
    # global balances
    Rtot = single_stage.global_balance_Rtot(Xfeed, Xret_out, parameter_config['salt_rejection_sw'])
    Mpermeate, Mconc = single_stage.global_balance_mfeed(Mfeed, Rtot)
    # Xc_out = single_stage.global_balance_xc(Xfeed, Rtot, parameter_config['salt_rejection_sw'])
    Vpermeate = Mpermeate * 3600 / 1000
    Plosses = parameter_config['Plosses']
    # first guess
    Rel_guess = Rtot / nelem
    CPF_guess = membrane_properties.concentration_polarization_factor(Rel_guess)
    AFS_global = single_stage.average_feed_salinity(Xfeed, Rtot, CPF_guess, TCF)
    Xp_out = (1 - parameter_config['salt_rejection_sw']) * AFS_global * DFs
    Posm_out = single_stage.osmotic_pressure_out(parameter_config, Xret_out, Xp_out)
    Posm_av = parameter_config['osmotic_coeff'] * AFS_global * 1e-4
    Amembr_tot = Mpermeate / (0.9 * Amembr_real * ((Pfeed + Posm_out * parameter_config['security_factor']) / 2 - Posm_av))#
    NPV = Amembr_tot / (parameter_config['membrane_area_sw'] * nelem)
    Rel_guess = np.array([Rtot/nelem for i in range(0, nelem)])
    Mp[0, 0] = Mpermeate/nelem/ndiscr
    Mp_cum[0, 0] = Mp[0, 0]
    Mp_cum_elem[0, 0] = Mp[0, 0]
    for i_elem in range(nelem):
        for i_subelem in range(ndiscr):
            Mp[i_subelem, i_elem] = Mpermeate/nelem/ndiscr
    Mf[0, 0] = Mfeed
    Mc[0, 0] = Mf[0, 0] - Mp[0, 0]
    Xf[0, 0] = Xfeed
    Xc_out_elem[0] = single_stage.global_balance_xc(Xf[0, 0], Rel_guess[0], parameter_config['salt_rejection_sw'])
    DeltaX[0] = (Xc_out_elem[0] - Xf[0, 0])/ndiscr
    Xc[0, 0] = Xf[0, 0] + DeltaX[0]
    Xp[0, 0] = (Mf[0, 0] * Xf[0, 0] - Mc[0, 0] * Xc[0, 0]) / Mp[0, 0]
    MXp_cum[0, 0] = Mp[0, 0] * Xp[0, 0]
    MXp_cum_elem[0, 0] = Mp[0, 0] * Xp[0, 0]
    Xp_cum_elem[0, 0] = Xp[0, 0]
    for i_subelem in range(1, ndiscr):
        Mf[i_subelem, 0] = Mc[i_subelem - 1, 0]
        Mc[i_subelem, 0] = Mf[i_subelem, 0] - Mp[i_subelem, 0]
        Xf[i_subelem, 0] = Xc[i_subelem - 1, 0]
        Xc[i_subelem, 0] = Xf[i_subelem, 0] + DeltaX[0]
        Xp[i_subelem, 0] = (Mf[i_subelem, 0] * Xf[i_subelem, 0] - Mc[i_subelem, 0] * Xc[i_subelem, 0]) / Mp[
            i_subelem, 0]
        Mp_cum[i_subelem, 0] = Mp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum[i_subelem, 0] = MXp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Mp_cum_elem[i_subelem, 0] = Mp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Xp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem, 0] / Mp_cum_elem[i_subelem, 0]
    Xp_out_elem[0] = MXp_cum_elem[ndiscr - 1, 0] / Mp_cum_elem[ndiscr - 1, 0]
    for i_elem in range(1, nelem):
        Mf[0, i_elem] = Mc[ndiscr-1, i_elem - 1]
        Mc[0, i_elem] = Mf[0, i_elem] - Mp[0, i_elem]
        Xf[0, i_elem] = Xc[ndiscr-1, i_elem - 1]
        Xc_out_elem[i_elem] = single_stage.global_balance_xc(Xf[0, i_elem], Rel_guess[i_elem], parameter_config['salt_rejection_sw'])
        DeltaX[i_elem] = (Xc_out_elem[i_elem] - Xf[0, i_elem])/ndiscr
        Xc[0, i_elem] = Xf[0, i_elem] + DeltaX[i_elem]
        Xp[0, i_elem] = (Mf[0, i_elem] * Xf[0, i_elem] - Mc[0, i_elem] * Xc[0, i_elem]) / Mp[0, i_elem]
        Mp_cum[0, i_elem] = Mp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem]
        MXp_cum[0, i_elem] = MXp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem] * Xp[0, i_elem]
        Mp_cum_elem[0, i_elem] = Mp[0, i_elem]
        MXp_cum_elem[0, i_elem] = Mp[0, i_elem] * Xp[0, i_elem]
        Xp_cum_elem[0, i_elem] = Xp[0, i_elem]
        for i_subelem in range(1, ndiscr):
            Mf[i_subelem, i_elem] = Mc[i_subelem - 1, i_elem]
            Mc[i_subelem, i_elem] = Mf[i_subelem, i_elem] - Mp[i_subelem, i_elem]
            Xf[i_subelem, i_elem] = Xc[i_subelem - 1, i_elem]
            Xc[i_subelem, i_elem] = Xf[i_subelem, i_elem] + DeltaX[i_elem]
            Xp[i_subelem, i_elem] = (Mf[i_subelem, i_elem] * Xf[i_subelem, i_elem] - Mc[i_subelem, i_elem]*
                                     Xc[i_subelem, i_elem])/Mp[i_subelem, i_elem]
            Mp_cum[i_subelem, i_elem] = Mp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum[i_subelem, i_elem] = MXp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Mp_cum_elem[i_subelem, i_elem] = Mp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]#
            Xp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem, i_elem] / Mp_cum_elem[i_subelem, i_elem]
        Xp_out_elem[i_elem] = MXp_cum_elem[ndiscr - 1, i_elem] / Mp_cum_elem[ndiscr - 1, i_elem]
    Xp_cum = MXp_cum[ndiscr-1, nelem-1]/Mpermeate
    Rel = np.array([0.1 for i in range(0, nelem)])
    Rel_calc = Rel_guess * 1
    CPF = CPF_guess
    Mpermeate_calc = Mpermeate
    R1_calc = 0.1
    solver = single_stage.ROsolver_singlestage_discrete(Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed,
                                                        Mf, Mp, Mc, Mp_cum, MXp_cum, Xp, Xc, Mfeed, Fw, Fs, TCF, AFS, CPF,
                                                        Amembr_real, Bmembr_real, Amembr, NPV, parameter_config, nelem, Plosses,
                                                        Posm_out, Mpermeate_calc, Xp_cum, ndiscr, Mp_cum_elem, MXp_cum_elem,
                                                        Xp_cum_elem, Xfs, Xp_out_elem, Amembr_tot)
    return solver


def definition_solver_doublestage_discrete(parameter_config, nelem_1, nelem_2, Mfeed, Xfeed, Xret_out):
    # Vfeed = parameter_config['Feed_flowrate'] # [m3/h]
    # Mfeed = Vfeed/3600*membrane_properties.density_seawater_liquid(parameter_config['Feed_temperature'], parameter_config['Feed_salinity'])
    Tfeed = parameter_config['Feed_temperature']
    # Rtot = parameter_config['Total_recovery_rate']
    toll_R1 = 1e-3
    toll_Rel = 1e-3
    ndiscr = parameter_config['ndiscr']
    nelem = nelem_1 + nelem_2
    Mf = np.zeros((ndiscr, nelem))
    Mc = np.zeros((ndiscr, nelem))
    Mp = np.zeros((ndiscr, nelem))
    Mp_cum = np.zeros((ndiscr, nelem))
    P = np.zeros(nelem)
    Xfs = np.zeros((ndiscr, nelem))
    Xf = np.zeros((ndiscr, nelem))
    Xp = np.zeros((ndiscr, nelem))
    MXp_cum_elem = np.zeros((ndiscr, nelem))
    Mp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_out_elem = np.zeros(nelem)
    Xc = np.zeros((ndiscr, nelem))
    Xc_out_elem = np.zeros(nelem)
    AFS = np.zeros(nelem)
    DeltaX = np.zeros(nelem)
    MXp_cum = np.zeros((ndiscr, nelem))
    Fw = np.zeros((ndiscr, nelem))
    Fs = np.zeros((ndiscr, nelem))
    # membrane properties
    Amembr = parameter_config['membrane_area_sw']
    Amembr_nom = membrane_properties.calculation_Amembr_nom_sw(parameter_config)
    Bmembr_nom = membrane_properties.calculation_Bmembr_nom_sw(parameter_config, Amembr_nom)
    TCF = membrane_properties.temperature_factor(parameter_config)
    DFw, DFs = membrane_properties.ageing_factor(parameter_config)
    Amembr_real = membrane_properties.calculation_Amembr_real(Amembr_nom, TCF, DFw)
    Bmembr_real = membrane_properties.calculation_Bmembr_real(Bmembr_nom, TCF, DFs)
    # global balances
    Rtot = single_stage.global_balance_Rtot(Xfeed, Xret_out, parameter_config['salt_rejection_sw'])
    Mpermeate, Mconc = single_stage.global_balance_mfeed(Mfeed, Rtot)
    Plosses = parameter_config['Plosses']
    R1_guess = Rtot * 0.95
    Xc_out_1 = single_stage.global_balance_xc(Xfeed, R1_guess, parameter_config['salt_rejection_sw'])
    Mpermeate_1 = R1_guess * Mfeed
    Mconc_1 = Mfeed - Mpermeate_1
    Xp_out_1 = (Mfeed * Xfeed - Mconc_1 * Xc_out_1) / Mpermeate_1
    # first guess
    Rel_guess = Rtot / nelem
    CPF_guess = membrane_properties.concentration_polarization_factor(Rel_guess)
    AFS_global_1 = single_stage.average_feed_salinity(Xfeed, R1_guess, CPF_guess, TCF)
    # Xp_out = (1 - parameter_config['salt_rejection_sw']) * AFS_global_1 * DFs
    Posm_out_1 = single_stage.osmotic_pressure_out(parameter_config, Xc_out_1, Xp_out_1)
    Pfeed = single_stage.feed_pressure(parameter_config, Posm_out_1, Plosses)
    Posm_av_1 = parameter_config['osmotic_coeff'] * AFS_global_1 * 1e-4
    Amembr_tot_1 = Mpermeate_1 / (0.9 * Amembr_real * ((Pfeed + Posm_out_1 * parameter_config['security_factor']) / 2 - Posm_av_1))
    NPV_1 = Amembr_tot_1 / (parameter_config['membrane_area_sw'] * nelem_1)
    NPV_2 = NPV_1 / 2
    Amembr_tot_2 = NPV_2 * nelem_2 * parameter_config['membrane_area_sw']
    Rel_guess = np.array([Rtot/nelem for i in range(0, nelem)])
    Amembr_tot = Amembr_tot_1 + Amembr_tot_2
    Mp[0, 0] = Mpermeate/nelem/ndiscr
    Mp_cum[0, 0] = Mp[0, 0]
    Mp_cum_elem[0, 0] = Mp[0, 0]
    for i_elem in range(nelem):
        for i_subelem in range(ndiscr):
            Mp[i_subelem, i_elem] = Mpermeate/nelem/ndiscr
    Mf[0, 0] = Mfeed
    Mc[0, 0] = Mf[0, 0] - Mp[0, 0]
    Xf[0, 0] = Xfeed
    Xc_out_elem[0] = single_stage.global_balance_xc(Xf[0, 0], Rel_guess[0], parameter_config['salt_rejection_sw'])
    DeltaX[0] = (Xc_out_elem[0] - Xf[0, 0])/ndiscr
    Xc[0, 0] = Xf[0, 0] + DeltaX[0]
    Xp[0, 0] = (Mf[0, 0] * Xf[0, 0] - Mc[0, 0] * Xc[0, 0]) / Mp[0, 0]
    MXp_cum[0, 0] = Mp[0, 0] * Xp[0, 0]
    MXp_cum_elem[0, 0] = Mp[0, 0] * Xp[0, 0]
    Xp_cum_elem[0, 0] = Xp[0, 0]
    for i_subelem in range(1, ndiscr):
        Mf[i_subelem, 0] = Mc[i_subelem - 1, 0]
        Mc[i_subelem, 0] = Mf[i_subelem, 0] - Mp[i_subelem, 0]
        Xf[i_subelem, 0] = Xc[i_subelem - 1, 0]
        Xc[i_subelem, 0] = Xf[i_subelem, 0] + DeltaX[0]
        Xp[i_subelem, 0] = (Mf[i_subelem, 0] * Xf[i_subelem, 0] - Mc[i_subelem, 0] * Xc[i_subelem, 0]) / Mp[
            i_subelem, 0]
        Mp_cum[i_subelem, 0] = Mp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum[i_subelem, 0] = MXp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Mp_cum_elem[i_subelem, 0] = Mp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Xp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem, 0] / Mp_cum_elem[i_subelem, 0]
    Xp_out_elem[0] = MXp_cum_elem[ndiscr - 1, 0] / Mp_cum_elem[ndiscr - 1, 0]
    for i_elem in range(1, nelem):
        Mf[0, i_elem] = Mc[ndiscr-1, i_elem - 1]
        Mc[0, i_elem] = Mf[0, i_elem] - Mp[0, i_elem]
        Xf[0, i_elem] = Xc[ndiscr-1, i_elem - 1]
        Xc_out_elem[i_elem] = single_stage.global_balance_xc(Xf[0, i_elem], Rel_guess[i_elem], parameter_config['salt_rejection_sw'])
        DeltaX[i_elem] = (Xc_out_elem[i_elem] - Xf[0, i_elem])/ndiscr
        Xc[0, i_elem] = Xf[0, i_elem] + DeltaX[i_elem]
        Xp[0, i_elem] = (Mf[0, i_elem] * Xf[0, i_elem] - Mc[0, i_elem] * Xc[0, i_elem]) / Mp[0, i_elem]
        Mp_cum[0, i_elem] = Mp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem]
        MXp_cum[0, i_elem] = MXp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem] * Xp[0, i_elem]
        Mp_cum_elem[0, i_elem] = Mp[0, i_elem]
        MXp_cum_elem[0, i_elem] = Mp[0, i_elem] * Xp[0, i_elem]
        Xp_cum_elem[0, i_elem] = Xp[0, i_elem]
        for i_subelem in range(1, ndiscr):
            Mf[i_subelem, i_elem] = Mc[i_subelem - 1, i_elem]
            Mc[i_subelem, i_elem] = Mf[i_subelem, i_elem] - Mp[i_subelem, i_elem]
            Xf[i_subelem, i_elem] = Xc[i_subelem - 1, i_elem]
            Xc[i_subelem, i_elem] = Xf[i_subelem, i_elem] + DeltaX[i_elem]
            Xp[i_subelem, i_elem] = (Mf[i_subelem, i_elem] * Xf[i_subelem, i_elem] - Mc[i_subelem, i_elem]*
                                     Xc[i_subelem, i_elem])/Mp[i_subelem, i_elem]
            Mp_cum[i_subelem, i_elem] = Mp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum[i_subelem, i_elem] = MXp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Mp_cum_elem[i_subelem, i_elem] = Mp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Xp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem, i_elem] / Mp_cum_elem[i_subelem, i_elem]
        Xp_out_elem[i_elem] = MXp_cum_elem[ndiscr - 1, i_elem] / Mp_cum_elem[ndiscr - 1, i_elem]
    Xp_cum = MXp_cum[ndiscr-1, nelem-1]/Mpermeate
    Rel = np.array([0.1 for i in range(0, nelem)])
    Rel_calc = Rel_guess * 1
    CPF = CPF_guess
    Mpermeate_calc = Mpermeate
    R1_calc = R1_guess * 1
    solver = single_stage.ROsolver_doublestage_discrete(Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed,
                                                        Mf, Mp, Mc, Mp_cum, MXp_cum, Xp, Xc, Mfeed, Fw, Fs, TCF, AFS, CPF,
                                                        Amembr_real, Bmembr_real, Amembr, NPV_1, NPV_2, parameter_config,
                                                        nelem_1, nelem_2, Plosses, Posm_out_1, Mpermeate_calc, Xp_cum,
                                                        ndiscr, Mp_cum_elem, MXp_cum_elem, Xp_cum_elem, Xfs, Xp_out_elem,
                                                        Amembr_tot_1, Amembr_tot_2)
    return solver

def definition_solver_doublestage_discrete_givenPfeed(parameter_config, nelem_1, nelem_2, Mfeed, Xfeed, Xret_out, Pfeed):
    # Vfeed = parameter_config['Feed_flowrate'] # [m3/h]
    # Mfeed = Vfeed/3600*membrane_properties.density_seawater_liquid(parameter_config['Feed_temperature'], parameter_config['Feed_salinity'])
    Tfeed = parameter_config['Feed_temperature']
    # Rtot = parameter_config['Total_recovery_rate']
    toll_R1 = 1e-3
    toll_Rel = 1e-3
    ndiscr = parameter_config['ndiscr']
    nelem = nelem_1 + nelem_2
    Mf = np.zeros((ndiscr, nelem))
    Mc = np.zeros((ndiscr, nelem))
    Mp = np.zeros((ndiscr, nelem))
    Mp_cum = np.zeros((ndiscr, nelem))
    P = np.zeros(nelem)
    Xfs = np.zeros((ndiscr, nelem))
    Xf = np.zeros((ndiscr, nelem))
    Xp = np.zeros((ndiscr, nelem))
    MXp_cum_elem = np.zeros((ndiscr, nelem))
    Mp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_cum_elem = np.zeros((ndiscr, nelem))
    Xp_out_elem = np.zeros(nelem)
    Xc = np.zeros((ndiscr, nelem))
    Xc_out_elem = np.zeros(nelem)
    AFS = np.zeros(nelem)
    DeltaX = np.zeros(nelem)
    MXp_cum = np.zeros((ndiscr, nelem))
    Fw = np.zeros((ndiscr, nelem))
    Fs = np.zeros((ndiscr, nelem))
    # membrane properties
    Amembr = parameter_config['membrane_area_sw']
    Amembr_nom = membrane_properties.calculation_Amembr_nom_sw(parameter_config)
    Bmembr_nom = membrane_properties.calculation_Bmembr_nom_sw(parameter_config, Amembr_nom)
    TCF = membrane_properties.temperature_factor(parameter_config)
    DFw, DFs = membrane_properties.ageing_factor(parameter_config)
    Amembr_real = membrane_properties.calculation_Amembr_real(Amembr_nom, TCF, DFw)
    Bmembr_real = membrane_properties.calculation_Bmembr_real(Bmembr_nom, TCF, DFs)
    # global balances
    Rtot = single_stage.global_balance_Rtot(Xfeed, Xret_out, parameter_config['salt_rejection_sw'])
    Mpermeate, Mconc = single_stage.global_balance_mfeed(Mfeed, Rtot)
    Plosses = parameter_config['Plosses']
    R1_guess = Rtot * 0.95
    Xc_out_1 = single_stage.global_balance_xc(Xfeed, R1_guess, parameter_config['salt_rejection_sw'])
    Mpermeate_1 = R1_guess * Mfeed
    Mconc_1 = Mfeed - Mpermeate_1
    Xp_out_1 = (Mfeed * Xfeed - Mconc_1 * Xc_out_1) / Mpermeate_1
    # first guess
    Rel_guess = Rtot / nelem
    CPF_guess = membrane_properties.concentration_polarization_factor(Rel_guess)
    AFS_global_1 = single_stage.average_feed_salinity(Xfeed, R1_guess, CPF_guess, TCF)
    # Xp_out = (1 - parameter_config['salt_rejection_sw']) * AFS_global_1 * DFs
    Posm_out_1 = single_stage.osmotic_pressure_out(parameter_config, Xc_out_1, Xp_out_1)
    Posm_av_1 = parameter_config['osmotic_coeff'] * AFS_global_1 * 1e-4
    Amembr_tot_1 = Mpermeate_1 / (0.9 * Amembr_real * ((Pfeed + Posm_out_1 * parameter_config['security_factor']) / 2 - Posm_av_1))#
    NPV_1 = Amembr_tot_1 / (parameter_config['membrane_area_sw'] * nelem_1)
    NPV_2 = NPV_1 / 2
    Amembr_tot_2 = NPV_2 * nelem_2 * parameter_config['membrane_area_sw']
    Rel_guess = np.array([Rtot/nelem for i in range(0, nelem)])
    Amembr_tot = Amembr_tot_1 + Amembr_tot_2
    Mp[0, 0] = Mpermeate/nelem/ndiscr
    Mp_cum[0, 0] = Mp[0, 0]
    Mp_cum_elem[0, 0] = Mp[0, 0]
    for i_elem in range(nelem):
        for i_subelem in range(ndiscr):
            Mp[i_subelem, i_elem] = Mpermeate/nelem/ndiscr
    Mf[0, 0] = Mfeed
    Mc[0, 0] = Mf[0, 0] - Mp[0, 0]
    Xf[0, 0] = Xfeed
    Xc_out_elem[0] = single_stage.global_balance_xc(Xf[0, 0], Rel_guess[0], parameter_config['salt_rejection_sw'])
    DeltaX[0] = (Xc_out_elem[0] - Xf[0, 0])/ndiscr
    Xc[0, 0] = Xf[0, 0] + DeltaX[0]
    Xp[0, 0] = (Mf[0, 0] * Xf[0, 0] - Mc[0, 0] * Xc[0, 0]) / Mp[0, 0]
    MXp_cum[0, 0] = Mp[0, 0] * Xp[0, 0]
    MXp_cum_elem[0, 0] = Mp[0, 0] * Xp[0, 0]
    Xp_cum_elem[0, 0] = Xp[0, 0]
    for i_subelem in range(1, ndiscr):
        Mf[i_subelem, 0] = Mc[i_subelem - 1, 0]
        Mc[i_subelem, 0] = Mf[i_subelem, 0] - Mp[i_subelem, 0]
        Xf[i_subelem, 0] = Xc[i_subelem - 1, 0]
        Xc[i_subelem, 0] = Xf[i_subelem, 0] + DeltaX[0]
        Xp[i_subelem, 0] = (Mf[i_subelem, 0] * Xf[i_subelem, 0] - Mc[i_subelem, 0] * Xc[i_subelem, 0]) / Mp[
            i_subelem, 0]
        Mp_cum[i_subelem, 0] = Mp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum[i_subelem, 0] = MXp_cum[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Mp_cum_elem[i_subelem, 0] = Mp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0]
        MXp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem - 1, 0] + Mp[i_subelem, 0] * Xp[i_subelem, 0]
        Xp_cum_elem[i_subelem, 0] = MXp_cum_elem[i_subelem, 0] / Mp_cum_elem[i_subelem, 0]
    Xp_out_elem[0] = MXp_cum_elem[ndiscr - 1, 0] / Mp_cum_elem[ndiscr - 1, 0]
    for i_elem in range(1, nelem):
        Mf[0, i_elem] = Mc[ndiscr-1, i_elem - 1]
        Mc[0, i_elem] = Mf[0, i_elem] - Mp[0, i_elem]
        Xf[0, i_elem] = Xc[ndiscr-1, i_elem - 1]
        Xc_out_elem[i_elem] = single_stage.global_balance_xc(Xf[0, i_elem], Rel_guess[i_elem], parameter_config['salt_rejection_sw'])
        DeltaX[i_elem] = (Xc_out_elem[i_elem] - Xf[0, i_elem])/ndiscr
        Xc[0, i_elem] = Xf[0, i_elem] + DeltaX[i_elem]
        Xp[0, i_elem] = (Mf[0, i_elem] * Xf[0, i_elem] - Mc[0, i_elem] * Xc[0, i_elem]) / Mp[0, i_elem]
        Mp_cum[0, i_elem] = Mp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem]
        MXp_cum[0, i_elem] = MXp_cum[ndiscr - 1, i_elem - 1] + Mp[0, i_elem] * Xp[0, i_elem]
        Mp_cum_elem[0, i_elem] = Mp[0, i_elem]
        MXp_cum_elem[0, i_elem] = Mp[0, i_elem] * Xp[0, i_elem]
        Xp_cum_elem[0, i_elem] = Xp[0, i_elem]
        for i_subelem in range(1, ndiscr):
            Mf[i_subelem, i_elem] = Mc[i_subelem - 1, i_elem]
            Mc[i_subelem, i_elem] = Mf[i_subelem, i_elem] - Mp[i_subelem, i_elem]
            Xf[i_subelem, i_elem] = Xc[i_subelem - 1, i_elem]
            Xc[i_subelem, i_elem] = Xf[i_subelem, i_elem] + DeltaX[i_elem]
            Xp[i_subelem, i_elem] = (Mf[i_subelem, i_elem] * Xf[i_subelem, i_elem] - Mc[i_subelem, i_elem]*
                                     Xc[i_subelem, i_elem])/Mp[i_subelem, i_elem]
            Mp_cum[i_subelem, i_elem] = Mp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum[i_subelem, i_elem] = MXp_cum[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Mp_cum_elem[i_subelem, i_elem] = Mp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem]
            MXp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem - 1, i_elem] + Mp[i_subelem, i_elem] * Xp[i_subelem, i_elem]
            Xp_cum_elem[i_subelem, i_elem] = MXp_cum_elem[i_subelem, i_elem] / Mp_cum_elem[i_subelem, i_elem]
        Xp_out_elem[i_elem] = MXp_cum_elem[ndiscr - 1, i_elem] / Mp_cum_elem[ndiscr - 1, i_elem]
    Xp_cum = MXp_cum[ndiscr-1, nelem-1]/Mpermeate
    Rel = np.array([0.1 for i in range(0, nelem)])
    Rel_calc = Rel_guess * 1
    CPF = CPF_guess
    Mpermeate_calc = Mpermeate
    R1_calc = R1_guess * 1
    solver = single_stage.ROsolver_doublestage_discrete(Rtot, Xret_out, R1_calc, toll_R1, Rel_calc, Rel, toll_Rel, P, Pfeed, Xf, Xfeed,
                                                        Mf, Mp, Mc, Mp_cum, MXp_cum, Xp, Xc, Mfeed, Fw, Fs, TCF, AFS, CPF,
                                                        Amembr_real, Bmembr_real, Amembr, NPV_1, NPV_2, parameter_config,
                                                        nelem_1, nelem_2, Plosses, Posm_out_1, Mpermeate_calc, Xp_cum,
                                                        ndiscr, Mp_cum_elem, MXp_cum_elem, Xp_cum_elem, Xfs, Xp_out_elem,
                                                        Amembr_tot_1, Amembr_tot_2)
    return solver





