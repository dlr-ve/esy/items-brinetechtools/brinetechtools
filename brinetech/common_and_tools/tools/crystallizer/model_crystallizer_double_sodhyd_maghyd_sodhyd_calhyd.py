import numpy as np

from ...common.economics import calculate_annual_depreciation_cost
from ...common.properties import density_seawater_liquid
from ...common import calculate_pump_efficiency
from ...common.iotools import ResultList
from .crystallization_process import crystallizer_start



def maghyd_calhyd_cryst_CC(total_flow_rate, Q_MgHydro_out_vol, Q_CaHydro_out_vol, param):
    
    velocity = param['fluid_vel'] * 3600
    section_area = total_flow_rate / velocity
    cryst_volume = section_area * param['reactor_length']
    Cp_0_cryst = 10**(param['k1_cryst_batch'] + param['k2_cryst_batch'] * np.log10(cryst_volume)
                      + param['k3_cryst_batch'] * np.log10(cryst_volume)**2)
    Cp_0_cryst_current = Cp_0_cryst * param['CEPCI_current'] / param['CEPCI_reference']
    Cbm_cryst = Cp_0_cryst_current * (param['b1_cryst_batch'] + param['b2_cryst_batch'] *
                                      param['Fm_cryst_batch'] * param['Fp_cryst_batch'])
    Cbm_tot_cryst = Cbm_cryst * 2
    A_filter_Mg = param['design_ratio_filter'] * Q_MgHydro_out_vol * param['MgOH2_density'] / 1000
    A_filter_Ca = param['design_ratio_filter'] * Q_CaHydro_out_vol * param['CaOH2_density'] / 1000
    A_filter = max([A_filter_Ca, A_filter_Mg])
    Cp_0_filter = 10**(param['k1_disc-drum_filter'] + param['k2_disc-drum_filter'] *
                       np.log10(A_filter) + param['k3_disc-drum_filter'] * np.log10(A_filter)**2)
    Cp_0_filter_current = Cp_0_filter * param['CEPCI_current'] / param['CEPCI_reference']
    Cbm_filter = Cp_0_filter_current * param['Fbm_disc-drum_filter']
    Cbm_tot_filter = Cbm_filter * 2
    return Cbm_tot_cryst, Cbm_tot_filter


def crystallizer_model(tech_config_and_params, proj_config_and_params, Cfeed, N_trains, Qfeed, electricity_cost):  # Qfeed in m3/h
    Qfeed_Mg_mol, Qfeed_Ca_mol, rho_feed, Cfeed_ppm, C_Na_NaOHsol_ppm, Q_NaOH_tot_mol = crystallizer_start(
        tech_config_and_params, Cfeed, Qfeed
    )

    # NaOH solution with water:
    Q_NaOH_tot_vol = Q_NaOH_tot_mol / tech_config_and_params['conc_NaOH_sol'] / 1000
    Q_MgHydro_out_mol = Qfeed_Mg_mol
    Q_MgHydro_out_vol = Q_MgHydro_out_mol * tech_config_and_params['MW_MgOH2'] / tech_config_and_params['MgOH2_density']
    Q_MgHydro_out_mass = Q_MgHydro_out_vol * tech_config_and_params['MgOH2_density'] / 1000  # [kg/h]
    Q_CaHydro_out_mol = Qfeed_Ca_mol
    Q_CaHydro_out_vol = Q_CaHydro_out_mol * tech_config_and_params['MW_CaOH2'] / tech_config_and_params['CaOH2_density']
    Q_CaHydro_out_mass = Q_CaHydro_out_vol * tech_config_and_params['CaOH2_density'] / 1000  # [kg/h]
    Mfeed_kg_h = Qfeed * rho_feed
    M_NaOH_kg_h = Q_NaOH_tot_vol * tech_config_and_params['NaOH_sol_density']
    Meffluent_kg_h = Mfeed_kg_h + M_NaOH_kg_h - Q_MgHydro_out_mass - Q_CaHydro_out_mass
    C_Na_effluent_ppm = (Mfeed_kg_h * Cfeed_ppm['Na'] + M_NaOH_kg_h * C_Na_NaOHsol_ppm) / Meffluent_kg_h
    # TODO: Check if the following variable should be used somewhere?
    # C_Cl_effluent_ppm = Mfeed_kg_h * Cfeed_ppm['Cl'] / Meffluent_kg_h
    C_Na_effluent_mol_kg = C_Na_effluent_ppm / 1000 / tech_config_and_params['MW_Na']
    C_NaCl_effluent_mol_kg = C_Na_effluent_mol_kg * 1
    C_NaCl_effluent_ppm = C_NaCl_effluent_mol_kg * 1000 * (tech_config_and_params['MW_Na'] + tech_config_and_params['MW_Cl'])
    rho_effluent = density_seawater_liquid(tech_config_and_params['T'] - 273, C_NaCl_effluent_ppm)
    Q_effluent = Meffluent_kg_h / rho_effluent  # [m3/h]
    C_NaCl_effluent = C_NaCl_effluent_ppm * rho_effluent / 1000 \
        / (tech_config_and_params['MW_Na'] + tech_config_and_params['MW_Cl'])

    # Q_effluent = Qfeed + Q_NaOH_tot_vol - Q_CaHydro_out_vol - Q_MgHydro_out_vol
    # Q_effluent = Meffluent_kg_h / 1000  # supposed density = 1000 kg/m3
    # C_Na_effluent = (Qfeed * Cfeed['Na'] + Q_NaOH_tot_mol) / Q_effluent
    # C_Cl_effluent = Qfeed * Cfeed['Cl'] / Q_effluent
    # TODO: Check if the following variable should be used somewhere?
    # C_OH_effluent = (Q_NaOH_tot_mol - 2 * (Q_CaHydro_out_mol + Q_MgHydro_out_mol)) / Q_effluent

    spec_cost_NaOH_cryst_mol = tech_config_and_params['specific_cost_NaOH_cryst'] * 1e-6 \
        * tech_config_and_params['MW_NaOH']  # [$/mol]
    spec_cost_water_l = tech_config_and_params['specific_cost_water'] / 1000  # [$/l]
    specific_cost_NaOH_sol = (spec_cost_NaOH_cryst_mol * tech_config_and_params['conc_NaOH_sol'] + spec_cost_water_l *
                              (1 - tech_config_and_params['conc_NaOH_sol'] * tech_config_and_params['MW_NaOH'] /
                               (tech_config_and_params['NaOH_density'] / 1000))) * 1000  # [$/m3]
    cost_NaOH_sol = specific_cost_NaOH_sol * Q_NaOH_tot_vol * 8760 \
        * tech_config_and_params['plant_availability']
    revenue_MgHydro_cryst = tech_config_and_params['price_MgHydro_cryst'] / 1e6 * tech_config_and_params['MgOH2_density'] * \
        Q_MgHydro_out_vol * 8760 * tech_config_and_params['plant_availability']
    revenue_CaHydro_cryst = tech_config_and_params['price_CaHydro_cryst'] / 1e6 * tech_config_and_params['CaOH2_density'] * \
        Q_CaHydro_out_vol * 8760 * tech_config_and_params['plant_availability']

    total_flow_rate = Qfeed + Q_NaOH_tot_vol

    Cbm_tot_cryst, Cbm_tot_filter = maghyd_calhyd_cryst_CC(total_flow_rate, Q_MgHydro_out_vol, Q_CaHydro_out_vol, param=tech_config_and_params)
    total_annual_capex = calculate_annual_depreciation_cost(
        proj_config_and_params['interest_rate'], tech_config_and_params['depreciation_time_cryst'], (Cbm_tot_cryst + Cbm_tot_filter)
    )
    pump_efficiency = calculate_pump_efficiency(total_flow_rate*24)
    pump_power_cryst = tech_config_and_params['Pscarico_pump_cryst'] * 1e5 * total_flow_rate / 3600 / pump_efficiency * 1e-3  # [kW]
    flowrate_sedim_Mg = Q_MgHydro_out_mass / tech_config_and_params['magma_density_sedim_Mg']  # [m3/h]
    flowrate_sedim_Ca = Q_CaHydro_out_mass / tech_config_and_params['magma_density_sedim_Ca']  # [m3/h]
    power_Mg_filtration = tech_config_and_params['ref_consumption_filter'] / tech_config_and_params['ref_flowrate_filter'] \
        * flowrate_sedim_Mg
    power_Ca_filtration = tech_config_and_params['ref_consumption_filter'] / tech_config_and_params['ref_flowrate_filter'] \
        * flowrate_sedim_Ca
    power_filtration_tot = power_Mg_filtration + power_Ca_filtration
    total_power = pump_power_cryst + power_filtration_tot
    cost_electric_energy_demand = total_power * electricity_cost * 8760 * tech_config_and_params['plant_availability']
    total_opex = cost_NaOH_sol + cost_electric_energy_demand   # [$/y]

    crystallizer_inputs = ResultList(title="crystallizer inputs", xls_sheet="crystallizer", results=[
        ('No. of trains', (N_trains, '-')),
        ('Qfeed per train', (Qfeed, 'm3/h')),
        ('rho feed', (rho_feed, 'kg/m3')),
        ('Mfeed per train', (Mfeed_kg_h, 'kg/h')),
        ('conc NaOH sol', (tech_config_and_params['conc_NaOH_sol'], 'mol/l')),
        ('rho NaOH sol', (tech_config_and_params['NaOH_sol_density'], 'kg/m3')),
        ('ion', ('C [mol/m3', 'C [ppm]')),
    ] + [(key, (Cfeed[key], Cfeed_ppm[key])) for key in Cfeed.keys()])

    crystallizer_results = ResultList(title="crystallizer results", xls_sheet="crystallizer", xls_y0=4, results=[
         ('Q Mg inlet mol', (Qfeed_Mg_mol, 'mol/h')),
         ('Q Ca inlet mol', (Qfeed_Ca_mol, 'mol/h')),
         ('Q NaOH tot mol', (Q_NaOH_tot_mol, 'mol/h')),
         ('Q NaOH tot vol', (Q_NaOH_tot_vol, 'm3/h')),
         ('Q Mg(OH)2 tot vol', (Q_MgHydro_out_vol, 'm3/h')),
         ('Q Ca(OH)2 tot vol', (Q_CaHydro_out_vol, 'm3/h')),
         ('Q Mg(OH)2 mass', (Q_MgHydro_out_mass, 'kg/h')),
         ('Q Ca(OH)2 mass', (Q_CaHydro_out_mass, 'kg/h')),
         ('Q effluent', (Q_effluent, 'm3/h')),
         ('C NaCl effluent', (C_NaCl_effluent, 'mol/m3')),
         ('NaOH cost', (cost_NaOH_sol, '$/y')),
         ('electric cons cryst', (pump_power_cryst, 'kW')),
         ('electric cons filter', (power_filtration_tot, 'kW')),
         ('cost electricity', (cost_electric_energy_demand, '$/y')),
         ('cost crystallizer', (Cbm_tot_cryst, '$')),
         ('cost filter', (Cbm_tot_filter, '$')),
         ('annual capex', (total_annual_capex, '$/y')),
         ('annual opex', (total_opex, '$/y')),
         ('revenue Mg(OH)2', (revenue_MgHydro_cryst, '$/y')),
         ('revenue Ca(OH)2', (revenue_CaHydro_cryst, '$/y')),
    ])

    other = ResultList(xls_sheet="crystallizer", xls_x0=9, xls_y0=8, results=[
        ('M effluent', (Meffluent_kg_h, 'kg/h')),
        ('C NaCl ppm', (C_NaCl_effluent_ppm, 'ppm')),
    ])

    return [crystallizer_inputs, crystallizer_results, other]
