from .logger import logger
import numpy as np
from . import tools, h2oprop

def price_tubes(thickness, material):
    if material == 'Cu90Ni10Fe1.5':
        price = 34.615 * thickness + 10.077
    elif material == 'Cu70Ni30Fe1.5':
        price = 65.358 * thickness - 0.0769
    elif material == '26Cr1-3.5Ni3.5Mo':  # Stainless Steel 316L
        price = 70 * thickness + 10
    elif material == 'Al-6XN':
        price = 130 * thickness + 3
    elif material == '20Cr22Ni7.5Mo0.5N':
        price = 110 * thickness + 41
    elif material == '1.45654':
        price = 110 * thickness + 22.333
    elif material == 'Titanium':
        price = 105 * thickness + 10.5
    return price


def capital_costs_gebel(material, area_hx, config):
    # tube_price = price_tubes(config['tube_thickness'], config['material'])
    # tube_price = 0.63 * config['current_CEPCI'] / 394 * 7850 * config['tube_thickness'] * 1e-3
    # tube_price = 5.95 * config['current_CEPCI'] / 394 * 8000 * config['tube_thickness'] * 1e-3
    tube_price = config['spec_tube_price'][material] * config['current_CEPCI'] / config['CEPCI_2000'] * config['density_tube'][material] \
                 * config['tube_thickness'] * 1e-3
    tot_tube_price = tube_price * area_hx
    evap_price = tot_tube_price * config['fev_tubes']
    pipe_price = tot_tube_price * config['fpipe_tubes']
    plant_price = evap_price * config['fplant_ev']
    return evap_price, plant_price


def bare_module_cost_equipment(Pabs, config, size, equipment, material):  # Pabs in bar
    Cp_0 = np.power(10, (config['k1'][equipment] + config['k2'][equipment] * np.log10(size) + config['k3'][equipment] * (np.log10(size))**2))
    Cp_0 = Cp_0 * config['current_CEPCI'] / config['reference_CEPCI']
    P = Pabs - 1
    if config['vessel_bool'][equipment]:
        Fp = (((P+1)* config['D_vessel'] /(2*(850-0.6*(P+1))))+ 0.00315)/0.0063
    else:
        if P > 5:
            Fp = np.power(10, (config['c1'][equipment] + config['c2'][equipment] * np.log10(P) + config['c3'][equipment] * (np.log10(P))**2))
        else:
            Fp = 1
    if config['vessel_bool'][equipment]:
        Fm = config['fm_vessel'][material]
        Cbm = Cp_0 * (config['b1'][equipment] + config['b2'][equipment] * Fm * Fp)
    elif config['heat_exchanger_bool'][equipment]:
        Fm = config['fm_heat_exchanger'][material]
        Cbm = Cp_0 * (config['b1'][equipment] + config['b2'][equipment] * Fm * Fp)
    elif config['evaporator_bool'][equipment]:
        Fbm = config['fbm_evap'][material]
        Cbm = Cp_0 * Fbm * Fp
    elif config['compressor_bool'][equipment]:
        Fbm = config['fbm_compressor'][material]
        Cbm = Cp_0 * Fbm
    else:
        logger.warning('choose one equipment among evaporator, heat_exchanger, vessel and compressor')
    return Cp_0, Cbm


def bare_module_cost_general_evap_cond(Pabs, config, size, equipment, material):
    Cp_0, Cbm = bare_module_cost_equipment(Pabs, config, size, equipment, material)
    if config['evaporator_bool'][equipment] and size <= config['max_size_evap']:
        Cp_0, Cbm = bare_module_cost_equipment(Pabs, config, size, equipment, material)
    elif config['evaporator_bool'][equipment] and size > config['max_size_evap']:
        Cp_0_maxarea, Cbm_maxarea = bare_module_cost_equipment(Pabs, config, config['max_size_evap'], equipment, material)
        Cbm = Cbm_maxarea * (size/config['max_size_evap'])**0.6
    elif config['heat_exchanger_bool'][equipment] and size <= config['max_size_heat_exchanger']:
        Cp_0, Cbm = bare_module_cost_equipment(Pabs, config, size, equipment, material)
    elif config['heat_exchanger_bool'][equipment] and size > config['max_size_heat_exchanger']:
        Cp_0_maxarea, Cbm_maxarea = bare_module_cost_equipment(Pabs, config, config['max_size_heat_exchanger'], equipment, material)
        Cbm = Cbm_maxarea * (size/config['max_size_heat_exchanger'])**0.59
    return Cp_0, Cbm


def capital_cost_esfahani(Atot):
    Ca = 140 * Atot
    Ceq = 4 * Ca
    Cs = 0.2 * Ceq
    Ctr = 0.05 * (Ca + Ceq + Cs)
    Cb = 0.15 * Ceq
    Cen = 0.1 * Ceq
    Cc = 0.1 * (Ca + Ceq + Cs)
    CC = Ca + Ceq + Cs + Ctr + Cb + Cen + Cc
    return CC

'''operative costs'''

def depreciation_calculation(z, Nyears, plant_price, depr=[]):
    q = 1 + z
    for i_n in range(1, Nyears+1):
        r = q**i_n * (q-1)/(q**i_n -1)
        depr.append(plant_price * r)
    return depr

def manteinance_costs(config, plant_price, labour_price):
    mant_cost = config['manteinance_cost_CAPEXperc'] * plant_price + config['manteinance_labour_PERSONNELperc'] * labour_price
    return mant_cost


def specific_electric_cost(config, spec_el_consumption, Mdist, electricity_cost):  #Mdist in kg/s, spec_el_con in kWh/m3
    operating_hours = config['annual_hours'] * config['plant_availability']
    electric_consumption = spec_el_consumption * (Mdist * 3600 / 1000 * operating_hours) # [kWh/y]
    annual_el_cost = electric_consumption * electricity_cost # [US$/y]
    spec_el_cost = spec_el_consumption * electricity_cost # [US$/m3]
    return annual_el_cost, spec_el_cost

def specific_electric_cost_input_elcost(config, spec_el_consumption, Mdist, el_cost):  #Mdist in kg/s, spec_el_con in kWh/m3
    operating_hours = config['annual_hours'] * config['plant_availability']
    electric_consumption = spec_el_consumption * (Mdist * 3600 / 1000 * operating_hours) # [kWh/y]
    annual_el_cost = electric_consumption * el_cost # [US$/y]
    spec_el_cost = spec_el_consumption * el_cost # [US$/m3]
    return annual_el_cost, spec_el_cost

def heat_cost_calculation(Pmotive, config):  # correl in [euro/MWhth], alla fine cambio per [US$/MWhth]
    spec_heat_cost = 1
    if config['heat_source'] == 'oil':
        if Pmotive <= 1:
            spec_heat_cost = 21.938 * Pmotive**3 -53.989 * Pmotive**2 + 53.046 * Pmotive - 2.991
        else:
            spec_heat_cost = 0.0037 * Pmotive**3 -0.1745 * Pmotive**2 + 3.3352 * Pmotive + 18.018
    elif config['heat_source'] == 'gas':
        # if Pmotive <=1:
        #     spec_heat_cost = 27.448 * Pmotive**3 - 64.9 * Pmotive**2 + 59.761 * Pmotive - 3.4537
        # else:
        #     spec_heat_cost = 0.0035 * Pmotive**3 - 0.1774 * Pmotive**2 + 3.5285 * Pmotive + 18.851
        spec_heat_cost = 8.7109 * np.log(Pmotive) + 19.657
    elif config['heat_source'] == 'waste_heat':
        spec_heat_cost = 0
    return spec_heat_cost * config['change_US$/euro']


def specific_heat_cost (config, Mmotive, Pmotive, Tmotive, Mdist):  #Mmotive in kg/s, Pmotive in bar, Tmotive in °C, Mdist in kg/s
    Q_req = Mmotive * h2oprop.latent_heat(Tmotive)  # [kWth]
    thermal_cost = Q_req * heat_cost_calculation(Pmotive, config) * 1e-3   # [US$/h]
    annual_thermal_cost = thermal_cost * config['annual_hours'] * config['plant_availability']   # [US$/y]
    spec_th_cost = thermal_cost/Mdist * 1000/3600  # [US$/m3]
    return annual_thermal_cost, spec_th_cost

def specific_heat_cost_input_heatcost (config, Mmotive, Pmotive, Tmotive, Mdist, heat_cost):  #Mmotive in kg/s, Pmotive in bar, Tmotive in °C, Mdist in kg/s, heat_cost in $/kWh
    Q_req = Mmotive * h2oprop.latent_heat(Tmotive)  # [kWth]
    thermal_cost = Q_req * heat_cost   # [US$/h]
    annual_thermal_cost = thermal_cost * config['annual_hours'] * config['plant_availability']   # [US$/y]
    spec_th_cost = thermal_cost/Mdist * 1000/3600  # [US$/m3]
    return annual_thermal_cost, spec_th_cost


def specific_chem_cost(config, Mcw, Mfeed, Mdist):
    chlorine_pre_annual_cons = config['annual_hours'] * (Mcw * 3600/1000) *\
                               1000 * config['dosing_pre_chlorine'] / config['conc_chlorine'] * 1e-9
    chlorine_pre_annual_cost = chlorine_pre_annual_cons * config['cost_chlorine']
    antiscalant_annual_cons = config['annual_hours'] * (Mfeed * 3600 / 1000) * \
                               1000 * config['dosing_antiscalant'] / config['conc_antiscalant'] * 1e-9
    antiscalant_annual_cost = antiscalant_annual_cons * config['cost_antiscalant']
    antifoaming_annual_cons = config['annual_hours'] * (Mfeed * 3600 / 1000) * \
                              1000 * config['dosing_antifoaming'] / config['conc_antifoaming'] * 1e-9
    antifoaming_annual_cost = antifoaming_annual_cons * config['cost_antifoaming']
    ca_hydroxide_annual_cons = config['annual_hours'] * (Mdist * 3600 / 1000) * \
                              1000 * config['dosing_calcium_hydroxide'] / config['conc_calcium_hydroxide'] * 1e-9
    ca_hydroxide_annual_cost = ca_hydroxide_annual_cons * config['cost_calcium_hydroxide']
    polyelectrolyte_annual_cons = config['annual_hours'] * (0.03 * Mdist * 3600 / 1000) * \
                              1000 * config['dosing_polyelectrolyte'] / config['conc_polyelectrolyte'] * 1e-9
    polyelectrolyte_annual_cost = polyelectrolyte_annual_cons * config['cost_polyelectrolyte']
    CO2_annual_cons = config['annual_hours'] * (Mdist * 3600 / 1000) * \
                              1000 * config['dosing_CO2'] / config['conc_CO2'] * 1e-9
    CO2_annual_cost = CO2_annual_cons * config['cost_CO2']
    chlorine_post_annual_cons = config['annual_hours'] * (Mdist * 3600 / 1000) * \
                               1000 * config['dosing_post_chlorine'] / config['conc_chlorine'] * 1e-9
    chlorine_post_annual_cost = chlorine_post_annual_cons * config['cost_chlorine']
    tot_annual_cost = chlorine_pre_annual_cost + antiscalant_annual_cost + antifoaming_annual_cost + ca_hydroxide_annual_cost \
                      + polyelectrolyte_annual_cost + CO2_annual_cost + chlorine_post_annual_cost  # [US$/year]
    spec_cost = tot_annual_cost / (Mdist * 3600 / 1000) / config['annual_hours']
    return tot_annual_cost, spec_cost







