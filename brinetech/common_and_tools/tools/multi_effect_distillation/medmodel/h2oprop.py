import numpy as np
from iapws import IAPWS95

'''thermodynamic properties of water (El Dessouki)'''


def sat_temp(p: float) -> float:  # T in [°C] and P in kPa
    # assert P > 0, 'ups'
    tbp = (42.6776-3892.7/(np.log(p/1000.)-9.48654))-273.15
    return tbp


def sat_pressure(t):  # T in [°C] and P in kPa
    pc = 22089  # critical pressure [kPa]
    tc = 647.286  # critical temperature [K]
    f = [-7.419242, 0.29721, -0.1155286, 0.008685635, 0.001094098, -0.00439993, 0.002520658, -0.000521868]
    f_sum = 0
    for i in range(1, 9):
        f_sum += f[i-1]*(0.01*(t + 273.15 - 338.15))**(i-1)
    pbt = pc * np.exp((tc/(t+273.15)-1)*f_sum)
    return pbt


def latent_heat(t):  # T is the saturation temperature in [°C] and lambda_vap in [kJ/kg]
    lambda_vap = 2501.897149 - 2.4070640037 * t + 1.192212 * 1e-3 * t**2 - 1.5863 * 1e-5 * t**3
    return lambda_vap


def enthalpy_satwat_liquid(t):  # T is the saturation temperature in [°C] and hswl in [kJ/kg]
    hswl = -0.033635409 + 4.207557011 * t - 6.200339 * 1e-4 * t**2 + 4.459374 * 1e-6 * t**3
    return hswl


def enthalpy_satwat_vapor(t):  # T is the saturation temperature in [°C] and hswv in [kJ/kg]
    hswv = 2501.689845 + 1.806916015 * t + 5.087717 * 1e-4 * t**2 - 1.1221 * 1e-5 * t**3
    return hswv


def density_satwat_vapor(t):   # rho_SatWV in [kg/m3], T in [°C]
    vc = 0.003172222   # critical volume [m3/kg]
    tc = 647.286   # critical temperature [K]
    f = [83.63213098, -0.668265339, 0.002495964, -5.04185E-6, 5.34205E-9, -2.3279E-12]
    f_sum = 0
    for i in range(1, 7):
        f_sum += f[i-1] * (t + 273.15)**(i-1)
    v = vc * (tc/(t + 273.15) - 1) * np.exp(f_sum)
    rho_satwat_v = 1/v
    return rho_satwat_v


def density_satwat_liquid(t):   # rho_SatWL in [kg/m3], T in [°C]
    vc = 0.003172222   # critical volume [m3/kg]
    tc = 647.286   # critical temperature [K]
    f = [-2.781015567, 0.002543267, 9.845047E-6, 3.636115E-9, -5.358938E-11, 7.019341E-14]
    f_sum = 0
    for i in range(1, 7):
        f_sum += f[i - 1] * (t + 273.15) ** (i - 1)
    v = vc * (tc / (t + 273.15) - 1) * np.exp(f_sum)
    rho_satwat_l = 1. / v
    return rho_satwat_l


def dynviscosity_satwat_liquid(t):  # mu in [kg/m s], t in [°C]
    mu = np.exp(-3.79418 + 604.129/(139.18 + t)) * 1e-3
    return mu


def dynviscosity_satwat_vapor(t):
    mu = np.exp(-3.609417664 + 275.928958 / (-227.0446083 - 0.896081232 * t - 0.002291383 * t**2)) * 1e-3
    return mu


def surface_tension_satwat (t):  # sigma in N/m, t in [°C]
    sigma = 7.5798 * 1e-2 - 1.4691 * 1e-4 * t - 2.2173 * 1e-7 * t**2
    return sigma


def thermal_conductivity_liquid(T):  # Perry pag 2-444, k in W/(m K), T in °C
    T = T + 273
    C1 = -0.432
    C2 = 0.0057255
    C3 = -0.000008078
    C4 = 1.861 * 1e-9
    k_liq = C1 + C2*T + C3 * T**2 + C4 * T**3
    return k_liq


def thermal_conductivity_vapor(T):  # Perry pag. 2-438, k in W/(m K), T in °C
    T = T + 273
    C1 = 6.2041 * 1e-6
    C2 = 1.3973
    k_vap = C1 * np.power(T, C2)
    return k_vap