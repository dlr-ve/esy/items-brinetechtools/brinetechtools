
#%%

import pandas as pd
from common_and_tools.common import density_mixtures


df_output = pd.read_excel('./results/intake_system/arabian_gulf_kuwait/average_tds/results_qfeedm3h_train=200.0.xls')

df_output

#%%

# mass and energy flow
no_of_trains = float(df_output.iloc[0,1])
Qfeed_out_m3h_tot = float(df_output.iloc[0,5]) * no_of_trains
recovery_intake = float(df_output.iloc[2,1])
electric_power_req_tot = float(df_output.iloc[2,5]) * no_of_trains   # kW_el
annual_electric_energy_req_tot = float(df_output.iloc[3,5]) * no_of_trains   # kWh_el/a

# costs
capital_cost_tot = float(df_output.iloc[1,10]) * no_of_trains  # [USD]
capex_tot = float(df_output.iloc[1,9]) * no_of_trains  # [USD/y]
fixed_opex_tot = float(df_output.iloc[3,9]) * no_of_trains  # [USD/y]
variable_opex_tot = float(df_output.iloc[5,9]) * no_of_trains  # [USD/y]
total_opex_tot = fixed_opex_tot + variable_opex_tot  # [USD/y]

print(f"no_of_trains: {no_of_trains}, Qfeed_out_m3h_tot: {Qfeed_out_m3h_tot}, recovery_intake: {recovery_intake}, electric_power_req_tot: {electric_power_req_tot}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}")
print(f"capital_cost_tot: {capital_cost_tot}, capex_tot: {capex_tot}, fixed_opex_tot: {fixed_opex_tot}, variable_opex_tot: {variable_opex_tot}")

#%%