
#%%

import pandas as pd
from common_and_tools.common import density_mixtures
from common_and_tools.common import density_seawater_liquid

df_output = pd.read_excel('./results/crystallizer/crystallizer_single_self_sodchl_electric/arabian_gulf_kuwait/average_tds/results_mfeedkgh_train_49004.33_electric.xls')

df_output

#%%
df_output.iloc[3,5]

#%%


## Flow rates
no_of_trains = float(df_output.iloc[1,1])
Qfeed_m3h_tot = float(df_output.iloc[3,1]) * no_of_trains
Mnacl_kgh_tot = float(df_output.iloc[3,5]) * no_of_trains
Mnacl_ton_annum_tot = float(df_output.iloc[4,5]) * no_of_trains
Qperm_m3h_tot = float(df_output.iloc[5,5]) * no_of_trains

## Flow concentrations
Xperm_NaCl_ppm = 0  # assumed, as it is distilled water

print(f"no_of_trains: {no_of_trains}, Qfeed_m3h_tot : {Qfeed_m3h_tot}, Mnacl_kgh_tot: {Mnacl_kgh_tot}, Mnacl_ton_annum_tot: {Mnacl_ton_annum_tot}, Qperm_m3h_tot: {Qperm_m3h_tot}")


#%%
df_output.iloc[7,5]

#%%
## Economic and energy requirements (same cells for single and double stage)
# convert to float from float64, as RCE gives wierd error: Unexpected error: ScriptException; cause: Unrecognized token 'np' 
total_capex_tot = float(df_output.iloc[1,11]) * no_of_trains
total_opex_tot = float(df_output.iloc[8,11]) * no_of_trains
annual_costs_tot_naclcryst = total_capex_tot + total_opex_tot # [USD/y] annualized capital + opex (fixed+variable)
perm_recovery_rate = float(df_output.iloc[7,5])
annual_electric_energy_req_tot = float(df_output.iloc[9,7]) * no_of_trains   # kWh_el/a

print(f"annual_costs_tot_naclcryst: {annual_costs_tot_naclcryst}, perm_recovery_rate: {perm_recovery_rate}, annual_electric_energy_req_tot: {annual_electric_energy_req_tot}")


# %%
