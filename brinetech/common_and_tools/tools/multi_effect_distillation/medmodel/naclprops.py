import numpy as np
from .modelparams import crosschecks
from .logger import logger
from . import h2oprop
import math


''' thermodynamic properties of NaCl-water solution '''

def bpe_brine_act_old(t, x):  # BPE in [°C], T in [°C] and X in [ppm]
    a = 8.325 * 1e-2 + 1.883 * 1e-4 * t + 4.02 * 1e-6 * t**2
    b = -7.625 * 1e-4 + 9.02 * 1e-5 * t - 5.2 * 1e-7 * t**2
    c = 1.522 * 1e-4 - 3 * 1e-6 * t - 3 * 1e-8 * t**2
    bpe = a * 1. * x * 1e-4 + b * 1. * x**2 * 1e-8 + c * 1. * x**3 * 1e-12
    return bpe


def bpe_brine_act(t, x):
    t = t + 273
    zc = 1
    za = -1
    alfa1 = 2
    m = x * 1e-3 /((1 - x * 1e-6) * 58.44)
    mc = ma = m
    Mw = 18
    beta0 = 0.0765
    beta1 = 0.2664
    Cca_phi = 0.00127
    R = 8.314
    # A_phi = -61.44534 * math.exp((t-273.15) / 273.15) + 2.864468 * (math.exp((t-273.15) / 273.15))**2 + 183.5379 * math.log((t)/273.15, math.e)
    # - 0.6820223 * (t-273.15) + 0.0007875695 * (t**2 - 273.15**2) + 58.95788 * 273.15 / t
    A_phi = 0.3915
    Bca_phi = beta0 + beta1 * math.exp(-alfa1 * math.sqrt(m))
    phi = 1 + (-A_phi * math.sqrt(m))/(1 + 1.2 * math.sqrt(m)) + m * Bca_phi + m**2 * Cca_phi
    lnA = - phi * Mw * 2* m / 1000
    invTdiff = R * lnA / (h2oprop.latent_heat(t-273) * 18)
    t_inn = 1/((1/t) + invTdiff)
    bpe = t_inn - t
    return bpe


def cp_seawater(t, x):  # Cp in [kJ/(kg °C)], T in [°C] and X in [ppm].
    # correlation valid for range of salinity X [ppm] between 20000 and 160000 and of Temperature between 20 and 180°C
    a = 4206.8 - 6.6197 * x * 1e-3 + 1.2288 * 1e-2 * x**2 * 1e-6
    b = -1.1262 + 5.4178 * 1e-2 * x * 1e-3 - 2.2719 * 1e-4 * x**2 * 1e-6
    c = 1.2026 * 1e-2 - 5.3566 * 1e-4 * x * 1e-3 + 1.8906 * 1e-6 * x**2 * 1e-6
    d = 6.8777 * 1e-7 + 1.517 * 1e-6 * x * 1e-3 - 4.4268 * 1e-9 * x**2 * 1e-6
    cp = (a + b * t + c * t**2 + d * t**3) * 1e-3
    return cp


def non_equilibrium_allowance(t_previous, t, x):  # for the effect j, T_previous and T are respectively the boiling
    # brine temperatures in effects (j-1) and j, X is the salinity of the entering brine
    dt = (t - bpe_brine_act(t, x))
    #logger.info('t, x, bpe, dt values in naclprops.py are: "' + str(t) + str(', ') + str(x) + str(', ') + str(bpe_brine_act(t, x)) + str(', ') + str(
    #    dt) + '"')
    #logger.info('x value is: "' + str(x) + '"')
    #logger.info('bpe value is: "' + str(bpe_brine_act(t, x)) + '"')
    #logger.info('dt value is: "' + str(dt) + '"')
    if dt < 0:
        dt = t
    if crosschecks:
        assert dt > 0, 'T-BPE should always be positive!'
    if t_previous - t < 0:
        t_previous = t + 0.5
    nea = 33 * np.power(t_previous - t, 0.55) / dt
    return nea


def enthalpy_seawater(t, x, p=0.1):  # from web.mit.edu/seawater/ matlab files for seawater thermodynamic properties
    # h_sw in [kJ/kg], T in [°C], X in [ppm], P in [kPa]--> X and P are converted to ppt and MPa resp. in the function
    p0 = 0.1  # MPa
    p = p/1000  # from kPa to MPa
    x = x/1000  # from ppm to ppt
    h_w = 141.355 + 4202.07 * t - 0.535 * t**2 + 0.004 * t**3
    b = [-2.34825E+04, 3.15183E+05, 2.80269E+06, -1.44606E+07, 7.82607E+03, -4.41733E+01, 2.13940E-01, -1.99108E+04, 2.77846E+04, 9.72801E+01]
    h_p0 = h_w - x/1000 * (b[0] + b[1] * x/1000 + b[2] * (x/1000)**2 + b[3] * (x/1000)**3 + b[4] * t + b[5] * t**2 +
                           b[6] * t**3 + b[7] * x/1000 * t + b[8] * (x/1000)**2 * t + b[9] * x/1000 * t**2)
    c = [9.967767E+02, -3.2406, 0.0127, -4.7723E-05, -1.1748, 0.01169, -2.6185E-05, 7.0661E-08]
    h_sw_p = (p - p0) * (c[0] + c[1] * t + c[2] * t**2 + c[3] * t**3 + x * (c[4] + c[5] * t + c[6] * t**2 + c[7] * t**3))
    h_sw = (h_p0 + h_sw_p) / 1000  # h_sw in [kJ/kg]
    return h_sw


def latent_heat_seawater(T, X):   #mit correlation, X max 240000 ppm
    lambda_w = 2.5008991412E+06 - 2.3691806479E+03 * T + 2.6776439436E-01 * T**2 - 8.1027544602E-03 * T**3 - 2.0799346624E-05 * T**4
    lambda_sw = lambda_w * (1-0.001*X/1000) * 1e-3
    return lambda_sw


def dynamic_viscosity_seawater(T, X):  #mit package for seawater MATLAB  [kg/ (m s)]
    X = X*1e-6
    a = [1.5700386464E-01,
        6.4992620050E+01,
        - 9.1296496657E+01,
        4.2844324477E-05,
        1.5409136040E+00,
        1.9981117208E-02,
        - 9.5203865864E-05,
        7.9739318223E+00,
        - 7.5614568881E-02,
        4.7237011074E-04]
    mu_w = a[3] + 1. / (a[0] * (T + a[1])** 2 + a[2])

    A = a[4] + a[5] * T + a[6] * T ** 2
    B = a[7] + a[8] * T + a[9] * T ** 2
    mu = mu_w * (1 + A * X + B * X ** 2)
    return mu

def conductivity_seawater(T, X):  #mit package for seawater MATLAB [W /(m K)]
    T = 1.00024 * T
    S = X * 1e-3 / 1.00472
    logk = (np.log10(240 + 0.0002 * S) + 0.434 * (2.3 - (343.5 + 0.037 * S)/ (T + 273.15))* (1 - (T + 273.15) /
                                                                                (647.3 + 0.03 * S))** (1 / 3) - 3)
    k = 10**logk
    return k
