'''
Functions dealing with economic depreciation
'''


def calculate_annual_depreciation_cost(interest_rate, num_years, initial_price):
    """
    Calculate the annual depreciation cost of a plant over a specified number of years.

    This function calculates the annual cost of depreciation by applying a depreciation formula
    over a given number of years. The depreciation is compounded annually based on the 
    interest rate provided.

    Parameters:
    interest_rate (float): Annual interest rate (as a decimal, e.g., 0.05 for 5%).
    num_years (int): The number of years over which the plant is depreciated.
    initial_price (float): The initial price of the plant.

    Returns:
    float: The annual depreciation cost [US$/y].
    """
    compounding_factor = 1 + interest_rate
    annuity_factor = compounding_factor ** num_years * (compounding_factor - 1) / (compounding_factor ** num_years - 1)
    annual_depreciation_cost = initial_price * annuity_factor
    return annual_depreciation_cost  # [US$/y]
