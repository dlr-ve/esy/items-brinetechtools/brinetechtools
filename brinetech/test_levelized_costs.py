
#%%

import pandas as pd
from common_and_tools.common import density_mixtures

#%%
# input parameters
Qfeed_intake_tot_m3h = 2500
Qupw_requirement_tot_m3h = 80.175*2
spec_cost_brine_disposal_usd_m3 = 0.04

## market values
market_value_nacl_usd_ton = 80
market_value_water_usd_m3 = 0.5
market_value_upw_usd_m3 = 2

## MgCryst
Qfeed_water_mgcryst_tot_m3h = 215.0114    # water need of MgCryst to produce NaOH solution
Qeffluent_mgcryst_tot_m3h = 731.3176  # waste effluent from MgCryst #! brine disposal cost to be taken into account
Mmgoh2_mgcryst_tot_tonh = 5.69976

# MaClCryst
Mnacl_naclcryst_tot_tonh = 13328.77/1000*3
#purgeratio_effective_naclcryst_percfactor = 1.476803/100  # [%-factor]

## recovery rates: permeate recovery for all, except for OARO, where it refers to concentrate recovery
recovery_intake = 1.0
recovery_pretreat = 0.95
recovery_nf1 = 0.491551447384132
recovery_nf2 = 0.490306
recovery_mgcryst = 0
recovery_swro = 0.6056
recovery_hpro = 0.25
recovery_oaro = 0.1954  #concentrate recovery
recovery_naclcryst = 0.910477
recovery_secondpass_ro_ix = 0.75
recovery_posttreat = 0.95

## annual costs: annualized capital cost + fixed and variable opex, including electricity and chemical costs, but excluding brine disposal cost
annual_costs_tot_intake = 320688.34
annual_costs_tot_pretreat = 984559.79
annual_costs_tot_nf1 = 2942834.05
annual_costs_tot_nf2 = 1505122.6
annual_costs_tot_mgcryst = 27010073.4
annual_costs_tot_swro = 2437083.3
annual_costs_tot_hpro = 1815008.31
annual_costs_tot_oaro = 1353411.21
annual_costs_tot_naclcryst = 1788735.96
annual_costs_tot_secondpass_ro_ix = 517549.66
annual_costs_tot_posttreat = 227795.01


# general
plant_availability = 0.94

#%%
### calculate flow rates at tech level

## Intake and pretreatment
Qperm_intake_tot_m3h = Qfeed_intake_tot_m3h * recovery_intake
Qloss_intake_tot_m3h = Qfeed_intake_tot_m3h - Qperm_intake_tot_m3h #! brine disposal cost to be taken into account

Qfeed_pretreat_tot_m3h = Qperm_intake_tot_m3h
Qperm_pretreat_tot_m3h = Qfeed_pretreat_tot_m3h * recovery_pretreat
Qbackwash_pretreat_tot_m3h = Qfeed_pretreat_tot_m3h - Qperm_pretreat_tot_m3h #! brine disposal cost to be taken into account


## NF1
Qfeed_nf1_tot_m3h = Qperm_pretreat_tot_m3h
Qperm_nf1_tot_m3h = Qfeed_nf1_tot_m3h * recovery_nf1
Qretent_nf1_tot_m3h = Qfeed_nf1_tot_m3h * (1 - recovery_nf1)

## NF2
Qfeed_nf2_tot_m3h = Qretent_nf1_tot_m3h
Qperm_nf2_tot_m3h = Qfeed_nf2_tot_m3h * recovery_nf2
Qretent_nf2_tot_m3h = Qfeed_nf2_tot_m3h * (1 - recovery_nf2)

## Mg Cryst
Qfeed_mgcryst_tot_m3h = Qretent_nf2_tot_m3h
Qperm_mgcryst_tot_m3h = 0

## SWRO
Qfeed_swro_tot_m3h = Qperm_nf1_tot_m3h + Qperm_nf2_tot_m3h
Qperm_swro_tot_m3h = Qfeed_swro_tot_m3h * recovery_swro
Qretent_swro_tot_m3h = Qfeed_swro_tot_m3h * (1 - recovery_swro)

## HPRO
Qfeed_hpro_tot_m3h = Qretent_swro_tot_m3h
Qperm_hpro_tot_m3h = Qfeed_hpro_tot_m3h * recovery_hpro
Qretent_hpro_tot_m3h = Qfeed_hpro_tot_m3h * (1 - recovery_hpro)

## OARO
Qfeed_oaro_tot_m3h = Qretent_hpro_tot_m3h
Qperm_oaro_tot_m3h = 0
Qretent_oaro_tot_m3h = Qfeed_oaro_tot_m3h * recovery_oaro

## NaCl Cryst
Qfeed_naclcryst_tot_m3h = Qretent_oaro_tot_m3h
Qperm_naclcryst_tot_m3h = Qfeed_naclcryst_tot_m3h * recovery_naclcryst
Qpurge_naclcryst_tot_m3h = Qfeed_naclcryst_tot_m3h - Qperm_naclcryst_tot_m3h #! brine disposal cost to be taken into account

## secondpass RO + IX
Qperm_secondpass_ro_ix_tot_m3h = Qupw_requirement_tot_m3h
Qfeed_secondpass_ro_ix_tot_m3h = Qperm_secondpass_ro_ix_tot_m3h / recovery_secondpass_ro_ix
Qretent_secondpass_ro_ix_tot_m3h = Qfeed_secondpass_ro_ix_tot_m3h * (1 - recovery_secondpass_ro_ix)

## posttreat
Qfeed_posttreat_tot_m3h = Qperm_swro_tot_m3h + Qperm_hpro_tot_m3h + Qperm_naclcryst_tot_m3h - Qfeed_secondpass_ro_ix_tot_m3h - Qfeed_water_mgcryst_tot_m3h + Qretent_secondpass_ro_ix_tot_m3h
Qperm_posttreat_tot_m3h = Qfeed_posttreat_tot_m3h * recovery_posttreat
Qwaste_posttreat_tot_m3h = Qfeed_posttreat_tot_m3h * (1 - recovery_posttreat) #! brine disposal cost to be taken into account


#%%

tech_names_dict = {
    "intake": None,
    "pretreat": None,
    "nf1": None,
    "nf2": None,
    "mgcryst": None,
    "swro": None,
    "hpro": None,
    "oaro": None,
    "naclcryst": None,
    "secondpass_ro_ix": None,
    "posttreat": None
}

#%%

recovery_dict = {
    "intake": recovery_intake,
    "pretreat": recovery_pretreat,
    "nf1": recovery_nf1,
    "nf2": recovery_nf2,
    "mgcryst": recovery_mgcryst,
    "swro": recovery_swro,
    "hpro": recovery_hpro,
    "oaro": recovery_oaro,
    "naclcryst": recovery_naclcryst,
    "secondpass_ro_ix": recovery_secondpass_ro_ix,
    "posttreat": recovery_posttreat
}

#%%
# Qfeed_m3h_dict

Qfeed_m3h_dict = {
    "intake": Qfeed_intake_tot_m3h,
    "pretreat": Qfeed_pretreat_tot_m3h,
    "nf1": Qfeed_nf1_tot_m3h,
    "nf2": Qfeed_nf2_tot_m3h,
    "mgcryst": Qfeed_mgcryst_tot_m3h,
    "swro": Qfeed_swro_tot_m3h,
    "hpro": Qfeed_hpro_tot_m3h,
    "oaro": Qfeed_oaro_tot_m3h,
    "naclcryst": Qfeed_naclcryst_tot_m3h,
    "secondpass_ro_ix": Qfeed_secondpass_ro_ix_tot_m3h,
    "posttreat": Qfeed_posttreat_tot_m3h
}

Qfeed_m3h_dict

#%%
# Qperm_m3h_dict

Qperm_m3h_dict = {
    "intake": Qperm_intake_tot_m3h,
    "pretreat": Qperm_pretreat_tot_m3h,
    "nf1": Qperm_nf1_tot_m3h,
    "nf2": Qperm_nf2_tot_m3h,
    "mgcryst": Qperm_mgcryst_tot_m3h,
    "swro": Qperm_swro_tot_m3h,
    "hpro": Qperm_hpro_tot_m3h,
    "oaro": Qperm_oaro_tot_m3h,
    "naclcryst": Qperm_naclcryst_tot_m3h,
    "secondpass_ro_ix": Qperm_secondpass_ro_ix_tot_m3h,
    "posttreat": Qperm_posttreat_tot_m3h
}

Qperm_m3h_dict

#%%
# Qretent_m3h_dict
# retentates: only the one used within the treatment chain. Waste/purge/effluent streams have separate dict
Qretent_m3h_dict = {
    #"intake": 0,
    #"pretreat": 0,
    "nf1": Qretent_nf1_tot_m3h,
    "nf2": Qretent_nf2_tot_m3h,
    #"mgcryst": 0,
    "swro": Qretent_swro_tot_m3h,
    "hpro": Qretent_hpro_tot_m3h,
    "oaro": Qretent_oaro_tot_m3h,
    #"naclcryst": 0,
    "secondpass_ro_ix": Qretent_secondpass_ro_ix_tot_m3h,
    #"posttreat": 0
}

Qretent_m3h_dict

#%%
# Qdisposal_m3h_dict

Qdisposal_m3h_dict = {
    "intake": Qloss_intake_tot_m3h,
    "pretreat": Qbackwash_pretreat_tot_m3h,
    #"nf1": 0,
    #"nf2": 0,
    "mgcryst": Qeffluent_mgcryst_tot_m3h,
    #"swro": 0,
    #"hpro": 0,
    #"oaro": 0,
    "naclcryst": Qpurge_naclcryst_tot_m3h,
    #"secondpass_ro_ix": 0,
    "posttreat": Qwaste_posttreat_tot_m3h
}

Qdisposal_m3h_dict

#%%
# Cannual_costs_tot_dict

Cannual_costs_tot_dict = {
    "intake": annual_costs_tot_intake,
    "pretreat": annual_costs_tot_pretreat,
    "nf1": annual_costs_tot_nf1,
    "nf2": annual_costs_tot_nf2,
    "mgcryst": annual_costs_tot_mgcryst,
    "swro": annual_costs_tot_swro,
    "hpro": annual_costs_tot_hpro,
    "oaro": annual_costs_tot_oaro,
    "naclcryst": annual_costs_tot_naclcryst,
    "secondpass_ro_ix": annual_costs_tot_secondpass_ro_ix,
    "posttreat": annual_costs_tot_posttreat,
    "brine_disposal": sum(Qdisposal_m3h_dict.values()) * 8760 * plant_availability * spec_cost_brine_disposal_usd_m3
}

Cannual_costs_tot_dict


#%%
# cost_share_maghydro_dict

cost_share_maghydro_dict = {
    'intake': ((1-recovery_dict['nf1'])*(1-recovery_dict['nf2']))+(((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'pretreat': ((1-recovery_dict['nf1'])*(1-recovery_dict['nf2']))+(((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf1': ((1-recovery_dict['nf1'])*(1-recovery_dict['nf2']))+(((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf2': (1-recovery_dict['nf2'])+((recovery_dict['nf2']*recovery_dict['swro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*recovery_dict['hpro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['secondpass_ro_ix']),
    'mgcryst': 1,
    'swro': (recovery_dict['swro']+(1-recovery_dict['swro'])*recovery_dict['hpro'])*Qfeed_water_mgcryst_tot_m3h/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'hpro': recovery_dict['hpro']*Qfeed_water_mgcryst_tot_m3h/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'oaro': 0,
    'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*(Qfeed_water_mgcryst_tot_m3h)/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']    ),
    'secondpass_ro_ix': 0,
    'posttreat': 0,
    }

cost_share_maghydro_dict["brine_disposal"] = cost_share_maghydro_dict["intake"]
cost_share_maghydro_dict

#%%
# cost_share_nacl_dict

cost_share_nacl_dict = {
    'intake': (recovery_dict['nf1']+((1-recovery_dict['nf1'])*recovery_dict['nf2']))*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*(1-recovery_dict['naclcryst']),
    'pretreat': (recovery_dict['nf1']+((1-recovery_dict['nf1'])*recovery_dict['nf2']))*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*(1-recovery_dict['naclcryst']),
    'nf1': (recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*(1-recovery_dict['naclcryst']),
    'nf2': recovery_dict['nf2']*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*(1-recovery_dict['naclcryst']),
    'mgcryst': 0,
    'swro': (1-recovery_dict['swro'])*(1-recovery_dict['hpro']),
    'hpro': (1-recovery_dict['hpro']),
    'oaro': 1,
    'naclcryst': (market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])),
    'secondpass_ro_ix': 0,
    'posttreat': 0,
    }

cost_share_nacl_dict["brine_disposal"] = cost_share_nacl_dict["intake"]
cost_share_nacl_dict

#%%
# cost_share_potable_water_dict

cost_share_potable_water_dict = {
    'intake': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'pretreat': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf1': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf2': ((recovery_dict['nf2']*recovery_dict['swro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*recovery_dict['hpro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'mgcryst': 0,
    'swro': (recovery_dict['swro']+(1-recovery_dict['swro'])*recovery_dict['hpro'])*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'hpro': recovery_dict['hpro']*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'oaro': 0,
    'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']-Qfeed_water_mgcryst_tot_m3h-Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'secondpass_ro_ix': (Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)/((Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)+(Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)),
    'posttreat': 1,
    }

cost_share_potable_water_dict["brine_disposal"] = cost_share_potable_water_dict["intake"]
cost_share_potable_water_dict

#%%
# cost_share_upw_dict

cost_share_upw_dict = {
    'intake': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'pretreat': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf1': (((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*recovery_dict['swro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*recovery_dict['hpro'])+((recovery_dict['nf1']+(1-recovery_dict['nf1'])*recovery_dict['nf2'])*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*(Qfeed_m3h_dict['secondpass_ro_ix'])/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'nf2': ((recovery_dict['nf2']*recovery_dict['swro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*recovery_dict['hpro'])+(recovery_dict['nf2']*(1-recovery_dict['swro'])*(1-recovery_dict['hpro'])*recovery_dict['naclcryst']))*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'mgcryst': 0,
    'swro': (recovery_dict['swro']+(1-recovery_dict['swro'])*recovery_dict['hpro'])*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'hpro': recovery_dict['hpro']*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'oaro': 0,
    'naclcryst': (market_value_water_usd_m3*Qperm_m3h_dict['naclcryst'])/((market_value_nacl_usd_ton*Mnacl_naclcryst_tot_tonh)+(market_value_water_usd_m3*Qperm_m3h_dict['naclcryst']))*Qfeed_m3h_dict['secondpass_ro_ix']/(Qperm_m3h_dict['swro']+Qperm_m3h_dict['hpro']+Qperm_m3h_dict['naclcryst']),
    'secondpass_ro_ix': (Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)/((Qretent_m3h_dict['secondpass_ro_ix']*market_value_water_usd_m3)+(Qperm_m3h_dict['secondpass_ro_ix']*market_value_upw_usd_m3)),
    'posttreat': 0,
    }

cost_share_upw_dict["brine_disposal"] = cost_share_upw_dict["intake"]
cost_share_upw_dict

#%%
# levelized cost (MgHydro)

levelized_cost_maghydro_dict = {key: cost_share_maghydro_dict[key] * Cannual_costs_tot_dict[key] / (Mmgoh2_mgcryst_tot_tonh * 8760 * plant_availability) for key in cost_share_maghydro_dict}

print(sum(levelized_cost_maghydro_dict.values()))
levelized_cost_maghydro_dict

#%%
# levelized cost (NaCl)

levelized_cost_nacl_dict = {key: cost_share_nacl_dict[key] * Cannual_costs_tot_dict[key] / (Mnacl_naclcryst_tot_tonh * 8760 * plant_availability) for key in cost_share_nacl_dict}

print(sum(levelized_cost_nacl_dict.values()))
levelized_cost_nacl_dict


#%%
# levelized cost (potable water)

levelized_cost_potable_water_dict = {key: cost_share_potable_water_dict[key] * Cannual_costs_tot_dict[key] / (Qperm_m3h_dict['posttreat'] * 8760 * plant_availability) for key in cost_share_potable_water_dict}

print(sum(levelized_cost_potable_water_dict.values()))
levelized_cost_potable_water_dict


#%%
# levelized cost (upw)

levelized_cost_upw_dict = {key: cost_share_upw_dict[key] * Cannual_costs_tot_dict[key] / (Qperm_m3h_dict['secondpass_ro_ix'] * 8760 * plant_availability) for key in cost_share_upw_dict}

print(sum(levelized_cost_upw_dict.values()))
levelized_cost_upw_dict

#%%
