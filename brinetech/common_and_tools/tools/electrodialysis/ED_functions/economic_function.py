import numpy as np
from ....common import calculate_annual_depreciation_cost


def get_opex(power_total, elec_cost, a_mem_tot_m2, tech_config_and_params, proj_config_and_params):

    
    # cost year of reference costs
    cost_year = tech_config_and_params['cost_year']
    # There are three main opex: 1) energy, 2) membrane replacement cost, 3) maintenance, chemical and labor
    ## 1. Energy
    opex_energy = power_total / 1000 * elec_cost * 8760 * tech_config_and_params['plant_availability']  # [USD_current/year]

    # 2. membrane replacement
    r = proj_config_and_params['interest_rate']
    capital_cost_mem = tech_config_and_params['sp_cost_membrane_ed_per_m2'] * a_mem_tot_m2 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year] # [USD_current]
    capex_mem = calculate_annual_depreciation_cost(proj_config_and_params['interest_rate'], tech_config_and_params['depr_period']['membrane'], capital_cost_mem)  # [USD_current/year]
    opex_mem = capex_mem * ((1 + r)**(-7) + (1 + r)**(-14))  # [USD_current/year]

    # 3. maintenance, chemical and labor
    opex_maint = tech_config_and_params['sp_cost_maint_ed'] * a_mem_tot_m2 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year]  # [USD_current/year]
    opex_chem = tech_config_and_params['sp_cost_chem_ed'] * a_mem_tot_m2 * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year]  # [USD_current/year]
    #opex_labor = config['num_of_operators'] * config['annual_salary_operator']  # [USD_current/year]  Nayar
    country = proj_config_and_params['country'][proj_config_and_params['sea']]
    num_plant_operators = tech_config_and_params['labor_nos']['chemical_plant_operator']
    cost_year_labor = str(proj_config_and_params['labor_salary']['cost_year'])
    adj_salary_plant_operator = (
        proj_config_and_params['labor_salary']['chemical_plant_operator'][country]
        * proj_config_and_params['CEPCI']['current']
        / proj_config_and_params['CEPCI'][cost_year_labor]
        )
    opex_labor = num_plant_operators * adj_salary_plant_operator  # [USD_current/a]
    #opex_labor = tech_config_and_params['staff'] * tech_config_and_params['av_personnel_cost_year'] / tech_config_and_params['Mfeed_ref_pers'] * Mfeed_plant_kgs * proj_config_and_params['CEPCI']['current'] / proj_config_and_params['CEPCI'][cost_year]  # [USD_current/year]  Same as Marina in MED model

    #opex_total = opex_energy + opex_mem + opex_maint + opex_chem + opex_labor

    return opex_energy, opex_mem, opex_maint, opex_chem, opex_labor
