import numpy as np

def hindrance_factor_diffusion(l):
    # ki_d = 1-2.3 * l + 1.154 * l**2 + 0.224 * l**3  # Bowen
    phi = (1-l)**2
    if l <= 0.95:
        ki_d = (1 + 9/8 * l * np.log(l) - 1.56034 * l + 0.528155 * l**2 + 1.91521 * l**3 - 2.81903 * l**4 +
               0.270788 * l**5 + 1.10115 * l**6 - 0.435933 * l**7) / phi
    else:
        ki_d = 0.984 * ((1-l) / l)** 5/2
    return ki_d

def hindrance_factor_convection(l):
    phi = (1 - l) ** 2
    # ki_c = (2-phi) * (1 + 0.054 * l - 0.988 * l**2 + 0.441 * l**3)  # Bowen
    ki_c = (1 + 3.867 * l - 1.907 * l**2 - 0.834 * l**3) / (1 + 1.867 * l - 0.741 * l**2)  # Geraldes
    return ki_c

def Sherwood_stirred_cell(Re, Sc, config):
    Sh = config['a1_masstransfer'] * np.power(Re, config['a2_masstransfer']) * np.power(Sc, config['a3_masstransfer'])
    return Sh

def mass_transfer_bulk(Sh, L_char, D_inf):
    kc = Sh * D_inf / L_char
    print('Kc (delete after correcting the error)=', kc)
    return kc

def Schmidt_number(eta, rho, D_inf):
    #print('value of Jv (delete after correcting the error):', Jv)
    Sc = eta / (rho * D_inf)
    return Sc

def correction_factor_kc_interface(Jv, kc):
    phi = Jv / kc
    corr_factor = phi + np.power((1+0.26 * phi**1.4), -1.7)
    return corr_factor

def mass_transfer_coeff_spiralwound(config, D_inf, Sc, Pe):
    #print('Value of eta_spacer (delete this after rectifying the error:', config['eta_spacer'])
    #print('Value of D_inf (delete this after rectifying the error:', D_inf)
    #print('Value of h_feedchannel (delete this after rectifying the error:', config['h_feedchannel'])
    #print('Value of Sc (delete this after rectifying the error:', Sc)
    #print('Value of Pe (delete this after rectifying the error:', Pe)
    #print('Value of L_mix (delete this after rectifying the error:', config['L_mix'])
    try:
        k_bulk = 0.753 * (config['eta_spacer'] / (2 - config['eta_spacer'])) ** 0.5 * (D_inf / config['h_feedchannel']) \
                 * np.power(Sc, -1 / 6) * np.power((Pe * config['h_feedchannel'] / config['L_mix']), 0.5)
    except RuntimeWarning:
        print('Some error with value in power. Values of spacer, D_inf, h, L:',config['eta_spacer'], D_inf, config['h_feedchannel'], config['L_mix'])
    #k_bulk = 0.753 * (config['eta_spacer'] / (2 - config['eta_spacer'])) ** 0.5 * (D_inf / config['h_feedchannel']) \
     #        * np.power(Sc, -1/6) * np.power((Pe * config['h_feedchannel']/ config['L_mix']), 0.5)
    return k_bulk

def Peclet_number(config, D_inf, uw):
    Pe = 2 * config['h_feedchannel'] * uw / D_inf
    return Pe

def activity_coefficient(A, z, I):  # I in mol/m3 come le concentrazioni, poi dentro e trasformata in mol/l
    I = I * 1e-3
    gamma = np.power(10, (-A * z**2 * (I ** (1/2) / (1 + (I) ** (1/2)) - 0.3 * I)))
    return gamma

def A_coefficient_bulk(T, config):
    a = np.power(config['e0'], 3) * np.power(config['Na'], 0.5) / (np.log(10) * 4 * np.pi * np.sqrt(2) * ((config['epsilon_r_water'] * config['epsilon_0']) *
                                                                                      config['k_b'] * T)**(3/2))
    return a

def A_coefficient_pore(T, config):
    a = np.power(config['e0'], 3) * np.power(config['Na'], 0.5) / (np.log(10) * 4 * np.pi * np.sqrt(2) * ((config['epsilon_r_pore'] * config['epsilon_0']) *
                                                                                      config['k_b'] * T)**(3/2))
    return a

def viscosity_salt_parameters(I_salt, salt, config):
    A = config['A1_visc_'+salt] * I_salt + config['A2_visc_'+salt] * I_salt**2 + config['A3_visc_'+salt] * I_salt**3
    B = config['B1_visc_'+salt] * I_salt + config['B2_visc_'+salt] * I_salt**2 + config['B3_visc_'+salt] * I_salt**3
    return A, B

def viscosity_mixture_parameters(I_tot, salts_list, Avisc, Bvisc, I_salt):
    Am = 0
    Bm = 0
    for salt in salts_list:
        Am += Avisc[salt] * I_salt[salt] / I_tot
        Bm += Bvisc[salt] * I_salt[salt] / I_tot
    return Am, Bm

def viscosity_mixture(Am, Bm, eta_solvent):
    eta_mix = np.power(10, (Am + Bm * np.log10(eta_solvent)))
    return eta_mix

def concentration_polarization_factor(r_el):
    CPF = 1.05 + (1.2-1.05)/(0.25-0.05)*(r_el-0.05)
    return CPF